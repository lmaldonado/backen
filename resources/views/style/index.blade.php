@extends('dashboard.layout.base')

@section('title')
    Estilos
@stop

@section('section')
    Estilos
@stop

@section('breadcrumb')
    Configuracion de estilos
@stop
@section('content')
@php
$cliente = 1;
if($countStyles == 0){
    $cservicio = "";
    $fuente = "";
    $cruces = "";
    $imagp = "";
    $images = "";
    $simbolo = "";
    $mdatetime = "";
}else{
    $result = $style;
    $cservicio = $result->contenedor_servicio;
    $fuente = $result->fuente;
    $cruces = unserialize($result->cruces);
    $imagp = unserialize($result->imagep);
    $images = unserialize($result->images);
    $simbolo = unserialize($result->simbolo);
    $mdatetime = $result->mdatetime;
}
@endphp
 <div class="col-xs-12">
     @if(isset($mod) && $mod == 1)
     <div class="alert alert-success alert-dismissible" >
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
         <h4><i class="icon fa fa-check"></i> Articulo modificado!</h4>
         Su estilo fue modificado!.
     </div>
         @php $mod = ""; @endphp
     @elseif(isset($mod) && $mod == 0)
         <div class="alert alert-success alert-dismissible" >
             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
             <h4><i class="icon fa fa-check"></i> Articulo Agregado!</h4>
             Su estilo fue agregado!.
         </div>
     @endif
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Configuracion de estilos</h3>
                </div>
                {!! $form->open(['url' => 'styles', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="col-lg-12">
                        <h4>Estilos Generales</h4>
                        <hr>
                        <div class="col-lg-6 form-group">
                            <label for="colorcontenedor">Contenedor de servicio</label>
                            <input type="text" name="colorcontenedor" id="colorcontenedor" class="form-control color" placeholder="Color del contenedor del servicio" value="{{$cservicio}}">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="colorfuente">Fuente</label>
                            <input type="text" name="colorfuente" id="colorfuente" class="form-control color" placeholder="Color del texto" value="{{$fuente}}">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="mdate" style="margin-right: 4%">Mostrar Fecha y Hora</label>
                            <input type="checkbox" class="js-switch" name="mdate" id="mdate" @if($mdatetime == 1) checked="checked" @endif/>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <h4>Imagenes</h4>
                        <hr>
                    <div class="col-lg-12 form-group">
                        <label for="inputCurces">Cruces</label>
                        <div class="file-loading">
                            <input id="cruces" type="file" name="inputCruces[]" multiple>
                        </div>
                        <input type="hidden" name="cruces" id="cruz">
                    </div>

                    <div class="col-lg-12 form-group">
                        <label for="inputIpred">Imagen Prediseñadas</label>
                        <div class="file-loading">
                            <input id="imagep" type="file" name="inputIpred[]" multiple>
                        </div>
                        <input type="hidden" name="imp" id="imp">
                    </div>

                    <div class="col-lg-12 form-group">
                        <label for="inputIpred">Imagen</label>
                        <div class="file-loading">
                            <input id="image" type="file" name="inputImage[]" multiple>
                        </div>
                        <input type="hidden" name="im" id="im">
                    </div>

                    <div class="col-lg-12 form-group">
                        <label for="inputIpred">Simbolo</label>
                        <div class="file-loading">
                            <input id="simbolo" type="file" name="simbolo[]" multiple>
                        </div>
                        <input type="hidden" name="sim" id="sim">
                    </div>
                    <div class="clearfix"></div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-right">
                    <input type="reset" value="Cancelar" id="cancelar" class="btn btn-default">
                    <input type="submit" value="Guardar" class="btn btn-info">
                </div>
                {!! $form->close() !!}
            </div>
            <!-- /.box -->
        </div>

        <!-- Modal -->
        <div class="modal modal-danger" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Peligro!</h4>
                    </div>
                    <div class="modal-body">
                        <p>¿ Seguro desea Eliminar el Servicio ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No</button>
                        <button type="button" class="btn btn-outline" id="si">Si</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
@stop
@section('script')
    <script>
        cruz = [];
        imp = [];
        im = [];
        sim = [];
        $(function () {
            @if(count($cruces) != 0)
            $("#cruz").val(JSON.stringify({!! json_encode($cruces) !!}));
            @endif
            @if(count($imagp) != 0)
            $("#imp").val(JSON.stringify({!! json_encode($imagp) !!}));
            @endif
            @if(count($images) != 0)
            $("#im").val(JSON.stringify({!! json_encode($images) !!}));
            @endif
            @if(count($simbolo) != 0)
            $("#sim").val(JSON.stringify({!! json_encode($simbolo) !!}));
            @endif
            $upload1 = $("#cruces");
            $upload1.fileinput({

                initialPreview: [
                    @foreach($cruces as $c)
                        @if($c != "")
                    "<img src='{{url("images/cruces",$c)}}' class='file-preview-image' alt='Desert' title='Desert' width = '100%'>",
                        @endif
                    @endforeach
                ],

                browseClass: "btn btn-primary",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                uploadAsync: false,
                allowedFileExtensions: ["jpg","jpeg", "gif", "png"] ,
                uploadUrl: '{{url("styles/images")}}'
            }).on("filebatchselected", function(event, files) {
                $upload1.fileinput("upload");
                $(".kv-file-remove").css("display","none");
                if(cruz.length === 0){
                    $("#cruz").val("");
                }
                cruz.push("{{$cliente."_"}}"+files[0].name);
                $("#cruz").val(JSON.stringify(cruz));
            });
            $upload2 = $("#imagep");
            $upload2.fileinput({
                initialPreview: [
                    @foreach($imagp as $p)
                    @if($p != "")
                        "<img src='{{url("images/imagep",$p)}}' class='file-preview-image' alt='Desert' title='Desert' width = '100%'>",
                    @endif
                    @endforeach
                ],
                browseClass: "btn btn-primary",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                uploadAsync: false,
                allowedFileExtensions: ["jpg","jpeg", "gif", "png"] ,
                uploadUrl: '{{url("styles/images")}}'
            }).on("filebatchselected", function(event, files) {
                $upload2.fileinput("upload");
                $(".kv-file-remove").css("display","none");
                if(imp.length === 0){
                    $("#imp").val("");
                }
                imp.push("{{$cliente."_"}}"+files[0].name);
                $("#imp").val(JSON.stringify(imp));
            });
            $upload3 = $("#image");
            $upload3.fileinput({
                initialPreview: [
                    @foreach($images as $i)
                    @if($i != "")
                        "<img src='{{url("images/imagenes",$i)}}' class='file-preview-image' alt='Desert' title='Desert' width = '100%'>",
                    @endif
                    @endforeach
                ],
                browseClass: "btn btn-primary",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                uploadAsync: false,
                allowedFileExtensions: ["jpg","jpeg", "gif", "png"] ,
                uploadUrl: '{{url("styles/images")}}'
            }).on("filebatchselected", function(event, files) {
                $upload3.fileinput("upload");
                $(".kv-file-remove").css("display","none");
                if(im.length === 0){
                    $("#im").val("");
                }
                im.push("{{$cliente."_"}}"+files[0].name);
                $("#im").val(JSON.stringify(im));
            });
            $upload4 = $("#simbolo");
            $upload4.fileinput({
                initialPreview: [
                    @foreach($simbolo as $s)
                    @if($s != "")
                        "<img src='{{url("images/simbolo",$s)}}' class='file-preview-image' alt='Desert' title='Desert' width = '100%'>",
                    @endif
                    @endforeach
                ],
                browseClass: "btn btn-primary",
                showCaption: false,
                showRemove: false,
                showUpload: false,
                uploadAsync: false,
                allowedFileExtensions: ["jpg","jpeg", "gif", "png"] ,
                uploadUrl: '{{url("styles/images")}}'
            }).on("filebatchselected", function(event, files) {
                $upload4.fileinput("upload");
                $(".kv-file-remove").css("display","none");
                if(sim.length === 0){
                    $("#sim").val("");
                }
                sim.push("{{$cliente."_"}}"+files[0].name);
                $("#sim").val(JSON.stringify(sim));
            });;
            var elem = document.querySelector('.js-switch');
            var init = new Switchery(elem);

            $("#imgcruces").click(function(){
                $("#cruces").append('<input type="file" name="inputCruces[]" class="inputCruses">')
            })

            $("#imgp").click(function(){
                $("#imagep").append('<input type="file" name="inputIpred[]" class="inputIpred">')
            })
            $("#img").click(function(){
                $("#images").append('<input type="file" name="inputImage[]" class="inputImage">')
            });

            $("#simbol").click(function(){
                $("#simbolo").append('<input type="file" name="simbolo[]" class="simbolo">')
            });
        });
    </script>
@stop