@extends('dashboard.layout.base')

@section('title')
	Configuracion de MF
@stop

@section('section')
	Configuracion
@stop

@section('breadcrumb')
	MF
@stop
@section('content')
<?php 
    $usuario = session()->get('key_login'); 

    if(!isset($usuario['nombre']) || $usuario['admin']!=1){        
      echo '<script> location.href = "./?ruta=login"; </script>';
    }
?>


<div class="content">
	<div class="row">
		<div class="col-lg-6">
			<h4>Seleccionar las canciones para MF</h4>
		</div>
		{{-- <div class="col-lg-6 ">
			<div class="form-group text-right">
				{!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
				<a href="{{ url('/servicios') }}" class="btn btn-danger">Cancelar</a>
			</div>
		</div> --}}
	</div>
	<div class="row">
		
		<div class="col-xs-12">
          
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Canciones</h3>
				</div>
				<div class="box-body">
					<div class="row">
                        <div class="col-xs-12">
                            <iframe src="http://mf.neo.co/modulos/mf_n/public/?ruta=configuracion&client=1&name=00:08:22:16:11:FC" width="100%" height="400" frameborder="0"></iframe>
                        </div>
						
					</div>
				</div>
			</div>
        </div>
    </div>
</div>

@stop



@section('script')


<script type="text/javascript">
    
	
</script>
@stop