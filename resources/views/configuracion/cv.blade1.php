@extends('dashboard.layout.base')

@section('title')
    Configuracion de CV
@stop

@section('section')
    Configuracion
@stop

@section('breadcrumb')
    CV
@stop
@section('content')
<?php 
    $usuario = session()->get('key_login'); 

    if(!isset($usuario['nombre']) || $usuario['admin']!=1){        
      echo '<script> location.href = "../public/?ruta=login"; </script>';
    }
?>



{!! Form::open(['url' => './?ruta=config/cv', 'method' => 'POST', 'files' => true]) !!}

<div class="content">
    <div class="row">
        <div class="col-lg-6">
            <h4>Datos de configuracion para las carteleras de velatorio </h4>
        </div>
        <div class="col-lg-6">
            {!! Form::submit('Guardar', ['class' => 'btn btn-success pull-right']) !!}
        </div>
        </div>
    <br>
    <div class="row">
        <div class="col-lg-4 col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Encabezado</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            {!! Form::label('Tipo') !!}
                            {!! Form::select('tipo_encabezado', ["texto"=>"Texto", "logo"=>"Logo"], $cv->tipo_encabezado, ['id'=>'lst-tipo-encabezado','class' => 'form-control']) !!}
                        </div>
                        {{-- <div id="ct-logo-encabezado" style="{{($cv->tipo_encabezado=='texto')?'display:none':''}}" class="form-group image-add-ct col-md-12">
                            <div class="row">
                                <div class="col-lg-4">
                                    {!! Form::button('Cargar Logo', ['class' => 'btn btn-success']) !!}
                                    <div style="position: relative; height: 0px">
                                        <input id="btn-up-logo"  name="logo_encabezado" class="inputFile-logo" type="file" accept="image/*" base-sixty-four-input="" style="">
                                        
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div id="logo-encabezado-cv" style="background-image: url({{ url($cv->logo_encabezado) }})">
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        <div id="ct-texto-encabezado" class="form-group col-md-12" style="{{($cv->tipo_encabezado=='logo')?'display:none':''}}">
                            {!! Form::text('texto_encabezado', $cv->texto_encabezado, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
<!-- INICIO SERVIDOR -->            
            {{-- <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Servidor</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="tipo_servidor">Tipo Servidor</label>
                            <div class="input-group ">
                                
                                <!-- {!! Form::select('tipo_servidor', [ "propio"=>"Servidor Propio", "online"=>"Servidor Online", "local"=>"Servidor Local"], $cv->tipo_servidor, ['id'=>'tipo_servidor','class' => 'form-control', 'onchange'=>'cambiarTipoServidor()']) !!} -->
                                <select name="tipo_servidor" id="tipo_servidor" class="form-control" onchange="cambiarTipoServidor()">
                                    @if($cv->tipo_servidor=='local')
                                        <option value="propio" >Servidor Propio</option>
                                        <option value="local" selected>Servidor Local</option>
                                        <option value="online">Servidor Online</option>
                                    @elseif($cv->tipo_servidor=='online')
                                        <option value="propio" >Servidor Propio</option>
                                        <option value="local" >Servidor Local</option>
                                        <option value="online" selected>Servidor Online</option>
                                    @else
                                        <option value="propio" >Servidor Propio</option>
                                        <option value="local" >Servidor Local</option>
                                        <option value="online">Servidor Online</option>
                                    @endif
                                </select>
                                <span class="input-group-btn" >
                                    <button type="button" title="Guardar servidor" id="btn_guardar_sala_propio" class="btn btn-primary btn-flat" onclick="guardarSala('propio')"><i class="fa fa-save"></i></button>
                                    <button type="button" id="btn_disable" class="btn btn-default btn-flat" disabled><i class="fa fa-server"></i></button>
                                    
                                </span>
                            </div>
                        </div>

                        <div class="from-group col-md-12" id="sel_sala_serv_propio">
                            <label for="sala_servidor_propio">Salas Propia</label>                            
                            
                            @foreach($salas as $sa)
                                @if($cv->tipo_servidor=='propio')
                                    {{$vali=false}}
                                    @foreach($sala_sel as $sasel)
                                        @if($sasel['sala'] == $sa->id)
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="salas_id[]" checked class="sals_id_inp" value="{{$sa->id}}">
                                                    {{$sa->nombre}}
                                                </label>
                                            </div>
                                            <div class="hide">{{$vali=true}}</div>
                                        @endif
                                    @endforeach
                                    @if(!$vali)
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="salas_id[]"  class="sals_id_inp" value="{{$sa->id}}">
                                                {{$sa->nombre}}
                                            </label>
                                        </div>
                                    @endif 
                                @else
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="salas_id[]"  class="sals_id_inp" value="{{$sa->id}}">
                                            {{$sa->nombre}}
                                        </label>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        
                        <div class="form-group col-md-12" id="tipo_serv_local">
                            <label for="servidor_ip">Dirección IP</label>
                            <div class="input-group ">
                                <input type="text" class="form-control" id="servidor_ip" name="servidor_ip" value="{{$cv->servidor_ip}}" placeholder="Ej. 192.168.10.100" title="Ej. 192.168.10.100">
                                <span class="input-group-btn">
                                    <button type="button" title="Buscar salas" class="btn btn-default btn-flat" onclick="buscarSala('local')"><i class="fa fa-search"></i></button>
                                    <button type="button" title="Guardar servidor" id="btn_guardar_sala_local" class="btn btn-primary btn-flat" onclick="guardarSala('local')"><i class="fa fa-save"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="from-group col-md-12" id="sel_sala_serv_ip">
                            <label for="sala_servidor_ip">Salas Local</label>

                                <div id="sala_servidor_ip">
                                    
                                </div>                            
                                                            
                        </div>
                        
                        <div class="form-group col-md-12" id="tipo_serv_online">
                            <label for="servidor_url">Dirección URL</label>
                            <div class="input-group ">
                                <input type="text" class="form-control" id="servidor_url" name="servidor_url" value="{{$cv->servidor_url}}" placeholder="Ej. http://dominio.com/api/" title="Ej. http://dominio.com/api/">
                                <span class="input-group-btn">
                                    <button type="button" title="Buscar salas" class="btn btn-default btn-flat" onclick="buscarSala('online')"><i class="fa fa-search"></i></button>
                                    <button type="button" title="Guardar servidor" id="btn_guardar_sala_online" class="btn btn-primary btn-flat" onclick="guardarSala('online')"><i class="fa fa-save"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-12" id="sel_sala_serv_online">
                            <label for="sala_servidor_online">Salas Online</label>
                            <div id="sala_servidor_online">
                                    
                            </div>  
                                
                        </div>
                    </div>
                </div>
            </div> --}}
<!-- FIN SERVIDOR -->
<!-- INICIO IMAGENES -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Imágenes</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            {!! Form::label('Cuadro Imagen 1') !!}
                            {!! Form::select('imagen1', $tipos_imagen, $cv->imagen1, ['id'=>'lst_sala','class' => 'form-control']) !!}
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('Cuadro Imagen 2') !!}
                            {!! Form::select('imagen2', $tipos_imagen, $cv->imagen2, ['id'=>'lst_sala','class' => 'form-control']) !!}
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('Cuadro Imagen 3') !!}
                            {!! Form::select('imagen3', $tipos_imagen, $cv->imagen3, ['id'=>'lst_sala','class' => 'form-control']) !!}
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('Cuadro Imagen 4') !!}
                            {!! Form::select('imagen4', $tipos_imagen, $cv->imagen4, ['id'=>'lst_sala','class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>
<!-- FIN IMAGENES -->

<!-- INICIO FONDOS -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Fondos Resolución(1920 x 1080 px)</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div id="ct-fondo-all" class="form-group col-lg-12 col-md-6">
                            <?php
                                $numImagen=0;
                            ?>
                            @foreach($fondos as $i=>$fondo)

                            <div id="ct-banner-{{$i}}" class="ct-imagen" style="background-image: url({{ url($fondo) }})">
                                <div class="btn-del btn-imagen btn-del-imagen btn btn-danger" data-file="{{$fondo}}" data-prefix="fondo" data-tipo="old">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </div>
                                <!-- <div class="btn-up btn-imagen btn btn-success">
                                    <i class="fa fa-upload" aria-hidden="true"></i>
                                    <div class="ct-file-input-btn-add">
                                        <div>
                                            <input class="input-imagen" data-ct="ct-fondo-{{$i}}" data-action="update" type="file" name="fondo[]">
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <?php
                                $numImagen++;
                            ?>
                            @endforeach
                        </div>
                        <div class="form-group col-lg-6 col-md-3">
                            <input type="hidden" id="fondo_delete" name="fondo_delete" value="">
                            <label for="btn_fondo_cv" class="btn btn-block btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Agregar</label>
                            <div class="ct-file-input-btn-add" style="position:absolute;top:80px;">
                                <div>
                                    <input class="input-imagen" id="btn_fondo_cv" type="file" data-action="add" data-prefix="fondo" data-numImages="{{ $numImagen }}" name="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- FIN FONDOS --> 
<!-- INICIO FONDOS DE SERVICIOS -->
            
<!-- FIN FONDOS --> 
        <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">General</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                        <div class="form-group col-md-6">
                            {!! Form::label('Múltiples Empresas') !!}
                        </div>
                        <div class="form-group col-md-6">
                        {!! Form::checkbox('multiples_empresas', 1, $cv->multiples_empresas,['id'=>'multiples_empresas', 'data-toggle' => 'toggle', 'data-on' => 'Si', 'data-off' => 'No']) !!}
                        </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="form-group col-md-6">
                                {!! Form::label('Servicios') !!}
                            </div>
                            <div class="form-group col-md-6">
                                <div class="bootstrap-timepicker">
                                    <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    {!! Form::text('tiempo_servicios', '00:00:00',['class' => 'form-control timepicker', 'style' => 'width:80px']) !!}
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Contenedores</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            {!! Form::label('Contendor 1') !!}
                            {!! Form::select('contenedor1', $tipos_contenedor, $cv->contenedor1, ['id'=>'lst_sala','class' => 'form-control']) !!}
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('Contendor 2') !!}
                            {!! Form::select('contenedor2', $tipos_contenedor, $cv->contenedor2, ['id'=>'lst_sala','class' => 'form-control']) !!}
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('Contendor 3') !!}
                            {!! Form::select('contenedor3', $tipos_contenedor, $cv->contenedor3, ['id'=>'lst_sala','class' => 'form-control']) !!}
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('Contendor 4') !!}
                            {!! Form::select('contenedor4', $tipos_contenedor, $cv->contenedor4, ['id'=>'lst_sala','class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>

           
        
            
<!-- INICIO BANERS -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Banners  Resolución(1900 x 200 px)</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div id="ct-banner-all" class="form-group col-lg-12 col-md-6">
                            <?php
                                $numImagen=0;
                            ?>
                            @foreach($banners as $i=>$banner)

                            <div id="ct-banner-{{$i}}" class="ct-imagen" style="background-image: url({{ url($banner) }})">
                                <div class="btn-del btn-imagen btn-del-imagen btn btn-danger" data-file="{{$banner}}" data-prefix="banner" data-tipo="old">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </div>
                                <!-- <div class="btn-up btn-imagen btn btn-success">
                                    <i class="fa fa-upload" aria-hidden="true"></i>
                                    <div class="ct-file-input-btn-add">
                                        <div>
                                            <input class="input-imagen" data-ct="ct-banner-{{$i}}" data-action="update" type="file" name="banner[]">
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <?php
                                $numImagen++;
                            ?>
                            @endforeach
                        </div>
                        <div class="form-group col-lg-12 col-md-12">
                             {!! Form::label( 'Intervalo Banner (Segundos)')!!}
                            {!! Form::number('tiempo_banner', $cv->tiempo_banner, ['id'=>'tiempo_banner','class' => 'form-control']) !!}
                            <input type="hidden" id="banner_delete" name="banner_delete" value=""><br>
                            <label for="btn_banner_cv" class="btn btn-block btn-primary" style="width:150px; "><i class="fa fa-plus" aria-hidden="true"></i> Agregar</label>
                            <div class="ct-file-input-btn-add1" style="position:absolute;top:20px;">
                                <div>
                                    <input class="input-imagen" id="btn_banner_cv" type="file" data-action="add" data-prefix="banner" data-numImages="{{ $numImagen }}" name="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- FIN BANERS -->

        </div>

        <div class="col-lg-4 col-md-6">
<!-- INICIO SIMBOLOS -->            
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Simbolos Salas</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        @foreach($salas as $sala)

                        <div class="form-group col-md-8">
                            {!! Form::label($sala->nombre) !!}
                            {!! Form::select('simbolos[]', $simbolos, (isset($sala_simbolos[$sala->id]))?$sala_simbolos[$sala->id]:NULL, ['class' => 'lst-simbolos form-control', 'data-id'=>$sala->id]) !!}
                            {!! Form::hidden('salas[]', $sala->id) !!}
                        </div>
                        <div class="col-md-3">
                            <div class="border">
                                <img id="simbolo-sala-{{$sala->id}}" src="" class="img-responsive" />
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div> 
<!-- FIN SIMBOLOS -->

<!-- INICIO PUBLICIDADES -->
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Publicidades Resolución(1900 x 800 px)</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div id="ct-publi-all" class="form-group col-lg-12 col-md-6">
                            <?php
                                $numImagen=0;
                            ?>
                            @foreach($publicidades as $i=>$publi)

                            <div id="ct-publi-{{$i}}" class="ct-imagen" style="background-image: url({{ url($publi) }})">
                                <div class="btn-del btn-imagen btn-del-imagen btn btn-danger" data-file="{{$publi}}" data-prefix="publi" data-tipo="old">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </div>
                                <!-- <div class="btn-up btn-imagen btn btn-success">
                                    <i class="fa fa-upload" aria-hidden="true"></i>
                                    <div class="ct-file-input-btn-add">
                                        <div>
                                            <input class="input-imagen" data-ct="ct-publi-{{$i}}" data-action="update" type="file" name="publi[]">
                                        </div>

                                    </div>
                                </div> -->
                            </div>
                            <?php
                                $numImagen++;
                            ?>
                            @endforeach
                        </div>
                        <div class="form-group col-lg-12 col-md-12">
                            <input type="hidden" id="publi_delete" name="publi_delete" value="">
                              {!! Form::label( 'Intervalo Publicidad (Segundos)')!!}
                            {!! Form::number('tiempo_publi', $cv->tiempo_publi, ['id'=>'tiempo_publi','class' => 'form-control']) !!}
                                <div class="">
                                    <!-- <i class="fa fa-plus" aria-hidden="true"> Agregar</i> -->
                                    <label for="inp-add-img" class="btn btn-imagen btn btn-primary pull-left" style="width:140px; "><i class="fa fa-plus" aria-hidden="true"> Agregar</i></label>
                                    <div class="ct-file-input-btn-add1  " >
                                        <div>
                                            <input id="inp-add-img" class="input-imagen  " type="file" data-action="add" data-prefix="publi" data-numImages="{{ $numImagen }}" name="" placeholder="Agreg" >

                                            </div>

                                        
                                    </div>
                                </div>
                                

                         
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<!-- FIN PUBLICIDADES -->
        </div>

    </div>
</div>
{!! Form::close() !!}

@stop

@section('script')
<script type="text/javascript">
    // SERVIDORES  
    // function cambiarTipoServidor(){
    //     var tipo = $('#tipo_servidor').val();
    //     $('#btn_guardar_sala_local').hide();
    //     $('#btn_guardar_sala_online').hide();
    //     $('#btn_guardar_sala_propio').hide();
    //     $('#sel_sala_serv_propio').hide();
    //     $('#btn_disable').hide();
    //     // console.log(tipo);
    //     if(tipo == 'online'){
    //         $('#tipo_serv_local').hide();
    //         $('#sel_sala_serv_propio').hide();
    //         $('#sel_sala_serv_ip').hide();
    //         $('#tipo_serv_online').show();
    //         // $('#sel_sala_serv_online').show();
    //         $('#btn_disable').show();
    //         $('#sala_servidor_ip .checkbox').each(function(){
    //             $(this).remove();
    //         });
    //     }else{ 
    //         if(tipo == 'local'){
    //             $('#tipo_serv_online').hide();
    //             $('#sel_sala_serv_propio').hide();
    //             $('#sel_sala_serv_online').hide();
    //             $('#tipo_serv_local').show();
    //             // $('#sel_sala_serv_ip').show(); 
    //             $('#btn_disable').show();
    //             $('#sala_servidor_online .checkbox').each(function(){
    //                 $(this).remove();
    //             });
    //         }else{
    //             $('#tipo_serv_online').hide();
    //             $('#tipo_serv_local').hide();
    //             $('#sel_sala_serv_online').hide();
    //             $('#sel_sala_serv_ip').hide(); 
    //             $('#btn_guardar_sala_propio').show();
    //             $('#sel_sala_serv_propio').show();
    //             $('#sala_servidor_ip .checkbox').each(function(){
    //                 $(this).remove();
    //             });
    //             $('#sala_servidor_online .checkbox').each(function(){
    //                 $(this).remove();
    //             });
    //         }
    //     }
    // }
    // function buscarSala(opc){
    //     $('#sel_sala_serv_online').hide();
    //     $('#sel_sala_serv_ip').hide();
    //     $('#btn_guardar_sala_local').hide();
    //     $('#btn_guardar_sala_online').hide();
    //     $('#sala_servidor_ip .checkbox').each(function(){
    //         $(this).remove();
    //     });
    //     $('#sala_servidor_online .checkbox').each(function(){
    //         $(this).remove();
    //     });
    //     if(opc == 'local'){
    //         var ip = 'http://'+$('#servidor_ip').val()+'/cartelera_backend_laravel/public/';
    //     }else{
    //         if(opc == 'online'){
    //             var ip = $('#servidor_url').val();
    //         }
    //     }        
    //     $.ajax({
    //         type:'GET',
    //         url: ip+"api/salas", 
    //         data :{
    //             '_token': '{{ csrf_token() }}'
    //         },
    //         success: function(result){
    //             // console.log(result);
    //             var servidor;
    //             for( var i=0; i<result.salas.length; i++){
    //                 if(opc == 'local'){
    //                     servidor = $("#sala_servidor_ip");
    //                     if(result){
    //                         $('#btn_guardar_sala_local').show();
    //                         $('#btn_guardar_sala_online').hide();
    //                         $('#sel_sala_serv_ip').show();
    //                         var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' class='sals_id_inp_local' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";
    //                     }                        
    //                 }else{
    //                     if(opc == 'online'){
    //                         servidor = $("#sala_servidor_online");
    //                         // capturar el select
    //                         // var lst_sala = document.getElementById("sala_servidor_online");
    //                         if(result){
    //                             $('#btn_guardar_sala_local').hide();
    //                             $('#btn_guardar_sala_online').show();
    //                             $('#sel_sala_serv_online').show();
    //                             var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' class='sals_id_inp_online' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";
    //                         }                                
    //                     }
    //                 }  
                    
    //                 servidor.append(nuevo);
    //             }                      
                
    //         },error:function(err){
    //             console.log(err);
    //         }
    //     });
    // }
    // function buscarSalaPrimera(){
    //     $('#sel_sala_serv_online').hide();
    //     $('#sel_sala_serv_ip').hide();
    //     $('#btn_guardar_sala_local').hide();
    //     $('#btn_guardar_sala_online').hide();
    //     $('#sala_servidor_ip .checkbox').each(function(){
    //         $(this).remove();
    //     });
    //     $('#sala_servidor_online .checkbox').each(function(){
    //         $(this).remove();
    //     });
    //     var tipo_serv = '{{$cv->tipo_servidor}}';
    //     var sala_id = '{{$cv->sala_id}}';
    //     var arr = sala_id.split(",");
    //     // console.log(arr);
    //     if(tipo_serv != 'propio'){
    //         if(tipo_serv == 'local'){
    //             var ip = 'http://'+$('#servidor_ip').val()+'/cartelera_backend_laravel/public/';
    //         }else{
    //             if(tipo_serv == 'online'){
    //                 var ip = $('#servidor_url').val();
    //             }
    //         }        
    //         $.ajax({
    //             type:'GET',
    //             url: ip+"api/salas", 
    //             data :{
    //                 '_token': '{{ csrf_token() }}'
    //             },
    //             success: function(result){
    //                 // console.log(result);
    //                 var servidor;
    //                 for( var i=0; i<result.salas.length; i++){
    //                     if(tipo_serv == 'local'){
    //                         // capturar el select
    //                         servidor = $("#sala_servidor_ip");
    //                         if(result){
    //                             $('#btn_guardar_sala_local').show();
    //                             $('#btn_guardar_sala_online').hide();
    //                             $('#sel_sala_serv_ip').show();
    //                             for(var j=0; j < arr.length ; j++){
    //                                 // console.log('Sala',arr[j],result.salas[i].id);
    //                                 if(arr[j] == result.salas[i].id){
    //                                     var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' checked class='sals_id_inp_local' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";
    //                                     break;
    //                                 }else{
    //                                     var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]'  class='sals_id_inp_local' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";
    //                                 }
    //                             }
                                
    //                         }  
    //                     }else{
    //                         if(tipo_serv == 'online'){
    //                             // capturar el select
    //                             servidor = $("#sala_servidor_online");
    //                             if(result){
    //                                 $('#btn_guardar_sala_local').hide();
    //                                 $('#btn_guardar_sala_online').show();
    //                                 $('#sel_sala_serv_online').show();
    //                                 for(var j=0; j < arr.length ; j++){
    //                                     // console.log('Sala',arr[j],result.salas[i].id);
    //                                     if(arr[j] == result.salas[i].id){
    //                                         var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' checked class='sals_id_inp_online' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";
    //                                         break;
    //                                     }else{
    //                                         var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' class='sals_id_inp_online' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";
    //                                     }
    //                                 }
                                    
    //                             }  
    //                         }
    //                     }  
    //                     servidor.append(nuevo);
    //                 }                      
                    
    //             },error:function(err){
    //                 console.log(err);
    //             }
    //         });
    //     }
        
    // }
    // function guardarSala(opc){
    //     if(opc == 'local'){
    //         var url = $('#servidor_ip').val();
    //         var sala = '';
    //         $('.sals_id_inp_local').each(function(even){
                    
    //             if($(this).prop( "checked" )){
                    
    //                 if(sala==''){
    //                     sala+=$(this).val();
    //                 }else{
    //                     sala+=','+$(this).val();
    //                 }

    //             }
                
    //         });
    //         // console.log(url,sala);
    //     }else{
    //         if(opc == 'online'){
    //             var url = $('#servidor_url').val();
    //             var sala = '';
    //             $('.sals_id_inp_online').each(function(even){
                    
    //                 if($(this).prop( "checked" )){
                        
    //                     if(sala==''){
    //                         sala+=$(this).val();
    //                     }else{
    //                         sala+=','+$(this).val();
    //                     }

    //                 }
                    
    //             });
    //             // console.log(url,sala);
    //         }else{
    //             // console.log(opc);
    //             var sala = '';
    //             var url = ''
    //             // var salas='';
    //             $('.sals_id_inp').each(function(even){
                    
    //                 if($(this).prop( "checked" )){
                        
    //                     if(sala==''){
    //                         sala+=$(this).val();
    //                     }else{
    //                         sala+=','+$(this).val();
    //                     }

    //                 }
                    
    //             });
                
    //         }
    //     }
    //     console.log('salas selec',sala);
    //     if(sala==''){
    //         alert('Debe seleccionar minimo una sala');
    //     }else{
    //         $.ajax({
    //             type:'POST',
    //             url: "../api/config/cv/salas", 
    //             data :{
    //                 '_token': '{{ csrf_token() }}',
    //                 'tipo': opc,
    //                 'url': url,
    //                 'sala': sala
    //             },
    //             success: function(result){
    //                 console.log(result);  
    //                 location.reload();              
    //             },error:function(err){
    //                 console.log(err);
    //             }
    //         });
    //     }
            
    // }
$(document).ready(function(){

    // cambiarTipoServidor();
    // buscarSalaPrimera();

    $('.timepicker').timepicker({
        showInputs: false,
        showMeridian:false
    });

/*************** ENCABEZADO ************/
    $('#lst-tipo-encabezado').on('change', function(){
        val = $(this).val();
        if(val == 'texto'){
            $('#ct-logo-encabezado').hide();
            $('#ct-texto-encabezado').show();
        }else{
            $('#ct-logo-encabezado').show();
            $('#ct-texto-encabezado').hide();
        }
    });


    $('.lst-simbolos').each(function(){
        $('#simbolo-sala-'+$(this).data('id')).attr('src', '{{ url(\App\CV::SIMBOLOS_DIR) }}/'+$(this).val());
    });

    $('.lst-simbolos').on('change', function(){
        $('#simbolo-sala-'+$(this).data('id')).attr('src', '{{ url(\App\CV::SIMBOLOS_DIR) }}/'+$(this).val());
    });


    $('#btn-up-logo').on('change', function(evt){

        var files = evt.target.files; // FileList object
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e ) {
                    $('#logo-encabezado-cv').css('background-image', 'url('+e.target.result+')');
                };
            })(f );
            reader.readAsDataURL(f );
        }
    });

/************* IMAGENES ******************/


    $('.input-imagen').on('change', function(evt){
        console.log($(this).data('ct'));
        $this = $(this);
        console.log($this.data('action'));
        var files = evt.target.files; // FileList object
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e ) {
                    
                    prefix = $this.data('prefix');
                    numImages = $this.data('numImages');
                    newObj = $('<div id="ct-'+prefix+'-'+numImages+'" class="ct-imagen" style="background-image: url('+e.target.result+')"><div class="btn-del btn-imagen btn-del-imagen btn btn-danger" data-prefix="'+prefix+'" data-tipo="new"><i class="fa fa-trash" aria-hidden="true"></i></div><div class="btn-up btn-imagen btn btn-success hide"><i class="fa fa-upload" aria-hidden="true"></i><div class="ct-file-input"><div class="new-input"></div></div></div></div>');  
                        $('#btn-add-img').css('background-image', '');
                    newObj.find('.btn-del-imagen').on('click', function(){
                        $(this).parent().fadeOut('slow',function(){
                            $(this).remove();
                        });
                    });

                    $('#ct-'+prefix+'-all').append(newObj);


                    newInput = $this.clone(true);
                    newInput.data('action', 'update');
                    newInput.data('ct', 'ct-'+prefix+'-'+numImages);
                    newInput.attr('name',prefix+'[]');
                    newObj.find('.new-input').append(newInput);
                    numImages++;
                    $this.data('numImages', numImages);
                    
                };
            })(f, $this);
            reader.readAsDataURL(f );
        }
    });
    

    $('.btn-del-imagen').on('click', function(){
        $this = $(this);
        
        if($this.data('tipo') == 'old'){
            prefix = $this.data('prefix');
            aux = $('#'+prefix+'_delete').val();
            if(aux != '')
                aux = aux+',';
            aux = aux+$this.data('file');   
            $('#'+prefix+'_delete').attr('value', aux); 
        }
        
        
        $this.parent().fadeOut('slow',function(){
            $(this).remove();
        });
    });
   

    
});
   
</script>
@stop