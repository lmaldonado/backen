@extends('dashboard.layout.base')

@section('title')
    Configuracion General
@stop

@section('section')
    Configuracion
@stop

@section('breadcrumb')
    General
@stop
@section('content')

{!! Form::open(['url' => 'config/configuracion', 'method' => 'POST', 'files' => true]) !!}

<div class="content">
    <div class="row">
        <div class="col-lg-12">
            Datos de configuracion para las carteleras {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Accesos</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            {!! Form::label('Email Admin') !!}
                            {!! Form::email('email', $config->email, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('Contraseña Config') !!}
                            <input type="password" name="password" class="form-control" placeholder="Contraseña">
                        </div>
                         <div class="form-group col-md-12">
                            {!! Form::label('Ip Config') !!}
                            {!! Form::text('iphost', $config->iphost, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('script')
<script>
$(document).ready(function(){

/*************** ENCABEZADO ************/
    
});
</script>
@stop