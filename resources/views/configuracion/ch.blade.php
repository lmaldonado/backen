@extends('dashboard.layout.base')

@section('title')
	Configuracion de CH / CA
@stop

@section('section')
	Configuracion
@stop

@section('breadcrumb')
	CH
@stop
@section('content')
<?php 
    $usuario = session()->get('key_login'); 

    if(!isset($usuario['nombre']) || $usuario['admin']!=1){        
      echo '<script> location.href = "./?ruta=login"; </script>';
    }
?>



{!! Form::open(['url' => './?ruta=config/ch', 'method' => 'POST', 'files' => true]) !!}

<div class="content">
	<div class="row">
		<div class="col-lg-6">
			<h4>Datos de configuracion velatorios</h4>
		</div>
		<div class="col-lg-6 ">
			<div class="form-group text-right">
				{!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
				<a href="{{ url('/?ruta=servicios') }}" class="btn btn-danger">Cancelar</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4 col-md-6">
		
			<input type="hidden" name="cliente_id" value="{{ $ch->cliente_id }}">
		
<!-- INICIO URL API WEB -->            
			{{-- <div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">URL de la web api</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="form-group col-md-12">
							{!! Form::label('url_web_api', 'URL de la web api') !!}
							{!! Form::text('url_web_api', $ch->url_web_api, ['id'=>'url_web_api','class' => 'form-control']) !!}
						</div>
						
					</div>
				</div>
			</div> --}}
<!-- FIN URL WEB -->

<!-- INICIO URL WEB -->            
			{{-- <div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Homenajes</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="form-group col-md-12">
							{!! Form::label('tipo_qrcode', 'Tipo') !!}
							
							<select name="tipo_qrcode" id="tipo_qrcode" class="form-control" onchange="cambiarQR()">
								@if($ch->tipo_qrcode == 'QR1')
									<option value="QR1" selected>Homenaje online</option>
									<option value="QR2">Micrositio</option>
								@elseif($ch->tipo_qrcode == 'QR2')
									<option value="QR1">Homenaje online</option>
									<option value="QR2" selected>Micrositio</option>
								@else
									<option value="QR1">Homenaje online</option>
									<option value="QR2" >Micrositio</option>
								@endif
							</select>
							
							
						</div>
						<div class="form-group col-md-12" id="cont_url_homenaje">
							{!! Form::label('url_web_homenaje', 'URL del micrositio') !!}
							{!! Form::url('url_web_homenaje', $ch->url_web_homenaje, ['id'=>'url_web_homenaje','class' => 'form-control','placeholder'=>'Ej. http://dominio.com','title'=>'Ej. http://dominio.com o https://dominio.com','pattern'=>'https?://.+']) !!}
						</div>
						<div class="form-group col-md-12 text-center" id="img_qr">
							<img src="{!! $ch->qrcode; !!}" width="100">
						</div>
					</div>
				</div>
			</div> --}}
<!-- FIN URL WEB -->

<!-- FIN SERVIDOR -->
<!-- INICIO LOGOS -->            
			{{-- <div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Encabezado</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="form-group col-md-12">
							{!! Form::label('Tipo') !!}
							{!! Form::select('tipo_encabezado', ["texto"=>"Texto", "logo"=>"Logo"], $ch->tipo_encabezado, ['id'=>'lst-tipo-encabezado','class' => 'form-control']) !!}
						</div> --}}
						{{-- <div id="ct-logo-encabezado" style="{{($ch->tipo_encabezado=='texto' || ($ch->tipo_encabezado==''))?'display:none':''}}" class="form-group image-add-ct col-md-12">
							<div class="row">
								<div class="col-lg-4">
									{!! Form::button('Cargar Logo', ['class' => 'btn btn-success']) !!}
									<div style="position: relative; height: 0px">
										<input id="btn-up-logo"  name="logo_encabezado" class="inputFile-logo" type="file" accept="image/*" base-sixty-four-input="" style="">
									</div>
								</div>
								<div class="col-lg-8" >
									<div id="logo-encabezado-ch" style="background-image: url({{ url($ch->logo_encabezado) }})">
									</div>
								</div>
							</div>
						</div> --}}
					{{-- 	<div id="ct-texto-encabezado" class="form-group col-md-12" style="{{($ch->tipo_encabezado=='logo')?'display:none':''}}">
							{!! Form::text('texto_encabezado', $ch->texto_encabezado, ['class' => 'form-control']) !!}
						</div>
					</div>
				</div>
			</div> --}}
<!-- FIN LOGOS -->

<!-- INICIO PIE -->
			{{-- <div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Pie de página</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div id="ct-logo-pie" class="form-group image-add-ct col-md-12">
							<div class="row">
								<div class="col-lg-4">
									{!! Form::button('Cargar Logo Pie', ['class' => 'btn btn-success']) !!}
									<div style="position: relative; height: 0px">
										<input id="btn-up-logo-pie" name="logo_pie" class="inputFile-logo" type="file" accept="image/*" base-sixty-four-input="" style="">
									</div>
								</div>
								<div class="col-lg-8">
									<div id="logo-pie-ch" style="background-image: url({{ url($ch->logo_pie) }})">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div> --}}
<!-- FIN PIE -->
<!-- INICIO PUBLICIDAD -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Homenajes</h3>
				</div>
<!-- INICIO INTERVALOS DE PUBLICIDAD -->
				<div class="box-body">
					<div class="row">
						<div class="form-group col-md-12">
							{!! Form::label('duracion_slider_homenaje', 'Duración Slider Homenaje (Segundos)') !!}
							{!! Form::text('duracion_slider_homenaje', $ch->duracion_slider_homenaje, ['id'=>'duracion_slider_homenaje','class' => 'form-control']) !!}
							<span>(20 Seg. por defecto)</span>
						</div>
					</div>
				</div>
				{{-- <div class="box-body">
					<div class="row">
						<div class="form-group col-md-12">
							{!! Form::label('duracion_transicion_slider_homenaje', 'Duración de transición del Slider Homenaje (Segundos)') !!}
							{!! Form::text('duracion_transicion_slider_homenaje', $ch->duracion_transicion_slider_homenaje, ['id'=>'duracion_transicion_slider_homenaje','class' => 'form-control']) !!}
							<span>(2 Seg. por defecto)</span>
						</div>
					</div>
				</div> --}}
<!-- FIN INTERVALOS DE PUBLICIDAD -->
			</div>
<!-- FIN PUBLICIDAD -->
<!-- INICIO SINCRONIZAR SERVICIOS -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Sincronizar Servicios</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="form-group col-md-12">
							<a  href="{{ url('/?ruta=config/ch/sync/servicios') }}" id="agregar" style="cursor:pointer; cursor: hand" class="borrar btn btn-info" title="Sincronizar"><i class="ace-icon glyphicon glyphicon-refresh"></i>Sincronizar </a>
						</div>
						
					</div>
				</div>
			</div>
<!-- FIN SINCRONIZAR SERVICIOS -->


		</div>

		<div class="col-lg-4 col-md-6">
			<!-- INICIO PUBLICIDAD -->
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Publicidad</h3>
				</div>
<!-- INICIO INTERVALOS DE PUBLICIDAD -->
				<div class="box-body">
					<div class="row">
						<div class="form-group col-md-12">
							{!! Form::label('intervalo_publicidad', 'Intervalo Publicidad (Minutos)') !!}
							{!! Form::text('intervalo_publicidad', $ch->intervalo_publicidad, ['id'=>'intervalo_publicidad','class' => 'form-control']) !!}
						</div>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="form-group col-md-12">
							{!! Form::label('duracion_publicidad', 'Duracion de Publicidad (Segundos)') !!}
							{!! Form::text('duracion_publicidad', $ch->duracion_publicidad, ['id'=>'duracion_publicidad','class' => 'form-control']) !!}
						</div>
					</div>
				</div>
<!-- FIN INTERVALOS DE PUBLICIDAD -->

<!-- INICIO BANERS PUBLICITARIOS-->
				<div class="box-header">
					<h3 class="box-title">Banners Publicitarios</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div id="ct-publi-all" class="form-group col-lg-12 col-md-6">
							<?php
								$numImagen = 0;
							?>
							@foreach($publicidades as $i=>$publi)

							<div id="ct-publi-{{$i}}" class="ct-imagen" style="background-image: url({{ url($publi) }})">
								<div class="btn-del btn-imagen btn-del-imagen btn btn-danger" data-file="{{$publi}}" data-prefix="publi" data-tipo="old">
									<i class="fa fa-trash" aria-hidden="true"></i>
								</div>
								{{-- <div class="btn-up btn-imagen btn btn-success">
									<i class="fa fa-upload" aria-hidden="true"></i>
									<div class="ct-file-input-btn-add">
										<div>
											<input class="input-imagen" data-ct="ct-publi-{{$i}}" data-action="update" type="file" name="publi[]">
										</div>
									</div>
								</div> --}}
							</div>
							<?php
								$numImagen++;
							?>
							@endforeach
						</div>
						<div class="form-group col-lg-6 col-md-3">
							<input type="hidden" id="publi_delete" name="publi_delete" value="">
							<label for="btn_foto_public" class="btn btn-block btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Agregar</label>
							<div class="ct-file-input-btn-add-publi" style="position:absolute;top:0px;">
								<div>
									<input class="input-imagen" id="btn_foto_public" type="file" data-action="add" data-prefix="publi" data-numImages="{{ $numImagen }}" name="">
								</div>
							</div>
						</div>
					</div>
				</div>
<!-- FIN BANERS PUBLICITARIOS-->
			</div>
<!-- FIN PUBLICIDAD -->



		</div>
		

		<div class="col-lg-4 col-md-6">
			{{-- INICIO PALABRAS PROHIBIDAS --}}
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title">Fitrado de Homenajes</h3>
				</div>
				<div class="box-body">
					<div class="row">							
						<div class="form-group col-md-12">
							<label>Número de palabras prohibidas para bloquear homenaje</label>
							<input type="number" name="num_palabras" class="form-control" min="0" step="1" value="{{$ch->num_palabras}}">
						</div>		
						<div class="form-group col-md-12">
							<label>Tipo de bloqueo</label>
							@if($ch->tipo_bloqueo == 1)
								<div class="radio">
								  <label>
								    <input type="radio" name="tipo_bloqueo" id="tipo_bloqueo1" value="ocultar">
								    Ocultar
								  </label>
								</div>
								<div class="radio">
								  <label>
								    <input type="radio" name="tipo_bloqueo" id="tipo_bloqueo2" value="borrar" checked>
								    Borrar
								  </label>
								</div>
							@else
								<div class="radio">
								  <label>
								    <input type="radio" name="tipo_bloqueo" id="tipo_bloqueo1" value="ocultar" checked>
								    Ocultar
								  </label>
								</div>
								<div class="radio">
								  <label>
								    <input type="radio" name="tipo_bloqueo" id="tipo_bloqueo2" value="borrar" >
								    Borrar
								  </label>
								</div>
							@endif								
						</div>
						<div class="form-group col-md-12">
							<label>Palabras prohibidas</label>							
							<div class="input-group input-group-sm">
                <input type="hidden" class="form-control" id="id_palabra" >
                <input type="text" class="form-control" id="palabra" placeholder="Palabra">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-danger btn-flat hide" id="nuevaPalabra" onclick="removerPalabra()"><span class="fa fa-remove"></span></button>
                      <button type="button" class="btn btn-primary btn-flat" id="btnguardar" onclick="agregarPalabra()">Agregar</button>
                    </span>
              </div>
              <span>Doble clic en la palabra para editar</span>
							<ul style="list-style: none; margin-left: -30px;">
								@if(isset($palabras))
									@foreach($palabras as $pa)
										<li style="display: block; float: left; margin: 10px; margin-left: 0px; cursor: pointer;user-select: none;" ondblclick="editarPalabra({{$pa->id}},'{{$pa->palabra}}')"><span style="background: #DEDEDE; padding: 5px; border-radius: 7px;">{{$pa->palabra}} <a onclick="eliminarPalabra({{$pa->id}})" title="Eliminar"><span class="fa fa-remove"></span></a></span></li>
									@endforeach
								@endif
							</ul>
						</div>					
					</div>
				</div>
			</div>
{{-- FIN PALABRAS PROHIBIDAS --}}
			
		</div>

	</div>
</div>

@stop



@section('script')

<script type="text/javascript">

	function agregarPalabra(){
		var id = $('#id_palabra').val();
		var palabra = $('#palabra').val();

		console.log(palabra);

		$.ajax({
			url: "./?ruta=nuevaPalabra",
			type: 'POST',
			data: {'id':id,'palabra':palabra},
			success: function (rta) {
				location.reload();
			}
		});
	}
	function eliminarPalabra(id){
		$.ajax({
			url: "./?ruta=eliminarPalabra",
			type: 'DELETE',
			data: {'id':id},
			success: function (rta) {
				location.reload();
			}
		});
	}
	function editarPalabra(id,palabra){
		$('#id_palabra').val(id);
		$('#palabra').val(palabra);
		$('#nuevaPalabra').removeClass('hide');
		$('#btnguardar').html('Editar');
	}
	function removerPalabra(){
		$('#id_palabra').val('');
		$('#palabra').val('');
		$('#nuevaPalabra').addClass('hide');
		$('#btnguardar').html('Agregar');
	}
	
	$('#agregar').on('click',function(){
		var ip
		 $.getJSON('//freegeoip.net/json/?callback=?', function(data) {
			ip=data.ip;
		});
		$.ajax({
			url: "{{ url('config/ch/sync/servicios') }}",
			type: 'POST',
			dataType: 'json',
			data: {'ip':ip,'idcliente':1},
			success: function (rta) {
				location.reload();

			}
		});

	})
   
</script>

<script type="text/javascript">
	// SERVIDORES  
   //  function cambiarTipoServidor(){
   //      var tipo = $('#tipo_servidor').val();
   //      $('#btn_guardar_sala_local').hide();
   //      $('#btn_guardar_sala_online').hide();
   //      $('#btn_guardar_sala_propio').hide();
   //      $('#btn_disable').hide();
   //      // console.log(tipo);
   //      if(tipo == 'online'){
   //          $('#tipo_serv_local').hide();
   //          // $('#sel_sala_serv_propio').hide();
   //          $('#sel_sala_serv_ip').hide();
   //          $('#tipo_serv_online').show();
   //          // $('#sel_sala_serv_online').show();
   //          $('#btn_disable').show();
   //          $('#sala_servidor_ip option').each(function(){
   //              $(this).remove();
   //          });
   //      }else{ 
   //          if(tipo == 'local'){
   //              $('#tipo_serv_online').hide();
   //              // $('#sel_sala_serv_propio').hide();
   //              $('#sel_sala_serv_online').hide();
   //              $('#tipo_serv_local').show();
   //              // $('#sel_sala_serv_ip').show(); 
   //              $('#btn_disable').show();
   //              $('#sala_servidor_online option').each(function(){
   //                  $(this).remove();
   //              });
   //          }else{
   //              $('#tipo_serv_online').hide();
   //              $('#tipo_serv_local').hide();
   //              $('#sel_sala_serv_online').hide();
   //              $('#sel_sala_serv_ip').hide(); 
   //              $('#btn_guardar_sala_propio').show();
   //              $('#sala_servidor_ip option').each(function(){
   //                  $(this).remove();
   //              });
   //              $('#sala_servidor_online option').each(function(){
   //                  $(this).remove();
   //              });
   //          }
   //      }
   //  }
   //  function buscarSala(opc){
   //      $('#sel_sala_serv_online').hide();
   //      $('#sel_sala_serv_ip').hide();
   //      $('#btn_guardar_sala_local').hide();
   //      $('#btn_guardar_sala_online').hide();
   //      $('#sala_servidor_ip option').each(function(){
   //          $(this).remove();
   //      });
   //      $('#sala_servidor_online option').each(function(){
   //          $(this).remove();
   //      });
   //      if(opc == 'local'){
   //          var ip = 'http://'+$('#servidor_ip').val()+'/cartelera_backend_laravel/public/';
   //      }else{
   //          if(opc == 'online'){
   //              var ip = $('#servidor_url').val();
   //          }
   //      }        
   //      $.ajax({
   //          type:'GET',
   //          url: ip+"api/salas", 
   //          data :{
			// 	'_token': '{{ csrf_token() }}'
			// },
   //          success: function(result){
   //              // console.log(result);

   //              for( var i=0; i<result.salas.length; i++){
   //                  if(opc == 'local'){
   //                      // capturar el select
   //                      var lst_sala = document.getElementById("sala_servidor_ip");
   //                      if(result){
   //                          $('#btn_guardar_sala_local').show();
   //                          $('#btn_guardar_sala_online').hide();
   //                          $('#sel_sala_serv_ip').show();
   //                      }                        
   //                  }else{
   //                      if(opc == 'online'){
   //                          // capturar el select
   //                          var lst_sala = document.getElementById("sala_servidor_online");
   //                          if(result){
   //                              $('#btn_guardar_sala_local').hide();
   //                              $('#btn_guardar_sala_online').show();
   //                              $('#sel_sala_serv_online').show();
   //                          }                                
   //                      }
   //                  }  
   //                  // capturar el select
   //                  // var lst_sala = document.getElementById("sala_servidor_ip");
                            
   //                  // determinar cuantos options tiene el select
   //                  var pos = lst_sala.length  ;
                    
   //                  // crear el option nuevo: value y text
   //                  var opt = document.createElement("option");
   //                  opt.value = result.salas[i].id;
   //                  opt.text = result.salas[i].nombre;
   //                  // opt.id = 'sal_val_'+result.sala.id; 
                    
   //                  // agregar el option nuevo justo entes del último
   //                  lst_sala.add(opt, pos);
                    
   //                  // regresar_sala();
                    
   //                  // seleccionar la nueva opcion
   //                  // lst_sala.selectedIndex = pos;
   //              }                      
                
   //          },error:function(err){
   //              console.log(err);
   //          }
   //      });
   //  }
   //  function guardarSala(opc){
   //      if(opc == 'local'){
   //          var url = $('#servidor_ip').val();
   //          var sala = $('#sala_servidor_ip').val();
   //          // console.log(url,sala);
   //      }else{
   //          if(opc == 'online'){
   //              var url = $('#servidor_url').val();
   //              var sala = $('#sala_servidor_online').val();
   //              // console.log(url,sala);
   //          }else{
   //              // console.log(opc);
   //              var sala = 1;
   //              var url = ''
   //          }
   //      }
   //      $.ajax({
   //          type:'POST',
   //          url: "../api/config/ch/sala", 
   //          data :{
   //              '_token': '{{ csrf_token() }}',
   //              'tipo': opc,
   //              'url': url,
   //              'sala': sala
			// },
   //          success: function(result){
   //              console.log(result); 
   //              location.reload();               
   //          },error:function(err){
   //              console.log(err);
   //          }
   //      });
   //  }
   //  function buscarSalaPrimera(){
   //      $('#sel_sala_serv_online').hide();
   //      $('#sel_sala_serv_ip').hide();
   //      $('#btn_guardar_sala_local').hide();
   //      $('#btn_guardar_sala_online').hide();
   //      $('#sala_servidor_ip option').each(function(){
   //          $(this).remove();
   //      });
   //      $('#sala_servidor_online option').each(function(){
   //          $(this).remove();
   //      });
   //      var tipo_serv = '{{$ch->tipo_servidor}}';
   //      var sala_id = '{{$ch->sala_id}}';
   //      console.log(tipo_serv,sala_id);
   //      if(tipo_serv != 'propio'){
   //          if(tipo_serv == 'local'){
   //              var ip = 'http://'+$('#servidor_ip').val()+'/cartelera_backend_laravel/public/';
   //          }else{
   //              if(tipo_serv == 'online'){
   //                  var ip = $('#servidor_url').val();
   //              }
   //          }        
   //          $.ajax({
   //              type:'GET',
   //              url: ip+"api/salas", 
   //              data :{
   //                  '_token': '{{ csrf_token() }}'
   //              },
   //              success: function(result){
   //                  // console.log(result);

   //                  for( var i=0; i<result.salas.length; i++){
   //                      if(tipo_serv == 'local'){
   //                          // capturar el select
   //                          var lst_sala = document.getElementById("sala_servidor_ip");
   //                          if(result){
   //                              $('#btn_guardar_sala_local').show();
   //                              $('#btn_guardar_sala_online').hide();
   //                              $('#sel_sala_serv_ip').show();
   //                          }  
   //                      }else{
   //                          if(tipo_serv == 'online'){
   //                              // capturar el select
   //                              var lst_sala = document.getElementById("sala_servidor_online");
   //                              if(result){
   //                                  $('#btn_guardar_sala_local').hide();
   //                                  $('#btn_guardar_sala_online').show();
   //                                  $('#sel_sala_serv_online').show();
   //                              }  
   //                          }
   //                      }  
   //                      // capturar el select
   //                      // var lst_sala = document.getElementById("sala_servidor_ip");
                                
   //                      // determinar cuantos options tiene el select
   //                      var pos = lst_sala.length  ;
                        
   //                      // crear el option nuevo: value y text
   //                      var opt = document.createElement("option");
   //                      opt.value = result.salas[i].id;
   //                      opt.text = result.salas[i].nombre;
   //                      // opt.id = 'sal_val_'+result.sala.id;

   //                      if(sala_id == result.salas[i].id){
   //                          // capturar el select
   //                          opt.selected='selected';
   //                      }
                        
   //                      // agregar el option nuevo justo entes del último
   //                      lst_sala.add(opt, pos);
                        
   //                      // regresar_sala();
                        
   //                      // seleccionar la nueva opcion
   //                      // lst_sala.selectedIndex = pos;
   //                  }                      
                    
   //              },error:function(err){
   //                  console.log(err);
   //              }
   //          });
   //      }
        
   //  }
    // CODIGO QR1
	function cambiarQR(){
		val = $('#tipo_qrcode').val();
		if(val == 'QR1'){
			$('#cont_url_homenaje').hide();
			$('#img_qr').hide();
		}else{
			if(val == 'QR2'){
				$('#cont_url_homenaje').show();
				$('#img_qr').show();
			}
		}
	}
	
	$(document).ready(function(){

		cambiarQR();
		// cambiarTipoServidor();
		// buscarSalaPrimera();

	/*************** ENCABEZADO ************/

		$('#lst-tipo-encabezado').on('change', function(){
			val = $(this).val();
			console.log(val);
			if(val == 'texto'){
				$('#ct-logo-encabezado').hide();
				$('#ct-texto-encabezado').show();
			}else{
				$('#ct-logo-encabezado').show();
				$('#ct-texto-encabezado').hide();
			}
		});

		$('#btn-up-logo').on('change', function(evt){

			var files = evt.target.files; // FileList object
			for (var i = 0, f; f = files[i]; i++) {
				var reader = new FileReader();
				reader.onload = (function(theFile) {
					return function(e ) {
						$('#logo-encabezado-ch').css('background-image', 'url('+e.target.result+')');
					};
				})(f );
				reader.readAsDataURL(f );
			}
		});

		$('#btn-up-logo-pie').on('change', function(evt){

			var files = evt.target.files; // FileList object
			for (var i = 0, f; f = files[i]; i++) {
				var reader = new FileReader();
				reader.onload = (function(theFile) {
					return function(e ) {
						$('#logo-pie-ch').css('background-image', 'url('+e.target.result+')');
					};
				})(f );
				reader.readAsDataURL(f );
			}
		});

	/************* IMAGENES ******************/

		$('.input-imagen').on('change', function(evt){
			console.log($(this).data('ct'));
			$this = $(this);
			var files = evt.target.files; // FileList object

			for (var i = 0, f; f = files[i]; i++) {
				var reader = new FileReader();
				reader.onload = (function(theFile) {
					return function(e ) {
						
							prefix = $this.data('prefix');
							numImages = $this.data('numImages');
							newObj = $('<div id="ct-'+prefix+'-'+numImages+'" class="ct-imagen" style="background-image: url('+e.target.result+')"><div class="btn-del btn-imagen btn-del-imagen btn btn-danger" data-prefix="'+prefix+'" data-tipo="new"><i class="fa fa-trash" aria-hidden="true"></i></div><div class="btn-up btn-imagen btn btn-success hide"><i class="fa fa-upload" aria-hidden="true"></i><div class="ct-file-input"><div class="new-input"></div></div></div></div>');  
							
							newObj.find('.btn-del-imagen').on('click', function(){
								$(this).parent().fadeOut('slow',function(){
									$(this).remove();
								});
							});

							$('#ct-'+prefix+'-all').append(newObj);

							newInput = $this.clone(true);
							newInput.data('action', 'update');
							newInput.data('ct', 'ct-'+prefix+'-'+numImages);
							newInput.attr('name',prefix+'[]');
							newObj.find('.new-input').append(newInput);
							numImages++;
							$this.data('numImages', numImages);

						
						
					};
				})(f, $this);
				reader.readAsDataURL(f);
			}
		});

		$('.btn-del-imagen').on('click', function(){
			$this = $(this);
			
			if($this.data('tipo') == 'old'){
				prefix = $this.data('prefix');
				aux = $('#'+prefix+'_delete').val();
				if(aux != '')
					aux = aux+',';
				aux = aux+$this.data('file');   
				$('#'+prefix+'_delete').attr('value', aux); 
			}
			
			$this.parent().fadeOut('slow',function(){
				$(this).remove();
			});
		});
		
		$('#check_mf').on('click',function(){
			if($('#check_mf').prop('checked')){            
				$('#musica_funcional').removeClass( "hide" );
			}else{
				
				$('#musica_funcional').addClass( "hide" );
			}
		})
		
		if ($('#check_mf').prop('checked')) {
			$('#musica_funcional').removeClass( "hide" );
		}else{
			$('#musica_funcional').addClass( "hide" );
		}
		
		$('#check_cr').on('click',function() {
			if($('#check_cr').prop('checked')){            
				$('#control_rondas').removeClass( "hide" );
			} else {
				
				$('#control_rondas').addClass( "hide" );
			}
		})
		
		if ($('#check_cr').prop('checked')) {
			$('#control_rondas').removeClass( "hide" );
		} else {
			$('#control_rondas').addClass( "hide" );
		}

	});
	
</script>
@stop