@extends('dashboard.layout.base')

@section('title')
    Configuracion General
@stop

@section('section')
    Configuracion
@stop

@section('breadcrumb')
    General
@stop
@section('content')

{!! Form::open(['url' => 'config', 'method' => 'POST', 'files' => true]) !!}

<div class="content">
    <div class="row">
        <div class="col-lg-12">
            Datos de configuracion para las carteleras de velatorio {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="box box-default">
                <div class="box-header">
                    <h3 class="box-title">Accesos</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            {!! Form::label('Contraseña Admin') !!}
                            {!! Form::text('pass_admin', $config->pass_admin, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('Contraseña Config') !!}
                            {!! Form::text('pass_config', $config->pass_config, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>
{!! Form::close() !!}

@stop

@section('script')
<script>
$(document).ready(function(){

/*************** ENCABEZADO ************/
    
});
</script>
@stop