@extends('dashboard.layout.base')

@section('title')
    Productos
@stop

@section('section')
    Productos
@stop

@section('breadcrumb')
    Productos
@stop
@section('content')
<?php 
    $usuario = session()->get('key_login'); 

    if(!isset($usuario['nombre']) || $usuario['admin']!=1){        
      echo '<script> location.href = "./?ruta=login"; </script>';
    }
?>


        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <button id="btn-add-sala" class="btn btn-info" data-info1=""><i class="ace-icon glyphicon glyphicon-plus"></i>Nuevo</button> 
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <table class="table table-hover" id="tabla">
                    <thead>
                        <tr>
                          <th>ID</th>
                          <th></th>
                          <th>Nombre</th>
                          <th>Precio</th>
                            <th></th>
                        </tr>
                    </thead>
                @foreach($producto as $prod)
                <tr>
                  <td width="5%">{{$prod->id}}</td>
                  <td width="10%"><img src="{{url($prod->foto)}}" alt="{{$prod->foto}}" width="50%"></td>
                  <td>{{$prod->nombre}}</td>
                  <td width="10%">{{$prod->precio}}</td>
                    <td>
                    <button data-info="{{json_encode($prod)}}" class="btn-edit-sala btn btn-xs btn-primary" title="Editar Producto">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <a href="{!! url('./?ruta=producto/eliminar&id='.$prod->id)  !!}" class="btn btn-xs btn-danger" title="Eliminar Producto">
                        <i class="fa fa-trash"></i>
                    </a>
                    </td>
                </tr>
                @endforeach
              </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    @include("configuracion.productos.form")

@stop
@section('script')
<script>
$(document).ready(function(){
    /*$('.icheckbox').iCheck({
        checkboxClass: 'icheckbox_minimal',
    });*/

    var myTable = $("#tabla") .DataTable( {
        "dom": '<"top"i>rt<"row"<"col-xs-6"l><"col-xs-6"p>>',
        "length":   false,
        "info":     false,
        bAutoWidth: false,

        select: {
            style: 'multi'
        }
    } );

    $('#buscar').on( 'keyup', function () {
        myTable.search( this.value ).draw();
    } );

    $('#btn-add-sala').on('click', function(){
        $("#title").html("Crear Producto");
        $("#btntitle").html("Crear");
        $("#myModal").modal();
    });

    $('#btn-add-foto').on('change', function(evt){
        var files = evt.target.files; // FileList object
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e ) {
                    $('#add-icon-item').css('background-image', 'url('+e.target.result+')');
                };
            })(f );
            reader.readAsDataURL(f);
        }
    });

    $('.btn-edit-sala').on('click', function(){
        info  = $(this).data('info');

        if(info.status == "1"){
            $("#status").attr("checked","checked");
        }else{
            $("#status").removeAttr("checked");
        }
        $("#nombre").val(info.nombre);
        $("#precio").val(info.precio);
        $('#add-icon-item').css('background-image', "url({{url()}}/"+info.foto+")");
        $("#title").html("Editar Producto: "+info.nombre);
        $("#btntitle").html("Editar");

        action = $('#form-update').attr('action');
        $('#form-update').attr('action', action+'/update&id='+info.id);
        $("#myModal").modal();

    });
});

</script>
@stop