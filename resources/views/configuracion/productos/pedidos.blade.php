@extends('dashboard.layout.base')

@section('title')
    Pedidos
@stop

@section('section')
    Pedidos
@stop

@section('breadcrumb')
    Pedidos
@stop
@section('content')
<?php 
    $usuario = session()->get('key_login'); 

    if(!isset($usuario['nombre']) || $usuario['admin']!=1){        
      echo '<script> location.href = "./?ruta=login"; </script>';
    }
?>


        <div class="col-xs-12">
            <div class="box">
                
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-hover" id="tabla">
                    <thead>
                        <tr>
                          <th>Pedido #</th>
                          <th>Nombre sala</th>
                          <th>fallecido </th>
                          <th>Producto</th>
                          <th>Precio</th>
                          <th>status</th>
                        </tr>
                    </thead>
                    @foreach($pedidos as $pedido)
                    <tr>
                      <td>{{$pedido->id}}</td>
                      <td>{{$pedido->salas->nombre}}</td>
                      <td>{{$pedido->servicios->nombres}} {{$pedido->servicios->apellidos}}</td>
                      <td>{{$pedido->productos->nombre}}</td>
                      <td>{{$pedido->productos->precio}}</td>
                      <td> @if($pedido->estado=="P")
                                <a href="{{url('/?ruta=pedido/cobrar&id='.$pedido->id.'&status=P')}}"  data-homenaje="{{ $pedido->toJson() }}" class="btn btn-xs btn-success btn-status" title="Cobrar Producto">
                                    Pendiente
                                </a>
                            @elseif($pedido->estado=="C")
                                <a href="{{url('/?ruta=pedido/cobrar&id='.$pedido->id.'&status=C')}}"  data-homenaje="{{ $pedido->toJson() }}" class="btn btn-xs btn-danger btn-status" title="cancelar Pedido">
                                    Cobrado
                                </a>
                            @endif
                            </td>
                    </tr>
                    @endforeach
                  </table>
                
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    

@stop
