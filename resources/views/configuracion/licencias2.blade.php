@extends('dashboard.layout.base')

@section('title')
	Configuracion Licencias
@stop

@section('section')
	Licencia
@stop

@section('breadcrumb')
	Licencia
@stop
@section('content')

<div class="content">
	<div class="row">
		{!! Form::open(['url' => 'config/licencia/validar' ,'method' => 'POST', 'files' => true]) !!}
		<div class="col-lg-4 col-md-6">
			<div class="box box-default">
				<div class="box-header">
					<h3 class="box-title">Activar Licencia</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="form-group col-md-12">
							{!! Form::label('Licencia Admin') !!}
							{!! Form::text('licencia', NULL, ['class' => 'form-control']) !!}
						</div>
						<div class="form-group col-md-12">
							{!! Form::submit('Activar', ['class' => 'btn btn-success']) !!}
						</div>
					</div>
				</div>
			</div>  
		</div>
		{!! Form::close() !!}
	</div>
	<div class="row">		
		@foreach($licencias as $licencia)

			<div class="col-lg-12">
				<!-- small box -->
				@if($licencia->status == "Active")
					<div class="small-box bg-green">
				@else
					<div class="small-box bg-red">
				@endif
					<div class="inner">
						<h3>{{ $licencia->product_name }}</h3>

						<p>Registro: <strong>{{ $licencia->reg_date }}</strong> </p>
						<p>Ciclo de pago: <strong>{{ $licencia->billing_cycle }}</strong> </p>
						<p>Vencimiento: <strong>{{ $licencia->next_due_date }}</strong> </p>
						<p>Licence Key: <strong>{{ $licencia->licence_key }}</strong> </p>
						<div>
							<form style="display: inline;" action="{{ url('config/licencia', [$licencia->id]) }}" method="POST">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
								<button class="btn btn-default" type="submit">Eliminar <i class="fa fa-times"></i></button>
							</form>
							@if($licencia->status == "Inactive")
								<form style="display: inline;" action="{{ url('config/licencia/validar') }}" method="POST">
									{{ csrf_field() }}
									<input type="hidden" name="licencia" value="{{$licencia->licence_key}}">
									<button class="btn btn-default" type="submit">Activar <i class="fa fa-times"></i></button>
								</form>
							@endif
						</div>
					</div>
					<div class="icon">
						<i class="fa fa-shopping-cart"></i>
					</div>
					<a href="#" class="small-box-footer">
						Más información <i class="fa fa fa-arrow-circle-right"></i>
					</a>
				</div>
			</div>
		<!-- ./col -->

		@endforeach
	</div>
</div>


@stop

@section('script')
<script>
$(document).ready(function(){

/*************** ENCABEZADO ************/
	
});
</script>
@stop