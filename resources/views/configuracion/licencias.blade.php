@extends('dashboard.layout.base')

@section('title')
	Configuracion Licencias
@stop

@section('section')
	Licencia
@stop

@section('breadcrumb')
	Licencia
@stop
@section('content')

	
<div class="content">
	<div id="activa" class ="hide">

			<div class="alert alert-block alert-success">
				<button type="button" class="close" data-dismiss="alert">
					<i class="ace-icon fa fa-times"></i>
				</button>

				<i class="ace-icon fa fa-check green"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">

				Licencia Valida
				 </font></font><strong class="green"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
					Neo
					 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sepelios</font></font></small>
				</strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">, para más Información entre en www.neosepelios.com.ar
				</font></font>
			</div>
		</div>
		<div id="inactiva" class ="hide">
			<div class="alert alert-block alert-warning">
				<button type="button" class="close" data-dismiss="alert">
					<i class="ace-icon fa fa-times"></i>
				</button>

				<i class="ace-icon fa fa-times green"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">

				Licencia Invalida
				 </font></font><strong class="green"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
					Neo
					 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sepelios</font></font></small>
				</strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">, Pongase en contacto con info@neospelios.com o verifique su licencia nuevamente.
				</font></font>
			</div>
		</div>
	<div class="box box-default ">
	<div class="box-header with-border">
		<h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Empresa</font></font></h3>

		<div class="box-tools pull-right">
		
		</div>
	  <!-- /.box-tools -->
	</div>
	<!-- /.box-header -->
	<div class="box-body" style="display: block;">
	<div class="form-group col-md-4">
		
		
		{!! Form::model($empresa,['url' => './?ruta=empresa/guardar','files' => true]) !!}
		
		
            {!! Form::label('Nombre') !!}
            {!! Form::text('Nombre', null, ['class' => 'form-control', 'id' => 'Nombre','disabled']) !!}
            {!! Form::label('Correo') !!}
            {!! Form::text('Correo_1', null, ['class' => 'form-control', 'id' => 'Correo_1','disabled']) !!}
            
        </div>
        <div class="col-md-4">
            <div class="form-group" style="clear: both">
                {!! Form::label('Logo ') !!}
                <div  class="img-item" id="add-logo" style="background:url({{ url('images/empresa/'.$ni->logo_encabezado) }});background-size:100% 100%;height:100px;width:100px"></div>
                <input id="inputLogo"  name="logo"  type="file" accept="image/*" base-sixty-four-input="" >
            </div>  
        </div>
        <div class="col-md-4">
            <div class="form-group" style="clear: both">
                {!! Form::label('Logo Pie') !!}
                <div  class="img-banner" id="add-logo_pie" style="background:url({{ url('images/empresa/'.$ni->logo_pie) }});background-size:100% 100%;height:100px;width:100px"></div>
                <input id="impuntlogo_pie"  name="logo_pie"  type="file" accept="image/*" base-sixty-four-input="" >
            </div>  
        </div>
        <div class="box-footer">
            <input type="hidden" name="id" value="1">
            {!! Form::submit('Guardar', ['class' => 'btn btn-primary','id'=>'guardar']) !!}
        </div>
        {!! Form::close() !!}

	</div>
	<!-- /.box-body -->

</div>

	
		<!--<div class="row">	-->
			<div class="box box-default">
				<div class="box-header">
					<h3 class="box-title" style="float: left;">Productos</h3>
					<button id ='verificar_licencias'class=" btn btn-success" onclick="verificar_licencias()" style="float: left; margin-left: 20px;">Verificar Licencias</button>
					<div class="col-md-3 hide" style="float: left;" id ="verificar_licencias2" >
		
			          <div class="box box-success">	
			            <div class="box-header">
			              <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Verificando</font></font></h3>
			            </div>
			            <div class="box-body"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;" id ="licencia_unica">
			              Verificando Licencias
			            </font></font></div>
			            <!-- /.box-body -->
			            <!-- Loading (remove the following to stop the loading)-->
			            <div class="overlay">
			              <i class="fa fa-refresh fa-spin"></i>
			            </div>
			            <!-- end loading -->
			          </div>
			          </div>
			          <!-- /.box -->
			      

				</div>

				<div class="box-body">
					<div class="row">
				@foreach($licencias as $licencia)
						
						@if($licencia->licence_key != '' )				
							<div class="col-lg-6">
								@if($licencia->licence_key != "")
									<div class="box box-default " >								
										<div class="box-header with-border"">										
								              <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{ $licencia->product_name }}
								              	</font></font></h3>
								               <div class="box-tools pull-right">
								               	@if($licencia->product_id == 10)
								               		@if($licencia->descarga == 1)
									               		{!! Form::select('size_'.$licencia->id, $salasLis,null,['class' => 'form-group', 'placeholder' => 'Seleccione sala','id'=>'size_'.$licencia->id] )!!}
									               		<button class="btn btn-xs bg-purple fa fa-eye" title="Vista previa CA" onclick="vista_previa( {{$licencia->id }} )"></button>
								               		@endif
								          			<a href="{{ url('?ruta=config/ch') }}" class="btn " title="Ajustes" style="border-radius: 50%; font-size: 23px; padding: 2px 6px"><i class="fa fa-cog"></i></a> 

									          	@endif
									          	@if($licencia->product_id == 35)
									          		@if($licencia->descarga == 1)
									          			<button class="btn btn-xs bg-purple fa fa-eye" title="Vista previa CV" onclick="vista_previa( {{$licencia->id }} )"></button>
									          		@endif
									          		<a href="{{ url('?ruta=config/cv') }}" class="btn " title="Ajustes" style="border-radius: 50%; font-size: 23px; padding: 2px 6px"><i class="fa fa-cog"></i></a>     
									          	@endif
									          	@if($licencia->product_id == 7)
									          		@if($licencia->descarga == 1)
									          			<button class="btn btn-xs bg-purple fa fa-eye" title="Vista previa CV" onclick="vista_previa( {{$licencia->id }} )"></button>
									          		@endif
									          		<a href="{{ url('?ruta=config/cv') }}" class="btn " title="Ajustes" style="border-radius: 50%; font-size: 23px; padding: 2px 6px"><i class="fa fa-cog"></i></a>     
									          	@endif
									          	@if($licencia->product_id == 5)
									          		@if($licencia->descarga == 1)
										          		{!! Form::select('size_'.$licencia->id, $salasLis,null,['class' => 'form-group', 'placeholder' => 'Seleccione sala','id'=>'size_'.$licencia->id] )!!}
										          		<button class="btn btn-xs bg-purple fa fa-eye" title="Vista previa CH" onclick="vista_previa( {{$licencia->id }} )"></button>
									          		@endif
									          		<a href="{{ url('?ruta=config/ch') }}" class="btn " title="Ajustes" style="border-radius: 50%; font-size: 23px; padding: 2px 6px"><i class="fa fa-cog"></i></a>     
									          	@endif
									          	@if($licencia->product_id == 9)
									          		@if($licencia->descarga == 1)
										          		{!! Form::select('size_'.$licencia->id, $salasLis,null,['class' => 'form-group', 'placeholder' => 'Seleccione sala','id'=>'size_'.$licencia->id] )!!}
										          		<button class="btn btn-xs bg-purple fa fa-eye" title="Vista previa CI" onclick="vista_previa( {{$licencia->id }} )"></button>
										          	@endif
									          		<a href="{{ url('?ruta=config/ch') }}" class="btn " title="Ajustes" style="border-radius: 50%; font-size: 23px; padding: 2px 6px"><i class="fa fa-cog"></i></a>     
									          	@endif
									          	@if($licencia->product_id == 33)
									          		@if($licencia->descarga == 1)
										          		{!! Form::select('size_'.$licencia->id, $salasLis,null,['class' => 'form-group', 'placeholder' => 'Seleccione sala','id'=>'size_'.$licencia->id] )!!}
										          		<button class="btn btn-xs bg-purple fa fa-eye" title="Vista previa Productos" onclick="vista_previa( {{$licencia->id }} )"></button>
										          	@endif

									          		<a href="{{ url('/?ruta=productos') }}" class="btn " title="Ajustes" style="border-radius: 50%; font-size: 23px; padding: 2px 6px"><i class="fa fa-cog"></i></a>     
									          	@endif
									          	@if($licencia->product_id == 32)
									          		@if($licencia->descarga == 1)
											          	{!! Form::select('size_'.$licencia->id, $salasLis,null,['class' => 'form-group', 'placeholder' => 'Seleccione sala','id'=>'size_'.$licencia->id] )!!}
											          		<button class="btn btn-xs bg-purple fa fa-eye" title="Vista previa CR" onclick="vista_previa( {{$licencia->id }} )"></button>
											        @endif
									          		<a href="http://cr.neo.co/modulos/cc0/public/?ruta=dashboard/tareas" target="_blank" class="btn " title="Ajustes" style="border-radius: 50%; font-size: 23px; padding: 2px 6px"><i class="fa fa-cog"></i></a>     
									          	@endif
									          	@if($licencia->product_id == 13)
									          		@if($licencia->descarga == 1)
											          	{!! Form::select('size_'.$licencia->id, $salasLis,null,['class' => 'form-group', 'placeholder' => 'Seleccione sala','id'=>'size_'.$licencia->id] )!!}
											          		<button class="btn btn-xs bg-purple fa fa-eye" title="Vista previa MF" onclick="vista_previa( {{$licencia->id }} )"></button>
											        @endif
									          		<a href="{{ url('/?ruta=config/mf') }}" class="btn " title="Ajustes" style="border-radius: 50%; font-size: 23px; padding: 2px 6px"><i class="fa fa-cog"></i></a>     
									          	@endif
									          	@if($licencia->product_id == 6)
									          		@if($licencia->descarga == 1)
											          	{{!! Form::select('size_'.$licencia->id, $salasLis,null,['class' => 'form-group', 'placeholder' => 'Seleccione sala','id'=>'size_'.$licencia->id] )!!}
											          	<button class="btn btn-xs bg-purple fa fa-eye" title="Vista previa CS" onclick="vista_previa( {{$licencia->id }} )"></button>
											        @endif
									          		<a href="{{ url('?ruta=config/cs') }}" class="btn " title="Ajustes" style="border-radius: 50%; font-size: 23px; padding: 2px 6px"><i class="fa fa-cog"></i></a>     
									          	@endif
									          	
								              </div>
							            </div>							            
								            <div class="box-body"  >
								            	<div class="col-lg-12">
									            	<div  class="col-lg-3" style="background-image: url('{{url($licencia->foto)}}');height: 140px;width: 190px;background-size: 190px;background-repeat: no-repeat;    float: left;"></div>
									            	<div class="col-lg-5" >
									            		<p> {{$licencia->info}}</p>
									            		<a href="http://{{$licencia->url}}" target="_blank" data-id="200">Más información</a>
									            	</div>
									            	<div class="col-lg-2" >
									            		@if($licencia->status == "Active" && $licencia->descarga != 0)
														<form style="display: inline;" action="{{ url('./?ruta=config/licencia') }}" method="POST">
															<input type="hidden" name="id" value="{{$licencia->id}}">
															{{ csrf_field() }}
															<button class="btn btn-default" type="submit">Desinstalar </button>
														</form>
														<br>
														<br>
														@if(  $licencia->descarga != 0)
														<button class="btn btn-default" onclick="modulo({{ $licencia->id }})" ="submit">Abir</button>
														@endif
														
														@else
															{{--<form style="display: inline;" action="{{ url('./?ruta=config/licencia/validar') }}" method="POST">
																{{ csrf_field() }}
																<input type="hidden" name="licencia" value="{{$licencia->licence_key}}">
																<button class="btn btn-default" type="submit">Activar </button>
															</form>--}}
															
														@endif
														
														@if(  $licencia->descarga == 0)
															<button  id='descarga_{{$licencia->id}}' style="margin-top:10px " class="btn btn-default" type="submit" onclick="onDownloadLinkClick( {{ $licencia->id }} )">Descargar </button>
															<br><br>
															<span id='barra1_{{$licencia->id}}' style="font-size: 10px;"></span><br>
															<div class="progress progress-sm active">
												                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width:0%" id='barra2_{{$licencia->id}}'>
												                </div>
												              </div>
														@endif
													</div>
													
												</div>
									        </div>									    
									</div>
								@else								
								<div class="box box-default ">	
									<div class="box-header with-border" ">
							              <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{ $licencia->product_name }}</font></font></h3>
							              
						            </div>
						            <div class="box-body" style="">
								        <div class="col-lg-12">	
								        	<div  class="col-lg-3" style="background-image: url('{{url($licencia->foto)}}');height: 140px;width: 190px;background-size: 190px;background-repeat: no-repeat;    float: left;">
								        	</div>	
											
											<div class="col-lg-5" >
							            		<p> {{$licencia->info}}</p>
							            		
									         </div>
									        <div class="col-lg-2" >
									        	<div class="small-box bg-aqua"  style="margin-left: 90px">
											        <div class="icon"><i class="fa fa-shopping-cart"></i></div>	
													<div class="icon"><i class="fa fa-shopping-cart"></i></div>
												</div>
												<button style="margin-top:100px;background-color: #3c8dbc;" class="btn btn-info" onclick="licencia('{{ $licencia->url }}')" id="agregar_{{$licencia->id}}"> Ver más </button>
												

												
											</div>

											 <div class="hide" id='crearLicencia_{{$licencia->id}}' style="margin-top: 10px" >
												{!! Form::open(['url' => './?ruta=config/licencia/validar' ,'method' => 'POST', 'files' => true]) !!}						<div class="form-group col-md-8	">
															
															{!! Form::text('licencia', NULL, ['class' => 'form-control']) !!}
														</div>
														<div class="form-group col-md-3">
															{!! Form::submit('Activar', ['class' => 'btn btn-success']) !!}
														</div>
													
												
												{!! Form::close() !!}
											</div>
											
										</div>						
								    </div>
						         			
								</div>
								
								@endif
							</div>
						@endif
					
				@endforeach
				</div>
				</div>
			</div>
			<div class="box box-default">
				<div class="box-header">
					<h3 class="box-title">Productos</h3>
				</div>
				<div class="box-body">
					<div class="row">
				@foreach($licencias as $licencia)

					
						@if($licencia->licence_key =='')			
							<div class="col-lg-6">
								
								
								<div class="box box-default ">	
									<div class="box-header with-border" ">
							              <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{ $licencia->product_name }}</font></font></h3>
							             {{--  <div class="box-tools pull-right">
								               	@if($licencia->product_id == 5)
									          		<a href="{{ url('?ruta=config/ch') }}" class="btn " title="Ajustes" style="border-radius: 50%; font-size: 23px; padding: 2px 6px"><i class="fa fa-cog"></i></a>     
									          	@endif
									          	@if($licencia->product_id == 32)
									          		<a href="{{ url('../../modulos/cc0/public/?ruta=dashboard/tarea') }}" class="btn " title="Ajustes" style="border-radius: 50%; font-size: 23px; padding: 2px 6px"><i class="fa fa-cog"></i></a>     
									          	@endif
									          	@if($licencia->product_id == 6)
									          		<a href="{{ url('config/cs') }}" class="btn " title="Ajustes" style="border-radius: 50%; font-size: 23px; padding: 2px 6px"><i class="fa fa-cog"></i></a>     
									          	@endif
								          </div> --}}	
						            </div>
						            <div class="box-body" style="">
								        <div class="col-lg-12">	
								        	<div  class="col-lg-3" style="background-image: url('{{url($licencia->foto)}}');height: 140px;width: 190px;background-size: 190px;background-repeat: no-repeat;    float: left;">
								        	</div>	
											<div class="col-lg-5" >
							            		<p> {{$licencia->info}}</p>
							            		
									         </div>
									        <div class="col-lg-2" >
									        	
									        		<div class="small-box bg-aqua"  style="margin-left: 90px">
												        <div class="icon"><i class="fa fa-shopping-cart"></i></div>	
														<div class="icon"><i class="fa fa-shopping-cart"></i></div>
													</div>
													<button style="margin-top:100px;background-color: #3c8dbc;" class="btn btn-info" onclick="licencia('{{ $licencia->url }}')" id="agregar_{{$licencia->id}}"> Ver más </button>
												

												
											</div>

											 <div class="hide" id='crearLicencia_{{$licencia->id}}' style="margin-top: 10px" >
												{!! Form::open(['url' => './?ruta=config/licencia/validar' ,'method' => 'POST', 'files' => true]) !!}						<div class="form-group col-md-8	">
															
															{!! Form::text('licencia', NULL, ['class' => 'form-control']) !!}
														</div>
														<div class="form-group col-md-3">
															{!! Form::submit('Activar', ['class' => 'btn btn-success']) !!}
														</div>
													
												
												{!! Form::close() !!}
											</div>
									        	
													
											
											
										</div>						
								    </div>
						         			
								</div>
								
								@endif
							</div>
						
				
				@endforeach
				</div>
				</div>
			</div>
		
	
</div>


@stop

@section('script')
<script>
$(document).ready(function(){

});	
    
	function onDownloadLinkClick(id) {
		$('#barra1_'+id).html('10% crando carpeta');
		$('#barra2_'+id).css('width','10%');
		$('#descarga_'+id).attr("disabled", true);
		var ruta='';
		var carpeta='';
    	$.ajax({
	        type: 'POST',
	        url: '../public/?ruta=carpeta',            
	        data :{
                    'id':id
                },
	        success: function (data) {	        	
	        	data=JSON.parse(data);
	        	$('#barra1_'+id).html('30% descargando');
	        	$('#barra2_'+id).css('width','30%');	        	
	
	        	$.ajax({
			        type: 'POST',
			        url: '../public/?ruta=api/download',            
			        data :{
		                    'id':id,'ruta1':data.ruta,'carpeta':data.carpeta
		                },
		         

			        success: function (data1) {
			        	data1=JSON.parse(data1);
			        	$('#barra1_'+id).html('70% descomprimiendo');
			        	$('#barra2_'+id).css('width','70%');
			        	$.ajax({
				        type: 'POST',
				        url: '../public/?ruta=api/descomprimir',            
				        data :{
			                    'id':id,'ruta2':data1.ruta,'carpeta2':data1.carpeta
			                },
			            
				        success: function (data) {
				        	$('#barra1_'+id).html('100% finalizado');
				        	$('#barra2_'+id).css('width','100%');
				        		location.reload();
				           
				            
				        },
				        error: function (e) {
				            alert('falló descomprimiendo...');
				        }
				    });
			           
			            
			        },
			        error: function (e) {
			            alert('falló al descargar archivo...');
			        }
			    });
	           
	            
	        },
	        error: function (e) {
	            alert('falló al crear carpeta ...');
	        }
	    });
    }
    function licencia(id) {

    	window.open(id, '_blank');
    }
    function vista_previa(id) {
    	console.log(location);
    	
    	var sala=$('#size_'+id).val();
    	switch(id) {

    		case 1: url = encodeURI(location.host+'/modulos/_ca/?sala='+sala);
    				window.open(location.origin+'/demo/?protocolo=http&recurso='+url+'&w=1920&h=1080&black=on', '_blank');
    				break;
    		case 2:url = encodeURI(location.host+'/modulos/_cv_n');
    				window.open(location.origin+'/demo/?protocolo=http&recurso='+url+'&w=1920&h=1080&black=on', '_blank');
    				break;
    		case 3:url = encodeURI(location.host+'/modulos/_cv_2');
    				window.open(location.origin+'/demo/?protocolo=http&recurso='+url+'&w=1920&h=1080&black=on', '_blank');
    				break;
    		case 4: url = encodeURI(location.host+'/modulos/ni/?sala='+sala);
    				window.open(location.origin+'/demo/?protocolo=http&recurso='+url+'&w=1920&h=1080&black=on', '_blank');
    				break;
    		case 5: url = encodeURI(location.host+'/modulos/ni/?sala='+sala);
    				window.open(location.origin+'/demo/?protocolo=http&recurso='+url+'&w=1920&h=1080&black=on', '_blank');
    				break;
    		case 6: url = encodeURI(location.host+'/modulos/ni/?sala='+sala);
    				window.open(location.origin+'/demo/?protocolo=http&recurso='+url+'&w=1920&h=1080&black=on', '_blank');
    				break;
    		case 7: url = encodeURI(location.host+'/modulos/ni/?sala='+sala);
    				window.open(location.origin+'/demo/?protocolo=http&recurso='+url+'&w=1920&h=1080&black=on', '_blank');
    				break;
    		case 8: url = encodeURI(location.host+'/modulos/ni/?sala='+sala);
    				window.open(location.origin+'/demo/?protocolo=http&recurso='+url+'&w=1920&h=1080&black=on', '_blank');
    				break;
    		case 9: url = encodeURI(location.host+'/modulos/ni/?sala='+sala);
    				window.open(location.origin+'/demo/?protocolo=http&recurso='+url+'&w=1920&h=1080&black=on', '_blank');
    				break;
    		}
    }
    function modulo(id) {

    	switch(id) {
		    case 1:
		        url = encodeURI('localhost:8080/modulos/_ca');
				window.location = 'http://localhost:8080/demo/?protocolo=http&recurso='+url+'&w=1920&h=1080&black=on';
		        break;
		    case 2:
		        url = encodeURI('localhost:8080/modulos/_cv_n');
				window.location = 'http://localhost:8080/demo/?protocolo=http&recurso='+url+'&w=1920&h=1080&black=on';
		        break;
		    case 3:
		         url = encodeURI('localhost:8080/modulos/_cv_2');
				window.location = 'http://localhost:8080/demo/?protocolo=http&recurso='+url+'&w=1920&h=1080&black=on';
		        break;
		    case 4:
		        url = encodeURI('localhost:8080/modulos/ni');
				window.location = 'http://localhost:8080/demo/?protocolo=http&recurso='+url+'&w=1280&h=800&black=on';
		        break;
		    case 5:
		        url = encodeURI('localhost:8080/modulos/ni');
				window.location = 'http://localhost:8080/demo/?protocolo=http&recurso='+url+'&w=1280&h=800&black=on';
		        break;		       
		    case 6:
		       	url = encodeURI('localhost:8080/modulos/ni');
				window.location = 'http://localhost:8080/demo/?protocolo=http&recurso='+url+'&w=1280&h=800&black=on';
		        break;		       
		    case 7:
		        url = encodeURI('localhost:8080/modulos/ni');
				window.location = 'http://localhost:8080/demo/?protocolo=http&recurso='+url+'&w=1280&h=800&black=on';
		        break;		        
		    case 8:
		        url = encodeURI('localhost:8080/modulos/ni');
				window.location = 'http://localhost:8080/demo/?protocolo=http&recurso='+url+'&w=1280&h=800&black=on';
		        break;		        
		    case 9:
		        url = encodeURI('localhost:8080/modulos/ni');
				window.location = 'http://localhost:8080/demo/?protocolo=http&recurso='+url+'&w=1280&h=800&black=on';
		        break;
		    
		        
		}
    }
    
       
    	
    $('#inputLogo').on('change', function(evt){
            var files = evt.target.files; // FileList object
            for (var i = 0, f; f = files[i]; i++) {
                var reader = new FileReader();
                reader.onload = (function(theFile) {
                    return function(e ) {
                        $('#add-logo').css('background-image', 'url('+e.target.result+')');
                        $('#add-logo').css('background-size','100% 100%');
                    };
                })(f );
                reader.readAsDataURL(f );
            }
        });
    $('#impuntlogo_pie').on('change', function(evt){
            var files = evt.target.files; // FileList object
            for (var i = 0, f; f = files[i]; i++) {
                var reader = new FileReader();
                reader.onload = (function(theFile) {
                    return function(e ) {
                        $('#add-logo_pie').css('background-image', 'url('+e.target.result+')');
                        $('#add-logo_pie').css('background-size','100% 100%');
                    };
                })(f );
                reader.readAsDataURL(f );
            }
        });
    $('#form_editar_sala').hide();
	$('#form_nueva_sala').hide();
	function sala_edit(){
		var id = $('#lst_sala').val();
		// console.log('sala: '+id);
		$('.status_sala_editar').each(function(){
			$(this).remove();
		})
		$.ajax({
			type:'GET',
			url: "../public/?ruta=servicio/sala&id="+id, 
			data : '',
			success: function(result){
				console.log('SALA',result);
				$('#id_sala_editar').val(result.id);
				$('#nombre_sala_editar').val(result.nombre);
				$('#codigo_sala_editar').val(result.codigo);
				if(result.status==1){
					// $('#status_sala_editar').attr('checked','checked');
					$('#status_sala_edit').append('<span class="status_sala_editar"><input id="status_sala_editar" type="checkbox" checked > Activa</span>');
				}else{
					$('#status_sala_edit').append('<span class="status_sala_editar"><input id="status_sala_editar" type="checkbox" > Activa</span>');
				}
				// $('#icono_sala_editar').attr('src','../images/salas/'+result.icono);
				// $('#image-preview-icono_sala').css('background-color','white');
				$('#image-preview-icono_sala').css('background-image','url(../public/images/salas/'+result.icono+')');
				// $('#image-preview-letra_sala').css('background-color','white');
				$('#image-preview-letra_sala').css('background-image','url(../public/images/salas/'+result.letra+')');
				$('#sala_select').hide();
				$('#titulo_sala').hide();
				$('#form_editar_sala').show();
			},error:function(err){
				console.log(err);
			}
		});
	}
	function sala_new(){
		$('#sala_select').hide();
		$('#titulo_sala').hide();
		$('#form_nueva_sala').show();
	}
	function regresar_sala(){
		$('#form_editar_sala').hide();
		$('#form_nueva_sala').hide();
		$('#sala_select').show();
		$('#titulo_sala').show();
	}
	function editar_sala(){
		var data = new FormData();
		// icono
		var input = $('#icono_sala_edit');
		$.each(input[0].files, function(i, file) {
			data.append('file_icono-'+i, file);				
		});
		// letra
		var input = $('#letra_sala_edit');
		$.each(input[0].files, function(i, file) {
			data.append('file_letra-'+i, file);				
		});

		var id = $('#id_sala_editar').val();
		
		data.append("nombre_sala", $('#nombre_sala_editar').val());
		data.append("codigo_sala", $('#codigo_sala_editar').val());
		

		if($('#status_sala_editar').prop('checked')){
			// status=1;
			data.append("status",1);
		}else{
			// status=0;
			data.append("status",0);
		}

		$.ajax({
			type:'POST',
			url: "../public/?ruta=servicios/sala/editar&id="+id, 
			data: data,
			cache: false,
			contentType: false,
			processData: false,
			success: function(result){
				// console.log('EDITAR SALA',result);
				$('#lst_sala').val(result.sala.id);
				$('#sal_val_'+id).text(result.sala.nombre);
				$('#form_editar_sala').hide();
				$('#sala_select').show();

				$.ajax({
					type:'POST',
					url: "http://localhost/modulos/cc0/public/api/editar_sala", 
					data: {
						"nombre":result.sala.nombre,
						"idsala":result.sala.id
					},
					success: function(result){
						console.log('SALA CR EDIT',result);
					},error:function(err){
						console.log(err);
					}
				})
			},error:function(err){
				console.log(err);
			}
		});
	}
	function guardar_nueva_sala(){
		var data = new FormData();
		// icono
		var input = $('#icono_sala_nueva');
		$.each(input[0].files, function(i, file) {
			data.append('file_icono-'+i, file);				
		});
		// letra
		var input = $('#letra_sala_nueva');
		$.each(input[0].files, function(i, file) {
			data.append('file_letra-'+i, file);				
		});

		// var id = $('#id_sala_editar').val();
		
		data.append("nombre_sala", $('#nombre_sala_nueva').val());
		data.append("codigo_sala", $('#codigo_sala_nueva').val());
		 

		if($('#status_sala_nueva').prop('checked')){
			// status=1;
			data.append("status",1);
		}else{
			// status=0;
			data.append("status",0);
		}

		$.ajax({
			type:'POST',
			url: "../public/?ruta=servicios/sala/nueva", 
			data: data,
			cache: false,
			contentType: false,
			processData: false,
			success: function(result){
				console.log(result);
				$('#lst_sala').val(result.sala.id);
				// $('#sal_val_'+id).text(result.sala.nombre);
				// $('#form_editar_sala').hide();
				// $('#sala_select').show();

				// capturar el select
				var lst_sala = document.getElementById("lst_sala");
						
				// determinar cuantos options tiene el select
				var pos = lst_sala.length - 3 ;
				
				// crear el option nuevo: value y text
				var opt = document.createElement("option");
				opt.value = result.sala.id;
				opt.text = result.sala.nombre;
				opt.id = 'sal_val_'+result.sala.id;
				
				// agregar el option nuevo justo entes del último
				lst_sala.add(opt, pos);
				
				regresar_sala();
				
				// seleccionar la nueva opcion
				lst_sala.selectedIndex = pos;
				
				cambiarsala();

				$.ajax({
					type:'POST',
					url: "http://localhost/modulos/cc0/public/api/crear_sala", 
					data: {
						"nombre":result.sala.nombre,
						"idsala":result.sala.id
					},
					success: function(result){
						console.log('SALA CR',result);
					},
					error: function(err){
						console.log(err);
					}
				});

			},error:function(err){
				console.log(err);
			}
		});
	}
	function eliminar_sala(x){
		// var id = $('#id_sala_editar').val();
		var id = x;
		console.log(id);
		$.ajax({
			type:'DELETE',
			url: "../public/?ruta=servicios/sala/eliminar&id="+id, 
			data : {
				'_token': '{{ csrf_token() }}'
			},success: function(result){
				// console.log('ELIMINAR SALA',result);
				$('#lst_sala').val(0);
				$('#sal_val_'+id).hide();
				regresar_sala();
				cambiarsala();
				$('#confirmarEliminacion').modal('toggle');

				$.ajax({
					type:'POST',
					url: "http://localhost/modulos/cc0/public/api/delete_sala", 
					data: {
						"idsala":id
					},
					success: function(result){
						console.log('SALA CR DELETE',result);
					},error:function(err){
						console.log(err);
					}
				})

			},error:function(err){
				console.log(err);
			}
		});
	}
	function eliminar_sala_icono_editar(){
		var id = $('#id_sala_editar').val();
		$.ajax({
			type:'DELETE',
			url: "../public/?ruta=servicios/sala/eliminar_ico&id="+id, 
			data :'',
			success: function(result){
				console.log(result);
				$('#icono_sala_edit').val(null);
				$('#image-preview-icono_sala').css('background-image','url("")');
				sala_edit();
			},error:function(err){
				console.log(err);
			}
		});
	}
	function eliminar_sala_imagen_editar(){
		var id = $('#id_sala_editar').val();
		$.ajax({
			type:'DELETE',
			url: "../public/?ruta=servicios/sala/eliminar_img&id="+id, 
			data :'',
			success: function(result){
				console.log(result);
				$('#letra_sala_edit').val(null);
				$('#image-preview-letra_sala').css('background-image','url("")');
				sala_edit();
			},error:function(err){
				console.log(err);
			}
		});
	}
	function eliminar_sala_icono_nuevo(){
		$('#icono_sala_nueva').val(null);
		$('#image-preview-icono_sala_nueva').css('background-image','url("")');
		$('#image-preview-icono_sala_nueva').css('background','white');
	}
	function eliminar_sala_imagen_nuevo(){
		$('#letra_sala_nueva').val(null);
		$('#image-preview-letra_sala_nueva').css('background-image','url("")');
		$('#image-preview-letra_sala_nueva').css('background','white');
	}
	function verificar_licencias(){
		$('#verificar_licencias').attr("disabled", true);
		$('#verificar_licencias2').removeClass("hide");
		$.ajax({
			type:'POST',
			url: '../public/?ruta=verifica_licencia',   
			success: function(result){
				console.log(result);
              	
                $.each(result, function(i, rta1) {
                	$('#licencia_unica').html( 'Verificando licencia '+rta1.lic_whcm );
                	$.ajax({
						type:'POST',
						url: '../public/?ruta=config/licencia/validar',
						data: {"licencia":rta1.lic_whcm},
						success: function(result){
							console.log(result);
							if (result.status='Active'){
								$('#activa').removeClass('hide');
							}else{
								$('#inactive').removeClass('hide');
							}

							
						}
					});

                });
                 setTimeout('document.location.reload()',5000);
                
			},error:function(err){
				//location.reload();
			}
		});
		
	}
	
</script>
@stop