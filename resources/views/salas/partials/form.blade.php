<!-- Modal -->
<div class="modal fade" role="dialog" id="myModal">
    {!! Form::open(['url' => 'salas', 'method' => 'POST',
                'files' => true, 'id' => 'form-update']) !!}
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="title">Editar Sala</h4>
            </div>
            <div class="modal-body">
                <div class="form-group col-lg-4">
                    <div class="form-group image-add-ct">
                        {!! Form::label('Icono') !!}
                        <div  class="add-image" id="add-icon-item" style=""></div>
                        <div style="height: 5px"></div>
                        <div id="icon_container" >
                            {!! Form::button('Cargar', ['class' => 'btn btn-success']) !!}
                            <input id="btn-add-foto"  name="icono" class="inputFile" type="file" accept="image/*" base-sixty-four-input="" style="">
                            <input type="hidden" name="im_icono" id="im_icono">
                        </div>
                    </div>
                    <div class="ct-foto">
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="nombre">Nombre:</label>
                        <input type="text" name="nombre" id="nombre" class="form-control">
                    </div>
                    <div class="col-lg-6 form-group">
                        <label for="hsala" style="margin-right: 4%">Activo</label>
                        <input type="checkbox" value="1" name="status" id="status" class="icheckbox"/>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="nombre">Codigo:</label>
                        <input type="text" name="codigo" id="codigo" class="form-control">
                    </div>
                    
                </div>
                <div class="clearfix"></div>
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="btntitle">Editar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    {!! Form::close() !!}
</div>
<!-- /.modal -->

