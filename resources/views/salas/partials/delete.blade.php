<!-- Modal -->
<div class="modal fade" role="dialog" id="modalDelete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="title">Eliminar Sala</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => 'salas/'.$sala->id, 'method' => 'DELETE',
                'files' => true, 'id' => 'delete']) !!}
                <input type="hidden" name="sala_id" id="sala_id" value="">
                <p>¿Seguro que desea eliminar esta sala?</p>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="$('#delete').submit()" id="btntitle">Eliminar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
