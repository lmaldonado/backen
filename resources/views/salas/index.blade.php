@extends('dashboard.layout.base')

@section('title')
    Salas
@stop

@section('section')
    Salas
@stop

@section('breadcrumb')
    Listado de salas
@stop
@section('content')
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <button id="btn-add-sala" class="btn btn-info"><i class="ace-icon glyphicon glyphicon-plus"></i>Agregar</button> 
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <table class="table table-hover" id="tabla">
                    <thead>
                        <tr>
                          <th>ID</th>
                          <!--<th>ID_online</th>-->
                          <th></th>
                          <th>Sala</th>
                          <th>Estado</th>
                        <th></th>
                        </tr>
                    </thead>
                @foreach($salas as $sala)
                <tr>
                  <td width="5%">{{$sala->id}}</td>
                  <!--<td width="5%">{{$sala->id_online}}</td>-->
                  <td width="10%"><img src="{{url($sala->icono)}}" alt="{{$sala->icono}}" width="50%"></td>
                  <td>{{$sala->nombre}}</td>
                  <td width="50%"><span class="label @if($sala->status == 1) label-success @else label-danger @endif">@if($sala->status == 1) Activo @else Inactivo @endif</span></td>
                    <td>
                    <button data-info="{{json_encode($sala)}}" class="btn-edit-sala btn btn-xs btn-primary" title="Editar Sala">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <button type="button" data-info="{{ $sala->toJson() }}" class="btn-delete-sala btn btn-xs btn-danger" title="Eliminar Sala">
                        <i class="fa fa-trash"></i>
                    </button>
                    </td>
                </tr>
                @endforeach
              </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    @include("salas.partials.form")
    @include("salas.partials.delete")

@stop
@section('script')
<script>
$(document).ready(function(){
    /*$('.icheckbox').iCheck({
        checkboxClass: 'icheckbox_minimal',
    });*/

    var myTable = $("#tabla") .DataTable( {
        "dom": '<"top"i>rt<"row"<"col-xs-6"l><"col-xs-6"p>>',
        "length":   false,
        "info":     false,
        bAutoWidth: false,

        select: {
            style: 'multi'
        }
    } );

    $('#buscar').on( 'keyup', function () {
        myTable.search( this.value ).draw();
    } );

    $('#btn-add-sala').on('click', function(){
        $("#title").html("Crear Sala");
        $("#btntitle").html("Crear");
        $("#myModal").modal();
    });

    $('#btn-add-foto').on('change', function(evt){
        var files = evt.target.files; // FileList object
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e ) {
                    $('#add-icon-item').css('background-image', 'url('+e.target.result+')');
                };
            })(f );
            reader.readAsDataURL(f);
        }
    });

    $('.btn-edit-sala').on('click', function(){
        info  = $(this).data('info');

        if(info.status == "1"){
            $("#status").attr("checked","checked");
        }else{
            $("#status").removeAttr("checked");
        }
        $("#nombre").val(info.nombre);
        $("#codigo").val(info.codigo);
        $('#add-icon-item').css('background-image', "url({{url()}}/"+info.icono+")");
        $("#title").html("Editar Sala "+info.nombre);
        $("#btntitle").html("Editar");

        action = $('#form-update').attr('action');
        $('#form-update').attr('action', action+'/'+info.id);
        $("#myModal").modal();

    });

    $('.btn-delete-sala').on('click', function(){
        info = $(this).data('info');
        $("#sala_id").val(info.id);
        $("#nombre").val(info.nombre);
        $("#title").html("Eliminar Sala "+info.nombre);
        $("#btntitle").html("Eliminar");
        $("#modalDelete").modal();
    });
});

</script>
@stop