@extends('dashboard.layout.base')

@section('title')
    Salas / {{$sala->nombre}}
@stop

@section('section')
    Salas
@stop

@section('breadcrumb')
    {{$sala->nombre}}
@stop
@section('content')
    @include('servicios.form')
@stop
