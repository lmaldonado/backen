@extends('dashboard.login.base')

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="../../index2.html"><b>Neo</b>Sepelios</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Inicia sesión</p>
	
            {!! Form::open(array('method' => 'POST','route' => 'auth/login1')) !!}
                <!--<div class="form-group has-feedback">
                    <input type="text" name="username" class="form-control" placeholder="Usuario">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>-->
                <div class="form-group has-feedback">
                    <input id="contrasenna" type="password" name="password" class="form-control" placeholder="Contraseña">
                    <input id="contrasenna2" type="" name="password_new" placeholder="Contraseña" class="hide">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
@stop
@section('script')
<script type="text/javascript">
   


  $(document).ready(function () {
    $("#contrasenna").keyup(function () {
        var value = $(this).val();
        $("#contrasenna2").val( md5(value));
        
    });
});

    

    
</script>
@stop