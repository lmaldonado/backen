@extends('dashboard.login.base')

@section('content')
<?php 
    use App\User;
    $usuarios = User::all();

    $usuario = session()->get('key_login'); 

    if(isset($usuario['nombre'])){        
      echo '<script> location.href = "../public/?ruta=servicios"; </script>';
    }
?>
    <div class="login-box">
        <div class="login-logo">
            <b>neo</b>Sepelios
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <h4 class="login-box-msg">Inicia sesión</h4>
    
            {!! Form::open(array('method' => 'POST','url' => './?ruta=validacion')) !!}
                <div class="form-group has-feedback">
                    <input type="text" name="username" class="form-control" placeholder="Usuario">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Contraseña">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary ">Entrar</button>

                        @if(count($usuarios)==0)
                            <hr>
                            <p>Registrar primer usuario</p>
                            <a href="{{ url('/?ruta=usuario/nuevo') }}" class="btn btn-warning ">ir</a>
                             
                        @endif
                    </div>
                    <!-- /.col -->
                </div>
            </form>

        </div>
       
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
@stop