@extends('dashboard.layout.base')

@section('title')
    Usuarios
@stop

@section('section')
    Usuarios
@stop

@section('content')
<?php 
    $usuario = session()->get('key_login'); 

    if(!isset($usuario['nombre']) || $usuario['admin']!=1){        
      echo '<script> location.href = "../public/?ruta=login"; </script>';
    }
?>
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">&nbsp;<a  href="{{ url('/?ruta=usuario/nuevo') }}" id="agregar" style="cursor:pointer; cursor: hand" class="borrar btn btn-info" title="Nuevo Servicio"><i class="ace-icon glyphicon glyphicon-plus"></i>Nuevo </a></h3>
                 <span class="input-icon form-search nav-search pull-right" style="padding-top: 5px">
                    <input  id="buscar"  type="text" autocomplete="off" id="nav-search-input" style="width: 13em" class="nav-search-input" placeholder="Buscar ...">
                </span>
            </div>    
            <!-- /.box-header -->
            <div class="box-body">
            <table class="table table-hover" id="tabla">
                <thead>
                    <tr>                                                    
                      <th>Nombre</th>
                      <th>Usuario</th>
                      <th>Email</th>                          
                      <th>Admin</th>
                      <th class="col-xs-1"></th>
                    </tr>
                </thead>
              @foreach($usuarios as $value)                  
                <tr>
                    <td>{{$value->name}}</td>
                    <td>{{$value->username}}</td>
                    <td>{{$value->email}}</td>
                    <td>
                        @if($value->admin == 1)
                            Si
                        @else
                            No
                        @endif
                    </td>
                    <td class="text-center">
                        <a href="{{ url('/?ruta=usuario/editar&user='.$value->id) }}" class="btn btn-primary btn-xs" title="Editar"><i class="fa fa-pencil"></i></a>
                        <button class="btn btn-danger btn-xs" title="Eliminar" onclick="confirmar({{$value}})"><i class="fa fa-trash"></i></button>
                    </td>                        
                </tr>                    
              @endforeach
          </table>
            </div>
        </div>
    </div>
        <!--Eliminar servicio-->
<div class="modal fade" role="dialog" id="modelEliminar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Eliminar Usuario</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => './?ruta=usuario/eliminar', 'method' => 'DELETE',
                'files' => true, 'id' => 'delete']) !!} 
                <input type="hidden" name="user_id" id="user_id" >
                <p>¿Seguro que desea eliminar a <strong id="user_name"></strong>?</p>

                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="btntitle">Eliminar</button>
                {!! Form::close() !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
        
@stop
@section('script')


   
    <script>
        function confirmar(x){
            
            $('#user_id').val(x.id);
            $('#user_name').text(x.name);
            $('#modelEliminar').modal('toggle');
            
        }
        function confirmarEliminarServicio(){
            var id = $('#servicio_id_eliminar').val();
            // console.log(id);
            $.ajax({
                type:'DELETE',
                url: "../public/?ruta=servicios/delete&id="+id, 
                data :{
                    '_token': '{{ csrf_token() }}'				
                },
                success: function(result){
                    // console.log(result);
                    $('#modelEliminar').modal('toggle');
                    location.reload();
                    
                },error:function(err){
                    console.log(err);
                }
            });
        }
        var myTable = $("#tabla") .DataTable( {
            "dom": '<"top"i>rt<"row"<"col-xs-6"l><"col-xs-6"p>>',
            "length":   false,
            "info":     false,
            "bSort": true,
            "aaSorting":[[1,'desc']],
            bAutoWidth: false,
            select: {
                style: 'multi'
            }
        } );
    
        $('#buscar').on( 'keyup', function () {
            myTable.search( this.value ).draw();
        } );

    $(document).ready(function(){
        
    });
    </script>
@stop