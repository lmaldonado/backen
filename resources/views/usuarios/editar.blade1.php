@extends('dashboard.layout.base')

@section('title')
    Editar Usuarios
@stop

@section('section')
    Editar Usuarios
@stop

@section('content')
<?php 
    $usuario = session()->get('key_login'); 

    if(!isset($usuario['nombre']) || $usuario['admin']!=1){        
      echo '<script> location.href = "../public/?ruta=login"; </script>';
    }
?>
    <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
        <div class="box box-primary">
            <div class="box-header ">
                <h3 class="box-title">Actualizar datos</h3>
                 
            </div>    
            <!-- /.box-header -->
            <div class="box-body">
                <form action="{{ '../public/?ruta=usuario/actualizar' }}" method="POST" class="form-horizontal">
                  <div class="form-group">
                    <label for="nombre" class="col-sm-3 control-label">Nombre:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="nombre" id="nombre" value="{{$user->name}}" placeholder="Escribir nombre">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="usuario" class="col-sm-3 control-label">Usuario:</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="usuario" id="usuario" value="{{$user->username}}" placeholder="Escribir nombre de usuario">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email:</label>
                    <div class="col-sm-9">
                      <input type="email" class="form-control" name="email" id="email" value="{{$user->email}}" placeholder="Escribir email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="password" class="col-sm-3 control-label">Contraseña:</label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control" minlength="3" name="password" id="password" placeholder="Escribir constraseña">
                    </div>
                  </div>
                  <input type="hidden" name="id" value="{{$user->id}}">
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                      <div class="checkbox">
                        <label>
                          @if($user->admin==1)
                            <input name="admin" checked type="checkbox"> Administrador
                          @else
                            <input name="admin" type="checkbox"> Administrador
                          @endif
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                      <button type="submit" class="btn btn-primary">Guardar</button>
                      <a href="{{ url('/?ruta=usuarios') }}" id="agregar" style="cursor:pointer; cursor: hand" class="btn btn-danger">Cancelar</a>
                    </div>
                  </div>
                </form>
            </div>
        </div>
    </div>
        
@stop
