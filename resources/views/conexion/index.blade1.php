@extends('dashboard.layout.base')

@section('title')
	Configuracion de Conexion
@stop

@section('section')
	Configuracion
@stop

@section('breadcrumb')
	de conexion
@stop
@section('content')
<?php 
    $usuario = session()->get('key_login'); 

    if(!isset($usuario['nombre']) || $usuario['admin']!=1){        
      echo '<script> location.href = "../public/?ruta=login"; </script>';
    }
?>

{!! Form::open(['url' => 'config/ch', 'method' => 'POST', 'files' => true]) !!}

<div class="content">
	<div class="row">
		<div class="col-lg-6">
			<h4>Datos de configuracion de conexion</h4>
		</div>
		<div class="col-lg-6 ">
			<div class="form-group text-right">
				{{-- {!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!} --}}
				{{-- <a href="{{ url('/servicios') }}" class="btn btn-danger">Cancelar</a> --}}
			</div>
		</div>
	</div>
	<div class="row">
        {{$cv_act=false}}
        {{$ind_act=false}}
        @foreach($licencias as $li)
            @if($li->product_id==10 && $li->status=='Active' && $li->descarga==1 || $li->product_id==6 && $li->status=='Active' && $li->descarga==1 || $li->product_id==5 && $li->status=='Active' && $li->descarga==1 || $li->product_id==9 && $li->status=='Active' && $li->descarga==1 || $li->product_id==33 && $li->status=='Active' && $li->descarga==1 || $li->product_id==32 && $li->status=='Active' && $li->descarga==1 || $li->product_id==13 && $li->status=='Active' && $li->descarga==1)
                <span class="hide">{{$ind_act=true}}</span>
            @endif
            @if($li->product_id==35 && $li->status=='Active' && $li->descarga==1 || $li->product_id==7 && $li->status=='Active' && $li->descarga==1)
                <span class="hide">{{$cv_act=true}}</span>
            @endif
        @endforeach
        @if($ind_act==true)
		<div class="col-xs-12 col-md-6 col-lg-4">		
			<!-- INICIO SERVIDOR TABLES -->            
			<div class="box box-primary">
			    <div class="box-header">
			        <h3 class="box-title">Servidor</h3>
			    </div>
			    <div class="box-body">
			        <div class="row">
			            <div class="form-group col-md-12">
			                <label for="tipo_servidor">Tipo Servidor</label>
			                <div class="input-group ">
			                    
			                    
			                    <select name="tipo_servidor" id="tipo_servidor" class="form-control" onchange="cambiarTipoServidor()">
			                        @if($ch->tipo_servidor=='local')
			                            <option value="propio" >Servidor Propio</option>
			                            <option value="local" selected>Servidor Local</option>
			                            <option value="online">Servidor Remoto</option>
                                        <option value="web" >Homenajes online</option>
			                        @elseif($ch->tipo_servidor=='online')
			                            <option value="propio" >Servidor Propio</option>
			                            <option value="local" >Servidor Local</option>
                                        <option value="online" selected>Servidor Remoto</option>
			                            <option value="web" >Homenajes online</option>
			                        @elseif($ch->tipo_servidor=='web')
			                            <option value="propio" >Servidor Propio</option>
			                            <option value="local" >Servidor Local</option>
			                            <option value="online">Servidor Remoto</option>
                                        <option value="web" selected>Homenajes online</option>
			                        @else
                                        <option value="propio" >Servidor Propio</option>
                                        <option value="local" >Servidor Local</option>
                                        <option value="online">Servidor Remoto</option>
                                        <option value="web" >Homenajes online</option>
                                    @endif
			                    </select>
			                    <span class="input-group-btn" >
			                        <button type="button" title="Guardar servidor" id="btn_guardar_sala_propio" class="btn btn-primary btn-flat" onclick="guardarSala('propio')"><i class="fa fa-save"></i></button>
			                        <button type="button" id="btn_disable" class="btn btn-default btn-flat" disabled><i class="fa fa-server"></i></button>
			                    </span>
			                </div>
			            </div>

                        <div class="form-group col-md-12" id="tipo_serv_web">
                            <label for="servidor_web">Dirección Web</label>
                            <div class="input-group ">
                                <input type="text" class="form-control" id="servidor_web" name="servidor_web" value="{{$ch->servidor_web}}" placeholder="Ej. http://dominio.com/api/" title="Ej. http://dominio.com/api/">
                                <span class="input-group-btn">
                                    <button type="button" title="Buscar salas" class="btn btn-default btn-flat" onclick="buscarSala('web')"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="from-group col-md-12" id="sel_sala_serv_web">
                            <label for="sala_servidor_web">Salas Web</label>
                            <div class="input-group">
                                <select name="sala_servidor_web" id="sala_servidor_web" class="form-control" onchange="">
                                    
                                </select>
                                <span class="input-group-btn">
                                    <button type="button" title="Guardar servidor" id="btn_guardar_sala_web" class="btn btn-primary btn-flat" onclick="guardarSala('web')"><i class="fa fa-save" ></i></button>
                                </span>
                            </div>
                            <span id="msj_error" style="color: red;"></span>
                        </div>
			            
			            <div class="form-group col-md-12" id="tipo_serv_local">
			                <label for="servidor_ip">Dirección IP</label>
			                <div class="input-group ">
			                    <input type="text" class="form-control" id="servidor_ip" name="servidor_ip" value="{{$ch->servidor_ip}}" placeholder="Ej. 192.168.10.100" title="Ej. 192.168.10.100">
			                    <span class="input-group-btn">
			                        <button type="button" title="Buscar salas" class="btn btn-default btn-flat" onclick="buscarSala('local')"><i class="fa fa-search"></i></button>
			                        <!-- <button type="button" title="Guardar servidor" id="btn_guardar_sala_local" class="btn btn-primary btn-flat" onclick="guardarSala('local')"><i class="fa fa-save"></i></button> -->
			                    </span>
			                </div>
			            </div>
			            <div class="from-group col-md-12" id="sel_sala_serv_ip">
			                <label for="sala_servidor_ip">Sala Local</label>
			                <div class="input-group">
			                    <select name="sala_servidor_ip" id="sala_servidor_ip" class="form-control" onchange="">
			                        
			                    </select>
			                    <span class="input-group-btn">
			                        <button type="button" title="Guardar servidor" id="btn_guardar_sala_local" class="btn btn-primary btn-flat" onclick="guardarSala('local')"><i class="fa fa-save"></i></button>
			                    </span>
			                </div>
			                    
			            </div>
			            
			            <div class="form-group col-md-12" id="tipo_serv_online">
			                <label for="servidor_url">Dirección URL</label>
			                <div class="input-group ">
			                    <input type="text" class="form-control" id="servidor_url" name="servidor_url" value="{{$ch->servidor_url}}" placeholder="Ej. http://dominio.com/api/" title="Ej. http://dominio.com/api/">
			                    <span class="input-group-btn">
			                        <button type="button" title="Buscar salas" class="btn btn-default btn-flat" onclick="buscarSala('online')"><i class="fa fa-search"></i></button>
			                        <!-- <button type="button" title="Guardar servidor" id="btn_guardar_sala_online" class="btn btn-primary btn-flat" onclick="guardarSala('online')"><i class="fa fa-save"></i></button> -->
			                    </span>
			                </div>
			            </div>
			            <div class="form-group col-md-12" id="sel_sala_serv_online">
			                <label for="sala_servidor_online">Sala Online</label>
			                <div class="input-group">
			                    <select name="sala_servidor_online" id="sala_servidor_online" class="form-control" onchange="">
			                        
			                    </select>
			                    <span class="input-group-btn">
			                        <button type="button" title="Guardar servidor" id="btn_guardar_sala_online" class="btn btn-primary btn-flat" onclick="guardarSala('online')"><i class="fa fa-save"></i></button>
			                    </span>
			                </div>
			                    
			            </div>
			        </div>
			    </div>
			</div>
			<!-- FIN SERVIDOR -->
		</div>
        @endif
        @if($cv_act==true)
		<div class="col-xs-12 col-md-6 col-lg-4">
			<!-- INICIO SERVIDOR CV_N -->            
			<div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Servidor CV </h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="tipo_servidor">Tipo Servidor</label>
                            <div class="input-group ">
                                
                                <select name="tipo_servidorCVN" id="tipo_servidorCVN" class="form-control" onchange="cambiarTipoServidorCV()">
                                    @if($cv->tipo_servidor=='local')
                                        <option value="propio" >Servidor Propio</option>
                                        <option value="local" selected>Servidor Local</option>
                                        <option value="online">Servidor Remoto</option>
                                        <option value="web">Homenajes online</option>
                                    @elseif($cv->tipo_servidor=='online')
                                        <option value="propio" >Servidor Propio</option>
                                        <option value="local" >Servidor Local</option>
                                        <option value="online" selected>Servidor Remoto</option>
                                        <option value="web">Homenajes online</option>
                                    @elseif($cv->tipo_servidor=='web')
                                        <option value="propio" >Servidor Propio</option>
                                        <option value="local" >Servidor Local</option>
                                        <option value="online" >Servidor Remoto</option>
                                        <option value="web" selected>Homenajes online</option>
                                    @else
                                        <option value="propio" >Servidor Propio</option>
                                        <option value="local" >Servidor Local</option>
                                        <option value="online">Servidor Remoto</option>
                                        <option value="web">Homenajes online</option>
                                    @endif
                                </select>
                                <span class="input-group-btn" >
                                    <button type="button" title="Guardar servidor" id="btn_guardar_sala_propioCVN" class="btn btn-primary btn-flat" onclick="guardarSalaCV('propio')"><i class="fa fa-save"></i></button>
                                    <button type="button" id="btn_disableCVN" class="btn btn-default btn-flat" disabled><i class="fa fa-server"></i></button>
                                    
                                </span>
                            </div>
                        </div>

                        <div class="from-group col-md-12" id="sel_sala_serv_propioCVN">
                            <label for="sala_servidor_propio">Salas Propia</label>   
                            <div id="sala_servidor_propioCVN">
                                
                            </div>                                 
                        </div>
                        <div class="form-group col-md-12" id="tipo_serv_webCVN">
                            <label for="servidor_webCVN">Dirección Web</label>
                            <div class="input-group ">
                                <input type="text" class="form-control" id="servidor_webCVN" name="servidor_webCVN" value="{{$cv->servidor_web}}" placeholder="Ej. http://dominio.com/api/" title="Ej. http://dominio.com/api/">
                                <span class="input-group-btn">
                                    <button type="button" title="Buscar salas" class="btn btn-default btn-flat" onclick="buscarSalaCV('web')"><i class="fa fa-search"></i></button>
                                    <button type="button" title="Guardar servidor" id="btn_guardar_sala_webCVN" class="btn btn-primary btn-flat" onclick="guardarSalaCV('web')"><i class="fa fa-save"></i></button>
                                </span>
                            </div>
                            <span id="msj_errorCV" style="color: red;"></span>
                        </div>
                        <div class="from-group col-md-12" id="sel_sala_serv_webCVN">
                            <label for="sala_servidor_webCVN">Salas Web</label>

                                <div id="sala_servidor_webCVN">
                                    
                                </div>                            
                                                            
                        </div>
                        <div class="form-group col-md-12" id="tipo_serv_localCVN">
                            <label for="servidor_ipCVN">Dirección IP</label>
                            <div class="input-group ">
                                <input type="text" class="form-control" id="servidor_ipCVN" name="servidor_ipCVN" value="{{$cv->servidor_ip}}" placeholder="Ej. 192.168.10.100" title="Ej. 192.168.10.100">
                                <span class="input-group-btn">
                                    <button type="button" title="Buscar salas" class="btn btn-default btn-flat" onclick="buscarSalaCV('local')"><i class="fa fa-search"></i></button>
                                    <button type="button" title="Guardar servidor" id="btn_guardar_sala_localCVN" class="btn btn-primary btn-flat" onclick="guardarSalaCV('local')"><i class="fa fa-save"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="from-group col-md-12" id="sel_sala_serv_ipCVN">
                            <label for="sala_servidor_ipCVN">Salas Local</label>

                                <div id="sala_servidor_ipCVN">
                                    
                                </div>                            
                                                            
                        </div>
                        
                        <div class="form-group col-md-12" id="tipo_serv_onlineCVN">
                            <label for="servidor_urlCVN">Dirección URL</label>
                            <div class="input-group ">
                                <input type="text" class="form-control" id="servidor_urlCVN" name="servidor_urlCVN" value="{{$cv->servidor_url}}" placeholder="Ej. http://dominio.com/api/" title="Ej. http://dominio.com/api/">
                                <span class="input-group-btn">
                                    <button type="button" title="Buscar salas" class="btn btn-default btn-flat" onclick="buscarSalaCV('online')"><i class="fa fa-search"></i></button>
                                    <button type="button" title="Guardar servidor" id="btn_guardar_sala_onlineCVN" class="btn btn-primary btn-flat" onclick="guardarSalaCV('online')"><i class="fa fa-save"></i></button>
                                </span>
                            </div>
                        </div>
                        <div class="form-group col-md-12" id="sel_sala_serv_onlineCVN">
                            <label for="sala_servidor_online">Salas Online</label>
                            <div id="sala_servidor_onlineCVN">
                                    
                            </div>  
                                
                        </div>
                    </div>
                </div>
            </div>
			<!-- FIN SERVIDOR -->
		</div>
        @endif
	</div>
</div>

@stop



@section('script')

<script type="text/javascript">
	// SERVIDORES  
    function cambiarTipoServidor(){
        var tipo = $('#tipo_servidor').val();
        $('#btn_guardar_sala_local').hide();
        $('#btn_guardar_sala_online').hide();
        $('#btn_guardar_sala_propio').hide();
        $('#btn_guardar_sala_web').hide();
        $('#btn_disable').hide();
        
        // console.log(tipo);
        if(tipo == 'online'){
            $('#tipo_serv_local').hide();
            $('#tipo_serv_web').hide();
            $('#sel_sala_serv_web').hide();
            $('#sel_sala_serv_ip').hide();
            $('#tipo_serv_online').show();
            // $('#sel_sala_serv_online').show();
            $('#btn_disable').show();
            $('#sala_servidor_ip option').each(function(){
                $(this).remove();
            });
            $('#sala_servidor_web option').each(function(){
                $(this).remove();
            });
        }else{ 
            if(tipo == 'local'){
                $('#tipo_serv_online').hide();
                $('#tipo_serv_web').hide();
                $('#sel_sala_serv_web').hide();
                $('#sel_sala_serv_online').hide();
                $('#tipo_serv_local').show();
                // $('#sel_sala_serv_ip').show(); 
                $('#btn_disable').show();
                $('#sala_servidor_online option').each(function(){
                    $(this).remove();
                });
                $('#sala_servidor_web option').each(function(){
                    $(this).remove();
                });
            }else{
                if(tipo=='web'){
                    
                    $('#tipo_serv_local').hide();
                    $('#sel_sala_serv_ip').hide();
                    $('#sel_sala_serv_online').hide();
                    $('#tipo_serv_online').hide();
                    $('#tipo_serv_web').show();
                    $('#btn_disable').show();
                    $('#sala_servidor_ip option').each(function(){
                        $(this).remove();
                    });
                    $('#sala_servidor_online option').each(function(){
                        $(this).remove();
                    });
                }else{
                    $('#tipo_serv_online').hide();
                    $('#tipo_serv_local').hide();
                    $('#tipo_serv_web').hide();
                    $('#sel_sala_serv_online').hide();
                    $('#sel_sala_serv_ip').hide(); 
                    $('#sel_sala_serv_web').hide(); 
                    $('#btn_guardar_sala_propio').show();
                    $('#sala_servidor_ip option').each(function(){
                        $(this).remove();
                    });
                    $('#sala_servidor_online option').each(function(){
                        $(this).remove();
                    });
                    $('#sala_servidor_web option').each(function(){
                        $(this).remove();
                    });
                }                    
            }
        }
    }
    function buscarSala(opc){
        $('#sel_sala_serv_online').hide();
        $('#sel_sala_serv_ip').hide();
        $('#sel_sala_serv_web').hide();
        $('#btn_guardar_sala_local').hide();
        $('#btn_guardar_sala_online').hide();
        $('#btn_guardar_sala_web').hide();
        $('#sala_servidor_ip option').each(function(){
            $(this).remove();
        });
        $('#sala_servidor_online option').each(function(){
            $(this).remove();
        });
        $('#sala_servidor_web option').each(function(){
            $(this).remove();
        });
        if(opc == 'local'){
            var ip = 'http://'+$('#servidor_ip').val()+'/cartelera_backend_laravel/public/?ruta=api/salas';
            var tipo = 'GET';
            var dato = {'_token': '{{ csrf_token() }}'};
        }else{
            if(opc == 'online'){
                var ip = $('#servidor_url').val()+'cartelera_backend_laravel/public/?ruta=api/salas';
                var tipo = 'GET';
                var dato = {'_token': '{{ csrf_token() }}'};
            }else{
                var ip = $('#servidor_web').val()+'wp-content/plugins/neo_homenajes/json/conector.php';
                var tipo = 'POST';
                var dato = {'salones': ''};
            }
        }        
        $.ajax({
            type: tipo,
            url: ip, 
            data: dato,
            success: function(result){
                
                // console.log(result);
                if(opc != 'web'){

                    for( var i=0; i<result.salas.length; i++){
                        if(opc == 'local'){
                            // capturar el select
                            var lst_sala = document.getElementById("sala_servidor_ip");
                            if(result){
                                $('#btn_guardar_sala_local').show();
                                $('#btn_guardar_sala_online').hide();
                                $('#btn_guardar_sala_web').hide();
                                $('#sel_sala_serv_ip').show();
                            }                        
                        }else{
                            if(opc == 'online'){
                                // capturar el select
                                var lst_sala = document.getElementById("sala_servidor_online");
                                if(result){
                                    $('#btn_guardar_sala_local').hide();
                                    $('#btn_guardar_sala_web').hide();
                                    $('#btn_guardar_sala_online').show();
                                    $('#sel_sala_serv_online').show();
                                }                                
                            }
                        }
                        var pos = lst_sala.length  ;
                        
                        // crear el option nuevo: value y text
                        var opt = document.createElement("option");
                        
                        opt.value = result.salas[i].id;
                        opt.text = result.salas[i].nombre;    

                        // agregar el option nuevo justo entes del último
                        lst_sala.add(opt, pos);
                        
                    }   
                }else{
                    result = JSON.parse(result);
                    result = result.resultado;
                    console.log(result);
                    for( var i=0; i<result.length; i++){
                        var lst_sala = document.getElementById("sala_servidor_web");
                        if(result){
                            $('#btn_guardar_sala_local').hide();
                            $('#btn_guardar_sala_online').hide();
                            $('#btn_guardar_sala_web').show();
                            $('#sel_sala_serv_web').show();
                        } 
                           
                        var pos = lst_sala.length  ;
                        
                        // crear el option nuevo: value y text
                        var opt = document.createElement("option");
                        
                        opt.value = result[i].id;
                        opt.text = result[i].nombre;                     
                        
                        // agregar el option nuevo justo entes del último
                        lst_sala.add(opt, pos);
                        
                    }   
                }
                                       
                
            },error:function(err){
                console.log(err);
            }
        });
    }
    function guardarSala(opc){
        if(opc == 'local'){
            var url = $('#servidor_ip').val();
            var sala = $('#sala_servidor_ip').val();
            // console.log(url,sala);
        }else{
            if(opc == 'online'){
                var url = $('#servidor_url').val();
                var sala = $('#sala_servidor_online').val();
                // console.log(url,sala);
            }else{
                if(opc == 'web'){
                    var url = $('#servidor_web').val();
                    var sala = $('#sala_servidor_web').val();
                }else{
                    // console.log(opc);
                    var sala = 1;
                    var url = ''
                }                    
            }
        }
        // console.log(sala,url);
        if(opc == 'web'){
            console.log('PASO WEB');
            $('#btn_guardar_sala_web i').remove();
            $('#btn_guardar_sala_web').append('<i class="fa fa-spinner fa-spin" style="font-size:18px; margin: -3px;"></i>');            
            $.ajax({
                type:'GET',
                url: "../public/?ruta=config/ch/sync/servicios&url="+url, 
                data :'',
                success: function(result){
                    $.ajax({
                        type:'POST',
                        url: "../public/?ruta=api/config/ch/sala", 
                        data :{
                            '_token': '{{ csrf_token() }}',
                            'tipo': opc,
                            'url': url,
                            'sala': sala
                        },
                        success: function(result){
                            console.log(result); 
                            location.reload();            
                        },error:function(err){
                            console.log(err);
                            $('#btn_guardar_sala_web i').remove();
                            $('#btn_guardar_sala_web').append('<i class="fa fa-save" ></i>'); 
                            $('#msj_error').text('Error de sincronización');
                        }
                    });              
                },error:function(err){
                    // console.log('ERROR DE SINCRONIZACION',err);
                    // $('#btn_guardar_sala_web i').remove();
                    // $('#btn_guardar_sala_web').append('<i class="fa fa-save" ></i>'); 
                    // $('#msj_error').text('Error de sincronización');
                    $.ajax({
                        type:'POST',
                        url: "../public/?ruta=api/config/ch/sala", 
                        data :{
                            '_token': '{{ csrf_token() }}',
                            'tipo': opc,
                            'url': url,
                            'sala': sala
                        },
                        success: function(result){
                            console.log(result); 
                            location.reload();            
                        },error:function(err){
                            console.log(err);
                            $('#btn_guardar_sala_web i').remove();
                            $('#btn_guardar_sala_web').append('<i class="fa fa-save" ></i>'); 
                            $('#msj_error').text('Error de sincronización');
                        }
                    }); 
                }
            }); 
        }else{
            $.ajax({
                type:'POST',
                url: "../public/?ruta=api/config/ch/sala", 
                data :{
                    '_token': '{{ csrf_token() }}',
                    'tipo': opc,
                    'url': url,
                    'sala': sala
                },
                success: function(result){
                    console.log(result); 
                    location.reload();            
                },error:function(err){
                    console.log(err);
                }
            });
        }
                
           
    }
    function buscarSalaPrimera(){
        $('#sel_sala_serv_online').hide();
        $('#sel_sala_serv_ip').hide();
        $('#sel_sala_serv_web').hide();
        $('#btn_guardar_sala_web').hide();
        $('#btn_guardar_sala_local').hide();
        $('#btn_guardar_sala_online').hide();
        $('#sala_servidor_ip option').each(function(){
            $(this).remove();
        });
        $('#sala_servidor_online option').each(function(){
            $(this).remove();
        });
        $('#sala_servidor_web option').each(function(){
            $(this).remove();
        });
        var tipo_serv = '{{$ch->tipo_servidor}}';
        var sala_id = '{{$ch->sala_id}}';
        // console.log(tipo_serv,sala_id);
        if(tipo_serv != 'propio'){
            if(tipo_serv == 'local'){
                var ip = 'http://'+$('#servidor_ip').val()+'/cartelera_backend_laravel/public/?ruta=api/salas';
                var tipo = 'GET';
                var dato = {'_token': '{{ csrf_token() }}'};
            }else{
                if(tipo_serv == 'online'){
                    var ip = $('#servidor_url').val()+'cartelera_backend_laravel/public/?ruta=api/salas';
                    var tipo = 'GET';
                    var dato = {'_token': '{{ csrf_token() }}'};
                }else{
                    var ip = $('#servidor_web').val()+'wp-content/plugins/neo_homenajes/json/conector.php';
                    var tipo = 'POST';
                    var dato = {'salones': ''};
                }
            }        
            $.ajax({
                type: tipo,
                url: ip, 
                data: dato,
                success: function(result){
                    // console.log(result);
                    if(tipo_serv != 'web'){
                        for( var i=0; i<result.salas.length; i++){
                            if(tipo_serv == 'local'){
                                // capturar el select
                                var lst_sala = document.getElementById("sala_servidor_ip");
                                if(result){
                                    $('#btn_guardar_sala_local').show();
                                    $('#btn_guardar_sala_online').hide();
                                    $('#sel_sala_serv_ip').show();
                                }  
                            }else{
                                if(tipo_serv == 'online'){
                                    // capturar el select
                                    var lst_sala = document.getElementById("sala_servidor_online");
                                    if(result){
                                        $('#btn_guardar_sala_local').hide();
                                        $('#btn_guardar_sala_online').show();
                                        $('#sel_sala_serv_online').show();
                                    }  
                                }
                            }  
                            // capturar el select
                            // var lst_sala = document.getElementById("sala_servidor_ip");
                                    
                            // determinar cuantos options tiene el select
                            var pos = lst_sala.length  ;
                            
                            // crear el option nuevo: value y text
                            var opt = document.createElement("option");
                            opt.value = result.salas[i].id;
                            opt.text = result.salas[i].nombre;
                            // opt.id = 'sal_val_'+result.sala.id;

                            if(sala_id == result.salas[i].id){
                                // capturar el select
                                opt.selected='selected';
                            }
                            
                            // agregar el option nuevo justo entes del último
                            lst_sala.add(opt, pos);
                            
                            // regresar_sala();
                            
                            // seleccionar la nueva opcion
                            // lst_sala.selectedIndex = pos;
                        } 
                    }else{
                        result = JSON.parse(result);
                        result = result.resultado;
                        for( var i=0; i<result.length; i++){
                            
                            // capturar el select
                            var lst_sala = document.getElementById("sala_servidor_web");
                            if(result){
                                $('#btn_guardar_sala_local').hide();
                                $('#btn_guardar_sala_online').hide();
                                $('#btn_guardar_sala_web').show();
                                $('#sel_sala_serv_web').show();
                            } 
                            
                            // capturar el select
                            // var lst_sala = document.getElementById("sala_servidor_ip");
                                    
                            // determinar cuantos options tiene el select
                            var pos = lst_sala.length  ;
                            
                            // crear el option nuevo: value y text
                            var opt = document.createElement("option");
                            opt.value = result[i].id;
                            opt.text = result[i].nombre;
                            // opt.id = 'sal_val_'+result.sala.id;

                            if(sala_id == result[i].id){
                                // capturar el select
                                opt.selected='selected';
                            }
                            
                            // agregar el option nuevo justo entes del último
                            lst_sala.add(opt, pos);
                            
                            // regresar_sala();
                            
                            // seleccionar la nueva opcion
                            // lst_sala.selectedIndex = pos;
                        } 
                    }
                                             
                    
                },error:function(err){
                    console.log(err);
                }
            });
        }
        
    }

    function cambiarTipoServidorCV(){
        var tipo = $('#tipo_servidorCVN').val();
        $('#btn_guardar_sala_localCVN').hide();
        $('#btn_guardar_sala_onlineCVN').hide();
        $('#btn_guardar_sala_propioCVN').hide();
        $('#btn_guardar_sala_webCVN').hide();
        $('#sel_sala_serv_propioCVN').hide();
        $('#btn_disableCVN').hide();
        // console.log(tipo);
        if(tipo == 'online'){
            $('#tipo_serv_localCVN').hide();
            $('#tipo_serv_webCVN').hide();
            $('#sel_sala_serv_webCVN').hide();
            $('#sel_sala_serv_propioCVN').hide();
            $('#sel_sala_serv_ipCVN').hide();
            
            $('#tipo_serv_onlineCVN').show();
            // $('#sel_sala_serv_onlineCVN').show();
            $('#btn_disableCVN').show();
            $('#sala_servidor_ipCVN .checkbox').each(function(){
                $(this).remove();
            });
            $('#sala_servidor_webCVN .checkbox').each(function(){
                $(this).remove();
            });
        }else{ 
            if(tipo == 'local'){
                $('#tipo_serv_onlineCVN').hide();
                $('#sel_sala_serv_propioCVN').hide();
                $('#sel_sala_serv_onlineCVN').hide();
                $('#tipo_serv_webCVN').hide();
                $('#sel_sala_serv_webCVN').hide();
                $('#tipo_serv_localCVN').show();
                // $('#sel_sala_serv_ipCVN').show(); 
                $('#btn_disableCVN').show();
                $('#sala_servidor_onlineCVN .checkbox').each(function(){
                    $(this).remove();
                });
                $('#sala_servidor_webCVN .checkbox').each(function(){
                    $(this).remove();
                });
            }else{
                if(tipo == 'web'){
                    $('#tipo_serv_onlineCVN').hide();
                    $('#sel_sala_serv_propioCVN').hide();
                    $('#sel_sala_serv_onlineCVN').hide();
                    $('#tipo_serv_webCVN').hide();
                    $('#sel_sala_serv_webCVN').hide();
                    $('#tipo_serv_localCVN').hide();
                    $('#tipo_serv_webCVN').show();
                    // $('#sel_sala_serv_ipCVN').show(); 
                    $('#btn_disableCVN').show();
                    $('#sala_servidor_onlineCVN .checkbox').each(function(){
                        $(this).remove();
                    });
                    $('#sala_servidor_ipCVN .checkbox').each(function(){
                        $(this).remove();
                    });
                }else{
                    $('#tipo_serv_onlineCVN').hide();
                    $('#tipo_serv_localCVN').hide();
                    $('#sel_sala_serv_onlineCVN').hide();
                    $('#sel_sala_serv_ipCVN').hide(); 
                    $('#tipo_serv_webCVN').hide();
                    $('#sel_sala_serv_webCVN').hide();
                    $('#btn_guardar_sala_propioCVN').show();
                    $('#sel_sala_serv_propioCVN').show();
                    $('#sala_servidor_ipCVN .checkbox').each(function(){
                        $(this).remove();
                    });
                    $('#sala_servidor_onlineCVN .checkbox').each(function(){
                        $(this).remove();
                    });
                    $('#sala_servidor_webCVN .checkbox').each(function(){
                        $(this).remove();
                    });
                    var tipo_serv = '{{$cv->tipo_servidor}}';
                        
                    if(tipo_serv!='propio'){
                        $('#sala_servidor_propioCVN .checkbox').each(function(){
                            $(this).remove();
                        });
                        buscarSalaCV('propio');
                    }
                }
                   
                
            }
        }
    }
	function buscarSalaCV(opc){
        $('#sel_sala_serv_onlineCVN').hide();
        $('#sel_sala_serv_ipCVN').hide();
        $('#sel_sala_serv_webCVN').hide();
        $('#btn_guardar_sala_webCVN').hide();
        $('#btn_guardar_sala_localCVN').hide();
        $('#btn_guardar_sala_onlineCVN').hide();
        $('#sala_servidor_ipCVN .checkbox').each(function(){
            $(this).remove();
        });
        $('#sala_servidor_onlineCVN .checkbox').each(function(){
            $(this).remove();
        });
        $('#sala_servidor_webCVN .checkbox').each(function(){
            $(this).remove();
        });
        if(opc == 'local'){
            var ip = 'http://'+$('#servidor_ipCVN').val()+'/cartelera_backend_laravel/public/?ruta=api/salas';
            var tipo = 'GET';
            var dato = {'_token': '{{ csrf_token() }}'};
        }else{
            if(opc == 'online'){
                var ip = $('#servidor_urlCVN').val()+'cartelera_backend_laravel/public/?ruta=api/salas';
                var tipo = 'GET';
                var dato = {'_token': '{{ csrf_token() }}'};
            }else{
                if(opc == 'web'){
                    var ip = $('#servidor_web').val()+'wp-content/plugins/neo_homenajes/json/conector.php';
                    var tipo = 'POST';
                    var dato = {'salones': ''};
                }else{
                    var ip = '../public/?ruta=api/salas';
                    var tipo = 'GET';
                    var dato = {'_token': '{{ csrf_token() }}'};
                }                
            }
        }        
        $.ajax({
            type: tipo,
            url: ip, 
            data: dato,
            success: function(result){
                // console.log(result);
                var servidor;
                if(opc != 'web'){
                    for( var i=0; i<result.salas.length; i++){
                        if(opc == 'local'){
                            servidor = $("#sala_servidor_ipCVN");
                            if(result){
                                $('#btn_guardar_sala_localCVN').show();
                                $('#btn_guardar_sala_onlineCVN').hide();
                                $('#sel_sala_serv_ipCVN').show();
                                var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' class='sals_id_inp_localCVN' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";
                            }                        
                        }else{
                            if(opc == 'online'){
                                servidor = $("#sala_servidor_onlineCVN");
                                // capturar el select
                                // var lst_sala = document.getElementById("sala_servidor_online");
                                if(result){
                                    $('#btn_guardar_sala_localCVN').hide();
                                    $('#btn_guardar_sala_onlineCVN').show();
                                    $('#sel_sala_serv_onlineCVN').show();
                                    var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' class='sals_id_inp_onlineCVN' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";
                                }                                
                            }else{                                
                               
                                servidor = $("#sala_servidor_propioCVN");
                                if(result){
                                    $('#btn_guardar_sala_localCVN').hide();
                                    $('#btn_guardar_sala_onlineCVN').hide();
                                    $('#sel_sala_serv_onlineCVN').hide();
                                    
                                    var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' class='sals_id_inpCVN' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";  
                                }                                     
                            }
                        }  
                        
                        servidor.append(nuevo);
                    }
                }else{
                    result = JSON.parse(result);
                    result = result.resultado;
                    for( var i=0; i<result.length; i++){
                        
                    servidor = $("#sala_servidor_webCVN");
                    // capturar el select
                    // var lst_sala = document.getElementById("sala_servidor_online");
                    if(result){
                        $('#btn_guardar_sala_localCVN').hide();
                        $('#btn_guardar_sala_onlineCVN').hide();
                        $('#btn_guardar_sala_webCVN').show();
                        $('#sel_sala_serv_webCVN').show();
                        var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' class='sals_id_inp_webCVN' value='"+result[i].id+"'>"+result[i].nombre+"</label></div>";
                    }       
                        servidor.append(nuevo);
                    }
                }
                                          
                
            },error:function(err){
                console.log(err);
            }
        });
    }
    function buscarSalaPrimeraCV(){
        $('#sel_sala_serv_onlineCVN').hide();
        $('#sel_sala_serv_ipCVN').hide();
        $('#sel_sala_serv_webCVN').hide();
        $('#btn_guardar_sala_webCVN').hide();
        $('#btn_guardar_sala_localCVN').hide();
        $('#btn_guardar_sala_onlineCVN').hide();
        $('#sala_servidor_ipCVN .checkbox').each(function(){
            $(this).remove();
        });
        $('#sala_servidor_onlineCVN .checkbox').each(function(){
            $(this).remove();
        });
        $('#sala_servidor_webCVN .checkbox').each(function(){
            $(this).remove();
        });
        var tipo_serv = '{{$cv->tipo_servidor}}';
        if(tipo_serv=='web'){
            var sala_id = '{{$sala_sel}}';
        }else{
            var sala_id = '{{$cv->salas_id}}';
        }        
        
        var arr = sala_id.split(",");
        // console.log(arr);
        
        if(tipo_serv == 'local'){
            var ip = 'http://'+$('#servidor_ipCVN').val()+'/cartelera_backend_laravel/public/?ruta=api/salas';
            var tipo = 'GET';
            var dato = {'_token': '{{ csrf_token() }}'};
        }else{
            if(tipo_serv == 'online'){
                var ip = $('#servidor_urlCVN').val()+'cartelera_backend_laravel/public/?ruta=api/salas';
                var tipo = 'GET';
                var dato = {'_token': '{{ csrf_token() }}'};
            }else{
                if(tipo_serv == 'web'){
                    var ip = $('#servidor_web').val()+'wp-content/plugins/neo_homenajes/json/conector.php';
                    var tipo = 'POST';
                    var dato = {'salones': ''};
                }else{
                    var ip = '../public/?ruta=api/salas';
                    var tipo = 'GET';
                    var dato = {'_token': '{{ csrf_token() }}'};
                }                
            }
        }        
        $.ajax({
            type: tipo,
            url: ip, 
            data: dato,
            success: function(result){
                // console.log(result);
                var servidor;
                if(tipo_serv != 'web'){
                    for( var i=0; i<result.salas.length; i++){
                        if(tipo_serv == 'local'){
                            // capturar el select
                            servidor = $("#sala_servidor_ipCVN");
                            if(result){
                                $('#btn_guardar_sala_localCVN').show();
                                $('#btn_guardar_sala_onlineCVN').hide();
                                $('#sel_sala_serv_ipCVN').show();
                                for(var j=0; j < arr.length ; j++){
                                    // console.log('Sala',arr[j],result.salas[i].id);
                                    if(arr[j] == result.salas[i].id){
                                        var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' checked class='sals_id_inp_localCVN' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";
                                        break;
                                    }else{
                                        var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]'  class='sals_id_inp_localCVN' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";
                                    }
                                }                                
                            }  
                        }else{
                            if(tipo_serv == 'online'){
                                // capturar el select
                                servidor = $("#sala_servidor_onlineCVN");
                                if(result){
                                    $('#btn_guardar_sala_localCVN').hide();
                                    $('#btn_guardar_sala_onlineCVN').show();
                                    $('#sel_sala_serv_onlineCVN').show();
                                    for(var j=0; j < arr.length ; j++){
                                        // console.log('Sala',arr[j],result.salas[i].id);
                                        if(arr[j] == result.salas[i].id){
                                            var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' checked class='sals_id_inp_onlineCVN' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";
                                            break;
                                        }else{
                                            var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' class='sals_id_inp_onlineCVN' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";
                                        }
                                    }
                                    
                                }  
                            }else{                               
                                servidor = $("#sala_servidor_propioCVN");
                                if(result){
                                    $('#btn_guardar_sala_localCVN').hide();
                                    $('#btn_guardar_sala_onlineCVN').hide();
                                    $('#sel_sala_serv_onlineCVN').hide();
                                    for(var j=0; j < arr.length ; j++){
                                        // console.log('Sala',arr[j],result.salas[i].id);
                                        if(arr[j] == result.salas[i].id){
                                            var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' checked class='sals_id_inpCVN' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";
                                            break;
                                        }else{
                                            var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' class='sals_id_inpCVN' value='"+result.salas[i].id+"'>"+result.salas[i].nombre+"</label></div>";
                                        }
                                    }                                                                        
                                }      
                            }
                        }  
                        servidor.append(nuevo);
                    }  
                }else{
                    result = JSON.parse(result);
                    result = result.resultado;
                    for( var i=0; i<result.length; i++){
                        
                        if(tipo_serv == 'web'){
                            // capturar el select
                            servidor = $("#sala_servidor_webCVN");
                            if(result){
                                $('#btn_guardar_sala_localCVN').hide();
                                $('#btn_guardar_sala_onlineCVN').hide();
                                $('#btn_guardar_sala_webCVN').show();
                                $('#sel_sala_serv_webCVN').show();
                                for(var j=0; j < arr.length ; j++){
                                    // console.log('Sala',arr[j],result.salas[i].id);
                                    if(arr[j] == result[i].id){
                                        var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' checked class='sals_id_inp_webCVN' value='"+result[i].id+"'>"+result[i].nombre+"</label></div>";
                                        break;
                                    }else{
                                        var nuevo = "<div class='checkbox'><label><input type='checkbox' name='salas_id[]' class='sals_id_inp_webCVN' value='"+result[i].id+"'>"+result[i].nombre+"</label></div>";
                                    }
                                }
                                
                            }  
                        }
                        servidor.append(nuevo);
                    }  
                }
                                        
                
            },error:function(err){
                console.log(err);
            }
        });
        
        
    }
    function guardarSalaCV(opc){
        if(opc == 'local'){
            var url = $('#servidor_ipCVN').val();
            var sala = '';
            $('.sals_id_inp_localCVN').each(function(even){
                    
                if($(this).prop( "checked" )){
                    
                    if(sala==''){
                        sala+=$(this).val();
                    }else{
                        sala+=','+$(this).val();
                    }
                }                
            });            
        }else{
            if(opc == 'online'){
                var url = $('#servidor_urlCVN').val();
                var sala = '';
                $('.sals_id_inp_onlineCVN').each(function(even){
                    
                    if($(this).prop( "checked" )){
                        
                        if(sala==''){
                            sala+=$(this).val();
                        }else{
                            sala+=','+$(this).val();
                        }
                    }                    
                });
            }else{
                if(opc == 'web'){
                    var url = $('#servidor_webCVN').val();
                    var sala = '';
                    $('.sals_id_inp_webCVN').each(function(even){
                        
                        if($(this).prop( "checked" )){
                            
                            if(sala==''){
                                sala+=$(this).val();
                            }else{
                                sala+=','+$(this).val();
                            }
                        }                        
                    });
                }else{
                    var sala = '';
                    var url = ''
                    $('.sals_id_inpCVN').each(function(even){
                        
                        if($(this).prop( "checked" )){
                            
                            if(sala==''){
                                sala+=$(this).val();
                            }else{
                                sala+=','+$(this).val();
                            }
                        }                        
                    });
                }                
            }
        }
        if(sala==''){
            alert('Debe seleccionar minimo una sala');
        }else{
            if(opc == 'web'){
                $('#btn_guardar_sala_webCVN i').remove();
                $('#btn_guardar_sala_webCVN').append('<i class="fa fa-spinner fa-spin" style="font-size:18px; margin: -3px;"></i>');  
                $.ajax({
                    type:'GET',
                    url: "../public/?ruta=config/ch/sync/servicios&url="+url, 
                    data :'',
                    success: function(result){
                        $.ajax({
                            type:'POST',
                            url: "../public/?ruta=api/config/cv/salas", 
                            data :{
                                '_token': '{{ csrf_token() }}',
                                'tipo': opc,
                                'url': url,
                                'sala': sala
                            },
                            success: function(result){
                                console.log(result);  
                                location.reload();              
                            },error:function(err){
                                console.log(err);
                                $('#btn_guardar_sala_webCVN i').remove();
                                $('#btn_guardar_sala_webCVN').append('<i class="fa fa-save"></i>'); 
                                $('#msj_errorCV').text('Error de sincronización');
                            }
                        });
                    },error: function(err){
                        // console.log('ERROR DE SINCRONIZACION',err);
                        // $('#btn_guardar_sala_webCVN i').remove();
                        // $('#btn_guardar_sala_webCVN').append('<i class="fa fa-save"></i>'); 
                        // $('#msj_errorCV').text('Error de sincronización');
                        $.ajax({
                            type:'POST',
                            url: "../public/?ruta=api/config/cv/salas", 
                            data :{
                                '_token': '{{ csrf_token() }}',
                                'tipo': opc,
                                'url': url,
                                'sala': sala
                            },
                            success: function(result){
                                console.log(result);  
                                location.reload();              
                            },error:function(err){
                                console.log(err);
                                $('#btn_guardar_sala_webCVN i').remove();
                                $('#btn_guardar_sala_webCVN').append('<i class="fa fa-save"></i>'); 
                                $('#msj_errorCV').text('Error de sincronización');
                            }
                        });
                    }
                });          
            }else{
                $.ajax({
                    type:'POST',
                    url: "../public/?ruta=api/config/cv/salas", 
                    data :{
                        '_token': '{{ csrf_token() }}',
                        'tipo': opc,
                        'url': url,
                        'sala': sala
                    },
                    success: function(result){
                        console.log(result);  
                        location.reload();              
                    },error:function(err){
                        console.log(err);
                    }
                });
            }
                         
        }            
    }
	$(document).ready(function(){

		cambiarTipoServidor();
		buscarSalaPrimera();
        
        buscarSalaPrimeraCV();
		cambiarTipoServidorCV();

	});
	
</script>
@stop