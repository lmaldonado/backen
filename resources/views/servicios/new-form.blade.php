
@if(isset($msj))
<div class="col-md-12 col-lg-8">
    <div class="callout callout-success">
        <p>This is a green callout.</p>
    </div>
</div>
@endif

@if(isset($servicio))
{!! Form::open(['url' => 'servicios/'.$servicio->id, 'method' => 'PUT', 'files' => true]) !!}
@else
{!! Form::open(['url' => 'servicios', 'method' => 'POST', 'files' => true]) !!}
@endif

<div class="col-md-12 col-lg-8">
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li  class="active" ><a href="#tab_1" data-toggle="tab">Datos del Fallecido</a></li>
        <li><a href="#tab_2" data-toggle="tab">Datos del Servicio</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <div class="box-body" >
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="form-group col-md-12">
                            {!! Form::label('Nombres') !!}
                            {!! Form::text('nombres', isset($servicio)?$servicio->nombres:NULL, ['class' => 'form-control', 'placeholder' => 'Introduzca el nombre']) !!}
                            </div>
                            <div class="form-group col-md-12">
                            {!! Form::label('Apellidos') !!}
                            {!! Form::text('apellidos', isset($servicio)?$servicio->apellidos:NULL, ['class' => 'form-control', 'placeholder' => 'Introduzca el apellido']) !!}
                            </div>
                            <div class="form-group col-md-12">
                                {!! Form::label('Fecha Nacimiento') !!}

                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  {!! Form::text('fecha_nac', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d',$servicio->fecha_nac)->format('d/m/Y')):(\Carbon\Carbon::now()->format('d/m/Y')),['class' => 'form-control pull-right datepicker', 'id'=>'dte-fecha-nac']) !!}
                                </div>
                                
                            </div>
                            <div class="form-group col-md-12">
                                {!! Form::label('Fecha Fallecimiento') !!}
                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  {!! Form::text('fecha_fac', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d',$servicio->fecha_fac)->format('d/m/Y')):(\Carbon\Carbon::now()->format('d/m/Y')),['class' => 'form-control pull-right datepicker', 'id'=>'dte-fecha-fac']) !!}
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                {!! Form::label('Religion') !!}
                                {!! Form::select('religion', $religiones, isset($servicio)?$servicio->religion:NULL, ['placeholder' => 'Seleccione...','id'=>'lst_sala','class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group image-add-ct">
                            {!! Form::label('Foto') !!}
                            <div  class="add-image" id="add-icon-item" style="background-image: url({{isset($servicio)?url('images/servicios').'/'.$servicio->foto:''}})"></div>
                            <div style="height: 5px"></div>
                            <div id="icon_container" >
                                {!! Form::button('Cargar Foto', ['class' => 'btn btn-success']) !!}
                                <input id="btn-add-foto"  name="foto" class="inputFile" type="file" accept="image/*" base-sixty-four-input="" style="">
                            </div>
                        </div>
                        <div class="ct-foto">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="tab_2">
            <div class="box-body" >
                <div class="row">
                    @if(isset($salas))
                    <div class="form-group col-md-6">
                        {!! Form::label('Sala') !!}
                        {!! Form::select('sala', $salas, isset($servicio)?$servicio->sala:NULL, ['placeholder' => 'Seleccione...','id'=>'lst_sala','class' => 'form-control']) !!}
                    </div>
                    @endif
                    <div class="form-group col-md-6">
                        {!! Form::label('Habilita') !!}
                        <div class="bootstrap-timepicker">
                            <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                              </div>
                              {!! Form::text('habilita', isset($servicio)?(\Carbon\Carbon::createFromFormat('H:i:s',$servicio->habilita)->format('h:i A')):'',['class' => 'form-control pull-right timepicker', 'id'=>'dte-habilita']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('Salida') !!}

                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          {!! Form::text('salida_date', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $servicio->salida)->format('d/m/Y')):\Carbon\Carbon::now()->format('d/m/Y'),['class' => 'form-control pull-right datepicker', 'id'=>'dte-salida-date']) !!}
                        </div>
                        <div class="bootstrap-timepicker" style="margin-top: 10px">
                            <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                              </div>
                              {!! Form::text('salida_time', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $servicio->salida)->format('h:i A')):'',['class' => 'form-control pull-right timepicker', 'id'=>'dte-salida-time']) !!}
                            </div>
                        </div>   
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('Servicio Cementerio') !!}
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          {!! Form::text('cementerio_date', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $servicio->servicio_cementerio)->format('d/m/Y')):\Carbon\Carbon::now()->format('d/m/Y'),['class' => 'form-control pull-right datepicker', 'id'=>'dte-cementerio-date']) !!}
                        </div>
                        <div class="bootstrap-timepicker" style="margin-top: 10px">
                            <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                              </div>
                              {!! Form::text('cementerio_time', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $servicio->servicio_cementerio)->format('h:i A')):'',['class' => 'form-control pull-right timepicker', 'id'=>'dte-cementerio-time']) !!}
                            </div>
                        </div>  
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('Velatorios') !!}
                        <div id="ct-velatorios">
                        @if(isset($servicio))    
                            @foreach($servicio->velatorios as $velatorio)

                                <div class="row velatorio"  style="margin-bottom: 10px">
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-7" style="padding-right: 0px">
                                                Fecha
                                                <div class="input-group date">
                                                  <div class="input-group-addon" style="padding: 6px 6px">
                                                    <i class="fa fa-calendar"></i>
                                                  </div>
                                                  {!! Form::text('velatorio_fecha[]', \Carbon\Carbon::createFromFormat('Y-m-d', $velatorio->fecha)->format('d/m/Y'),['class' => 'form-control pull-right datepicker', 'id'=>'dte-cementerio-date']) !!}
                                                </div> 
                                            </div>
                                            <div class="col-md-5">
                                                <div style="margin-left: -15px">Desde</div>
                                                <div class="bootstrap-timepicker row">
                                                    <div class="input-group">
                                                      <div class="input-group-addon" style="padding: 6px 6px">
                                                        <i class="fa fa-clock-o"></i>
                                                      </div>
                                                      {!! Form::text('velatorio_desde[]', \Carbon\Carbon::createFromFormat('H:i:s', $velatorio->desde)->format('h:i A'),['class' => 'form-control pull-right timepicker', 'id'=>'dte-cementerio-time', 'style'=>'padding: 6px;']) !!}
                                                    </div>
                                                </div>   
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-4">
                                        Hasta
                                        <div class="bootstrap-timepicker">
                                            <div class="input-group">
                                              <div class="input-group-addon" style="padding: 6px 6px">
                                                <i class="fa fa-clock-o"></i>
                                              </div>
                                              {!! Form::text('velatorio_hasta[]', \Carbon\Carbon::createFromFormat('H:i:s', $velatorio->hasta)->format('h:i A'),['class' => 'form-control pull-right timepicker', 'id'=>'dte-cementerio-time', 'style'=>'padding: 6px;']) !!}
                                            </div>
                                        </div>    
                                    </div>
                                </div>


                                <!---------------------------------->
                            @endforeach  
                        @endif
                        </div>
                        <button type="button" id="btn-add-velatorio" class="btn btn-block btn-primary btn-sm">Agregar Velatorio</button>
                    </div>
                    <div class="form-group col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                {!! Form::label('Destino') !!}
                                {!! Form::select('destino', $destinos, isset($servicio)?$servicio->destino:NULL, ['placeholder' => 'Seleccione...','id'=>'lst_destino','class' => 'form-control']) !!}
                            </div>
                            <div class="col-md-12 input-destino">
                                {!! Form::label('Nombre Destino') !!}
                                {!! Form::text('nombre_destino', null, ['class' => 'form-control', 'placeholder' => 'destino']) !!}
                            </div>
                            <div class="col-md-12 input-destino">
                                {!! Form::label('logo Destino') !!}
                                {!! Form::file('logo_destino', null, ['class' => 'form-control', 'placeholder' => 'Logo del Destino']) !!} 
                            </div>
                            <div class="col-md-12 input-destino">
                                {!! Form::label('Mapa Destino') !!}
                                {!! Form::file('mapa_destino', null, ['class' => 'form-control', 'placeholder' => 'destino']) !!}
                            </div>   
                            <div class="col-md-12 input-destino">
                                {!! Form::label('Direccion Destino') !!}
                                {!! Form::text('direccion_destino', null, ['class' => 'form-control', 'placeholder' => 'destino']) !!}destino                      </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </br>

    <div class="box-footer">
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
        <a href="{{ url('/?ruta=dashboard/clientes') }}" class="btn btn-danger">Cancelar</a>
    </div>

</div>
</div>

{!! Form::close() !!}


<!-- TEMPLATES -->
<div id="tmp-ct" style="display: none" >

<div class="row velatorio"  style="margin-bottom: 10px">
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-7" style="padding-right: 0px">
                Fecha
                <div class="input-group date">
                  <div class="input-group-addon" style="padding: 6px 6px">
                    <i class="fa fa-calendar"></i>
                  </div>
                  {!! Form::text('velatorio_fecha[]', \Carbon\Carbon::now()->format('d/m/Y'),['class' => 'form-control pull-right datepicker2', 'id'=>'dte-cementerio-date']) !!}
                </div> 
            </div>
            <div class="col-md-5">
                <div style="margin-left: -15px">Desde</div>
                <div class="bootstrap-timepicker row">
                    <div class="input-group">
                      <div class="input-group-addon" style="padding: 6px 6px">
                        <i class="fa fa-clock-o"></i>
                      </div>
                      {!! Form::text('velatorio_desde[]', '',['class' => 'form-control pull-right timepicker2', 'id'=>'dte-cementerio-time', 'style'=>'padding: 6px;']) !!}
                    </div>
                </div>   
            </div>
        </div>
        
    </div>
    <div class="col-md-4">
        Hasta
        <div class="bootstrap-timepicker">
            <div class="input-group">
              <div class="input-group-addon" style="padding: 6px 6px">
                <i class="fa fa-clock-o"></i>
              </div>
              {!! Form::text('velatorio_hasta[]', '',['class' => 'form-control pull-right timepicker2', 'id'=>'dte-cementerio-time', 'style'=>'padding: 6px;']) !!}
            </div>
        </div>    
    </div>
</div>

</div>

@section('script')
<script>

$(document).ready(function(){

    //INICIALIZATE

    $('.datepicker').datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy'
    });

    $('.timepicker').timepicker({
      showInputs: false
    });


    $('#lst_destino').on('change', function(){
        if($(this).val() == 'Agregar'){
            $('.input-destino').showdestino     }else{
            $('.input-destino').hidedestino     }
    });

    $('#btn-add-foto').on('change', function(evt){

        var files = evt.target.files; // FileList object
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e ) {
                    $('#add-icon-item').css('background-image', 'url('+e.target.result+')');
                };
            })(f );
            reader.readAsDataURL(f );
        }
    });

    $('#btn-add-velatorio').on('click', function(){
        velatorio = $('#tmp-ct .velatorio').clone();
        velatorio.appendTo('#ct-velatorios');

        velatorio.find('.datepicker2').datepicker({
          autoclose: true,
          format: 'dd/mm/yyyy'
        });

        velatorio.find('.timepicker2').timepicker({
          showInputs: false
        });
    });

});

</script>
@stop