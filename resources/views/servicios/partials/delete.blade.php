<!--Eliminar servicio-->
<div class="modal fade" role="dialog" id="modalDelete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Eliminar Servicio</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => 'servicios/'.$value->id, 'method' => 'DELETE',
                'files' => true, 'id' => 'delete']) !!}
                <input type="text" name="servicio_id" id="servicio_id" value="">
                <p>¿Seguro que desea eliminar el servicio?</p>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="$('#delete').submit()" id="btntitle">Eliminar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

