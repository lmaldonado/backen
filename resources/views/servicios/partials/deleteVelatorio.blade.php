<div class="modal fade" role="dialog" id="modalDeleteVelatorio">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Eliminar Velatorio</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => 'velatorios/'.$velatorio->id, 'method' => 'DELETE',
                'files' => true, 'id' => 'eliminarVelatorio']) !!}
                    <input type="hidden" name="velatorio_id" id="velatorio_id" value="">
                    <input type="hidden" name="servicio_id" id="servicio_id" value="">
                    <p>¿Seguro que desea eliminar el velatorio?</p>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="$('#eliminarVelatorio').submit()" id="btntitle">Eliminar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
