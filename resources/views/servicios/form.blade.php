
@if(isset($msj))
<div class="col-md-12 col-lg-8">
	<div class="callout callout-success">
		<p>This is a green callout.</p>
	</div>
</div>
@endif

{{--Cambia el tipo de formulario dependiendo de si estamos creando uno nuevo
		o editando un servicio existente.--}}

@if(isset($servicio))
{!! Form::open(['url' => './?ruta=servicios/update&id='.$servicio->id, 'method' => 'POST', 'files' => true,'id'=>'formupdate']) !!}
@else
{!! Form::open(['url' => './?ruta=servicios/nuevo', 'method' => 'POST', 'files' => true,'id'=>'formcreate']) !!}
@endif



<div class="content">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
			
				<span style="font-size:25px;">Servicios</span>
			
				<div class=" pull-right">
					{!! Form::submit('Guardar', ['class' => 'btn btn-success']) !!}
					<a href="{{ url('/?ruta=servicios') }}" class="btn btn-danger">Cancelar</a>
				</div>
		
				
		</div>
		<br><br>
		<div class="col-lg-6 col-md-6">
			
			
			
			{{-- BOX FALLECIDO --}}
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Fallecido</h3>
				</div>
				
				<div class="box-body row">	
					<div class="form-group col-lg-8 col-md-6 col-sm-8">
						{!! Form::label('Nombres') !!}
						{!! Form::text('nombres', isset($servicio)?$servicio->nombres:NULL, ['class' => 'form-control', 'placeholder' => 'Introduzca el nombre']) !!}

						{!! Form::label('Apellidos', '',['style'=>'margin-top:10px;']) !!}
						{!! Form::text('apellidos', isset($servicio)?$servicio->apellidos:NULL, ['class' => 'form-control', 'placeholder' => 'Introduzca el apellido']) !!}
					</div>

					<div class="form-group col-lg-4 col-md-6 col-sm-4" id="form_foto_perfil">
						{!! Form::label('Foto') !!}
						<br>
						{{-- IMAGE PREVIEW del mapa de ruta del Cementario --}}
						
						<div class="preview_box" style="width:100px; height:100px;">										

							<div class="add-image" id="image-preview-perfil" style="background-image:url({{isset($servicio)?'./'.$servicio->foto:''}});margin-left:0px; width:100px; height:100px;">
								<label for="foto_perfil" id="image-label-perfil" title="Cambiar foto" style="cursor:pointer;position:absolute;width:49px;height:100px;padding-left:0px;padding-top:0px;background:rgba(255, 255, 255, 0.0);"><i class="fa fa-camera" style="background:rgba(255, 255, 255, 0.5);padding:4px;border-radius: 0px 0px 8px 0px;"></i></label>											
								<input id="foto_perfil"  name="foto" class="inputFile" type="file" accept="image/*" base-sixty-four-input="" style="">
								@if(isset($servicio))
									<input type="hidden" id="id_servicios_foto" value="{{$servicio->id}}">
								@else
									<input type="hidden" id="id_servicios_foto" >
								@endif
							</div>
						</div>
						@if(isset($servicio))
							<div title="Eliminar foto" onclick="eliminar_foto_perfil()" style="cursor:pointer;position:absolute;width:47px;height:100px;top:25px;left:67px;padding-top:0px;padding-right:0px;font-size:17px;text-align:right;background:rgba(255, 255, 255, 0.0);">
								<i class="fa fa-trash" id="image-close-perfil" style="background:rgba(255, 255, 255, 0.5);padding:2px 5px;border-radius: 0px 0px 0px 8px;"></i>
							</div>
						@else
							<div title="Eliminar foto" onclick="eliminar_foto_perfil_nueva()" style="cursor:pointer;position:absolute;width:47px;height:100px;top:25px;left:67px;padding-top:0px;padding-right:0px;font-size:17px;text-align:right;background:rgba(255, 255, 255, 0.0);">
								<i class="fa fa-trash" id="image-close-perfil" style="background:rgba(255, 255, 255, 0.5);padding:2px 5px;border-radius: 0px 0px 0px 8px;"></i>
							</div>
						@endif
					</div>
					<br>
					
					<div class="form-group col-lg-4 col-md-4">
						{!! Form::label('Fecha Nacimiento') !!}
						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							@if(isset($servicio) && $servicio->fecha_nac!='0000-00-00')
								{!! Form::text('fecha_nac', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d',$servicio->fecha_nac)->format('d/m/Y')):(\Carbon\Carbon::now()->format('d/m/Y')),['class' => 'form-control pull-right datepicker2', 'id'=>'dte-fecha-nac','autocomplete'=>'off']) !!}
							@elseif(isset($servicio) && $servicio->fecha_nac=='0000-00-00')
								{!! Form::text('fecha_nac', '',['class' => 'form-control pull-right datepicker2', 'id'=>'dte-fecha-nac','autocomplete'=>'off']) !!}
							@else
								{!! Form::text('fecha_nac', (\Carbon\Carbon::now()->format('d/m/Y')),['class' => 'form-control pull-right datepicker2', 'id'=>'dte-fecha-nac','autocomplete'=>'off']) !!}
							@endif
						</div>
					</div>
					
					<div class="form-group col-lg-4 col-md-4">
						{!! Form::label('Fecha Fallecimiento') !!}
						<div class="input-group date">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
								{!! Form::text('fecha_fac', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d',$servicio->fecha_fac)->format('d/m/Y')):(\Carbon\Carbon::now()->format('d/m/Y')),['class' => 'form-control pull-right datepicker2 showMeridian', 'id'=>'dte-fecha-fac','autocomplete'=>'off']) !!}
						</div>
					</div>
					
					<div class="form-group col-md-4 col-md-4">
						<div class="">
							{!! Form::label('Religión') !!}
							{!! Form::select('religion_id', $religiones, isset($servicio)?$servicio->religion_id:NULL, ['placeholder' => '','id'=>'lst','class' => 'form-control']) !!}
						</div>
					</div>
				</div>
			</div> {{-- /.box: fallecido --}}			
	
			@if($CV->multiples_empresas==1)
				{{-- BOX: DATOS EMPRESA --}}
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Empresa contratante</h3>
					</div>
					<div class="box-body row">
						<div class="form-group col-md-12">
							
							{{-- SELECT: aparece por defecto... --}}
							<div class="form-group seleccionar-empresa" id="seleccionar-empresa">
								{!! Form::label('Nombre') !!}
								<div class="input-group">
									<!-- {!! Form::select('empresa_id', $empresas, isset($servicio)?$servicio->empresa:NULL, ['placeholder' => '', 'id'=>'lst_empresa','class' => 'form-control']) !!} -->
									<select name="empresa_id" id="empresa_nombre" class="form-control" onchange="cambio_emp_sel();">
										<option id="emp_val_nulo" value="0"></option>
									@if($empresas)
										@foreach($empresas as $emp)
											
											@if(isset($servicio) && $servicio->empresa_id == $emp->id)
												<option id="emp_val_{{$emp->id}}" value="{{$emp->id}}" selected>{{$emp->nombre}}</option>
											@else
												<option id="emp_val_{{$emp->id}}" value="{{$emp->id}}">{{$emp->nombre}}</option>
											@endif

										@endforeach
											<!-- <option value="0">Agregar</option> -->
									@endif
									</select>
									<span class="input-group-addon" title="Editar" id="btn_editar_empresa" onclick="empresa_edit()">
										<!-- <i class="fa fa-edit" id="empresa_edit" onclick="empresa_edit()"></i> -->
										<i class="fa fa-edit" ></i>
									</span>
									<span class="input-group-addon" title="Agregar nueva" onclick="empresa_new()">
										<!-- <i class="fa fa-edit" id="empresa_edit" onclick="empresa_edit()"></i> -->
										<i class="fa fa-plus"></i>
									</span>
								</div>
							</div>
							
							{{-- FORMULARIO: oculto por defecto; aparece al seleccionar "Agregar" en las opciones del SELECT --}}
							<div class="form-group agregar-empresa" id="agregar_empresa">
								{{-- {!! Form::open(['url' => 'api/empresas', 'method' => 'POST', 'files' => true, 'id' => 'guardarEmpresa']) !!} --}}
								<!-- <form action="" method="POST"> -->
									{!! Form::label('Nombre') !!}
									<div class="input-group">
										{!! Form::text('nombre_empresa', '', ['class' => 'form-control', 'placeholder'=> 'Introduzca la empresa','id'=>'nombre_nueva_empresa']) !!}
										<!-- <span class="input-group-addon" id="load_logo_empresa">
											<i class="fa fa-camera"></i>
										</span> -->
										<span class="input-group-addon" title="Guardar" onclick="guardar_nueva_empresa()">
											<i class="fa fa-save"></i>
										</span>
										<span class="input-group-addon" title="Regresar" onclick="ocultar_editar_empresa()">
											<i class="fa fa-undo"></i>
										</span>
									</div>
								<!-- </form> -->
								{{-- {!! Form::close() !!} --}}
							</div>

							{{-- FORMULARIO: oculto por defecto; aparece al seleccionar "Ediatr" en las opciones del SELECT --}}
							<div class="form-group editar-empresa" id="editar-empresa">
								{{-- {!! Form::open(['url' => 'api/empresas', 'method' => 'POST', 'files' => true, 'id' => 'guardarEmpresa']) !!} --}}
								<form action="" method="POST">
									{!! Form::label('Nombre') !!}
									<div class="input-group">
										<!-- {!! Form::text('nombre_empresa', '', ['class' => 'form-control', 'placeholder'=> 'Introduzca la empresa']) !!} -->
										<input type="text" name="empresa_editar" class="form-control" id="empresa_edit_nombre">
										<input type="hidden" name="empresa_editar" class="form-control" id="empresa_edit_id">
										<!-- <span class="input-group-addon" id="load_logo_empresa">
											<i class="fa fa-camera"></i>
										</span> -->
										<span class="input-group-addon"  title="Guardar" onclick="guardar_edit_empresa()">
											<i class="fa fa-save"></i>
										</span>
										<!-- <span class="input-group-addon" title="Eliminar" onclick="eliminar_empresa()"> -->
										<span class="input-group-addon" title="Eliminar" onclick="confirmarEliminacion('empresa_edit_id','empresa_edit_nombre','empresa')">
											<i class="fa fa-trash"></i>
										</span>
										<span class="input-group-addon" title="Regresar" onclick="ocultar_editar_empresa()">
											<i class="fa fa-undo"></i>
										</span>
									</div>
								</form>
								{{-- {!! Form::close() !!} --}}
							</div>
							
						</div>
					</div>
				</div> {{-- /.box: datos_empresa --}}
			@endif
			
			<!-- EL SCRIPT PARA EMPRESAS -->
			<script>
				// VER CAMBIO EMPRESA
				function cambio_emp_sel(){
					if($('#empresa_nombre').val() == 0){
						$('#btn_editar_empresa').hide();
					}else{
						$('#btn_editar_empresa').show();
					}
				}
				// OCULTA EDITAR EMPRESA
				function ocultar_editar_empresa(){
					$('#seleccionar-empresa').show();
					$('#editar-empresa').hide();
					$('#agregar_empresa').hide();
				};
				// MUESTRA EL FORM PARA EDITAR EMPRESA Y ASIGNA LA EMPRESA SELECCIONADA
				function empresa_edit(){

					var id_empresa=$('#empresa_nombre').val();
					var nombre_empresa=$('#emp_val_'+id_empresa).text();
					$('#empresa_edit_nombre').val(nombre_empresa);
					$('#empresa_edit_id').val(id_empresa);
					// console.log($('#empresa_nombre').val());
					// console.log($('#emp_val_'+id_empresa).text());
					$('#seleccionar-empresa').hide();
					$('#editar-empresa').show();
					
				};
				// GUARDA LOS CAMBIOS EN LA EMPRESA SELECCIONADA
				function guardar_edit_empresa(){
					var nuevo_nombre_empresa=	$('#empresa_edit_nombre').val();
					var id_emp= $('#empresa_edit_id').val();
					// console.log(id_emp+' '+nuevo_nombre_empresa);

					$.ajax({
						type:'POST',
						url: "./?ruta=api/empresa/editar&id="+id_emp, 
						data : {
							'_token': '{{ csrf_token() }}',
							'nombre':nuevo_nombre_empresa,
							'id_empresa':id_emp
						},success: function(result){
							// console.log(result);
							$('#emp_val_'+id_emp).text(nuevo_nombre_empresa);
							$('#seleccionar-empresa').show();
							$('#editar-empresa').hide();
						},error:function(err){
							console.log(err);
						}
					});
				}
				// MUESTRA EL FORM PARA AGREGAR UNA NUEVA EMPRESA
				function empresa_new(){
					$('#seleccionar-empresa').hide();
					$('#agregar_empresa').show();
				}
				// GUARDA LA NUEVA EMPRESA
				function guardar_nueva_empresa(){
					var nombre_empresa = $('#nombre_nueva_empresa').val();
					console.log(nombre_empresa);

					$.ajax({
							type: 'POST',
							url: './?ruta=api/empresa',
							data: {
								'_token': '{{ csrf_token() }}',
								'empresa': nombre_empresa
							},success:function(result){
								console.log(result);

								// capturar el select
								var lst_empresa = document.getElementById("empresa_nombre");
								
								// determinar cuantos options tiene el select
								var pos = lst_empresa.length ;
								
								// crear el option nuevo: value y text
								var opt = document.createElement("option");
								opt.value = result.id;
								opt.text = result.nombre;
								opt.id = "emp_val_"+result.id;
								
								// agregar el option nuevo justo entes del último
								lst_empresa.add(opt, pos);
								
								$('#agregar_empresa').hide();
								$('#seleccionar-empresa').show();								
								
								// seleccionar la nueva opcion
								lst_empresa.selectedIndex = pos;
								cambio_emp_sel();
							},error:function(err){}
					});
				}

				function eliminar_empresa(x){
					// var id = $('#empresa_edit_id').val();
					var id = x;
					// console.log(id);
					$.ajax({
						type:'DELETE',
						url: "./?ruta=api/empresa/eliminar&id="+id, 
						data : {
							'_token': '{{ csrf_token() }}'
						},success: function(result){
							// console.log(result);
							$('#emp_val_'+id).hide();
							$('#empresa_nombre').val(0);
							$('#seleccionar-empresa').show();
							$('#editar-empresa').hide();
							$('#confirmarEliminacion').modal('toggle');
							cambio_emp_sel();
						},error:function(err){
							console.log(err);
						}
					});
				}				
			
			</script>
			<!-- FIND E SCRIPT PARA EMPRESAS -->
      
	
			{{-- BOX: DATOS SALAS --}}
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Velatorios</h3>
				</div>
				<div class="box-body row">
						
						@if(isset($salas))
							<div class="form-group col-md-12" >
								<label id="titulo_sala">Lugar</label>
								<div class="input-group" id="sala_select">
									
									<select name="sala_id" id="lst_sala" class="form-control" onchange='cambiarsala()'>
										<option id="sal_val_nulo" value="0"></option>
										@foreach($salas as $sal)
												
											@if(isset($servicio) && $servicio->sala_id == $sal->id)
												<option id="sal_val_{{$sal->id}}" value="{{$sal->id}}" selected>{{$sal->nombre}}</option>
											@else
												<option id="sal_val_{{$sal->id}}" value="{{$sal->id}}">{{$sal->nombre}}</option>
											@endif

										@endforeach
										@if(isset($servicio) && $servicio->sala_id == -10)
											<option id="sal_val_dom" value="-10" selected>En domicilio</option>
										@else
											<option id="sal_val_dom" value="-10" >En domicilio</option>
										@endif
										@if(isset($servicio) && $servicio->sala_id == 99)
											<option id="sal_val_99" value="99" selected>En privado</option>
										@else
											<option id="sal_val_99" value="99">En privado</option>
										@endif
										@if(isset($servicio) && $servicio->sala_id == 100)
											<option id="sal_val_100" value="100" selected>A confirmar</option>
										@else
											<option id="sal_val_100" value="100">A confirmar</option>
										@endif
									</select>
									<span class="input-group-addon" title="Editar sala" onclick="sala_edit()" id="btn_editar_sala">
										<i class="fa fa-edit" ></i>
									</span>
									<span class="input-group-addon" title="Agregar sala" onclick="sala_new()">
										<!-- <i class="fa fa-edit" id="empresa_edit" onclick="empresa_edit()"></i> -->
										<i class="fa fa-plus"></i>
									</span>
									{{-- <span class="input-group-addon" title="" id="btn_home">
										<i class="fa fa-home"></i>
									</span>
									<span class="input-group-addon" title="" id="btn_privado">
										<i class="fa fa-calendar"></i>
									</span> --}}
								</div>
							
						@endif
						
						<div id="form_editar_sala">
							<!-- <br> -->
							<form >
								<div class="form-group"><label for="id_sala_editar"><span style="opacity:0;">-</span>Id <span style="opacity:0;">------</span> Nombre</label><br>
									<div class="col-xs-1" style="padding:0px;">										
										<input class="form-control" type="text" name="id_sala_editar" id="id_sala_editar" disabled>
									</div>
									<div class="col-xs-11" style="padding:0px; padding-left:5px;">
										<div class="input-group">									
											<input class="form-control" type="text" name="nombre_sala_editar" id="nombre_sala_editar" >
											<span class="input-group-addon" title="Guardar" onclick="editar_sala()" >
												<i class="fa fa-save" ></i>
											</span>
											<span class="input-group-addon" title="Eliminar" onclick="confirmarEliminacion('id_sala_editar','nombre_sala_editar','sala')" >
												<i class="fa fa-trash" ></i>
											</span>
											<span class="input-group-addon" title="Regresar" onclick="regresar_sala()">
												<i class="fa fa-undo"></i>
											</span>
										</div>
									</div>
								</div>					
								<br>
								<!-- <div class="form-group">
									<label for="nombre_sala_editar">Nombre</label>
									<input class="form-control" type="text" name="nombre_sala_editar" id="nombre_sala_editar" >
								</div> -->
								<div class="checkbox">
									<label id="status_sala_edit">
									</label>
								</div>
								<div class="form-group">
									<label for="codigo_sala_editar">Codigo</label>
									<input class="form-control" type="text" name="codigo_sala_editar" id="codigo_sala_editar" >
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label style="margin-left:-15px;">Icono</label><br>

										<div class="col-xs-12" id="icono_sala">
											
											<br>
											{{-- IMAGE PREVIEW del mapa de ruta del Cementario --}}
											
											<div class="preview_box" style="position:absolute;width:100px; height:100px;left:-15px;top:0px;">										

												<div class="add-image" id="image-preview-icono_sala" style="margin-left:0px; width:100px; height:100px;">
													<label for="icono_sala_edit" id="image-label-icono_sala" title="Cambiar icono" style="cursor:pointer;position:absolute;width:49px;height:100px;padding-left:0px;padding-top:0px;background:rgba(255, 255, 255, 0.0);"><i class="fa fa-camera" style="background:rgba(255, 255, 255, 0.5);padding:4px;border-radius: 0px 0px 8px 0px;"></i></label>											
													<input id="icono_sala_edit"  name="mapa_icono_sala" class="inputFile" type="file" accept="image/*" base-sixty-four-input="" style="">
													
												</div>
											</div>
											<div title="Eliminar icono" onclick="eliminar_sala_icono_editar()" style="cursor:pointer;position:absolute;width:47px;height:100px;top:0px;left:38px;padding-top:0px;padding-right:0px;font-size:17px;text-align:right;background:rgba(255, 255, 255, 0.0);">
												<i class="fa fa-trash" id="image-close-icono_sala" style="background:rgba(255, 255, 255, 0.5);padding:2px 5px;border-radius: 0px 0px 0px 8px;"></i>
											</div>
											
										</div>					

									</div>
								</div>
								<div class="col-xs-6">
								<div class="form-group">
									<label style="margin-left:-15px;">Imagen</label><br>
										
									<div class="col-xs-12" id="letra_sala">
										
										<br>
										{{-- IMAGE PREVIEW del mapa de ruta del Cementario --}}
										
										<div class="preview_box" style="position:absolute;width:100px; height:100px;left:-15px;top:0px;">										

											<div class="add-image" id="image-preview-letra_sala" style="margin-left:0px; width:100px; height:100px;">
												<label for="letra_sala_edit" id="image-label-letra_sala" title="Cambiar imagen" style="cursor:pointer;position:absolute;width:49px;height:100px;padding-left:0px;padding-top:0px;background:rgba(255, 255, 255, 0.0);"><i class="fa fa-camera" style="background:rgba(255, 255, 255, 0.5);padding:4px;border-radius: 0px 0px 8px 0px;"></i></label>											
												<input id="letra_sala_edit"  name="mapa_letra_sala" class="inputFile" type="file" accept="image/*" base-sixty-four-input="" style="">
												
											</div>
										</div>
										<div title="Eliminar imagen" onclick="eliminar_sala_imagen_editar()" style="cursor:pointer;position:absolute;width:47px;height:100px;top:0px;left:38px;padding-top:0px;padding-right:0px;font-size:17px;text-align:right;background:rgba(255, 255, 255, 0.0);">
											<i class="fa fa-trash" id="image-close-letra_sala" style="background:rgba(255, 255, 255, 0.5);padding:2px 5px;border-radius: 0px 0px 0px 8px;"></i>
										</div>
										
									</div>	

									</div>
								</div>
								
							</form><br><br><br><br><br><br>
						</div>
						
						<div id="form_nueva_sala">
							<!-- <br> -->
							<form >
								<label for="nombre_sala_nueva">Nombre</label>
								<div class="input-group">									
								<input class="form-control" type="text" name="nombre_sala_nueva" id="nombre_sala_nueva" >
									<span class="input-group-addon" title="Guardar" onclick="guardar_nueva_sala()" >
										<i class="fa fa-save" ></i>
									</span>
									<span class="input-group-addon" title="Regresar" onclick="regresar_sala()">
										<i class="fa fa-undo"></i>
									</span>
								</div>								
								<div class="checkbox">
									<label>
										<input type="checkbox" id="status_sala_nueva"> Activa
									</label>
								</div>
								<div class="form-group">
									<label for="codigo_sala_nueva">Codigo</label>
									<input class="form-control" type="text" name="codigo_sala_nueva" id="codigo_sala_nueva" >
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label style="margin-left:-15px;">Icono</label><br>
										
										<div class="col-xs-12" id="icono_sala_nueva_form">
											
											<br>
											{{-- IMAGE PREVIEW del mapa de ruta del Cementario --}}
											
											<div class="preview_box" style="position:absolute;width:100px; height:100px;left:-15px;top:0px;">										

												<div class="add-image" id="image-preview-icono_sala_nueva" style="margin-left:0px; width:100px; height:100px;">
													<label for="icono_sala_nueva" id="image-label-icono_sala_nueva" title="Cambiar icono" style="cursor:pointer;position:absolute;width:49px;height:100px;padding-left:0px;padding-top:0px;background:rgba(255, 255, 255, 0.0);"><i class="fa fa-camera" style="background:rgba(255, 255, 255, 0.5);padding:4px;border-radius: 0px 0px 8px 0px;"></i></label>											
													<input id="icono_sala_nueva"  name="mapa_icono_sala_nueva" class="inputFile" type="file" accept="image/*" base-sixty-four-input="" style="">
													
												</div>
											</div>
											<div title="Eliminar icono" onclick="eliminar_sala_icono_nuevo()" style="cursor:pointer;position:absolute;width:47px;height:100px;top:0px;left:38px;padding-top:0px;padding-right:0px;font-size:17px;text-align:right;background:rgba(255, 255, 255, 0.0);">
												<i class="fa fa-trash" id="image-close-icono_sala_nueva" style="background:rgba(255, 255, 255, 0.5);padding:2px 5px;border-radius: 0px 0px 0px 8px;"></i>
											</div>
											
										</div>
																			
									</div>
								</div>
								<div class="col-xs-6">
								<div class="form-group">
									<label style="margin-left:-15px;">Imagen</label><br>

									<div class="col-xs-12" id="imagen_sala_nueva_form">
									
										<br>
										{{-- IMAGE PREVIEW del mapa de ruta del Cementario --}}
										
										<div class="preview_box" style="position:absolute;width:100px; height:100px;left:-15px;top:0px;">										

											<div class="add-image" id="image-preview-letra_sala_nueva" style="margin-left:0px; width:100px; height:100px;">
												<label for="letra_sala_nueva" id="image-label-letra_sala_nueva" title="Cambiar imagen" style="cursor:pointer;position:absolute;width:49px;height:100px;padding-left:0px;padding-top:0px;background:rgba(255, 255, 255, 0.0);"><i class="fa fa-camera" style="background:rgba(255, 255, 255, 0.5);padding:4px;border-radius: 0px 0px 8px 0px;"></i></label>											
												<input id="letra_sala_nueva"  name="mapa_letra_sala_nueva" class="inputFile" type="file" accept="image/*" base-sixty-four-input="" style="">
												
											</div>
										</div>
										<div title="Eliminar imagen" onclick="eliminar_sala_imagen_nuevo()" style="cursor:pointer;position:absolute;width:47px;height:100px;top:0px;left:38px;padding-top:0px;padding-right:0px;font-size:17px;text-align:right;background:rgba(255, 255, 255, 0.0);">
											<i class="fa fa-trash" id="image-close-letra_sala_nueva" style="background:rgba(255, 255, 255, 0.5);padding:2px 5px;border-radius: 0px 0px 0px 8px;"></i>
										</div>
										
									</div>

								</div>
								</div>
							</form><br><br><br><br><br><br>
						</div>
						<!-- <br> -->
						@if(isset($servicio) && $servicio->sala_id==-10)
							<!-- <br> -->
							<div id="campo_domicilio" class="form-group" style="padding-top: 20px">
								{!! Form::label('Domicilio') !!}
								{!! Form::text('domicilio', isset($servicio)?$servicio->domicilio:NULL, ['class' => 'form-control', 'placeholder' => 'Introduzca el domicilio']) !!}
							</div>
						@else
							<div id="campo_domicilio" class="form-group" style="padding-top: 20px">
								{!! Form::label('Domicilio') !!}
								{!! Form::text('domicilio', isset($servicio)?$servicio->domicilio:NULL, ['class' => 'form-control', 'placeholder' => 'Introduzca el domicilio']) !!}
							</div>
						@endif
						<div id="velatorios" style="padding-top: 20px">
						
							<div id="example-2">
							<label id="btnAdd-2">Velatorios</label>
							@if(!isset($servicio) || count($servicio->velatorios)==0)
								<div class="col-md-1 pull-right">
									<button type="button" id="btnAdd-2" class="btn  btn-xs" title="Agregar velatorio"><i class="fa fa-plus"></i></button>
								</div>
							@endif
							<div class="col-md-1 pull-right " id="btn_nuevo_velatorio_extra">
								<button type="button" id="btnAdd-2" class="btn  btn-xs" title="Agregar velatorio"><i class="fa fa-plus"></i></button>
							</div>
								@if(isset($servicio))
									@foreach($servicio->velatorios as $velatorio)
									@include("servicios.partials.deleteVelatorio")
									<div class="row velatorioss" >
										<div class="col-md-12">
											<div class="row " id="velatorio_{{$velatorio->id}}">
												<table border="0" align="center" width="95%" style="padding-left=20px" padding-left="10px">
													<td width="30%">
														Día
														<div class="input-group date" style="">
															<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
															</div>
															{!! Form::text('velatorio_fecha[]', \Carbon\Carbon::createFromFormat('Y-m-d',$velatorio->fecha)->format('d/m/Y'),['class' => 'form-control datepicker','id'=>'fecha_vela_'.$velatorio->id,'autocomplete'=>'off']) !!}
														</div>
													</td> 
													<td width="1.5%"><input type="hidden" name="velatorio__id[]" value="{{$velatorio->id}}"></td>
													<td width="28%">
														Desde
														<div class="bootstrap-timepicker" style="">
															<div class="input-group">
																<div class="input-group-addon">
																<i class="fa fa-clock-o"></i>
																</div>
																{!! Form::text('velatorio_desde[]', \Carbon\Carbon::createFromFormat('H:i:s',$velatorio->desde)->format('H:i'),['class' => 'form-control timepicker','autocomplete'=>'off']) !!}
															</div>
														</div>   
													</td>
													<td width="1.5%"></td>
													<td width="28%">
														Hasta
														<div class="bootstrap-timepicker">
															<div class="input-group">
																<div class="input-group-addon">
																	<i class="fa fa-clock-o"></i>
																</div>
																{!! Form::text('velatorio_hasta[]', \Carbon\Carbon::createFromFormat('H:i:s',$velatorio->hasta)->format('H:i'),['class' => 'form-control timepicker','autocomplete'=>'off']) !!}
															</div>
														</div>  
													</td>
													<td width="10%">
														<div style="margin-top:25px; margin-left:5px;">
															<button type="button" class="btn btn-xs " onclick="eliminarVela({{$velatorio->id}})" title="Eliminar"><i class="fa fa-trash"></i></button>
															<!-- @if($servicio->velatorios->last() === $velatorio)
																<button type="button" id="btnAdd-2" class="btn  btn-xs" title="Agregar velatorio"><i class="fa fa-plus"></i></button>
															@endif								 -->
															<span id="btn_nuevo_vela_{{$velatorio->id}}" class="velatorio_btn hide">
																<button type="button" id="btnAdd-2" class="btn  btn-xs" title="Agregar velatorio"><i class="fa fa-plus"></i></button>
															</span>							
														</div>														
													</td>
												</table>
											</div>
										</div>
									</div>
									@endforeach
								@endif
								<!-- <br> -->
								
								<div class="row" id="nuevo_velatorio">
									<div class="col-md-12">
										<div class="row group">
											<table border="0" align="center" width="95%" style="padding-left=20px" padding-left="10px">
												<td width="30%">
													Día
													<div class="input-group date" style="">
														<div class="input-group-addon">
														<i class="fa fa-calendar"></i>
														</div>
														{!! Form::text('velatorio_fecha[]', '',['class' => 'form-control datepicker','autocomplete'=>'off']) !!}
														<!-- <input type="text" class="form-control datepicker" name="velatorio_fecha[]"> -->
													</div>
												</td> 
												<td width="1.5%"></td>
												<td width="28.5%">
													Desde
													<div class="bootstrap-timepicker" style="">
														<div class="input-group">
															<div class="input-group-addon">
															<i class="fa fa-clock-o"></i>
															</div>
															{!! Form::text('velatorio_desde[]', '',['class' => 'form-control timepicker','autocomplete'=>'off']) !!}
														</div>
													</div>   
												</td>
												<td width="1.5%"></td>
												<td width="28.5%">
													Hasta
													<div class="bootstrap-timepicker" style="">
														<div class="input-group">
															<div class="input-group-addon">
																<i class="fa fa-clock-o"></i>
															</div>
															{!! Form::text('velatorio_hasta[]', '',['class' => 'form-control timepicker','autocomplete'=>'off']) !!}
														</div>
													</div>  
												</td>
												<td align="">
													<div style="margin-top:25px; margin-left:5px;">
														<button type="button" class="btn btn-xs btnRemove"><i class="fa fa-trash"></i></button>
													</div>
												</td>
											</table>
										</div>
									</div>
								</div>
								
							</div>
							
						</div>
					</div>
				</div>
			</div> {{-- /.box: datos_salas --}}

		</div><div class="col-lg-6 col-md-6">
	
			{{-- BOX: DESTINOS --}}
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Destino</h3>
				</div>
				<div class="box-body row">
					<div class="form-group col-md-12">
						
						{{-- SALIDA --}}
						
						<div class="row">
						
							<div class="col-lg-6 col-xs-12" >
							{!! Form::label('Salida') !!}
								<div class="form-group" >
									<div class="col-xs-7 salida_fecha" style="margin-left:-15px;margin-right:-20px;">
										<div class="form-group">
											<div class="input-group date" >
												<div class="input-group-addon" >
													<i class="fa fa-calendar" ></i>
												</div>
												{!! Form::text('salida_date', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $servicio->salida)->format('d/m/Y')):\Carbon\Carbon::now()->format('d/m/Y'),['class' => 'form-control pull-right datepicker2', 'id'=>'dte-salida-date','autocomplete'=>'off']) !!}
											</div>
										</div>
										
									</div>
									<div class="col-xs-6 salida_hora" style="margin-right:-30px;">
										<div class="form-group">
											<div class="bootstrap-timepicker" >
												<div class="input-group">
													<div class="input-group-addon">
														<i class="fa fa-clock-o"></i>
													</div>
													{!! Form::text('salida_time', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $servicio->salida)->format('H:i')):'',['class' => 'form-control pull-right timepicker', 'id'=>'dte-salida-hora','style'=>'','autocomplete'=>'off']) !!}
												</div>
											</div>
										</div>
										
									</div>
								</div>
																
							</div><div class="col-lg-6 col-xs-12" >
							{!! Form::label('Servicio Cementerio') !!}
								<div class="form-gruop">
									<div class="col-xs-7 servicio_fecha" style="margin-left:-15px;margin-right:-20px;">
										<div class="form-group">
											<div class="input-group date" >
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												{!! Form::text('cementerio_date', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $servicio->servicio_cementerio)->format('d/m/Y')):\Carbon\Carbon::now()->format('d/m/Y'),['class' => 'form-control pull-right datepicker2', 'id'=>'dte-cementerio-date','autocomplete'=>'off']) !!}
											</div>
										</div>
											
									</div>
									<div class="col-xs-6 servicio_hora" style="margin-right:-30px;">
										<div class="form-group">
											<div class="input-group bootstrap-timepicker" >
												<div class="input-group-addon">
													<i class="fa fa-clock-o"></i>
												</div>
												{!! Form::text('cementerio_time', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $servicio->servicio_cementerio)->format('h:i A')):'',['class' => 'form-control pull-right timepicker', 'id'=>'dte-cementerio-time','style'=>'','autocomplete'=>'off']) !!}
											</div> 
										</div>
										
									</div>
								</div>
								
							</div>
							
						</div> {{-- /.row --}}
					</div>
					
					{{-- SELECT: visible por defecto; se oculta al hacer click en la opción "agregar" --}}
					<div class="row">
						<div class="col-xs-12" style="margin-top:-15px;">
								<div class="form-group col-lg-12" id="select_destino" >
								{!! Form::label('Destino') !!}
								<div class="input-group" >
									<!-- {!! Form::select('destino_id', $destinos, isset($servicio)?$servicio->destino_id:NULL, ['id'=>'lst_destino','class' => 'form-control','onchange'=>'cambiarDestino()']) !!} -->
									<select name="destino_id" id="lst_destino" class="form-control" onchange='cambiarDestino()'>
									<option id="emp_val_nulo" value="0"></option>
										@foreach($destinos as $des)
											@if(isset($servicio) && $servicio->destino_id == $des->id)
												<option id="des_val_{{$des->id}}" value="{{$des->id}}" selected>{{$des->nombre}}</option>
											@else
												<option id="des_val_{{$des->id}}" value="{{$des->id}}" >{{$des->nombre}}</option>
											@endif
										@endforeach
											<!-- <option value="0">Agregar</option> -->
									
									</select>
									<span class="input-group-addon" title="Editar" id="btn_editar_destino" onclick="detinoEditar()">
										<i class="fa fa-edit" ></i>
									</span>
									<span class="input-group-addon" title="Agregar nuevo" onclick="detino_new()">
										<i class="fa fa-plus" ></i>
									</span>
								</div>
							</div>
						</div>
					</div>
										
					{{-- FORMULARIO: oculta por defecto; se muestra al hacer click en la opción "editar" --}}
					{!! Form::open(['url' => 'api/destinos', 'method' => 'POST', 'files' => true, 'id' => 'FormEditarDestino']) !!}
					<!-- <form enctype="multipart/form-data" name="formproy" class="form-horizontal FormEditarDestino"> -->
					<div class="row" >
						<div class="col-xs-12">
						<div id="form_destinoss" style="margin-top:-30px;">
							<div class="form-group">
							
								<div class="col-xs-12" >
									{!! Form::label('Destino') !!}
									
									<div class="input-group">
										{!! Form::text('nombre_destino_editar', null, ['class' => 'form-control', 'placeholder' => 'destino','id'=>'nombre_destino_editar']) !!}
										<span class="input-group-addon" title="Guardar" onclick="editarDestino()">
											<i class="fa fa-save"></i>
										</span>
										<span class="input-group-addon" title="Eliminar" onclick="confirmarEliminacion('id_destino_editar','nombre_destino_editar','destino')">
											<i class="fa fa-trash"></i>
										</span>
										<span class="input-group-addon" title="Regresar" onclick="close_destino()">
											<i class="fa fa-undo"></i>
										</span>
									</div>
									<input type="hidden" id="id_destino_editar" name="id_destino_editar">
									<br>
									<div class="form-group">
										{!! Form::label('Dirección') !!}
										{!! Form::text('direccion_destino_editar', null, ['class' => 'form-control', 'placeholder' => 'Dirección','id'=>'direccion_destino_editar']) !!}
									</div>
								</div>
								
								<!-- <div class="col-xs-12" id="logo_destinos">
									{!! Form::label('Cementerio') !!}
									
									
									{{-- Image preview del logo del Cementario --}}
									
									<div class="preview_box">
										<input class="file_input" type="file" name="mapafile" id="mapaFile" data-jpreview-container="#demo-1-container" multiple="false">
										<div class="preview_controls">
											<i class="fa fa-camera" id="change_image"></i>
											<i class="fa fa-times" id="remove_image"></i>
										</div>
										<div id="demo-1-container" class="jpreview_container">
										</div>
									</div>
									
									{{-- Deprecated
									<div class="add-image" id="" style="width: 100px; height: 100px;">
										<i class="fa fa-camera" style="padding-left: 5px; color: gray;"></i>
										<i class="fa fa-remove" id="image-close" style="padding-left: 60px; color:gray;"></i>
										<input id="logo_upload"  name="foto" type="file" accept="image/*" 
											style="
												position: relative;
												top: -231px;
												height: 231px;
												width: 229px;
												opacity: 0;
												border: 1px solid;">
										<img src="" id="logo_preview" style="width: 96px; height: 85px; position: absolute; padding-left: 1px; top: 45px;">
									</div>
									--}}

								</div> -->
								
								<div class="col-xs-12" id="mapa_destinos">
									{!! Form::label('Mapa ruta') !!}
									<!-- <input type="file"  id="demo2" multiple="false"> -->
									<br>
									{{-- IMAGE PREVIEW del mapa de ruta del Cementario --}}
									
									<div class="preview_box" style="width:100px; height:100px;">										

										<div class="add-image" id="image-preview-destino" style="margin-left:0px; width:100px; height:100px;">
											<label for="demo2" id="image-label-destino" title="Cambiar mapa" style="cursor:pointer;position:absolute;width:49px;height:100px;padding-left:0px;padding-top:0px;background:rgba(255, 255, 255, 0.0);"><i class="fa fa-camera" style="background:rgba(255, 255, 255, 0.5);padding:4px;border-radius: 0px 0px 8px 0px;"></i></label>											
											<input id="demo2"  name="mapa_destino" class="inputFile" type="file" accept="image/*" base-sixty-four-input="" style="">
											
										</div>
									</div>
									<div title="Eliminar mapa" onclick="eliminar_img_destino_editar()" style="cursor:pointer;position:absolute;width:47px;height:100px;top:25px;left:67px;padding-top:0px;padding-right:0px;font-size:17px;text-align:right;background:rgba(255, 255, 255, 0.0);">
										<i class="fa fa-trash" id="image-close-destino" style="background:rgba(255, 255, 255, 0.5);padding:2px 5px;border-radius: 0px 0px 0px 8px;"></i>
									</div>
									
								</div>
							</div>
						</div>
						</div>
					</div>
						
					<!-- </form> -->
					{!! Form::close() !!}
					<div class="row">
						<div class="col-xs-12">
						{{-- FORMULARIO: oculta por defecto; se muestra al hacer click en la opción "agregar" --}}
						{!! Form::open(['url' => 'api/destinos', 'method' => 'POST', 'files' => true, 'id' => 'guardarNuevoDestino']) !!}
						<div id="form_destino_nuevo" style="margin-top:-31px;">
							<div class="form-group">
								<div class="col-xs-12" >
									{!! Form::label('Destino') !!}
									
									<div class="input-group">
										<!-- {!! Form::text('nombre_destino_nuevo', null, ['class' => 'form-control', 'placeholder' => 'destino','id'=>'nombre_destino_nuevo']) !!} -->
										<input type="text" name="nombre_destino_nuevo" id="nombre_destino_nuevo" placeholder="Escribir nombre" class="form-control">
										<span class="input-group-addon" title="Guardar" onclick="nuevoDestino()">
											<i class="fa fa-save"></i>
										</span>
										<span class="input-group-addon" title="Regresar" onclick="close_destino()">
											<i class="fa fa-undo"></i>
										</span>
									</div>
									<br>
									<div class="form-group">
										{!! Form::label('Dirección') !!}
										<!-- {!! Form::text('direccion_destino_nuevo', null, ['class' => 'form-control', 'placeholder' => 'Dirección','id'=>'direccion_destino_nuevo']) !!} -->
										<input type="text" name="direccion_destino_nuevo" id="direccion_destino_nuevo" placeholder="Escribir direccion" class="form-control">
									</div>
								</div>
								
								<!-- <div class="col-lg-6" id="logo_destino">
									{!! Form::label('Cementerio') !!}
									
									
									{{-- Image preview del logo del Cementario --}}
									
									<div class="preview_box">
										<input class="file_input" type="file" name="test1" id="demo" data-jpreview-container="#demo-1-container" multiple="false">
										<div class="preview_controls">
											<i class="fa fa-camera" id="change_image"></i>
											<i class="fa fa-times" id="remove_image"></i>
										</div>
										<div id="demo-1-container" class="jpreview_container">
										</div>
									</div>
									
									{{-- Deprecated
									<div class="add-image" id="" style="width: 100px; height: 100px;">
										<i class="fa fa-camera" style="padding-left: 5px; color: gray;"></i>
										<i class="fa fa-remove" id="image-close" style="padding-left: 60px; color:gray;"></i>
										<input id="logo_upload"  name="foto" type="file" accept="image/*" 
											style="
												position: relative;
												top: -231px;
												height: 231px;
												width: 229px;
												opacity: 0;
												border: 1px solid;">
										<img src="" id="logo_preview" style="width: 96px; height: 85px; position: absolute; padding-left: 1px; top: 45px;">
									</div>
									--}}
									
									
									
								</div> -->
								
								<div class="col-xs-12" id="mapa_destinos">
									{!! Form::label('Mapa ruta') !!}
									<!-- <input type="file"  id="demo2" multiple="false"> -->
									<br>
									{{-- IMAGE PREVIEW del mapa de ruta del Cementario --}}
									
									<div class="preview_box" style="width:100px; height:100px;">										

										<div class="add-image" id="image-preview-destino-nuevo" style="margin-left:0px; width:100px; height:100px;">
											<label for="demo3" id="image-label-destino-nuevo" title="Cambiar mapa" style="cursor:pointer;position:absolute;width:49px;height:100px;padding-left:0px;padding-top:0px;background:rgba(255, 255, 255, 0.0);"><i class="fa fa-camera" style="background:rgba(255, 255, 255, 0.5);padding:4px;border-radius: 0px 0px 8px 0px;"></i></label>											
											<input id="demo3"  name="mapa_destino" class="inputFile" type="file" accept="image/*" base-sixty-four-input="" style="">
											
										</div>
									</div>
									<div title="Eliminar mapa" onclick="eliminar_destino_mapa_nuevo()" style="cursor:pointer;position:absolute;width:47px;height:100px;top:25px;left:67px;padding-top:0px;padding-right:0px;font-size:17px;text-align:right;background:rgba(255, 255, 255, 0.0);">
										<i class="fa fa-trash" id="image-close-destino-nuevo" style="background:rgba(255, 255, 255, 0.5);padding:2px 5px;border-radius: 0px 0px 0px 8px;"></i>
									</div>
									
								</div>
								
							</div>
						</div>
						{!! Form::close() !!}
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12" style="margin-top: -5px;">
							<div class="form-group col-lg-12"  >
								<div class="checkbox">
								    <label>
										@if(isset($servicio) && $servicio->cortejo_act == 1)
								      		<input type="checkbox" name="cortejo_act" id="sel_cortejo" checked> Cortejo funebre
								      	@else
											<input type="checkbox" name="cortejo_act" id="sel_cortejo" > Cortejo funebre
								      	@endif
								    </label>
								</div>
							</div>
							<div class="form-group col-lg-12" id="text_cortejo" style="margin-top: -10px;">
								<label for="cortejo">Enlace cortejo funebre</label>
								<textarea id="cortejo" name="tex_cortejo" class="form-control" rows="2" >@if(isset($servicio)){{$servicio->cortejo_link}}@endif</textarea>
							</div>
						</div>
					</div>
				</div>
						
			</div> {{-- /.box: destinos --}}
			
			{{-- BOX: MISAS --}}
			<div class="box" >
				<div class="box-header">
					<h3 class="box-title" id="titulo_misa_responso">Misa / Responso</h3>
				</div>
				
				<div class="box-body row">
					
					<div class="col-lg-12" id="select_lugar">
						<div class="form-group">
							{!! Form::label('Lugar') !!}
							<div class="input-group">
								<!-- {!! Form::select('responso_id', $responso, isset($servicio) ? $servicio->responso_id : NULL, ['placeholder' => '', 'id'=>'lst_responso','class' => 'form-control']) !!} -->
								<select name="responso_id" id="responso_select" class="form-control" onchange="cambiarMisa()">
											<option id="responso_selectn" value="0"></option>
									@foreach($responso as $resp)
										@if(isset($servicio) && $servicio->responso_id == $resp->id)
											@if($resp->tipo==0)
												<option id="resp_val_{{$resp->id}}" value="{{$resp->id}}" selected>Misa: {{$resp->nombre}}</option>
											@else	
											<option id="resp_val_{{$resp->id}}" value="{{$resp->id}}" selected>Responso: {{$resp->nombre}}</option>
											@endif
										@else
											@if($resp->tipo==0)
												<option id="resp_val_{{$resp->id}}" value="{{$resp->id}}">Misa: {{$resp->nombre}}</option>
											@else	
											<option id="resp_val_{{$resp->id}}" value="{{$resp->id}}">Responso: {{$resp->nombre}}</option>
											@endif
										@endif
									@endforeach
										<!-- <option value="0">Agregar</option> -->
								
								</select>
								<span class="input-group-addon" title="Editar" id="btn_editar_misa" onclick="responso_edit()">
									<i class="fa fa-edit"></i>
								</span>
								<span class="input-group-addon" title="Agregar nuevo" onclick="responso_nuevo()">
									<i class="fa fa-plus"></i>
								</span>
							</div>
						</div>
						
					<!-- </div> -->
						<div class="row" id="fecha_responso">
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
										<div class="col-xs-7" style="margin-left:-15px;margin-right:-20px;">
											{!! Form::label('Fecha') !!}
											<br>
											<div class="form-group " id="dte-responso-date">
												<div class="input-group date" style="width:100%;">
													<div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													</div>
													{!! Form::text('responso_date', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $servicio->responso)->format('d/m/Y')):\Carbon\Carbon::now()->format('d/m/Y'),['class' => 'form-control pull-right datepicker2', 'id'=>'dte-responso-date','autocomplete'=>'off']) !!}
												</div>	
											</div>
										</div>
										<div class="col-xs-6" style="margin-right:-30px;">
											{!! Form::label('Hora:') !!}
											<br>
											<div class="form-group ">	
												<div class="input-group bootstrap-timepicker" style="width:100%;">
													<div class="input-group-addon">
														<i class="fa fa-clock-o"></i>
													</div>
													{!! Form::text('responso_time', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $servicio->responso)->format('H:i')):'',['class' => 'form-control pull-right timepicker', 'id'=>'dte-responso-hora','autocomplete'=>'off']) !!}
												</div>			
											</div>
										</div>
								</div>
								
							</div>
							
						</div>	
					</div>
					
					{{-- FORMULARIO: Oculto por defecto; se puestra al seleccionar "EDITAR" en el select --}}
					<div class="col-lg-12" id="form_responso_editar">
					<!-- {!! Form::open(['url' => 'responso/misa', 'method' => 'POST', 'files' => true, 'id' => 'guardarLugar']) !!} -->
						
						<div class="form-group" id="agregar-lugar">
							{!! Form::label('Lugar') !!}
							<div class="input-group">
								{!! Form::text('nombre_responso_misa', '', ['class' => 'form-control', 'placeholder'=> 'Introduzca el lugar','id'=>'lugar_editar_misa']) !!}
								<div class="input-group-addon" onclick="guardar_misa_editar()">
									<i class="fa fa-save"></i>
								</div>
								<div class="input-group-addon" title="Eliminar" onclick="confirmarEliminacion('misa_editar_id','lugar_editar_misa','responso')">
									<i class="fa fa-trash" ></i>
								</div>
								<div class="input-group-addon" title="Regresar" onclick="regresar_misa()">
									<i class="fa fa-undo" ></i>
								</div>
								<input type="hidden" id="misa_editar_id">
							</div>
						</div>						
						<div class="form-group">
							{!! Form::label('Dirección') !!}
							{!! Form::text('direccion_responso_misa', null, [
								'class' => 'form-control', 
								'placeholder' => 'Dirección',
								'id'=>'direccion_editar_misa'
							]) !!}
						</div>
						<div class="form-group" id="tipo_misa_cont">
															
						</div>
						<!-- <div class="row">
							<div class="col-lg-12">
								{!! Form::label('fecha-hora', 'Fecha - hora:') !!}
							</div>
							
							<div class="form-group col-lg-6 col-md-6 col-sm-6" id="dte-responso-date">
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									{!! Form::text('responso_date', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $servicio->responso)->format('d/m/Y')):\Carbon\Carbon::now()->format('d/m/Y'),['class' => 'form-control pull-right datepicker', 'id'=>'dte-responso-date']) !!}
								</div>	
							</div>
							
							<div class="form-group col-lg-6 col-md-6 col-sm-6">	
								<div class="input-group bootstrap-timepicker">
									<div class="input-group-addon">
										<i class="fa fa-clock-o"></i>
									</div>
									{!! Form::text('responso_time', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $servicio->responso)->format('H:i')):'',['class' => 'form-control pull-right timepicker', 'id'=>'dte-responso-hora']) !!}
								</div>			
							</div>
						</div> -->

					<!-- {!! Form::close() !!} -->
					</div> {{-- /.FORMULARIO --}}

					{{-- FORMULARIO: Oculto por defecto; se puestra al seleccionar "Agregar" en el select --}}
					<div class="col-lg-12" id="form_responso_nuevo">
					{!! Form::open(['url' => 'responso/misa', 'method' => 'POST', 'files' => true, 'id' => 'guardarLugar']) !!}
						
						<div class="form-group" id="agregar-lugar">
							{!! Form::label('Lugar') !!}
							<div class="input-group">
								{!! Form::text('nombre_responso_misa', '', ['class' => 'form-control', 'placeholder'=> 'Introduzca el lugar','id'=>'lugar_nueva_misa']) !!}
								<div class="input-group-addon" title="Guardar" onclick="guardar_misa_new()">
									<i class="fa fa-save"></i>
								</div>
								<div class="input-group-addon" title="Regresar" onclick="regresar_misa()">
									<i class="fa fa-undo" ></i>
								</div>
							</div>
						</div>
						
						<div class="form-group">
							{!! Form::label('Dirección') !!}
							{!! Form::text('direccion_responso_misa', null, [
								'class' => 'form-control', 
								'placeholder' => 'Dirección',
								'id'=>'direccion_nueva_misa'
							]) !!}
						</div>
						<div class="form-group">
							<input type="radio" name="tipo_misa_nueva" id="tipo_nueva_misa1" value="misa" >
							{!! Form::label('misa', 'Misa')!!} <span style="padding-left: 20px;"></span>
							<input type="radio" name="tipo_misa_nueva" id="tipo_nueva_misa2" value="responso" >
							{!! Form::label('responso', 'Responso') !!}
						</div>
						<!-- <div class="row">
							<div class="col-lg-12">
								{!! Form::label('fecha-hora', 'Fecha - hora:') !!}
							</div>
							
							<div class="form-group col-lg-6 col-md-6 col-sm-6" id="dte-responso-date">
								<div class="input-group date">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									{!! Form::text('responso_date', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $servicio->responso)->format('d/m/Y')):\Carbon\Carbon::now()->format('d/m/Y'),['class' => 'form-control pull-right datepicker', 'id'=>'dte-responso-date']) !!}
								</div>	
							</div>
							
							<div class="form-group col-lg-6 col-md-6 col-sm-6">	
								<div class="input-group bootstrap-timepicker">
									<div class="input-group-addon">
										<i class="fa fa-clock-o"></i>
									</div>
									{!! Form::text('responso_time', isset($servicio)?(\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $servicio->responso)->format('H:i')):'',['class' => 'form-control pull-right timepicker', 'id'=>'dte-responso-hora']) !!}
								</div>			
							</div>
						</div> -->

					{!! Form::close() !!}
					</div> {{-- /.FORMULARIO --}}
					
				</div> {{-- /.box-body --}}
			</div> {{-- /.box: misas --}}
			
		</div>		
	</div>
</div>
{{-- {!! Form::close() !!} --}}
</form>
<!-- Modal -->
<div class="modal fade" id="confirmarEliminacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirmar</h4>
      </div>
      <div class="modal-body">
        <p id="msj_eliminar"> Hola </p>
				<input type="hidden" id="id_campo_eliminar">
				<input type="hidden" id="tipo_eliminacion">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-danger" onclick="confirmadoEliminacion()">Confirmar</button>
      </div>
    </div>
  </div>
</div>

@section('script')
<script type="text/javascript" src="{{ url('js/jpreview.js') }}"></script>
<script>
	'use strict';

	var url = 'http://localhost/cartelera_backend_laravel/public/';

	function btn_velatorio_nuevo(){
		$(".velatorio_btn").last().removeClass('hide');
		// var cont = 0;

		// $('.velatorioss').each(function(){

		// })

	}

	function eliminar_foto_perfil(){
		// alert($('#id_servicios_foto').val());
		var id = $('#id_servicios_foto').val();
		$.ajax({
			type:'DELETE',
			url: "./?ruta=servicios/eliminar_img&id="+id, 
			data :{
				'_token': '{{ csrf_token() }}'				
			},
			success: function(result){
				console.log(result);
				$('#foto_perfil').val(null);
				$('#image-preview-perfil').css('background-image','url("")');
				$('#image-preview-perfil').css('background','white');
				// detinoEditar();
			},error:function(err){
				console.log(err);
			}
		});
	}
	function eliminar_foto_perfil_nueva(){
		$('#foto_perfil').val(null);
		$('#image-preview-perfil').css('background-image','url("")');
		$('#image-preview-perfil').css('background','white');
	}
	function confirmarEliminacion(x,y,tipo){
		var id = $('#'+x).val();
		var nombre = $('#'+y).val();
		$('#id_campo_eliminar').val(id);
		$('#tipo_eliminacion').val(tipo);
		if(tipo == 'empresa'){
			$('#msj_eliminar').text('¿Desea eliminar la empresa '+nombre+'?');
		}
		if(tipo == 'destino'){
			$('#msj_eliminar').text('¿Desea eliminar el destino '+nombre+'?');
		}
		if(tipo == 'sala'){
			$('#msj_eliminar').text('¿Desea eliminar la sala '+nombre+'?');
		}
		if(tipo == 'responso'){
			$('#msj_eliminar').text('¿Desea eliminar la misa o responso '+nombre+'?');
		}
		
		$('#confirmarEliminacion').modal('toggle');
	}
	function confirmadoEliminacion(){
		var id_elem = $('#id_campo_eliminar').val();
		var tipo_elem = $('#tipo_eliminacion').val();
		if(tipo_elem == 'empresa'){
			eliminar_empresa(id_elem);
		}
		if(tipo_elem == 'destino'){
			eliminar_destino(id_elem);
		}
		if(tipo_elem == 'sala'){
			eliminar_sala(id_elem);
		}
		if(tipo_elem == 'responso'){
			eliminar_misa(id_elem);
		}
	}

	// +++++++++++++++++++++++++++++++++++ SALA +++++++++++++++++++++++++++++++++++++++++++++++//

	function cambiarsala(){
		var sala = $('#lst_sala').val();
		console.log(sala);
		if(sala==-10){
			// $("#direccion_domicio_actual").hide();
			$("#velatorios").show();
			$("#campo_domicilio").show();
			$("#btn_home").show();
			$("#btn_privado").hide();
			$("#btn_editar_sala").hide();
		}else if(sala==99 || sala==100 || sala==0){
			$("#btn_privado").show();
			$("#btn_home").hide();
			$("#velatorios").hide();
			$("#campo_domicilio").hide();
			$("#btn_editar_sala").hide();
			// $("#direccion_domicio_actual").hide();
		}else{
			$("#velatorios").show();
			$("#btn_editar_sala").show();
			$("#campo_domicilio").hide();
			$("#btn_home").hide();
			$("#btn_privado").hide();
			// $("#direccion_domicio_actual").hide();
		}
	}
	function sala_edit(){
		var id = $('#lst_sala').val();
		// console.log('sala: '+id);
		$('.status_sala_editar').each(function(){
			$(this).remove();
		})
		$.ajax({
			type:'GET',
			url: "./?ruta=servicio/sala&id="+id, 
			data : '',
			success: function(result){
				console.log('SALA',result);
				$('#id_sala_editar').val(result.id);
				$('#nombre_sala_editar').val(result.nombre);
				$('#codigo_sala_editar').val(result.codigo);
				if(result.status==1){
					// $('#status_sala_editar').attr('checked','checked');
					$('#status_sala_edit').append('<span class="status_sala_editar"><input id="status_sala_editar" type="checkbox" checked > Activa</span>');
				}else{
					$('#status_sala_edit').append('<span class="status_sala_editar"><input id="status_sala_editar" type="checkbox" > Activa</span>');
				}
				// $('#icono_sala_editar').attr('src','../images/salas/'+result.icono);
				// $('#image-preview-icono_sala').css('background-color','white');
				$('#image-preview-icono_sala').css('background-image','url(./images/salas/'+result.icono+')');
				// $('#image-preview-letra_sala').css('background-color','white');
				$('#image-preview-letra_sala').css('background-image','url(./images/salas/'+result.letra+')');
				$('#sala_select').hide();
				$('#titulo_sala').hide();
				$('#form_editar_sala').show();
			},error:function(err){
				console.log(err);
			}
		});
	}
	function regresar_sala(){
		$('#form_editar_sala').hide();
		$('#form_nueva_sala').hide();
		$('#sala_select').show();
	}
	function editar_sala(){
		var data = new FormData();
		// icono
		var input = $('#icono_sala_edit');
		$.each(input[0].files, function(i, file) {
			data.append('file_icono-'+i, file);				
		});
		// letra
		var input = $('#letra_sala_edit');
		$.each(input[0].files, function(i, file) {
			data.append('file_letra-'+i, file);				
		});

		var id = $('#id_sala_editar').val();
		
		data.append("nombre_sala", $('#nombre_sala_editar').val());
		data.append("codigo_sala", $('#codigo_sala_editar').val());
		

		if($('#status_sala_editar').prop('checked')){
			// status=1;
			data.append("status",1);
		}else{
			// status=0;
			data.append("status",0);
		}

		$.ajax({
			type:'POST',
			url: "./?ruta=servicios/sala/editar&id="+id, 
			data: data,
			cache: false,
			contentType: false,
			processData: false,
			success: function(result){
				// console.log('EDITAR SALA',result);
				$('#lst_sala').val(result.sala.id);
				$('#sal_val_'+id).text(result.sala.nombre);
				$('#form_editar_sala').hide();
				$('#sala_select').show();

				$.ajax({
					type:'POST',
					url: "http://cr.neo.co/modulos/cc0/public/?ruta=api/editar_sala", 
					data: {
						"nombre":result.sala.nombre,
						"idsala":result.sala.id
					},
					success: function(result){
						console.log('SALA CR EDIT',result);
					},error:function(err){
						console.log(err);
					}
				})
			},error:function(err){
				console.log(err);
			}
		});
	}
	function sala_new(){
		$('#sala_select').hide();
		$('#titulo_sala').hide();
		$('#form_nueva_sala').show();
	}
	function guardar_nueva_sala(){
		var data = new FormData();
		// icono
		var input = $('#icono_sala_nueva');
		$.each(input[0].files, function(i, file) {
			data.append('file_icono-'+i, file);				
		});
		// letra
		var input = $('#letra_sala_nueva');
		$.each(input[0].files, function(i, file) {
			data.append('file_letra-'+i, file);				
		});

		// var id = $('#id_sala_editar').val();
		
		data.append("nombre_sala", $('#nombre_sala_nueva').val());
		data.append("codigo_sala", $('#codigo_sala_nueva').val());
		 

		if($('#status_sala_nueva').prop('checked')){
			// status=1;
			data.append("status",1);
		}else{
			// status=0;
			data.append("status",0);
		}

		$.ajax({
			type:'POST',
			url: "./?ruta=servicios/sala/nueva", 
			data: data,
			cache: false,
			contentType: false,
			processData: false,
			success: function(result){
				console.log(result);
				$('#lst_sala').val(result.sala.id);
				// $('#sal_val_'+id).text(result.sala.nombre);
				// $('#form_editar_sala').hide();
				// $('#sala_select').show();

				// capturar el select
				var lst_sala = document.getElementById("lst_sala");
						
				// determinar cuantos options tiene el select
				var pos = lst_sala.length - 3 ;
				
				// crear el option nuevo: value y text
				var opt = document.createElement("option");
				opt.value = result.sala.id;
				opt.text = result.sala.nombre;
				opt.id = 'sal_val_'+result.sala.id;
				
				// agregar el option nuevo justo entes del último
				lst_sala.add(opt, pos);
				
				regresar_sala();
				
				// seleccionar la nueva opcion
				lst_sala.selectedIndex = pos;
				
				cambiarsala();

				// $.ajax({
				// 	type:'POST',
				// 	url: "http://localhost:8080/modulos/cc0/public/?ruta=api/crear_sala", 
				// 	data: {
				// 		"nombre":result.sala.nombre,
				// 		"idsala":result.sala.id
				// 	},
				// 	success: function(result){
				// 		console.log('SALA CR',result);
				// 	},
				// 	error: function(err){
				// 		console.log(err);
				// 	}
				// });

			},error:function(err){
				console.log(err);
			}
		});
	}
	function eliminar_sala(x){
		// var id = $('#id_sala_editar').val();
		var id = x;
		console.log(id);
		$.ajax({
			type:'DELETE',
			url: "./?ruta=servicios/sala/eliminar&id="+id, 
			data : {
				'_token': '{{ csrf_token() }}'
			},success: function(result){
				// console.log('ELIMINAR SALA',result);
				$('#lst_sala').val(0);
				$('#sal_val_'+id).hide();
				regresar_sala();
				cambiarsala();
				$('#confirmarEliminacion').modal('toggle');

				$.ajax({
					type:'POST',
					url: "http://cr.neo.co/modulos/cc0/public/?ruta=api/delete_sala", 
					data: {
						"idsala":id
					},
					success: function(result){
						console.log('SALA CR DELETE',result);
					},error:function(err){
						console.log(err);
					}
				})

			},error:function(err){
				console.log(err);
			}
		});
	}
	function eliminar_sala_icono_editar(){
		var id = $('#id_sala_editar').val();
		$.ajax({
			type:'DELETE',
			url: "./?ruta=servicios/sala/eliminar_ico&id="+id, 
			data :'',
			success: function(result){
				console.log(result);
				$('#icono_sala_edit').val(null);
				$('#image-preview-icono_sala').css('background-image','url("")');
				sala_edit();
			},error:function(err){
				console.log(err);
			}
		});
	}
	function eliminar_sala_imagen_editar(){
		var id = $('#id_sala_editar').val();
		$.ajax({
			type:'DELETE',
			url: "./?ruta=servicios/sala/eliminar_img&id="+id, 
			data :'',
			success: function(result){
				console.log(result);
				$('#letra_sala_edit').val(null);
				$('#image-preview-letra_sala').css('background-image','url("")');
				sala_edit();
			},error:function(err){
				console.log(err);
			}
		});
	}
	function eliminar_sala_icono_nuevo(){
		$('#icono_sala_nueva').val(null);
		$('#image-preview-icono_sala_nueva').css('background-image','url("")');
		$('#image-preview-icono_sala_nueva').css('background','white');
	}
	function eliminar_sala_imagen_nuevo(){
		$('#letra_sala_nueva').val(null);
		$('#image-preview-letra_sala_nueva').css('background-image','url("")');
		$('#image-preview-letra_sala_nueva').css('background','white');
	}

	//+++++++++++++++++++++++++++++++++ VELATORIO ++++++++++++++++++++++++++++++++++++++++++++++//

	function eliminarVela(x){
		// alert(x);
		// $('#velatorio_'+x).hide();
		$.ajax({
			type:'DELETE',
			url: "./?ruta=servicios/velatorio/eliminar&id="+x, 
			data : {
				'_token': '{{ csrf_token() }}'
			},success: function(result){
				console.log(result);
				
				$('#fecha_vela_'+x).val(null);
				// $('#velatorio_'+x).hide();
				$('#velatorio_'+x).remove();
				btn_velatorio_nuevo();

			},error:function(err){
				console.log(err);
			}
		});
		
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ******************************* MISA / RESPONSO ++++++++++++++++++++++++++++++++++++++++//
	// MOSTRAR EL DESTINO SELECCIONADO
	function cambiarMisa(){
			var misa_id=$('#responso_select').val()
			if(misa_id>0){
				$('#btn_editar_misa').show();
				$.ajax({
					type:'GET',
					url: "./?ruta=servicios/misa&id="+misa_id, 
					data : {
						'_token': '{{ csrf_token() }}'
					},success: function(result){
						// $('#direccion_responso').show();
						// $('#tipo_responso').show();
						$('#fecha_responso').show();
						if(result.tipo==0){
							$('#titulo_misa_responso').text('Misa');
							// $('#tipo_misa1').attr('checked');	$('#tipo_misa2').removeAttr('checked');						
						}
						if(result.tipo==1){
							$('#titulo_misa_responso').text('Responso');
							// $('#tipo_misa2').attr('checked');$('#tipo_misa1').removeAttr('checked');
						}
						// $('#direccion_misa').text(result.direccion);
					},error:function(err){
						console.log(err);
					}
				});
			}else{
				$('#titulo_misa_responso').text('Misa / Responso');
				// $('#direccion_responso').hide();
				// $('#tipo_responso').hide();
				$('#fecha_responso').hide();
				$('#btn_editar_misa').hide();
			}			
		}
	function responso_edit(){
		$('#select_lugar').hide();
		$('#form_responso_editar').show();
		$('.tipo_misa_cont2').each(function(){
			$(this).remove();
		})
		var misa_id=$('#responso_select').val()
			if(misa_id>0){
				$.ajax({
					type:'GET',
					url: "./?ruta=servicios/misa&id="+misa_id, 
					data : {
						'_token': '{{ csrf_token() }}'
					},success: function(result){
						$('#misa_editar_id').val(result.id);
						$('#lugar_editar_misa').val(result.nombre);
						$('#direccion_editar_misa').val(result.direccion);
						// $('input:radio[name=tipo_misa_editar]:checked').removeAttr('checked');
						// $('#tipo_editar_misa2').removeAttr('checked');
						if(result.tipo==0){
							// $('#tipo_editar_misa1').attr('checked','checked');
							$('#tipo_misa_cont').append('<span class="tipo_misa_cont2"><input type="radio" checked name="tipo_misa_editar" id="tipo_editar_misa1" value="misa" ><label>Misa</label> <span style="padding-left: 20px;"></span><input type="radio" name="tipo_misa_editar" id="tipo_editar_misa2" value="responso" ><label>Responso</label></span>');
						}
						if(result.tipo==1){
							// $('#tipo_editar_misa2').attr('checked','checked');
							$('#tipo_misa_cont').append('<span class="tipo_misa_cont2"><input type="radio" checked name="tipo_misa_editar" id="tipo_editar_misa1" value="misa" ><label>Misa</label> <span style="padding-left: 20px;"></span><input type="radio" checked name="tipo_misa_editar" id="tipo_editar_misa2" value="responso" ><label>Responso</label></span>');
						}
					},error:function(err){
						console.log(err);
					}
				});
			}	
	}
	function regresar_misa(){
		$('#select_lugar').show();
		$('#fecha_responso').show();
		$('#form_responso_editar').hide();
		$('#form_responso_nuevo').hide();
	}
	function responso_nuevo(){
		$('#select_lugar').hide();
		$('#fecha_responso').hide();
		$('#form_responso_nuevo').show();
	}
	function guardar_misa_new(){
		var misa = $('#lugar_nueva_misa').val();
		var direccion = $('#direccion_nueva_misa').val();
		var tipo_misa = $('input:radio[name=tipo_misa_nueva]:checked').val();
		var tipo = 0;
		if(tipo_misa == 'misa'){
			tipo = 0;
		}
		if(tipo_misa == 'responso'){
			tipo = 1;
		}
			$.ajax({
					type:'POST',
					url: "./?ruta=servicios/misa/nuevo", 
					data :{
						'_token': '{{ csrf_token() }}',
						'nombre':misa,
						'direccion':direccion,
						'tipo':tipo
					},success: function(result){
						// console.log(result);
						// capturar el select
						var lst_misa = document.getElementById("responso_select");
						
						// determinar cuantos options tiene el select
						var pos = lst_misa.length ;
						
						// crear el option nuevo: value y text
						var opt = document.createElement("option");
						opt.value = result.misa.id;
						if(result.misa.tipo == 0){
							opt.text = 'Misa: '+result.misa.nombre;
						}else{
							opt.text = 'Responso: '+result.misa.nombre;
						}
						
						opt.id = 'resp_val_'+result.misa.id;
						
						// agregar el option nuevo justo entes del último
						lst_misa.add(opt, pos);
						
						regresar_misa();
						
						// seleccionar la nueva opcion
						lst_misa.selectedIndex = pos;
						
						cambiarMisa();
						$('#lugar_nueva_misa').val('');
						$('#direccion_nueva_misa').val('');
					},error:function(err){
						console.log(err);
					}
				});
	}
	function guardar_misa_editar(){
		var id = $('#misa_editar_id').val();
		var misa = $('#lugar_editar_misa').val();
		var tipo_misa = $('input:radio[name=tipo_misa_editar]:checked').val();
		var tipo = 0;
		if(tipo_misa == 'misa'){
			tipo = 0;
		}
		if(tipo_misa == 'responso'){
			tipo = 1;
		}
		var direccion = $('#direccion_editar_misa').val();	
		console.log('Tipo misa');
		// console.log(tipo_misa);
			$.ajax({
					type:'POST',
					url: "./?ruta=servicios/misa/editar&id="+id, 
					data :{
						'_token': '{{ csrf_token() }}',
						'nombre':misa,
						'direccion':direccion,
						'tipo':tipo
					},success: function(result){
						// console.log(result);
						if(result.misa.tipo == 0){
							$('#resp_val_'+result.misa.id).text('Misa: '+misa);
						}else{
							$('#resp_val_'+result.misa.id).text('Responso: '+misa);
						}
						
						regresar_misa();
						cambiarMisa();
					},error:function(err){
						console.log(err);
					}
				});
	}
	function eliminar_misa(x){
		var id = x;
		// var id = $('#misa_editar_id').val();
		// console.log(id);
		$.ajax({
			type:'DELETE',
			url: "./?ruta=servicios/misa/eliminar&id="+id, 
			data :{
				'_token': '{{ csrf_token() }}'
			},success: function(result){
				// console.log(result);
				$('#resp_val_'+id).hide();
				$('#responso_select').val(0);
				regresar_misa();
				cambiarMisa();
				$('#confirmarEliminacion').modal('toggle');
			},error:function(err){
				console.log(err);
			}
		});

	}

	// ****************************************************************************************

	// ********************************* Destino *********************************************//
	// MOSTRAR EL DESTINO SELECCIONADO
	function cambiarDestino(){
		var destino_id=$('#lst_destino').val()
		if(destino_id>0){
			$('#btn_editar_destino').show();
			// $('#destino_sel').show();
			// $.ajax({
			// 	type:'GET',
			// 	url: "../servicios/destino/"+destino_id, 
			// 	data : {
			// 		'_token': '{{ csrf_token() }}'
			// 	},success: function(result){
			// 		$('#direccion_destino').text(result.direccion);
			// 		$('#mapas_destino').attr("src", "../images/destinos/"+result.mapa);
			// 	},error:function(err){
			// 		console.log(err);
			// 	}
			// });
		}else{
			$('#btn_editar_destino').hide();
			// $('#destino_sel').hide();
			// $('#select_destino').hide();
		}			
	}
	// AL CERRAR DESTINO EDITAR, SE MUESTRA EL DESTINO SELECCIONADO
	function close_destino(){
		$('#form_destinoss').hide();
		$('#form_destino_nuevo').hide();
		$('#destino_sel').show();
		$('#select_destino').show();
	}
	// SELECCIONA EL DESTINO SELECCIONADO Y LO MUESTRA EN EL INPUT DE EDITAR
	function detinoEditar(){
		$('#form_destinoss').show();
		$('#select_destino').hide();
		$('#destino_sel').hide();
		var destino_id=$('#lst_destino').val();
		if(destino_id>0){
			$.ajax({
				type:'GET',
				url: "./?ruta=servicios/destino&id="+destino_id, 
				data : {
					'_token': '{{ csrf_token() }}'
				},success: function(result){
					$('#id_destino_editar').val(result.id);
					$('#nombre_destino_editar').val(result.nombre);
					$('#direccion_destino_editar').val(result.direccion);
					// $('#image-preview-destino').css('background-color','white');
					$('#image-preview-destino').css('background-image','url(./images/destinos/'+result.mapa+')');
				},error:function(err){
					console.log(err);
				}
			});
		}
	}
	// EDITA EL DESTINO SELECCIONADO
	function editarDestino(){

		var data = new FormData();
		var input = $('#demo2');

		$.each(input[0].files, function(i, file) {
			data.append('file-'+i, file);				
		});

		var id = $('#id_destino_editar').val();
		
		data.append("nombre_destino_editar", $('#nombre_destino_editar').val());
		data.append("direccion_destino_editar", $('#direccion_destino_editar').val());
		data.append('_token', '{{ csrf_token() }}'); 
		
		console.log(data);
		$.ajax({
				type:'POST',
				url: "./?ruta=servicios/destino/editar&id="+id, 
				data: data,			
				cache: false,
				contentType: false,
				processData: false,
				success: function(result){
					// console.log(result);
					$('#des_val_'+result.destino.id).text($('#nombre_destino_editar').val());
					$('#demo2').val(null);
					$('#image-preview-destino').css('background-image','url("")');
					// $('#image-preview-destino').css('background','white');
					close_destino();
					cambiarDestino();
				},error:function(err){
					console.log(err);
				}
			});
	}
	function detino_new(){
		$('#form_destino_nuevo').show();
		$('#select_destino').hide();
		$('#destino_sel').hide();
	}
	// AGREGA UN NUEVO DESTINO
	function nuevoDestino(){
		var data = new FormData();
		var input = $('#demo3');

		$.each(input[0].files, function(i, file) {
			data.append('file-'+i, file);				
		});
		
		data.append("nombre_destino_nuevo", $('#nombre_destino_nuevo').val());
		data.append("direccion_destino_nuevo", $('#direccion_destino_nuevo').val());
		data.append('_token', '{{ csrf_token() }}');
		$.ajax({
				type:'POST',
				url: "./?ruta=servicios/destino/nuevo", 
				data: data,
				cache: false,
				contentType: false,
				processData: false,
				success: function(result){
					// console.log(result);
					var destino_nuevo = result.destino;
					
					// capturar el select
					var lst_destino = document.getElementById("lst_destino");
					
					// determinar cuantos options tiene el select
					var pos = lst_destino.length ;
					
					// crear el option nuevo: value y text
					var opt = document.createElement("option");
					opt.value = result.destino.id;
					opt.text = result.destino.nombre;
					opt.id = 'des_val_'+result.destino.id;
					
					// agregar el option nuevo justo entes del último
					lst_destino.add(opt, pos);
					
					close_destino();
					
					// seleccionar la nueva opcion
					lst_destino.selectedIndex = pos;
					
					$('#nombre_destino_nuevo').val('');
					$('#direccion_destino_nuevo').val('');
					$('#demo3').val(null);
					$('#image-preview-destino-nuevo').css('background-image','url("")');
					
					eliminar_destino_mapa_nuevo();
					cambiarDestino();

				},error:function(err){
					console.log(err);
				}
			});
	}
	function eliminar_img_destino_editar(){
		var id = $('#id_destino_editar').val();
		$.ajax({
			type:'DELETE',
			url: "./?ruta=servicios/destino/eliminar_img&id="+id, 
			data :{
				'_token': '{{ csrf_token() }}'				
			},
			success: function(result){
				console.log(result);
				$('#demo2').val(null);
				$('#image-preview-destino').css('background-image','url("")');
				detinoEditar();
			},error:function(err){
				console.log(err);
			}
		});
	}
	function eliminar_destino_mapa_nuevo(){
		$('#demo3').val(null);
		$('#image-preview-destino-nuevo').css('background-image','url("")');
		$('#image-preview-destino-nuevo').css('background','white');
	}
	function eliminar_destino(x){
		var id = x;
		// var id = $('#id_destino_editar').val();
		console.log(id);
		$.ajax({
			type:'DELETE',
			url: "./?ruta=servicios/destino/eliminar&id="+id, 
			data :{
				'_token': '{{ csrf_token() }}'				
			},
			success: function(result){
				console.log(result);
				$('#des_val_'+id).hide();
				$('#lst_destino').val(0);
				close_destino();
				cambiarDestino();
				$('#confirmarEliminacion').modal('toggle');
			},error:function(err){
				console.log(err);
			}
		});
	}

	// CORTEJO

	function cortejo(){
		if($('#sel_cortejo').prop('checked')){
			$('#text_cortejo').show();
		}else{
			$('#text_cortejo').hide();
		}
	}
		
	//*********************************************************************** */

	$(document).ready(function(){
		btn_velatorio_nuevo();

		$('#text_cortejo').hide();
		cortejo();
		$('#sel_cortejo').on('change',function(){
			if($(this).prop('checked')){
				$('#text_cortejo').show();
			}else{
				$('#text_cortejo').hide();
			}
		})

		$('#btn_nuevo_velatorio_extra').hide();
		// $('#image-preview-perfil').css('background-image','url(../images/servicios/'+result.mapa+')');
		/*********************** INICIALIZA EL FORMULARIO ***********************/
		
		// if($('#lst_sala').val()=='0') $('#div_domicilio').removeClass('hide');
		// else $('#div_domicilio').addClass('hide');
		
		// if($('#lst_sala').val()=='99') $('#div_confirmar').removeClass('hide');
		// else $('#div_confirmar').addClass('hide');

		// if($('#lst_sala').val()!='99' || $('#lst_sala').val()!='0') $('#div_confirmar').removeClass('hide');
		// else $('#div_confirmar').addClass('hide');

		// $('#btn_nuevo_vela').hide();

		$('#form_editar_sala').hide();
		$('#form_nueva_sala').hide();
		$('#nuevo_velatorio').hide();
		// $("#campo_domicilio").hide();
		$("#velatorios").hide();
		$("#form_responso_editar").hide();
		$("#form_responso_nuevo").hide();
		$("#form_destinoss").hide();
		$("#form_destino_nuevo").hide();
		$("#logo_destino").hide();
		$("#mapa_destino").hide();
		$("#seleccionar-lugar").hide();
		// $("#dte-responso-date").hide();
		// $("#dte-responso-hora").hide();
		// $("#select_lugar").hide();
		// $('#agregar-lugar').hide();
		cambio_emp_sel();
		cambiarsala();
		cambiarDestino();
		cambiarMisa();
		// Guarda la selección previa de todos los select para restaurarla
		// en caso de cancelar la edición.
		var previous_responso = $("#lst_responso").prop('selectedIndex');
		var previous_empresa = $("#lst_empresa").prop('selectedIndex');
		var previous_destino = $("#lst_destino").prop('selectedIndex');
		console.log("Select indexes: responso = " + previous_responso + 
		            "; empresa = " + previous_empresa + 
		            "; destino = " + previous_destino);
		
		/************************************************************************/
		
		/*****************************************************/
		/*                CONTROL DE DESTINOS                */
		/*****************************************************/
		
		// $('#lst_destino').on('change', function() {
		// 	if($(this).val() == 'Agregar') {
		// 		$('#destino_sel').hide();
		// 		$('#select_destino').hide();
		// 		$("#form_destino").show();
		// 		$("#logo_destino").show();
		// 		$("#mapa_destino").show();
		// 	}
		// });
		
		$("#close_form_destino").on('click', function() {
			$("#form_destino").hide();
			$("#logo_destino").hide();
			$("#mapa_destino").hide();
			$('#lst_destino').prop('selectedIndex', previous_destino);
			$('#select_destino').show();
			$('#destino_sel').show();
			cambiarDestino();
			$('#form_destinoss').hide();
		});
		
		$("#load_logo_destino").click(function() {
			$("#destino_logo_upload").click();
		})
		
		
		$('#btn-add-foto').on('change', function(evt){

			var files = evt.target.files; // FileList object
			for (var i = 0, f; f = files[i]; i++) {
				var reader = new FileReader();
				reader.onload = (function(theFile) {
					return function(e ) {
						$('#add-icon-item').css('background-image', 'url('+e.target.result+')');
					};
				})(f );
				reader.readAsDataURL(f );
			}
		});
		
		$("#destino_edit").click(function () {
			if ($("#lst_destino").prop('selectedIndex') == 0) {
				alert("Seleccione una opción");
			} else {
				$('#select_destino').hide();
				$("#form_destino").show();
				$("#logo_destino").show();
				$("#mapa_destino").show();
			}
		});
		
		// Preview cementerio
		
		$('#demo').jPreview();
		
		$("#change_image").on("click", function() {
			$("#demo").click();
		});
		
		$("#remove_image").on("click", function () {
			console.log("Clearing input: " + $("#demo").val());
			$("#demo").val('');
			console.log("Deleting image preview...");
			$("#demo-1-container").empty();
		});
		
		// Preview mapa ruta
		
		$('#demo2').jPreview();
		
		$("#change_image_2").on("click", function() {
			$("#demo2").click();
		});
		
		$("#remove_image_2").on("click", function () {
			console.log("Clearing input: " + $("#demo2").val());
			$("#demo2").val('');
			console.log("Deleting image preview...");
			$("#demo-2-container").empty();
		});
		
		/************* FIN CONTROL DE DESTINOS *************/
		
		/*********************************************/
		/*           CONTROL DE VELATORIOS           */
		/*********************************************/
		

		//añadir velatorio
		$('#btn-add-velatorio').on('click', function(){
			velatorio = $('#tmp-ct .velatorio').clone();
			velatorio.appendTo('#ct-velatorios');

			velatorio.find('.datepicker2').datepicker({
			autoclose: true,
			format: 'dd/mm/yyyy'
			});

			velatorio.find('.timepicker2').timepicker({
			showInputs: false,
			showMeridian:false
			});
		});
		
		
		// $('#lst_sala').on('change',function() {
			
		// 	if($('#lst_sala').val()=='100' || $('#lst_sala').val()=='99' || $('#lst_sala').val()=='' ) {
		// 		$("#velatorios").hide();
		// 		$('#div_domicilio').addClass('hide');
		// 	} else {
		// 		if($('#lst_sala').val()=='0') {
		// 			$('#div_domicilio').removeClass('hide');
		// 		} 
		// 		$("#velatorios").show();
		// 	}
		// });
		
		$('#btnAdd-2').on('click',function(){
			$('#nuevo_velatorio').show();
		})

		//ocultar velatorio
		$('#example-2').multifield({
			section: '.group',
			btnAdd:'#btnAdd-2',
			btnRemove:'.btnRemove',
		});

		//eliminar velatorio
		// $('.btn-delete-velatorio').on('click', function(){
		// 	velatorio = $(this).data('velatorio');
		// 	$("#velatorio_id").val(velatorio.id);
		// 	$("#servicio_id").val(velatorio.servicio_id);
		// 	$("#title").html("Eliminar Velatorio ");
		// 	$("#btntitle").html("Eliminar");
		// 	$("#modalDeleteVelatorio").modal();
		// });
		
		
		/*
		
		O J O
		Parece estar relacionado con la opción confirmar
		
		$('#confirmar').on('click',function(){
			//alert($('#confirmar').attr('checked',true));
			if($('#confirmar').prop('checked')){
				$('#dte-salida-date').prop( "disabled", true );
				$('#dte-salida-hora').prop( "disabled", true );
			}else{
				$('#dte-salida-date').prop( "disabled", false );
				$('#dte-salida-hora').prop( "disabled", false );
			}
		})
		
		if($('#confirmar').prop('checked')){
				$('#dte-salida-date').prop( "disabled", true );
				$('#dte-salida-hora').prop( "disabled", true );
			}else{
				$('#dte-salida-date').prop( "disabled", false );
				$('#dte-salida-hora').prop( "disabled", false );
		}
		*/
		
		/************** FIN CONTROL DE VELATORIOS **************/
		
		/***************************************************/
		/*           CONTROL DE DATOS DE DIFUNTO           */
		/***************************************************/
		
		
		//upload foto
		$.uploadPreview({
			input_field: "#image-upload",
			preview_box: "#image-preview",
			preview_close: "#image-close",
			label_field: "#image-label",
			success_callback: function() {
				console.log("Sucess loaded")
			}
		});
		//upload foto perfil
		$.uploadPreview({
			input_field: "#foto_perfil",
			preview_box: "#image-preview-perfil",
			preview_close: "#image-close-perfil",
			label_field: "#image-label-perfil",
			success_callback: function() {
				console.log("Sucess loaded")
			}
		});

		//upload mapa destino editar
		$.uploadPreview({
			input_field: "#demo2",
			preview_box: "#image-preview-destino",
			preview_close: "#image-close-destino",
			label_field: "#image-label-destino",
			success_callback: function() {
				console.log("Sucess loaded")
			}
		});
		//upload mapa destino nuevo
		$.uploadPreview({
			input_field: "#demo3",
			preview_box: "#image-preview-destino-nuevo",
			preview_close: "#image-close-destino-nuevo",
			label_field: "#image-label-destino-nuevo",
			success_callback: function() {
				console.log("Sucess loaded")
			}
		});
		//upload icono sala editar
		$.uploadPreview({
			input_field: "#icono_sala_edit",
			preview_box: "#image-preview-icono_sala",
			preview_close: "#image-close-icono_sala",
			label_field: "#image-label-icono_sala",
			success_callback: function() {
				console.log("Sucess loaded")
			}
		});
		//upload letra sala editar
		$.uploadPreview({
			input_field: "#letra_sala_edit",
			preview_box: "#image-preview-letra_sala",
			preview_close: "#image-close-letra_sala",
			label_field: "#image-label-letra_sala",
			success_callback: function() {
				console.log("Sucess loaded")
			}
		});
		//upload icono sala nueva
		$.uploadPreview({
			input_field: "#icono_sala_nueva",
			preview_box: "#image-preview-icono_sala_nueva",
			preview_close: "#image-close-icono_sala_nueva",
			label_field: "#image-label-icono_sala_nueva",
			success_callback: function() {
				console.log("Sucess loaded")
			}
		});
		//upload letra sala nueva
		$.uploadPreview({
			input_field: "#letra_sala_nueva",
			preview_box: "#image-preview-letra_sala_nueva",
			preview_close: "#image-close-letra_sala_nueva",
			label_field: "#image-label-letra_sala_nueva",
			success_callback: function() {
				console.log("Sucess loaded")
			}
		});
		
		//remove foto
		$('#delete-image').on('click', function() {
			$(this).parent().fadeOut('slow',function(){
				$(this).css("background-image", "none");
			});
		});
		

		$('.datepicker').datepicker({
			autoclose: true,
			format: 'dd/mm/yy'
		});
		$('.datepicker2').datepicker({
			autoclose: true,
			format: 'dd/mm/yyyy'
		});
		$('.datepicker_vela').datepicker({
			autoclose: true,
			format: 'dd/mm/yyyy'
		});

		$('.timepicker').timepicker({
			showInputs: false,
			showMeridian:false
		});
		
		
		/*********** FIN CONTROL DATOS DIFUNTO ***********/

		/***********************************/
		/*        CONTROLADOR MISAS        */
		/***********************************/

		/**
		 * Muestra/oculta los campos del formulario de 
		 * cración de misas/responsos
		 */
		$('input[name="tipo_responso"]').on('click', function (evt) {
			if($('input:radio:checked').val() == 'misa') {
				// code here...
			} else if($('input:radio:checked').val() == 'responso') {
				// code here...
			}
		});
		
		/**
		 * Ocultar guardar lugar misa o responso
		 */
		$('#mostrar-seleccionar-lugar').on('click', function(){
			$('#lst_responso').prop('selectedIndex', previous_responso);
			console.log("List responso selected: " + previous_responso);
			$('#select_lugar').show();
			$("#seleccionar-lugar").hide();
		});
		
		/**
		 * Muestra el formulario para agregar nuevo responso/misa
		 */
		$('#lst_responso').on('change', function(){
			if($(this).val() == 'Agregar'){
				$("#select_lugar").hide();
				$("#seleccionar-lugar").show();
			}
		});
		
		/**
		 * Captura el click en el boton guardar del formulario
		 * y llama la función guardarLugar()
		 */
		$('#guardar-lugar').click(function() { guardarLugar(); });
		
		/**
		 * Intercepta el "submit" del formulario de creación de misas/responsos.
		 * Envía los datos necesario para la creación de una misa/responso 
		 * nuevos y procesa la respuesta de la API del Back-end.
		 */
		function guardarLugar() {
			$.ajax({
				type: 'POST',
				url: '../api/responso',
				data: {
					'_token': '{{ csrf_token() }}',
					'nombre': $('input[name="nombre_responso_misa"]').val(),
					'url_maps': null,
					'direccion': $('input[name="direccion_responso_misa"]').val(),
					'id_online': null
				}
			}).done(function(data) {
				
				// console.log(data);
				
				// capturar el select
				var lst_responso = document.getElementById("lst_responso");
				
				// determinar cuantos options tiene el select
				var pos = lst_responso.length - 1;
				
				// crear el option nuevo: value y text
				var opt = document.createElement("option");
				opt.value = data.id;
				opt.text = data.nombre;
				
				// agregar el option nuevo justo entes del último
				lst_responso.add(opt, pos);
				
				// ocultar el formulario y mostrar el select
				$("#mostrar-seleccionar-lugar").click();
				
				// seleccionar la nueva opcion
				lst_responso.selectedIndex = pos;
				
			});
		}
		
		$("#responso_edit").click(function() {
			if ($("#lst_responso").prop('selectedIndex') == 0) {
				alert("Seleccione una opción");
			}
			else {
				$("#select_lugar").hide();
				$("#seleccionar-lugar").show();
			}
		})
		
		/******* FIN CONTROLADOR DE MISAS ********/

		/************************************/
		/*       CONTROLADOR EMRPESAS       */
		/************************************/
		
		/**
		 * Ocultar empresa, guardar empresa
		 */
		$('#ocultar-seleccionar-empresa, #guardar-empresa').on('click', function(){
			$('#agregar-empresa').hide();
			$('#seleccionar-empresa').show();
			$("#lst_empresa").prop('selectedIndex', previous_empresa);
		});
		
		/**
		 * Muestra y oculta los controles de selección y creación de empresas
		 */
		$("#lst_empresa").on('change', function(){
			if($(this).val() == 'Agregar'){
				$('#lst_empresa').parent().hide();
				$('#agregar-empresa').removeClass('agregar-empresa')
				$('#agregar-empresa').show();
			}else{
				$('#agregar-empresa').addClass('agregar-empresa')
			}
		});
		
		/**
		 * Intercepta la acción "submit" del formulario de creación de empresas
		 */
		$("#guardar-empresa").click(function() {
			console.log("Click en enviar empresa");
			guardarEmpresa();
		});
		
		/**
		 * Ejecuta el "submot" del formulario de creación de empresas.
		 * Envía los datos necesario para la creación de una empresa 
		 * nueva y procesa la respuesta la API del Back-end.
		 */
		// function guardarEmpresa() {
						
		// 	$.ajax({
		// 		type: 'POST',
		// 		url: '../api/empresa',
		// 		data: {
		// 			'_token': '{{ csrf_token() }}',
		// 			'empresa': $('input[name="nombre_empresa"]').val()
		// 		}
		// 	}).done(function(data) {
		// 		//console.log(data.nombre);
				
		// 		// capturar el select
		// 		var lst_empresa = document.getElementById("lst_empresa");
				
		// 		// determinar cuantos options tiene el select
		// 		var pos = lst_empresa.length - 1;
				
		// 		// crear el option nuevo: value y text
		// 		var opt = document.createElement("option");
		// 		opt.value = data.id;
		// 		opt.text = data.nombre;
				
		// 		// agregar el option nuevo justo entes del último
		// 		lst_empresa.add(opt, pos);
				
		// 		$("#seleccionar-empresa").show();
		// 		$('#agregar-empresa').hide();
				
		// 		// seleccionar la nueva opcion
		// 		lst_empresa.selectedIndex = pos;
				
		// 	});	
		// }
		
		
		$("#empresa_edit").click(function () {
			if ($("#lst_empresa").prop('selectedIndex') == 0) {
				alert("Seleccione una opción");
			}
			else {
				
				// alert("Editando " + $("#lst_empresa>option:selected").text());
				
				$("#seleccionar-empresa").hide();
				$('#agregar-empresa').show();
			}
		})
		
		/* FIN CONTROLADOR ENPRESAS */

	});
</script>
@stop