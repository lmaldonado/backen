    @extends('dashboard.layout.base')

@section('title')
    Servicios
@stop

@section('section')
    Servicios
@stop

@section('breadcrumb')
    Lista de servicios
@stop

@section('content')
<?php 
    $usuario = session()->get('key_login'); 

    if(!isset($usuario['nombre']) ){        
      echo '<script> location.href = "./?ruta=login"; </script>';
    }
?>


<?php 
    use Illuminate\Http\RedirectResponse;
    use Illuminate\Routing\Redirector;
    use App\CH;

    $CH = CH::cliente(1)->first();

    //if($CH->tipo_servidor=='local' || $CH->tipo_servidor=='online'){
        
      //  echo '<script> location.href = "config/ch"; </script>';
    //}

?>

        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">&nbsp;<a  href="{{ url('/?ruta=servicios/create') }}" id="agregar" style="cursor:pointer; cursor: hand" class="borrar btn btn-info" title="Nuevo Servicio"><i class="ace-icon glyphicon glyphicon-plus"></i>Nuevo </a></h3>
                    
                     <span class="input-icon form-search nav-search pull-right" style="padding-top: 5px">
                        <input  id="buscar"  type="text" autocomplete="off" id="nav-search-input" style="width: 13em" class="nav-search-input" placeholder="Buscar ...">

                    </span>
                </div>

    
                <!-- /.box-header -->
                <div class="box-body">
                <table class="table table-hover" id="tabla">
                    <thead>
                        <tr>
                          {{-- <th>ID</th> --}}
                          <th>Sala</th>
                          <th></th>
                          
                          <th>Nombre</th>
                          <th>Ingreso</th>
                          <th>Salida</th>                          
                          <th>Homenajes</th>
                          <th>Estado</th>
                          <th class="text-center"></th>
                        </tr>
                    </thead>
                 @foreach($servicio as $value)

                  @include("servicios.partials.delete")
                  
                    <tr>
                        {{-- <td width="5%">{{$value->id}}</td> --}}
                        <td>
                            @if(isset($value->sala->nombre))
                                {{$value->sala->nombre}}
                                
                               
                            @endif
                             @if($value->sala_id==0)
                                En Domicilio
                            @endif
                             @if($value->sala_id==99)
                                En Privado
                            @endif
                        </td>
                        <td width="10%"><img src="{{url($value->foto)}}" alt="{{$value->foto}}" width="50%"></td>
                        
                        <td >{{$value->nombres." ".$value->apellidos}}</td>
                        <td width="12%">{{\Carbon\Carbon::parse($value->fecha_inicio )->format('d/m')}} {{\Carbon\Carbon::parse($value->fecha_inicio )->format('H:i')}}</td>
                        <td width="12%">{{\Carbon\Carbon::parse($value->salida )->format('d/m')}} {{\Carbon\Carbon::parse($value->salida )->format('H:i')}}</td>
                      
                        
                        <td>
                            <a href="{!! url('/?ruta=servicios/homenajes&id='.$value->id)  !!}" class="btn btn-xs bg-purple fa fa-eye" title="Ver Homenajes"> ({{ count($value->homenajes) }})</a>   
                        </td>
                        <td>
                            @if($value->status=="0")
                                <a href="{{url('/?ruta=servicios/status&id='.$value->id.'&status=inactivar')}}"  data-homenaje="{{ $value->toJson() }}" class="btn btn-xs btn-success btn-status" title="Inactivar servicio">
                                    Activo
                                </a>
                            @elseif($value->status=="1")
                                <a href="{{url('/?ruta=servicios/status&id='.$value->id.'&status=activar')}}"  data-homenaje="{{ $value->toJson() }}" class="btn btn-xs btn-danger btn-status" title="Activar servicio">
                                    Inactivo
                                </a>
                            @endif
                        </td> 
                        <td width="10%">
                            <a href="{{url("/?ruta=servicios/ver&id=$value->id")}}"
                               class="btn btn-xs btn-primary" title="Editar Servicio">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <button type="button"  onclick="eliminarServicio({{$value->id}})" class="btn-delete-servicio btn btn-xs btn-danger" title="Eliminar Servicio">
                                <i class="fa fa-trash"></i>
                            </button>
                        </td>
                    </tr>
                    
                  @endforeach 

              </table>
                </div>
            </div>
        </div>
        <!--Eliminar servicio-->
<div class="modal fade" role="dialog" id="modelEliminar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Eliminar Servicio</h4>
            </div>
            <div class="modal-body">
                
               <input type="hidden" name="servicio_id" id="servicio_id_eliminar" >
                <p>¿Seguro que desea eliminar el servicio?</p>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="confirmarEliminarServicio()" id="btntitle">Eliminar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
        
@stop
@section('script')


   
    <script>
        function eliminarServicio(x){
            
            $('#servicio_id_eliminar').val(x);
            $('#modelEliminar').modal('toggle');
            
        }
        function confirmarEliminarServicio(){
            var id = $('#servicio_id_eliminar').val();
            // console.log(id);
            $.ajax({
                type:'DELETE',
                url: "./?ruta=servicios/delete&id="+id, 
                data :{
                    '_token': '{{ csrf_token() }}'				
                },
                success: function(result){
                    // console.log(result);
                    $('#modelEliminar').modal('toggle');
                    location.reload();
                    
                },error:function(err){
                    console.log(err);
                }
            });
        }
        var myTable = $("#tabla") .DataTable( {
            "dom": '<"top"i>rt<"row"<"col-xs-6"l><"col-xs-6"p>>',
            "length":   false,
            "info":     false,
            "bSort": true,
            "aaSorting":[[1,'desc']],
            bAutoWidth: false,
            select: {
                style: 'multi'
            }
        } );
    
        $('#buscar').on( 'keyup', function () {
            myTable.search( this.value ).draw();
        } );

    $(document).ready(function(){
        // $('.btn-delete-servicio').on('click', function(){
        // servicio = $(this).data('servicio');
        // $("#servicio_id").val(servicio.id);
        // $("#title").html("Eliminar Servicio "+servicio.nombres+" "+servicio.apellidos);
        // $("#btntitle").html("Eliminar");
        // $("#modalDelete").modal();
        // });
    });
    </script>
@stop