@extends('dashboard.layout.base')



@section('section')
    Servicios
@stop

{{-- 
@section('breadcrumb')
    Formulario
@stop 
--}}

@section('content')
<?php 
    $usuario = session()->get('key_login'); 

    if(!isset($usuario['nombre']) || $usuario['admin']!=1){        
      echo '<script> location.href = "./?ruta=login"; </script>';
    }
?>
    @include('servicios.form')
@stop
