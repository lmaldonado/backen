<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NeoSepelios | Administrador</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ url('admin/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ url('admin/plugins/datatables/dataTables.bootstrap.css') }}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ url('admin/plugins/datepicker/datepicker3.css') }}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ url('admin/plugins/timepicker/bootstrap-timepicker.css') }}">
    <!-- bootstrap colorpicker -->
    <link rel="stylesheet" href="{{ url('admin/plugins/colorpicker/bootstrap-colorpicker.css') }}">

    <!-- switchery -->
    <link rel="stylesheet" href="{{ url('admin/plugins/switchery/dist/switchery.css') }}">
    <!-- file-upload -->
    <link rel="stylesheet" href="{{ url('admin/plugins/bootstrap-fileinput/css/fileinput.css') }}">
    <link rel="stylesheet" href="{{ url('admin/plugins/bootstrap-fileinput/css/fileinput-rtl.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ url('admin/plugins/iCheck/minimal/blue.css') }}">
    <!-- Theme style 
    <link rel="stylesheet" href="{{ url('admin/dist/css/AdminLTE.min.css') }}">-->
    <link rel="stylesheet" href="{{ url('admin/dist/css/AdminLTE.css') }}">
    
    {{-- Image Preview --}}
    <link rel="stylesheet" type="text/css" href="{{ url('css/jpreview.css') }}">
    

    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->

    <link rel="stylesheet" href="{{ url('admin/dist/css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/icono.css') }}">
    <link rel="stylesheet" href="{{ url('css/bootstrap-toggle.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/style.css') }}">

</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
    <div class="wrapper">
        {{--Main header--}}
        @include('dashboard.includes.main_header')

       
        {{--Sidebar menu--}}
        @include('dashboard.includes.sidebar_menu')


        {{--Content Wrapper--}}
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    @yield('title')
                </h1>
               
            </section>

            {{-- Contains page content--}}

            <!-- Main content -->
            <section class="content">
                <div  class="row">
                    @yield('content')
                </div>
            </section>
        </div>

        {{--Main footer (Comentado por Dario a solicitud de David)--}}
        @include('dashboard.includes.main_footer')
        

        {{--Control Sidebar / options--}}
        @include('dashboard.includes.control_sidebar')
    </div>
    <!-- jQuery 2.2.3 -->
    <script src="{{ url('admin/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <!-- jQuery 2.2.3 -->
    <script src="{{ url('admin/plugins/jQueryUI/jquery-ui.js') }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ url('admin/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ url('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>

    <!-- DataTables
    <script src="{{ url('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script> -->
    <script src="{{ url('admin/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ url('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ url('admin/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ url('admin/plugins/fastclick/fastclick.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ url('admin/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- bootstrap timepicker -->
    <script src="{{ url('admin/plugins/timepicker/bootstrap-timepicker.js') }}"></script>
    <!-- bootstrap colorpicker -->
    <script src="{{ url('admin/plugins/colorpicker/bootstrap-colorpicker.js') }}"></script>
    <!-- Bootstrap-filesupload -->
    <script src="{{ url('admin/plugins/bootstrap-fileinput/js/fileinput.js') }}"></script>
    <!-- switchery -->
    <script src="{{ url('admin/plugins/switchery/dist/switchery.js') }}"></script>
    <!-- ICheck -->
    <script src="{{ url('admin/plugins/iCheck/icheck.js') }}"></script>
      
    <script src="{{ url('js/typeahead.jquery.js') }}"></script>

    <!-- Upload Imagen -->
    <script src="{{ url('js/jquery.uploadPreview.js') }}"></script>
    <!-- Multifield -->
    <script src="{{ url('js/jquery.multifield.js') }}"></script>


    <script src="{{ url('js/typeahead.jquery.js') }}"></script>


    <script>
        var AdminLTEOptions = {
            //Enable sidebar expand on hover effect for sidebar mini
            //This option is forced to true if both the fixed layout and sidebar mini
            //are used together
            sidebarExpandOnHover: false,
            //BoxRefresh Plugin
            enableBoxRefresh: true,
            //Bootstrap.js tooltip
            enableBSToppltip: true,
            sidebarPushMenu: true,
        };
    </script>
    <!-- AdminLTE App -->
    <script src="{{ url('admin/dist/js/app.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ url('js/bootstrap-toggle.min.js') }}"></script>
    <!-- page script -->
    <script>
        $(function () {
            var myTable = $("#example1") .DataTable( {
                "dom": '<"top"i>rt<"row"<"col-xs-6"l><"col-xs-6"p>>',
                "length":   false,
                "info":     false,
                bAutoWidth: false,
                "language": {
                    "url": "../admin/Spanish.json"
                },

                select: {
                    style: 'multi'
                }
            } );

            $('#buscar').on( 'keyup', function () {
                myTable.search( this.value ).draw();
            } );

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "language": {
                    "url": "../admin/Spanish.json"
                }
            });
            /**
             * @author Gustavo Pino
             * @date 01/11/2017
             */

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "language": {
                    "url": "../admin/Spanish.json"
                }
            });
            //Colorpicker
            $('.color').colorpicker();

            //$(":file").filestyle();
            setTimeout(function () {
                $(".alert").fadeOut();
            },4000);
        });
    </script>


    <script src="{{ url('js/dropzone.js') }}"></script>
    <script>

        Dropzone.options.imagen = {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 4, // MB
            dictDefaultMessage: '<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Presionar para </span> Cargar Imagen \
				 <br /> \
				<i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',
            addRemoveLinks: true

        };
        Dropzone.options.grupo = { // The camelized version of the ID of the form element
            addRemoveLinks: true,
            paramName: "file",
            // The configuration we've talked about above
            autoProcessQueue: false,
            uploadMultiple: false,

            maxFiles: 1,
            dictCancelUpload: true,//cancelarrchivo al subir
            dictCancelUploadConfirmation: true,//confirma la cancelacion
            dictDefaultMessage: '<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Presionar para </span> Cargar Imagen \
				 <br /> \
				<i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',

            // The setting up of the dropzone
            init: function() {
                var myDropzone = this;

                $( "#botonregistrar" ).click(function( e ) {
                   // e.preventDefault();
                   // e.stopPropagation();
                    myDropzone.processQueue()
                });
            }
        }
    </script>
    @yield('script')
</body>
</html>
