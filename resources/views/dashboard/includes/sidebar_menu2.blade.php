<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<br>
		<?php 
		use Illuminate\Support\Facades\Request;
		
		$licencias = DB::table('licencias')->select('product_id')->get();
		$product_ids = array();
		
		foreach ($licencias as $licencia) {
			$product_ids[] = $licencia->product_id;
		}
		
		$method = Request::getMethod();
		$pathInfo = Request::getPathInfo();
		$route = $pathInfo;
		?>

				<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">

			<li class="treeview {{ stristr($route,'clientes')?'active':'' }}">
				<a href="{{ url('servicios') }}">
					<i class="fa fa-user"></i>
					<span>Servicios</span>
					</span>
				</a>
			</li>
				<li class="treeview {{ stristr($route,'style')?'active':'' }}">
					<a href="#">
						<i class="glyphicon glyphicon-cog"></i> <span>Configuración</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-left pull-right"></i>
					</span>
					</a>
					<ul class="treeview-menu" style="display: none;">
						<!-- YA LAS SALAS SE MANEJARAN POR EL FORMULARIO DE SERVICIOS -->
						<!-- <li class="treeview {{ stristr($route,'salas')?'active':'' }}">
							<a href="{{ url('salas') }}">
								<i class="fa fa-files-o"></i>
								<span>Salas</span>

							</a>
						</li> -->
						<li><a href="{{ url('config/licencia') }}"><i class="glyphicon glyphicon-link"></i> Licencia</a></li>
						<li><a href="{{ url('config/configuracion') }}"><i class="glyphicon glyphicon-picture"></i> General</a></li>
						<li>
							<a href="#"><i class="glyphicon glyphicon-picture"></i> CV
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>

							<ul class="treeview-menu" style="display: none;">
								
								<li><a href="{{ url('config/cv') }}"><i class="glyphicon glyphicon-picture"></i> CV</a></li>
								
								<li><a href="{{ url('config/cs') }}"><i class="glyphicon glyphicon-picture"></i> CS</a></li>
							</ul>
						</li>
						<li><a href="{{ url('config/ch') }}"><i class="glyphicon glyphicon-picture"></i> CH / CA</a></li>
						<li><a href="{{ url('productos') }}"><i class="glyphicon glyphicon-picture"></i> CD</a></li>
						<li ><a href="index2.html"><i class="fa fa-circle-o"></i> Usuario  <span class="pull-right-container">
					  <i class="fa fa-angle-left pull-right"></i> </a>
							<ul class="treeview-menu" style="display: none;">
								<li><a href="{{ url('/?ruta=dashboard/importe') }}"><i class="glyphicon glyphicon-picture"></i> </a></li>
							</ul>
						</li>

					</ul>
				</li>
			 
			<li class="treeview">
				<a href="{{route('auth/logout')}}">
					<i class="fa fa-sign-out"></i>
					<span>Salir</span>
				</a>
			</li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>