<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<br>
		<?php 
		use Illuminate\Support\Facades\Request;
		use App\CH;

		$usuario = session()->get('key_login'); 

	    
		
		$licencias = DB::table('licencias')->select('product_id')->get();
		$product_ids = array();
		
		foreach ($licencias as $licencia) {
			$product_ids[] = $licencia->product_id;
		}
		$CH = CH::cliente(1)->first();
		$method = Request::getMethod();
		$pathInfo = Request::getPathInfo();
		$route = $pathInfo;
		?>

				<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			@if(isset($usuario['nombre']))
				<li class="treeview {{ stristr($route,'clientes')?'active':'' }}">
					<a href="{{ url('/?ruta=servicios') }}">
						<i class="fa fa-user"></i>
						<span>Servicios</span>
						</span>
					</a>
				</li>
			@endif
			@if($usuario['admin']==1)
			    <li class="treeview ">
					<a href="{{ url('/?ruta=pedidos') }}">
						<i class="fa fa-tag"></i>
						<span>Pedidos</span>
						</span>
					</a>
				</li>
				<li class="treeview ">
					<a href="{{ url('/?ruta=config/conexion') }}">
						<i class="fa fa-desktop"></i>
						<span>Conexion</span>
						</span>
					</a>
				</li>
			
				<li class="treeview {{ stristr($route,'style')?'active':'' }}">
					<a href="#">
						<i class="glyphicon glyphicon-cog"></i> <span>Configuración</span>
					<span class="pull-right-container">
					  <i class="fa fa-angle-left pull-right"></i>
					</span>
					</a>
					<ul class="treeview-menu" style="display: none;">
						
						<li><a href="{{ url('/?ruta=config/licencia') }}"><i class="glyphicon glyphicon-link"></i> Licencia</a></li>
						<li><a href="{{ url('/?ruta=config/configuracion') }}"><i class="glyphicon glyphicon-picture"></i> General</a></li>
						
						<li><a href="{{ url('/?ruta=config/ni') }}"><i class="glyphicon glyphicon-picture"></i> Ajustes</a></li>
						<li ><a href="{{ url('/?ruta=usuarios') }}"><i class="fa fa-users"></i> Usuarios  <span class="pull-right-container"></a>
						</li>

					</ul>
				</li>
			@endif
			<li class="treeview">
				<a href="{{ url('/?ruta=logout') }}">
					<i class="fa fa-sign-out"></i>
					<span>Salir</span>
				</a>
			</li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>