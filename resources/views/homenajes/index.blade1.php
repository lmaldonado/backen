@extends('dashboard.layout.base')

@section('title')
    @if ($servicio->apellidos!=null or $servicio->nombres!=null)
    <a href="{{url('/?ruta=servicios')}}" >
        {{ $servicio->apellidos.' '.$servicio->nombres }}
    </a>
    @else
        Homenajes
    @endif
@stop

@section('section')
    Servicios
@stop

@section('breadcrumb')
    Lista de homenajes
@stop

@section('content')
<?php 
    $usuario = session()->get('key_login'); 

    if(!isset($usuario['nombre']) || $usuario['admin']!=1){        
      echo '<script> location.href = "../public/?ruta=login"; </script>';
    }
?>


    @include("homenajes.partials.update")
    @include("homenajes.partials.hide")
    @include("homenajes.partials.delete")
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <a href="javascript:" id="add-homenaje" style="cursor:pointer; cursor: hand" class=" btn btn-info" title="Nuevo homenaje" data-toggle="modal" data-target="#myModal">
                        <i class="ace-icon glyphicon glyphicon-plus"></i>Nuevo 
                    </a>
                </h3>
                <span class="input-icon form-search nav-search pull-right" style="padding-top: 5px">
                    <input  id="buscar"  type="text" autocomplete="off" id="nav-search-input" style="width: 13em" class="nav-search-input" placeholder="Buscar..">     
                </span>
            </div>
            <div class="box-body">
                <table class="table table-hover" id="tabla" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th></th>
                            <th>Nombre</th>
                            <th>Mensaje</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($homenajes as $homenaje)
                        <tr>
                            <td width="5%">{{$homenaje->id}}</td>
                            <td width="10%">
                                <img src="{{url($homenaje->foto)}}" alt="{{$homenaje->foto}}" width="50%">
                            </td>
                            <td>{{$homenaje->nombre}}</td>
                            <td>{{str_limit($homenaje->mensaje,60)}}</td>
                            <td width="10%">{{\Carbon\Carbon::parse($homenaje->created_at)->format("d/m/Y")}}</td>
                            <td width="10%">
                                <button type="button" data-homenaje="{{ $homenaje->toJson() }}" class="btn btn-xs btn-primary btn-edit" title="Editar Homenaje">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                @if ($homenaje->deleted_at == date('0000-00-00 00:00:00'))
                                <a href="{{url('/?ruta=servicios/homenajesHide&id='.$homenaje->id.'&accion=mostrar')}}"  data-homenaje="{{ $homenaje->toJson() }}" class="btn btn-xs btn-success btn-hide" title="Ocultar Homenaje">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @else
                                <a href="{{url('/?ruta=servicios/homenajesHide&id='.$homenaje->id.'&accion=ocultar')}}"  data-homenaje="{{ $homenaje->toJson() }}" class="btn btn-xs btn-success btn-hide" title="Mostrar Homenaje">
                                    <i class="fa fa-eye-slash"></i>
                                </a>
                                @endif
                                <button type="button" data-homenaje="{{ $homenaje->toJson() }}" class="btn btn-xs btn-danger btn-delete" title="Eliminar Homenaje">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>

$(document).ready(function(){
    //Eliminar
    $('.btn-delete').on('click', function(){
        $data = $(this).data('homenaje');
        // $("#nombre").val($data.nombre);
        // $("#mensaje").val($data.mensaje);
        $('#add-icon-item').css('background-image', 'url('+$data.foto+')');
        $('#homenaje_id1').val($data.id);
        // $("#title").html("Editar Homenaje")
        // $("#btntitle").html("Eliminar");
        $("#modalDelete").modal('toggle');
    });

    //Ocultar
    $('.btn-hide').on('click', function(){
        $data = $(this).data('homenaje');
        $("#nombre").val($data.nombre);
        $("#mensaje").val($data.mensaje);
        $('#add-icon-item').css('background-image', 'url('+$data.foto+')');
        $('#homenaje_id2').val($data.id);
        $("#title").html("Ocultar Homenaje")
        $("#btntitle").html("Ocultar");
        //$("#modalHide").modal();
    });

    //Editar
    $('.btn-edit').on('click', function(){
        $data = $(this).data('homenaje');
        $("#nombre").val($data.nombre);
        $("#mensaje").val($data.mensaje);
        $('#add-icon-item').css('background-image', 'url('+$data.foto+')');
        $('#homenaje_id').val($data.id);
        $("#title").html("Editar Homenaje")
        $("#btntitle").html("Actualizar");
        $("#myModal").modal();
    });

    //Agregar
    $('#add-homenaje').on('click', function(){
        $("#nombre").val("");
        $("#mensaje").val("");
        $("#add-icon-item").css('background-image', '');
        $('#homenaje_id').val("");
        $("#title").html("Agregar Homenaje")
        $("#btntitle").html("Guardar");
    });
  
});
        var myTable = $("#tabla") .DataTable( {
            "dom": '<"top"i>rt<"row"<"col-xs-6"l><"col-xs-6"p>>',
            "length":   false,
            "info":     false,
            bAutoWidth: false,

            select: {
                style: 'multi'
            }
        } );

        $('#buscar').on( 'keyup', function () {
            myTable.search( this.value ).draw();
        } );

        $('#btn-add-foto').on('change', function(evt){

            var files = evt.target.files; // FileList object
            for (var i = 0, f; f = files[i]; i++) {
                var reader = new FileReader();
                reader.onload = (function(theFile) {
                    return function(e ) {
                        $('#add-icon-item').css('background-image', 'url('+e.target.result+')');
                    };
                })(f );
                reader.readAsDataURL(f);
            }
        });

        /*function updateModal(value,foto,val) {
            $("#img_foto").val(foto);

            var img = $(val).parents("tr").find("td").prev().children("img").attr("src");
            $("#servicio option[value='"+value+"']").prop("selected",true);
            var i = 0;
            var arr = [];
            $(val).parents("tr").find("td").each(function(){
                arr[i] = $(this).html();
                i++;

            });
            $("#nombre").val(arr[2]);
            $("#mensaje").val(arr[3]);
            $('#add-icon-item').css('background-image', 'url('+img+')');
            $("#update").val(arr[0]);
            $("#title").html("Editar Homenaje")
            $("#btntitle").html("Editar");
            $("#myModal").modal();

        }*/

    </script>
@stop