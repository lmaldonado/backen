<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		#header { 
    	position: fixed; 
    	left: 0px; 
    	top: -30px; 
    	right: 0px; 
    	height: 80px;  
    	text-align: center; 
    	margin-bottom: 50px;
    }
    #contenido{
    	margin-top: 40px;
    }
		#tabla {
			border: 1px solid lightgrey;
		}
		#tabla td{
			border: 1px solid lightgrey;
		}
		*{
			font-family: serif;
		}
	</style>
</head>
<body>
	<div id="header">
		<table class="table">
			<tr>				
				<td width="100%" style="text-align: center; padding-left: 250px">
					<img src="./images/empresa/{{$ni->logo_encabezado}}" width="200">
				</td>
			</tr>
		</table>		
	</div>
	<div id="contenido">
		@if(isset($oculto) && $oculto==true)
			<h3 style="text-align: center;">Homenajes Ocultos de {{$servicio->nombres}} {{$servicio->apellidos}}</h3>
		@else
			<h3 style="text-align: center;">Homenajes de {{$servicio->nombres}} {{$servicio->apellidos}}</h3>
		@endif
		<!-- <br> -->
		
		@if(count($homenajes) == 0)
			<div style="width: 100% text-align: center;">
				<h4 style=" width: 100%; text-align: center;">No hay homenajes</h4>	
			</div>			
		@else
			<table id="tabla">
				@foreach($homenajes as $home)
				<tr>
					<td style=" width: 150px; text-align: center;"><img src="{{$home->foto}}" alt="{{$home->foto}}" width="100%"></td>
					<td style=" padding: 10px; width: 520px; height: 50px; text-align: justify; "><strong>{{$home->nombre}}</strong><br><p>{{$home->mensaje}}</p></td>
				</tr>		
				@endforeach
			</table>
		@endif	
		
	</div>
		
</body>
</html>

<?php 
// dd($ni);
//require_once ('./dompdf-master/src/Dompdf.php');
require_once ('dompdf/autoload.inc.php');
 
use Dompdf\Dompdf;
use Dompdf\Options;

$options = new Options();
$options->set('defaultFont', 'Times');
//$dompdf = new Dompdf($options);
// instantiate and use the dompdf class
$pdf = new Dompdf($options);

// Definimos el tamaño y orientación del papel que queremos.
$pdf->set_paper("A4", "portrait");

 
// Cargamos el contenido HTML.
$pdf->load_html(ob_get_clean());
//$pdf->load_html(utf8_decode($html));
 
// Renderizamos el documento PDF.
$pdf->render();
 
// Enviamos el fichero PDF al navegador.
//$pdf->stream('FicheroEjemplo.pdf');
$pdf->stream('homenajes-'.$servicio->id.'.pdf',array("Attachment"=>0));

?>