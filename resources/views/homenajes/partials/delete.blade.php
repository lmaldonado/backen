<!-- Modal -->
<div class="modal fade" role="dialog" id="modalDelete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="title">Eliminar Homenaje</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => './?ruta=servicios/homenajes&id='.$servicio->id, 'method' => 'DELETE',
                'files' => true, 'id' => 'delete']) !!}
                <input type="hidden" name="homenaje_id1" id="homenaje_id1" value="">
                <p>¿Seguro que desea eliminar el homenaje?</p>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="$('#delete').submit()" id="btntitle">Eliminar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>