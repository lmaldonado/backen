<!-- Modal -->
<div class="modal fade" role="dialog" id="modalHide">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="title">Ocultar Homenaje</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => 'servicios/'.$servicio->id.'/homenajes', 'method' => 'PUT',
                'files' => true, 'id' => 'hide']) !!}
                <div class="col-lg-12">
                    <input type="hidden" name="homenaje_id2" id="homenaje_id2" value="">
                    <div  class="add-image" id="add-icon-item" style=""></div>
                    <input id="btn-add-foto"  name="foto" class="inputFile" type="file" accept="image/*" base-sixty-four-input="" style="">  
                    <div style="height: 5px"></div>
                    <h2 align="center">¿Seguro que desea ocultar este homenaje?</h2>
                    
                </div>
                <div class="clearfix"></div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="$('#hide').submit()" id="btntitle">Ocultar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>