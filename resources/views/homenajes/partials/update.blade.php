<!-- Modal -->
<div class="modal fade" role="dialog" id="myModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="title">Editar Homenaje</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => './?ruta=servicios/homenajes&id='.$servicio->id, 'method' => 'POST',
                'files' => true, 'id' => 'update']) !!}
                <input type="hidden" name="homenaje_id" id="homenaje_id" value="">
                <div class="form-group col-lg-4">
                    <div class="form-group">
                        {!! Form::label('Foto') !!}
                        <div  class="add-image" id="add-icon-item" style=""></div>
                        <div style="height: 5px"></div>
                        {!! Form::button('Cargar Foto', ['class' => 'btn btn-success fa fa-camera']) !!}
                        <div align="center" >
                            <input id="btn-add-foto"  name="foto" class="inputFile" type="file" accept="image/*" base-sixty-four-input="" style="">
                            <input type="hidden" name="img_foto" id="img_foto">
                        </div>
                    </div>
                    <div class="ct-foto">
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="form-group">
                        <label for="nombre">Nombre:</label>
                        <input type="text" name="nombre" id="nombre" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nombre">Mensaje:</label>
                        <textarea name="mensaje" id="mensaje" rows="6" class="form-control"></textarea>
                    </div>
                </div>
                <div class="clearfix"></div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="$('#update').submit()" id="btntitle">Editar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


