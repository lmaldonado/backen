<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		#tabla {
			border: 1px solid lightgrey;
		}
		#tabla td{
			border: 1px solid lightgrey;
		}
		@font-face {
		    font-family: "DejaVu Sans";
		    font-style: normal;
		    font-weight: bold;
		}
	</style>
</head>
<body>
	<h3 style="text-align: center;">Homenajes de {{$servicio->nombres}} {{$servicio->apellidos}}</h3>
	<br>
	<table id="tabla">
		@foreach($homenajes as $home)
		<tr>
			<td style=" padding: 10px; width: 150px"><img src="{{$home->foto}}" alt="{{$home->foto}}" width="100%"></td>
			<td style=" padding: 10px; width: 400px; height: 50px; text-align: justify; "><strong>{{$home->nombre}}</strong><br><p>{{$home->mensaje}}</p></td>
		</tr>		
		@endforeach
	</table>
</body>
</html>

<?php 
//dd($homenajes);
//require_once ('./dompdf-master/src/Dompdf.php');
require_once ('dompdf/autoload.inc.php');
 
use Dompdf\Dompdf;
use Dompdf\Options;

$options = new Options();
$options->set('defaultFont', '/DejaVu Sans');
//$dompdf = new Dompdf($options);
// instantiate and use the dompdf class
$pdf = new Dompdf($options);

// Definimos el tamaño y orientación del papel que queremos.
$pdf->set_paper("A4", "portrait");

 
// Cargamos el contenido HTML.
$pdf->load_html(ob_get_clean());
//$pdf->load_html(utf8_decode($html));
 
// Renderizamos el documento PDF.
$pdf->render();
 
// Enviamos el fichero PDF al navegador.
//$pdf->stream('FicheroEjemplo.pdf');
$pdf->stream('homenajes-'.$servicio->id.'.pdf',array("Attachment"=>0));

?>