/**
 * Created by Saiafar on 18/8/2016.
 */
$(document).ready(function(){


    /********************************* INIT ***********************************************/
    //$('.inputFile').parent().css('height', '0px');

    loadGrupos();
    $('.inputFileGaleria').on('change', function(evt){
        $('#ct-inputs-galeria').append($(this));
        clon = $(this).clone(true);
        $(this).css('display','none');
        $(this).attr('name', 'galery[]');
        $('#add-image-galery').append(clon);
        var files = evt.target.files; // FileList object
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function(theFile, h) {
                return function(e) {
                    addImageGalery(e.target.result, false, theFile.name);
                };
            })(f);
            reader.readAsDataURL(f);
        }
    });

    /**********************************************************************  GRUPOS   ****/

    $('#formGrupo').on('submit', function (e){
        $url = $(this).attr('action');
        e.preventDefault();
        var data = new FormData(this);
        $.ajax( {
            url: $url,
            type: 'POST',
            data: data,
            processData: false,
            contentType: false
        } ).done(function(data){
            data = $.parseJSON(data);
            if(!data.error){
                if(data.action == 'store'){
                    closeFormGrupo();
                    if(data.grupo){
                        id = addNewGrupo(data.grupo);
                        $('#panel_right').animate({
                            scrollTop: $("#"+id).offset().top
                        }, 2000);
                    }
                }else if(data.action == 'update'){
                    grupoEd = $('#'+data.grupo.id);
                    grupoEd.find('.text-grupo').text(data.grupo.nombre);
                    grupoEd.find('.img-grupo').css('background-image', 'url(img_grupos/'+data.grupo.id+'.jpg)');
                    closeFormGrupo();
                    endEditGrupos();
                }

            }

        });
    });


    $('#btn-add-grupo').on('click', function(){
        openFormGrupo();
    });

    $('#btn-cancel-grupo').on('click', function(){
        $('.back-form').fadeOut();
        $('#form-grupo').fadeOut();
    });

    $('#inputFileGrupo').on('change', function(evt){
        var files = evt.target.files; // FileList object
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e) {
                    $('#add-icon-grupo').css('background-image', 'url('+e.target.result+')')
                };
            })(f);
            reader.readAsDataURL(f);
        }
    });

    /*$('#btn-save-grupo').on('click', function(){
        event.preventDefault();
    });*/

    /**********************************************************************  ITEMS   ****/

    $('#formItem').on('submit', function (e){
        $url = $(this).attr('action');
        e.preventDefault();
        var data = new FormData(this);
        $.ajax( {
            url: $url,
            type: 'POST',
            data: data,
            processData: false,
            contentType: false
        } ).done(function(data){
            data = $.parseJSON(data);
        });
    });

    $('#btn-add-item').on('click', function(){
        $('#hdn-id-grupo-item').attr('value', $(this).data('grupo'));
        openFormItem();
    });

    $('#btn-cancel-item').on('click', function(){
        $('.back-form').fadeOut();
        $('#form-item-right').fadeOut();
        $('#form-item').fadeOut();


    });

    $('#inputIconItem').on('change', function(evt){
        var files = evt.target.files; // FileList object
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e) {
                    $('#add-icon-item').css('background-image', 'url('+e.target.result+')')
                };
            })(f);
            reader.readAsDataURL(f);
        }
    });



/**************************** ITEMS RELACIONADOS *************************/
    $('#btn-add-relacionado').on('click', function(){
        $(this).hide();
        $('#form-item-right').addClass('form-item-right-open');
    });

    $('#btn-listo-relacionados').on('click', function(){
        $('#btn-add-relacionado').show();
        $('#form-item-right').removeClass('form-item-right-open');
    });

    $('#select-grupos').on('click', function(){
        list = $('#list-grupos');
        if(list.css('display') == 'block'){
            $('#list-grupos').hide();
        }else{
            $('#list-grupos').show();
        }
    });


});
/************************************************************************/

function loadGrupos(){
    $.get('api/grupos', function(data){
        //-- data = $.parseJSON(data);
        console.log(data);
        for(i=0; i<data.length; i++){
            addNewGrupo(data[i]);
            addOptionListGrupos(data[i]);
        }
    });
}

function addOptionListGrupos(grupo){
    console.log('adoptiond');
    optionGrupo = $('<li/>',{
        id:'option-grupo-'+grupo.id
    }).html('<a>'+grupo.nombre+'</a>');
    optionGrupo.data('grupo', grupo);
    $('#list-grupos').append(optionGrupo);
    optionGrupo.on('click', function(){

        $(this).parent().hide();
        grupo = $(this).data('grupo');
        $('#select-grupos').html(grupo.nombre+'<span class="caret"></span>');
        $('#listado-items-a-relacionar').empty();
        $.get('api/items?grupo='+grupo.id, function(data){
            //-- data = $.parseJSON(data);
            for(i =0; i<data.length; i++){
                newItemRel = $('#template-item-a-relacionar').clone();
                newItemRel.attr('id','ritem-'+data[i].id);
                newItemRel.find('.text-item').text(data[i].nombre);
                newItemRel.find('.img-item').css('background-image', 'url(img_items/'+data[i].id+'.jpg)');
                newItemRel.data('item', data[i]);
                newItemRel.on('click', function(){
                    item = $(this).data('item');
                    $(this).addClass('item-selected');
                    addItemRel(item, false);
                });
                $('#item-relacionados .item').each(function(ele){
                    console.log($(this).attr('id'),data[i].id);
                    if($(this).data('id') == data[i].id){
                        newItemRel.addClass('item-selected');
                    }
                });
                $('#listado-items-a-relacionar').append(newItemRel);
                newItemRel.show();
            }
        });
    });
}
/***********************************************************************************************ADD GRUPO***/
function addNewGrupo(grupo){
    newGrupo = $('#templateGrupo').clone();
    newGrupo.attr('id', grupo.id);
    newGrupo.find('.text-grupo').text(grupo.nombre);
    newGrupo.find('.img-grupo').css('background-image', 'url(img_grupos/'+grupo.id+'.jpg)');
    $('#content-right').append(newGrupo);
    newGrupo.bind('taphold',function(){
        $(document).on('click', function(e){
            console.log('click document');
            target = e.target;
            if(target != 'btn-del-grupo' || target != 'btn-edit-grupo'){
                endEditGrupos();
            }
        });
        $('.grupo').addClass('del-active');
        $('.btn-del-grupo').addClass('show-btn-del');
        $('.btn-edit-grupo').fadeIn();
    });
    newGrupo.on('click', function(){
        endEditItem();
        $('.grupoSelected').removeClass('grupoSelected');
        $(this).addClass('grupoSelected');
        $('#content-right').addClass('inGrupo');
        $('.item-tmp').remove();
        $.get('api/items?grupo='+grupo.id, function(data) {
            //-- data = $.parseJSON(data);
            if(data.length == 0){
                $('.btn-add-item').show();
                $('.btn-add-item').addClass('show-add-item');
            }else{
                $('.btn-add-item').hide();
                $('.btn-add-item').removeClass('show-add-item');
            }
            for (i = 0; i < data.length; i++) {
                addNewItem(data[i]);
            }
            $('#content-center').append('<div style="clear:both" class="item-tmp"></div>');
            auxH = $("#content-center").height();
            auxH2 = $('#panel_mid').height();
            $("#content-center").css('margin-top', ((auxH2-auxH)/2)+'px');
        });
    });

    newGrupo.find('.btn-edit-grupo div div').on('click', function(){
        loadFormGrupo($(this).parent().parent().parent().attr('id'));
        openFormGrupo();

    });
    btnDel = newGrupo.find('.btn-del-grupo');
    btnDel.data('id', grupo.id);
    btnDel.on('click', function(){
        deleteGrupo($(this).data('id'));
    });
    newGrupo.show();
    return grupo.id;
}

function addNewItem(item){
    newItem = $('#templateItem').clone();

    newItem.attr('id', item.id);
    $('#btn-add-item').data('grupo', item.grupo_id);
    newItem.addClass('item-tmp');
    newItem.find('.text-item').text(item.nombre);
    newItem.find('.img-item').css('background-image', 'url(img_items/'+item.id+'.jpg)');
    newItem.bind('taphold',function(){
        $(document).on('click', function(e){
            console.log('click document');
            target = e.target;
            if(target != 'btn-del-item' || target != 'btn-edit-item'){
                endEditItem();
            }
        });
        $('#content-center .btn-edit-item').fadeIn();
        $('#content-center .btn-add-item').fadeIn();
        $('#content-center .btn-add-item').addClass('show-add-item');
        //$(this).hide();
        $('#content-center .item').addClass('del-active');
        $('#content-center .btn-del-item').addClass('show-btn-del');
        auxH = $("#content-center").height();
        auxH2 = $('#panel_mid').height();
        console.log((auxH2-auxH)/2);
        auxH = $("#content-center").height();
        auxH2 = $('#panel_mid').height();
        console.log((auxH2-auxH)/2);
        setTimeout(function(){
            auxH = $("#content-center").height();
            auxH2 = $('#panel_mid').height();
            console.log((auxH2-auxH)/2);
            $("#content-center").css('margin-top', ((auxH2-auxH)/2)+'px');
        }, 200)
    });

    newItem.on('click', function(){

    });
    newItem.find('.btn-edit-item div div').on('click', function(){
        console.log('view Item');
        loadFormItem($(this).parent().parent().parent().attr('id'));
        openFormItem();


    });
    btnDel = newGrupo.find('.btn-del-item');
    btnDel.data('id', item.id);
    btnDel.on('click', function(){
        deleteItem($(this).data('id'));
    });

    $('#content-center').append(newItem);
    newItem.show();
}

function openFormItem(){
    $('#hdn-id-grupo-item').val($('.grupoSelected').attr('id'));
    $('#hdn-id-item').attr('value', '');
    //$('#hdn-id-grupo-item').attr('value', '');
    $('#txt-nombre-item').attr('value', '');
    $('#txa-desc-item').text('');
    $('#txa-desc-int-item').text('');
    $('#add-icon-item').css('background-image', '');
    $('#ct-item-rel-inputs').empty();
    $('.item-img').remove();
    $('#form-item').data('numGalery', 0);
    $('.back-form').fadeIn();
    $('#form-item').fadeIn();
    $('#form-item-right').fadeIn();
 //   $('#add-image').append(fileGalery);
}

function loadFormItem(id){
    $.get('api/item/'+id+'/show', function(data){
        //-- data = $.parseJSON(data);
        $('#formItem').attr('action', 'api/item/update');
        $('#hdn-id-item').attr('value',data.item.id);
        $('#txt-nombre-item').attr('value',data.item.nombre);
        $('#txa-desc-item').text(data.item.descripcion);
        $('#txa-desc-int-item').text(data.item.descripcion_interna);
        $('#add-icon-item').css('background-image', 'url(img_items/'+data.item.id+'.jpg)');

        for(i=0; i < data.galery.length; i++){
            addImageGalery(data.galery[i], true, '');
        }
        $('#item-relacionados').empty();
        for(i=0; i < data.rel.length; i++){
            addItemRel(data.rel[i], true);
        }
    });
}

function addImageGalery(img_url, bd, name){
    num = $('#form-item').data('numGalery');
    img = $('<div/>',{
        class:'item-img',
        css:{
            'background-image':'url('+img_url+')'
        }
    }).append('<div class="content-btn-del-image"><div class="btn-del btn-del-image show-btn-del"><i class="fa fa-times" aria-hidden="true"></i></div></div>');
    if(!bd)
        img.data('name', name);
    img.data('url', img_url);
    img.data('bd', bd);
    img.find('.btn-del').on('click', function(){
        img = $(this).parent().parent();
        if(img.data('bd')){
            $("<input/>", {
                name:'galery_del[]',
                type:'hidden',
                value:img.data('url')
            }).appendTo('#ct-galery-del');
        }else{
            $('.inputFileGaleria').each(function(){
                name = $(this).val().split('\\');
                name = name.pop()
                console.log(name,img.data('name'));
                if(name == img.data('name')){
                    $(this).remove();
                }
            });
        }
        img.remove();
        w = $('#item-galeria-container').width();
        $('#item-galeria-container').css('width', (w-315)+'px');
    });
    w = $('#item-galeria-container').width();
    $('#item-galeria-container').css('width', w+315+'px');
    $('#item-galeria-container').append(img);
}

function closeFormItem(){

}

function openFormGrupo(){
    $('.back-form').fadeIn();
    $('#form-grupo').fadeIn();

}

function closeFormGrupo(){
    $('.back-form').fadeOut();
    $('#form-grupo').fadeOut();
    document.getElementById('formGrupo').reset();
}

function showLoading(){

}

function loadFormGrupo(id){
    $.get('api/grupo/'+id+'/show', function(grupo){
        //-- grupo = $.parseJSON(grupo);
        $('#formGrupo').attr('action', 'api/grupo/update');
        $('#hdn-id-grupo').attr('value',grupo.id);
        $('#txt-nombre-grupo').attr('value',grupo.nombre);
        $('#txa-desc-grupo').text(grupo.descripcion);
        $('#add-icon-grupo').css('background-image', 'url(img_grupos/'+grupo.id+'.jpg)');
    });
}

function deleteGrupo(id){
    $.ajax({
        url: 'api/grupo/'+id+'/delete',
        type: 'DELETE',
        //data: id,
        processData: false,
        contentType: false
    }).done(function(data){
        //-- data = $.parseJSON(data);
        if(data){
            $('#'+data).remove();
        }
    });
}

function deleteItem(id){
    $.ajax({
        url: 'api/item/'+id+'/delete',
        type: 'DELETE',
        //data: id,
        processData: false,
        contentType: false
    }).done(function(data){
        //-- data = $.parseJSON(data);
        if(data){
            $('#'+data).remove();
        }
    });
}

function endEditGrupos(){
    $(document).off('click');
    $('.grupo').removeClass('del-active');
    $('.btn-del-grupo').removeClass('show-btn-del');
    $('.btn-edit-grupo').fadeOut();
}

function endEditItem(){
    $(document).off('click');
    $('#content-center .item').removeClass('del-active');
    $('#content-center .btn-edit-item').fadeOut();
    $('#content-center .item .btn-del-item').removeClass('show-btn-del');
    $('#content-center .btn-edit-item').fadeOut();
    $('#content-center #btn-add-item').removeClass('show-add-item');
}

function addItemRel(item, bd){
    if(!bd){
        input = $("<input/>", {
            id:'hdn-item-rel-'+item.id,
            name:'items_rel[]',
            type:'hidden',
            value:item.id
        }).appendTo('#ct-item-rel-inputs');
    }


    // $('#ct-item-rel-inputs').append($("<div>"))
    //$('#').append('<input id="hdn-item-relacionado-'+item.id+'" type="hidden" name="relacionados{}" value="'+item.id+'"/>');
    relItem = $('#template-item-relacionado').clone();
    relItem.data('id', item.id);
    relItem.attr('id', 'item-rel-'+item.id);
    relItem.find('.text-item-grupo').text(item.grupo.nombre);
    relItem.find('.text-item').text(item.nombre);
    relItem.find('.img-item').css('background-image','url(img_items/'+item.id+'.jpg)');
    relItem.find('.btn-del-item').on('click', function(){
        idItem = $(this).parent().parent().data('id');
        if(bd){
            $("<input/>", {
                name:'items_rel_del[]',
                type:'hidden',
                value:item.id
            }).appendTo('#ct-item-rel-del');
        }else{
            $("#hdn-item-rel-"+idItem).remove();
        }
        $(this).parent().parent().remove();
    });

    $('#item-relacionados').append(relItem);
    relItem.show();
}