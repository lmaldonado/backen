/**
 * Created by Saiafar on 18/8/2016.
 */
var modo = '';
var tserv_selected = 0;
var backTo = '';
var viewActive = '';
var arrayRelTserv = [];
$(document).ready(function(){
    /********************************* INIT ***********************************************/
    //$('.inputFile').parent().css('height', '0px');
    loadTiposServicios();
    //loadGrupos();
    $('.inputFileGaleria').on('change', function(evt){
        $('#ct-inputs-galeria').append($(this));
        clon = $(this).clone(true);
        $(this).css('display','none');
        $(this).attr('name', 'galery[]');
        $('#add-image-galery').append(clon);
        var files = evt.target.files; // FileList object
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function(theFile, h) {
                return function(e) {
                    addImageGalery(e.target.result, false, theFile.name);
                };
            })(f);
            reader.readAsDataURL(f);
        }
    });

    $('#btn-volver').on('click', function(){
        showContent(backTo);
    });

    /********************************************************************** EVENTS CONTENT ***************/
    $('#content-center-form-tipos-servicios').on('show', function(){

        $(document).off('click');
        setTitle('Nuevo Tipos de Servicios');
        showBarTop('btns-form');
        hideBtnBack();
        $('#btn-add-tserv-rel').addClass('show-add-item');
        $('#btn-add-tserv-rel').show();
        $('#btn-save-form').off('click');
        $('#btn-save-form').on('click', function(){
            $('#fromTipoServ').submit();
        });
        $('#btn-cancel-form').off('click');
        $('#btn-cancel-form').on('click', function(){
            showContent('content-center-tipos-servicios');
        });
    });

    $('#content-center-tipos-servicios').on('show', function(){

        setTitle('Tipos de Servicios');
        backTo = '';
        hideBarTop('btns-form');
        endEditItem();
    });

    $('#content-center-grupos').on('show', function(){
        endEditItem();
        setTitle('Grupos');
        backTo = 'content-center-tipos-servicios';
        showBtnBack();
        hideBarTop('btns-form');

    });

    $('#content-form-grupo').on('show', function(){

        setTitle('Nuevo Grupo');
        showBarTop('btns-form');
        hideBtnBack();
        $('#btn-save-form').off('click');
        $('#btn-save-form').on('click', function(){
            $('#formGrupo').submit();
        });
        $('#btn-cancel-form').off('click');
        $('#btn-cancel-form').on('click', function(){
            showContent('content-center-grupos');
        });
    });

    $('#content-center-items').on('show', function(){
        setTitle('Items');
        backTo = 'content-center-grupos';
    });

    $('#content-center-items').on('show', function(){
        endEditItem();
        setTitle('Items');
        backTo = 'content-center-grupos';
        showBtnBack();
        hideBarTop('btns-form');
    });

    $('#content-form-item').on('show', function(){

        showBarTop('btns-form');
        hideBtnBack();
        showBarRight();
        if(modo == "new Item"){
            setTitle('Nuevo Item');
            $('#btn-add-item-rel').addClass('show-add-item');
            $('#btn-add-item-rel').show();
            $('#hdn-id-grupo-item').val($('.grupoSelected').attr('id'));
            $('#hdn-id-item').attr('value', '');
            //$('#hdn-id-grupo-item').attr('value', '');
            $('#txt-nombre-item').attr('value', '');
            $('#txa-desc-item').text('');
            $('#txa-desc-int-item').text('');
            $('#add-icon-item').css('background-image', '');
            $('#ct-item-rel-inputs').empty();
            $('.item-img').remove();
            $('#form-item').data('numGalery', 0);
            $('#add-icon-item > div').show();
            $('#inputIconItem').show();

            $('#btn-save-form').off('click');
            $('#btn-save-form').on('click', function(){
                $('#formItem').submit();
            });
            $('#btn-cancel-form').off('click');
            $('#btn-cancel-form').on('click', function(){
                showContent('content-center-items');
            });
        }else if(modo = 'edit_item_tserv'){
            setTitle('Editando Item en Servicio');
            $('#hdn_id_item_tserv').attr('value', tserv_selected);
            $('#txt-nombre-item').attr('readonly', 'readonly');
            $('#add-image-galery').hide();
            $('#add-icon-item > div').hide();
            $('#inputIconItem').hide();

            $('#btn-save-form').off('click');
            $('#btn-save-form').on('click', function(){
                $('#formItem').submit();
            });
            $('#btn-cancel-form').off('click');
            $('#btn-cancel-form').on('click', function(){
                hideBarRight();
                showContent('content-center-form-tipos-servicios');
            });

        }




    });

    $('#content-center-info-item').on('show', function(){
        backTo = 'content-center-items';
    });

    /**********************************************************************  TIPOS SERVICIOS *************/

    $('#fromTipoServ').on('submit', function (e){
        $url = $(this).attr('action');
        e.preventDefault();
        var data = new FormData(this);
        $.ajax( {
            url: $url,
            type: 'POST',
            data: data,
            processData: false,
            contentType: false
        } ).done(function(data){
            //- data = $.parseJSON(data);
            loadTiposServicios();
        });
    });

    $('#btn-add-tserv').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        showContent('content-center-form-tipos-servicios');
    });

    $('#btn-add-tserv-rel').on('click', function(){
        modo = 'add_rel_tserv';
        showBarTop('btns-rel-tserv');
        loadGrupos();

        $('#content-center-form-tipos-servicios .item').removeClass('del-active');
        $('#content-center-form-tipos-servicios .btn-del-item').removeClass('show-btn-del');

    });

    $('#inputIconTServ').on('change', function(evt){
        var files = evt.target.files; // FileList object
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e ) {
                    $('#add-icon-tserv').css('background-image', 'url('+e.target.result+')')
                };
            })(f );
            reader.readAsDataURL(f );
        }
    });

    /**********************************************************************  GRUPOS   ****/

    $('#formGrupo').on('submit', function (e){
        $url = $(this).attr('action');
        e.preventDefault();
        var data = new FormData(this);
        $.ajax( {
            url: $url,
            type: 'POST',
            data: data,
            processData: false,
            contentType: false
        } ).done(function(data){
            //- data = $.parseJSON(data);
            if(!data.error){
                loadGrupos();
            }

        });
    });


    $('#btn-add-grupo').on('click', function(){
        showContent('content-form-grupo');
    });

    $('#inputFileGrupo').on('change', function(evt){
        var files = evt.target.files; // FileList object
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e) {
                    $('#add-icon-grupo').css('background-image', 'url('+e.target.result+')')
                };
            })(f);
            reader.readAsDataURL(f);
        }
    });

    /*$('#btn-save-grupo').on('click', function(){
        event.preventDefault();
    });*/

    /**********************************************************************  ITEMS   ****/

    $('#formItem').on('submit', function (e){
        $url = $(this).attr('action');
        e.preventDefault();
        var data = new FormData(this);
        $.ajax( {
            url: $url,
            type: 'POST',
            data: data,
            processData: false,
            contentType: false
        } ).done(function(data){
            //- data = $.parseJSON(data);
            if(data == true){
                if(modo = 'edit_item_tserv'){
                    showContent('content-center-form-tipos-servicios');
                }else{
                    showContent('content-center-items');
                }
            }
        });
    });

    $('#btn-add-item').on('click', function(){
        $('#hdn-id-grupo-item').attr('value', $(this).data('grupo'));
        modo = "new Item";
        showContent('content-form-item');
    });

    $('#inputIconItem').on('change', function(evt){
        var files = evt.target.files; // FileList object
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e) {
                    $('#add-icon-item').css('background-image', 'url('+e.target.result+')')
                };
            })(f);
            reader.readAsDataURL(f);
        }
    });

    $('#btn-add-item-rel').on('click', function(){
        modo = 'add_rel_item';
        showBarTop('btns-rel-item');
        loadGrupos();

        $('#content-center-form-tipos-servicios .item').removeClass('del-active');
        $('#content-center-form-tipos-servicios .btn-del-item').removeClass('show-btn-del');

    });

    $('#btn-listo-rel-item').on('click', function(){
        showContent('content-form-item');
        showBarTop('btns-form');
    });

    $('#btn-cancel-rel-item').on('click', function(){
        showContent('content-form-item');
        showBarTop('btns-form');
    });


/**************************** ITEMS RELACIONADOS *************************/
    $('#btn-add-relacionado').on('click', function(){
        $(this).hide();
        $('#form-item-right').addClass('form-item-right-open');
    });

    $('#btn-listo-relacionados').on('click', function(){
        $('#btn-add-relacionado').show();
        $('#form-item-right').removeClass('form-item-right-open');
    });

    $('#select-grupos').on('click', function(){
        list = $('#list-grupos');
        if(list.css('display') == 'block'){
            $('#list-grupos').hide();
        }else{
            $('#list-grupos').show();
        }
    });

    $('#btn-listo-resltserv').on('click', function(){

        showContent('content-center-form-tipos-servicios');
        showBarTop('btns-form');
    });

    $('#btn-cancel-resltserv').on('click', function(){

        showContent('content-center-form-tipos-servicios');
        showBarTop('btns-form');
    });




});
/************************************************************************/

function loadTiposServicios(){
    $.get('api/tiposervicios', function(data){
        //-- data = $.parseJSON(data);
        console.log(data);
        $('#content-center-tipos-servicios .item-tmp').remove();
        for(i=0; i<data.length; i++){
            addNewServicio(data[i]);
        }
        $('#content-center-tipos-servicios').append('<div style="clear:both" class="item-tmp"></div>');
        showContent('content-center-tipos-servicios');
        viewActive = 'tserv';
    });
}

function addNewServicio(tserv){
    newTserv = $('#templateItem').clone();
    newTserv.attr('id', tserv.id);
    newTserv.addClass('item-tmp');
    newTserv.find('.text-item').text(tserv.nombre);
    newTserv.find('.img-item').css('background-image', 'url(img_tiposervicios/'+tserv.id+'.jpg)');

    newTserv.on('click', function(){
        tserv_selected = $(this).attr('id');
        if($(this).hasClass('')){
        }
        loadGrupos();
    });

    newTserv.bind('taphold',function(){
        $(document).on('click', function(e){
            target = e.target;
            console.log('click document --->', target);
            if(target != 'btn-del-item' || target != 'btn-edit-item'){
                endEditItem();
            }
        });
        $('#content-center-tipos-servicios .btn-edit-item').fadeIn();
        $('#content-center-tipos-servicios .btn-add-item').fadeIn();
        $('#content-center-tipos-servicios .btn-add-item').addClass('show-add-item');
        $('#content-center-tipos-servicios .item').addClass('del-active');
        $('#content-center-tipos-servicios .btn-del-item').addClass('show-btn-del');
        setTimeout(function(){
            centerContent('content-center-tipos-servicios');
        }, 200)
    });

    newTserv.find('.btn-edit-item').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        showContent('content-center-form-tipos-servicios');
        tserv_selected = $(this).parent().attr('id');
        loadFormTserv($(this).parent().attr('id'));
    });

    $('#content-center-tipos-servicios').append(newTserv);
    newTserv.show()
}

function loadGrupos(){
    $('#content-center-grupos .grupo').remove();
    $('#content-center-grupos .clear').remove();
    $.get('api/grupos', function(data){
        //-- data = $.parseJSON(data);
        console.log(data);
        for(i=0; i<data.length; i++){
            addNewGrupo(data[i]);
        }
        divClear = $('<div/>',{
            class:'clear',
            css:{
                clear:'both'
            }
        });
        $('#content-center-grupos').append(divClear);
        showContent('content-center-grupos');
        viewActive = 'grupo';
    });
}

/***********************************************************************************************ADD GRUPO***/
function addNewGrupo(grupo){
    newGrupo = $('#templateGrupo').clone();
    newGrupo.attr('id', grupo.id);
    newGrupo.find('.text-grupo').text(grupo.nombre);
    newGrupo.find('.img-item').css('background-image', 'url(img_grupos/'+grupo.id+'.jpg)');

    for(j = 0; j < grupo.items.length; j++){
        for(k = 0; k < grupo.items[j].tipo_servicios.length; k++){
            if(grupo.items[j].tipo_servicios[k].pivot.tipo_servicio_id == tserv_selected){
                newGrupo.addClass('resalt');
            }
        }
    }
    /*************EVENTOS************/
    newGrupo.on('click', function(){
        $('.grupoSelected').removeClass('grupoSelected');
        $(this).addClass('grupoSelected');
        $('#content-right').addClass('inGrupo');
        $('#content-center-items .items').remove();
        $.get('api/items?grupo='+grupo.id, function(data) {
            //-- data = $.parseJSON(data);
            if(data.length == 0){
                $('.btn-add-item').show();
                $('.btn-add-item').addClass('show-add-item');
            }else{
                $('.btn-add-item').hide();
                $('.btn-add-item').removeClass('show-add-item');

            }
            for (i = 0; i < data.length; i++) {
                addNewItem(data[i]);
            }
            $('#content-center-items').append('<div style="clear:both" class="items"></div>');
            showContent('content-center-items');
        });
    });

    /*************************/
    newGrupo.bind('taphold',function(){
        $(document).on('click', function(e){
            console.log('click document');
            target = e.target;
            if(target != 'btn-del-grupo' || target != 'btn-edit-grupo'){
                endEditItem();
            }
        })

        $('#content-center-grupos .btn-edit-item').fadeIn();
        $('#content-center-grupos .btn-add-item').fadeIn();
        $('#content-center-grupos .btn-add-item').addClass('show-add-item');
        $('#content-center-grupos .item').addClass('del-active');
        $('#content-center-grupos .btn-del-item').addClass('show-btn-del');
    });

    newGrupo.find('.btn-edit-item').on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        showContent('content-form-grupo');
        loadFormGrupo($(this).parent().attr('id'));
    });
    btnDel = newGrupo.find('.btn-del-item');
    btnDel.data('id', grupo.id);
    btnDel.on('click', function(e){
        e.preventDefault();
        e.stopPropagation();
        deleteGrupo($(this).data('id'));
    });

    $('#content-center-grupos').append(newGrupo);
    newGrupo.show();
    return grupo.id;
}

function addNewItemRel(grupo){
    newGrupo = $('#templateItemRel').clone();
    newGrupo.attr('id', grupo.id);
    newGrupo.find('.text-grupo').text(grupo.nombre);
    newGrupo.find('.img-grupo').css('background-image', 'url(img_grupos/'+grupo.id+'.jpg)');
    $('#content-right').append(newGrupo);
    newGrupo.bind('taphold',function(){
        $(document).on('click', function(e){
            console.log('click document');
            target = e.target;
            if(target != 'btn-del-grupo' || target != 'btn-edit-grupo'){
                endEditGrupos();
            }
        });
        $('.grupo').addClass('del-active');
        $('.btn-del-grupo').addClass('show-btn-del');
        $('.btn-edit-grupo').fadeIn();
    });


    newGrupo.find('.btn-edit-grupo div div').on('click', function(){
        loadFormGrupo($(this).parent().parent().parent().attr('id'));
        openFormGrupo();

    });
    btnDel = newGrupo.find('.btn-del-grupo');
    btnDel.data('id', grupo.id);
    btnDel.on('click', function(){
        deleteGrupo($(this).data('id'));
    });
    newGrupo.show();
    return grupo.id;
}

function addNewItem(item){
    newItem = $('#templateItem').clone();
    newItem.data('item', item);
    newItem.attr('id', item.id);
    $('#btn-add-item').data('grupo', item.grupo_id);
    newItem.addClass('item-tmp');
    newItem.find('.text-item').text(item.nombre);
    newItem.find('.img-item').css('background-image', 'url(img_items/'+item.id+'.jpg)');

    if(modo == 'add_rel_tserv') {
        if($.inArray(item.id, arrayRelTserv) > -1){
            newItem.addClass('item-selected');
        }
    }else{

        for(j = 0; j < item.tipo_servicios.length; j++){
            if(item.tipo_servicios[j].pivot.tipo_servicio_id == tserv_selected){
                newItem.addClass('resalt');
            }
        }


        newItem.bind('taphold', function () {
            $(document).on('click', function (e) {
                console.log('click document');
                target = e.target;
                if (target != 'btn-del-item' || target != 'btn-edit-item') {
                    endEditItem();
                    centerContent('content-center-items');
                }
            });
            $('#content-center-items .btn-edit-item').fadeIn();
            $('#content-center-items .btn-add-item').fadeIn();
            $('#content-center-items .btn-add-item').addClass('show-add-item');
            $('#content-center-items .item').addClass('del-active');
            $('#content-center-items .btn-del-item').addClass('show-btn-del');
            centerContent('content-center-items');
        });
    }
    newItem.on('click', function(){
        $this = $(this);
        item = $this.data('item');
        if(modo == 'add_rel_tserv'){
            if($this.hasClass('item-selected')){
                $this.removeClass('item-selected');
                for(i = 0; i < arrayRelTserv.length; i++){
                    if(arrayRelTserv[i] == $this.attr('id')){
                        arrayRelTserv.splice(i,1);
                        deleteItemReltserv(item.id);
                    }
                }
            }else{
                arrayRelTserv.push(parseInt($this.attr('id')));
                $this.addClass('item-selected');
                addNewItemRelTServ(item, false);

                $('#ct-rel-tserv-item').append($('<input/>',{id:'rel-tserv-'+item.id, name:'items_rel[]', type:'hidden', value:item.id}));
            }

        }else if(modo == ''){
            //openFormItem();
            loadInfoItem($(this).attr('id'));
            showContent('content-center-info-item');
        }else if(modo == 'add_rel_item'){
            if($this.hasClass('item-selected')){
                $this.removeClass('item-selected');

            }else{
                item = $(this).data('item');
                $(this).addClass('item-selected');
                addItemRel(item, false, false, true);
            }

        }


    });

    newItem.find('.btn-edit-item div div').on('click', function(){
        console.log('view Item');
        loadFormItem($(this).parent().parent().parent().attr('id'));
        openFormItem();
    });
    btnDel = newGrupo.find('.btn-del-item');
    btnDel.data('id', item.id);
    btnDel.on('click', function(){
        deleteItem($(this).data('id'));
    });

    $('#content-center-items').append(newItem);
    newItem.show();
}

function addNewItemRelTServ(item, bd, edit, del){
    edit || ( edit = true );
    del || ( del = true );
    newItemRel = $('#template-item-relacionado').clone();
    newItemRel.data('bd',bd);
    newItemRel.data('item', item);
    newItemRel.attr('id', 'item-rel-tserv-'+item.id);
    newItemRel.addClass('item-tmp');
    newItemRel.find('.text-item').text(item.nombre);
    newItemRel.find('.text-item-grupo').text(item.grupo.nombre);
    newItemRel.find('.img-item').css('background-image', 'url(img_items/'+item.id+'.jpg)');

    if(edit){
        newItemRel.find('.btn-edit-item').on('click', function(){
            modo = 'edit_item_tserv';
            showContent('content-form-item');
            item = $(this).parent().data('item');
            loadFormItem(item.id);
        });
    }else{
        newItemRel.find('.btn-edit-item').remove();
    }

    if(del){
        btnDel = newItemRel.find('.btn-del-item');
        btnDel.data('id', item.id);
        btnDel.on('click', function(){
            for(i = 0; i < arrayRelTserv.length; i++){
                if(arrayRelTserv[i] == $(this).data('id')){
                    arrayRelTserv.splice(i,1);
                }
            }
            deleteItemReltserv($(this).data('id'), bd);
        });
    }else{
        newItemRel.find('.btn-del-item').remove();
    }

    $('.ct-items-tserv').append(newItemRel);
    newItemRel.show();
}

function loadInfoItem(id){
    $.get('api/item/'+id+'/show', function(data){
        //-- data = $.parseJSON(data);
        $('#info_item_img').attr('src', 'img_items/'+data.item.id+'.jpg');
        $('#info_item_nombre').text(data.item.nombre);
        $('#info_item_desc').text(data.item.descripcion);
        $('#info_item_desc_in').text(data.item.descripcion_interna);

        for(i=0; i < data.galery.length; i++){
            img = $('<div/>',{
                class:'item-img',
                css:{
                    'background-image':'url('+data.galery[i]+')'
                }
            })
            $('#info_item_galery').append(img);
        }
        centerContent('content-center-info-item');

    });

}

function loadFormTserv(id){

    $('.ct-items-tserv .item-rel').remove();
    $.get('api/tiposervicios/'+id+'/show', function(data) {
        //-- data = $.parseJSON(data);
        $('#fromTipoServ').attr('action', 'api/tiposervicios/update');
        $('#hdn-id-tserv').attr('value',data.tserv.id);
        $('#txt-nombre-tserv').val(data.tserv.nombre);
        $('#add-icon-tserv').css('background-image', 'url(img_tiposervicios/'+data.tserv.id+'.jpg)');
        console.log(data);
        if(data.tserv.items.length > 0){
            for(i = 0; i < data.tserv.items.length; i++){
                addNewItemRelTServ(data.tserv.items[i], true);
            }
        }

        console.log(data);
    });
}

function loadFormItem(id){
    if(modo == 'edit_item_tserv'){
        $get_param = '?id_tserv='+tserv_selected;
    }else{
        $get_param = '';
    }

    $.get('api/item/'+id+'/show'+$get_param, function(data){
        //-- data = $.parseJSON(data);
        console.log(modo);
        if(modo == 'edit_item_tserv'){
            $('#formItem').attr('action', 'api/item/update_tserv');

            $('#item-relacionados .item-rel').remove();
            for(i=0; i < data.rel.length; i++){
                addItemRel(data.rel[i], true, false, true);
            }
        }else{
            $('#formItem').attr('action', 'api/item/update');
        }

        $('#hdn-id-item').attr('value',data.item.id);
        $('#txt-nombre-item').attr('value',data.item.nombre);
        $('#txa-desc-item').text(data.item.descripcion);
        $('#txa-desc-int-item').text(data.item.descripcion_interna);
        $('#add-icon-item').css('background-image', 'url(img_items/'+data.item.id+'.jpg)');
        $('#item-galeria-container .item-img').remove();
        for(i=0; i < data.galery.length; i++){
            addImageGalery(data.galery[i], true, '');
        }

    });
}

function addImageGalery(img_url, bd, name){
    num = $('#form-item').data('numGalery');
    img = $('<div/>',{
        class:'item-img',
        css:{
            'background-image':'url('+img_url+')'
        }
    }).append('<div class="content-btn-del-image"><div class="btn-del btn-del-image show-btn-del"><i class="fa fa-times" aria-hidden="true"></i></div></div>');
    if(!bd)
        img.data('name', name);
    img.data('url', img_url);
    img.data('bd', bd);
    img.find('.btn-del').on('click', function(){
        img = $(this).parent().parent();
        if(img.data('bd')){
            $("<input/>", {
                name:'galery_del[]',
                type:'hidden',
                value:img.data('url')
            }).appendTo('#ct-galery-del');
        }else{
            $('.inputFileGaleria').each(function(){
                name = $(this).val().split('\\');
                name = name.pop()
                console.log(name,img.data('name'));
                if(name == img.data('name')){
                    $(this).remove();
                }
            });
        }
        img.remove();
        w = $('#item-galeria-container').width();
        $('#item-galeria-container').css('width', (w-315)+'px');
    });
    w = $('#item-galeria-container').width();
    $('#item-galeria-container').css('width', w+315+'px');
    $('#item-galeria-container').append(img);
}

function loadFormGrupo(id){
    $.get('api/grupo/'+id+'/show', function(grupo){
        //-- grupo = $.parseJSON(grupo);
        $('#formGrupo').attr('action', 'api/grupo/update');
        $('#hdn-id-grupo').attr('value',grupo.id);
        $('#txt-nombre-grupo').attr('value',grupo.nombre);
        $('#txa-desc-grupo').text(grupo.descripcion);
        $('#add-icon-grupo').css('background-image', 'url(img_grupos/'+grupo.id+'.jpg)');
        $('#add-icon-grupo i').removeClass('fa-plus').addClass('fa-pencil');
    });
}

function deleteGrupo(id){
    $.ajax({
        url: 'api/grupo/'+id+'/delete',
        type: 'DELETE',
        //data: id,
        processData: false,
        contentType: false
    }).done(function(data){
        //-- data = $.parseJSON(data);
        if(data){
            $('#'+data).remove();
        }
    });
}

function deleteItem(id){
    $.ajax({
        url: 'api/item/'+id+'/delete',
        type: 'DELETE',
        //data: id,
        processData: false,
        contentType: false
    }).done(function(data){
        //-- data = $.parseJSON(data);
        if(data){
            $('#'+data).remove();
        }
    });
}

function deleteItemReltserv(id, bd){
    $('#item-rel-tserv-'+id).remove();
    $('#rel-tserv-'+id).remove();
    if(bd){
        $("<input/>", {
            name:'items_rel_del[]',
            type:'hidden',
            value:id
        }).appendTo('#ct-rel-tserv-item-del');
    }
}

function endEditItem(){
    console.log('endItem');
    $(document).off('click');
    $('.contentActive .item').removeClass('del-active');
    $('.contentActive .btn-edit-item').hide();
    $('.contentActive .item .btn-del-item').removeClass('show-btn-del');
    $('.contentActive .btn-edit-item').hide();
    $('.contentActive .btn-add-item').removeClass('show-add-item');
    $('.contentActive .btn-add-item').hide();
    centerContent($('.contentActive').attr('id'));
}

function addItemRel(item, bd, edit, del){
    console.log('add rel', edit);
    if(!bd){
        input = $("<input/>", {
            id:'hdn-item-rel-'+item.id,
            name:'items_rel[]',
            type:'hidden',
            value:item.id
        }).appendTo('#ct-item-rel-inputs');
    }


    // $('#ct-item-rel-inputs').append($("<div>"))
    //$('#').append('<input id="hdn-item-relacionado-'+item.id+'" type="hidden" name="relacionados{}" value="'+item.id+'"/>');
    newItemRel = $('#template-item-relacionado').clone();
    newItemRel.data('bd',bd);
    newItemRel.attr('id', 'item-rel-tserv-'+item.id);
    newItemRel.addClass('item-tmp');
    newItemRel.find('.text-item').text(item.nombre);
    newItemRel.find('.text-item-grupo').text(item.grupo.nombre);
    newItemRel.find('.img-item').css('background-image', 'url(img_items/'+item.id+'.jpg)');

    if(edit){
        newItemRel.on('taphold', function(){
            $('.ct-items-tserv .btn-add-item').show();
            $('.ct-items-tserv .btn-add-item').addClass('show-add-item');
            $('#content-center-form-tipos-servicios .item').addClass('del-active');
            $('#content-center-form-tipos-servicios .btn-del-item').addClass('show-btn-del');
        });
    }else{
        newItemRel.find('.btn-edit-item').remove();
    }

    if(del){
        newItemRel.find('.btn-del-item').on('click', function(){
            idItem = $(this).parent().parent().data('id');
            if(bd){
                $("<input/>", {
                    name:'items_rel_del[]',
                    type:'hidden',
                    value:item.id
                }).appendTo('#ct-item-rel-del');
            }else{
                $("#hdn-item-rel-"+idItem).remove();
            }
            $(this).parent().parent().remove();
        });
    }else{
        newItemRel.find('.btn-del-item').remove();
    }

    $('#item-relacionados').append(newItemRel);
    newItemRel.show();
}

function showBarTop(bar){
    $('#top-bar .btn-form').hide();
    $('#'+bar).show();
}

function hideBarTop(bar){
    $('#'+bar).hide();
}

function showBarRight(){
    $('#panel_right').css('right', '0%');
}

function hideBarRight(){
    $('#panel_right').css('right', '-18%');
}

function showBtnBack(){
    $('#btn-volver').fadeIn();
}

function hideBtnBack(){
    $('#btn-volver').fadeOut();
}

function showContent(content){
    console.log(content);
    actShow = $(".contentActive").attr('id');
    hideContent(actShow);
    //backTo = actShow;
    $(".contentActive").removeClass('contentActive');
    centerContent(content);
    $("#"+content).addClass('contentActive');
    $("#"+content).trigger('show');
    $("#"+content).fadeIn();
}

function hideContent(content){
    $("#"+content).fadeOut();
}

function centerContent(content){
    auxH = $("#"+content).height();
    auxH2 = $('#panel_mid').height();
    if(auxH < auxH2){
        $("#"+content).css('margin-top', ((auxH2-auxH)/2)+'px');
    }
}

function setTitle(text){
    $('#bar-text').html(text);
}