<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class CH extends Model {

    protected $table = "ch";

    public $timestamps = false;

    const CH_DIR = 'images/ch';
    const PUBLI_DIR = 'images/ch/publicidades';
    const LOGOS_DIR = 'images/ch/logos';
    const QRCODES_DIR = 'images/ch/qrcodes';
    
    public function scopeCliente($query, $id){
        return $query->where('cliente_id', $id);
    }

    public function saveCH($array){
        \DB::beginTransaction();
        try{
            $this->fill($array);
            $bool = $this->save();
        }catch (\Exception $exception){
            \DB::rollback();
            return $exception->getMessage();
        }
        \DB::commit();
        return $bool;
    }

    public function updateCH($array, $id)
    {
        \DB::beginTransaction();
        try{
            $bool = $this->where("cliente_id",$id)->update($array);
        }catch (\Exception $exception){
            \DB::rollback();
            return $exception->getMessage();
        }
        \DB::commit();
        return $bool;
    }

}
