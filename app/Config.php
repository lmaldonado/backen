<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model {

	protected $table = "config";
    public $timestamps = false;

    const DEFAULT_PASS = 'NeoCVSepelios';

    public function scopeCliente($query, $id){
        return $query->where('cliente_id', $id);
    }



}