<?php 
namespace App;

use App\Homenaje;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;

class Servicio extends Model {

	public $timestamps = false;

    const FOTO_URL = 'images/servicios/';
    
    protected $dates = [
        'salida',
        'fecha_inicio',
        'fecha_fin'
    ];

    protected $fillable = ['responso_id','empresa_id'];
     
	public function scopeSala($query, $sala){
		$rs = $query->where('salas_id', $sala);
		return $rs;
	}


	public static function formatHour($hour){
		$aux = explode(' ', $hour);
		$aux2 = explode(':',$aux[0]);

		if($aux[1] == 'PM'){
			$aux2[0]+=12;
		}
		return $aux2[0].':'.$aux2[1].':00'; 
	}

	public function salas()
    {
        return $this->belongsTo(Sala::class);
    }

    public function homenajes()
    {
        return $this->hasMany(Homenaje::class);
    }

	public function velatorios(){
		return $this->hasMany('App\Velatorio');
	}

	public function sala(){
		return $this->belongsTo('App\Sala');
	}

	public function religion(){
		return $this->belongsTo('App\Religion');
	}

	public function destino(){
		return $this->belongsTo('App\Destino');
	}
    public function misa(){
        return $this->belongsTo('App\Responso','responso_id');
    }
    public function empresa(){
        return $this->belongsTo('App\Empresa','empresa_id');
    }


	/*public static function descargarImagen($url,$name){
		$ch = curl_init ( $url );
        curl_setopt( $ch , CURLOPT_HEADER, 0);
        curl_setopt( $ch , CURLOPT_RETURNTRANSFER, 1);
        $raw=curl_exec( $ch );
        curl_close ( $ch );
        $cake = explode('.', $url);
        $ext = array_pop($cake);
        $name2 = 'servicios/'.$name.'.'.$ext;
        Storage::put($name2, $raw);

        return $name.'.'.$ext;
	}*/

	/*public static function getServices(){
		$curl = curl_init();
        curl_setopt_array($curl, array(
         CURLOPT_RETURNTRANSFER => 1,
         CURLOPT_URL => 'http://homenaje.de/wp-content/plugins/neo_homenajes/json/conector.php',
         CURLOPT_USERAGENT => 'Request services',
         CURLOPT_POST => 1,
         CURLOPT_POSTFIELDS => array(
            'listar' => ''
         )
        ));
        
        $resp = curl_exec($curl);
        curl_close($curl);

        return $resp;
	}

	public static function AllHomenajes(){
	    $curl = curl_init();
        curl_setopt_array($curl, array(
         CURLOPT_RETURNTRANSFER => 1,
         CURLOPT_URL => 'http://homenaje.de/wp-content/plugins/neo_homenajes/json/conector.php',
         CURLOPT_USERAGENT => 'Request homenajes',
         CURLOPT_POST => 1,
         CURLOPT_POSTFIELDS => array(
            'homenaje' => ''
         )
        ));
        
        $resp = curl_exec($curl);
        curl_close($curl);

        return $resp;

	}*/

}
