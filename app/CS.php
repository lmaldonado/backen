<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class CS extends Model {

    protected $table = "cs";

    public $timestamps = false;

    const SIMBOLOS_DIR = 'images/simbolos';
    const CV_DIR = 'images/cs';
    const FONDOS_DIR = 'images/cs/fondos';
    const BANNERS_DIR = 'images/cs/banners';
    const PUBLI_DIR = 'images/cs/publicidades';
    const SERVICIOS_DIR = 'images/cs/fondo_servicio';    
    const QRCODES_DIR = 'images/cs/qrcodes';
    

    public static function getTipo_imagen(){
        return ['ninguna'=>'Vacio', 'foto'=>"Foto", 'cruz' => "Cruz", 'icono'=>"Icono Sala", 'simbolo'=>"Simbolo Sala"];
    }

    public static function getTipo_contenedor(){
        return ['nada'=>'Vacio', 'Salida'=>"Salida", 'Velatorios' => "Velatorios", 'Cementerio'=>"Cementerio", 'Ingreso'=>"Ingreso", 'Empresa'=>"Empresa"];
    }

    public function scopeCliente($query, $id){
        return $query->where('cliente_id', $id);
    }

    public function saveCV($array){
        \DB::beginTransaction();
        try{
            $this->fill($array);
            $bool = $this->save();
        }catch (\Exception $exception){
            \DB::rollback();
            return $exception->getMessage();
        }
        \DB::commit();
        return $bool;
    }

    public function updateCV($array, $id)
    {
        \DB::beginTransaction();
        try{
            $bool = $this->where("cliente_id",$id)->update($array);
        }catch (\Exception $exception){
            \DB::rollback();
            return $exception->getMessage();
        }
        \DB::commit();
        return $bool;
    }

}
