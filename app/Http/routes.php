<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Route::get('/', function () {
// 	return view('auth.login');
// });

//
	Route::get('/', 'RouterGetController@router');
	Route::post('/', 'RouterPostController@router');
	Route::delete('/', 'RouterDeleteController@router');
	
	
// 	Route::get('install', 'InstallController@migration');

// 	// Authentication routes...
// 	Route::get('auth/login', 'Auth\AuthController@getLogin');
// 	Route::get('login', 'Auth\AuthController@getLoginservicio');
// 	Route::get('insert', 'insertController@registro_usuario');//ruta para registrar usuarios
// 	Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
// 	Route::post('auth/login1', ['as' =>'auth/login1', 'uses' => 'Auth\AuthController@postLoginservicio']);
// 	Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

		

// 	//SOLO LOS USUARIOS AUTENTICADOS PUEDEN VER ESTAS RUTAS
// 	Route::group(['middleware' => 'auth'], function () {
// 		//rutas tipo servicio
// 		Route::get('servicios', 'ServicioController@index');
// 		Route::get('servicios/create', 'ServicioController@create');
// 		Route::get('servicios/{id}', 'ServicioController@show');
// 		Route::get('servicios/{id}/edit', 'ServicioController@edit');
// 		Route::put('servicios/{id}', 'ServicioController@update');
// 		Route::post('servicios', 'ServicioController@store');
// 		Route::delete('servicios/{id}', 'ServicioController@destroy');
// 		Route::get('servicios/{id}/status/{status}', 'ServicioController@status');
// 		Route::get('servicios/destino/{id}', 'ServicioController@destino');
// 		Route::post('servicios/destino/editar/{id}', 'ServicioController@destinoEdit');
// 		Route::post('servicios/destino/nuevo', 'ServicioController@destinoNew');
// 		Route::delete('servicios/destino/eliminar/{id}', 'ServicioController@eliminar_destino');
// 		Route::delete('servicios/destino/eliminar_img/{id}', 'ServicioController@eliminar_destino_img');
// 		Route::get('servicios/misa/{id}', 'ServicioController@misa');
// 		Route::post('servicios/misa/editar/{id}', 'ServicioController@editar_misa');
// 		Route::post('servicios/misa/nuevo', 'ServicioController@nueva_misa');
// 		Route::delete('servicios/misa/eliminar/{id}', 'ServicioController@eliminar_misa');
// 		Route::delete('servicios/velatorio/eliminar/{id}', 'ServicioController@eliminar_velatorio');
// 		Route::get('servicios/sala/{id}', 'ServicioController@sala');
// 		Route::post('servicios/sala/editar/{id}', 'ServicioController@editar_sala');
// 		Route::post('servicios/sala/nueva', 'ServicioController@nueva_sala');
// 		Route::delete('servicios/sala/eliminar/{id}', 'ServicioController@eliminar_sala');
// 		Route::delete('servicios/sala/eliminar_ico/{id}', 'ServicioController@eliminar_sala_icono');
// 		Route::delete('servicios/sala/eliminar_img/{id}', 'ServicioController@eliminar_sala_img');
// 		Route::delete('servicios/eliminar_img/{id}', 'ServicioController@eliminar_img_serv');

// 		// Salas
// 		Route::get('salas', 'SalaController@index');
// 		Route::get('salas/{id}', 'SalaController@show');
// 		Route::post('salas', 'SalaController@store');
// 		Route::post('salas/{id}', 'SalaController@update');
// 		Route::delete('salas/{id}', 'SalaController@destroy');

// 		//rutas tipo velatorio
// 		Route::delete('velatorios/{id}', 'VelatorioController@destroy');

// 		//rutas tipo empresa
// 		Route::post('empresas', 'EmpresaController@store');

// 		//rutas tipo responso misa
// 		Route::post('responso/misa', 'ResponsoController@store');
		
// 		//Routes for licencia
// 		Route::get('config/licencia', 'LicenciaController@show');
// 		Route::post('config/licencia', 'LicenciaController@update');
// 		Route::post('config/licencia/validar', 'LicenciaController@check_license');
// 		Route::delete('config/licencia/{id}', 'LicenciaController@delete');

		
// 		//Routes for resource Config
// 		Route::get('config', 'ConfigController@index');
// 		Route::get('config/configuracion', 'ConfigController@configuracion');
// 		Route::post('config/configuracion', 'ConfigController@configuracionStore');

// 		//Route::post('config', 'StylesController@store');
// 		//Routes for resource cv
// 		Route::get('config/cv', 'CVController@index');
// 		Route::post('config/cv', 'CVController@store');
// 		  //Routes for resource cs
// 		Route::get('config/cs', 'CSController@index');
		
// 		Route::post('config/cs', 'CSController@store');
// 		//Routes for resource ch
// 		Route::get('config/ch', 'CHController@index');
// 		Route::get('config/ch/sync/servicios', 'CHController@sync');
// 		Route::get('config/conexion', 'CHController@conexion');
// 		//Route::post('config/ch/sync/servicios', 'CHController@sync1');
// 		Route::post('config/ch', 'CHController@store');
// 		Route::post('config/ch/asociarSalas', 'CHController@asociarSalas'); 
// 		// RUTAS NI
// 		Route::get('config/ni', 'NIController@index');
// 		Route::post('config/ni', 'NIController@store');
// 		//Rutas de Homenaje
// 		Route::get('servicios/{id}/homenajes', 'HomenajeController@index');
// 		Route::post('servicios/{id}/homenajes', 'HomenajeController@store');
// 		Route::get('servicios/{id}/{accion}/homenajesHide', 'HomenajeController@update');
// 		Route::delete('servicios/{id}/homenajes', 'HomenajeController@destroy');
// 		Route::get('homenaje/{id}', 'HomenajeController@show');

// 		Route::get('productos','ProductoController@index');
// 		Route::post('productos', 'ProductoController@store');
// 		Route::post('productos/{id}', 'ProductoController@update');

// 	});
	
	
// 	//API
// 	Route::get('api/servicios', 'ServicioController@api_showAll');
// 	Route::get('api/servicios/{id}', 'ServicioController@api_show');
// 	Route::get('api/servicios_por_sala/{salas}', 'ServicioController@api_show_by_room');
// 	//Route::get('api/styles', 'StylesController@api_showAll');
// 	//Route::get('api/styles/{id}', 'StylesController@api_show');
// 	Route::get('api/homenaje', 'HomenajeController@api_showAll');
// 	Route::get('api/homenaje/{id}', 'HomenajeController@api_show');
// 	Route::delete('api/homenaje/{id}', 'HomenajeController@api_delete');
// 	Route::get('api/servicios/{id}/homenajes', "HomenajeController@api_showAll");
// 	Route::get('api/config/cv', 'CVController@api_config');
// 	Route::get('api/config/cs', 'CSController@api_config');
// 	Route::get('api/config/ch', 'CHController@api_config');
// 	Route::get('api/config/ni', 'NIController@api_config');
// 	Route::post('api/config/ch/sala', 'CHController@api_sala');
// 	Route::post('api/config/cv/salas', 'CVController@api_salas');
// 	Route::get('api/productos', 'ProductoController@api_showAll');
// 	Route::get('api/salas', 'CSController@api_salas');

// 	Route::get('servicio/sala/{id}', 'ServicioController@sala');
// 	//DEBUGGING ALL QUERYS
// 	/*Event::listen('illuminate.query', function($query)
// 	{
// 		var_dump($query);
// 	});*/
// 	Route::get('api/ip', 'ServicioController@ip');

// 	// Validación de la licencia desde las carteleras
// 	Route::get('api/licencia/{product_id}', 'LicenciaController@validar');
	
// 	// API Store empresa
// 	Route::get('api/empresa/index', 'EmpresaController@api_index');
// 	Route::post('api/empresa', 'EmpresaController@api_store');
// 	Route::post('api/empresa/editar/{id}', 'EmpresaController@cambio');
// 	Route::delete('api/empresa/eliminar/{id}', 'EmpresaController@destroy');
	
// 	// API Store responso
// 	Route::post('api/responso', 'ResponsoController@api_store');

// 	//plugin worpress
// //  Route::group( ['middleware' => 'cors'], function(){
// 		Route::get('plugin/servicios', 'ServicioController@servicios_plugin');
// 		Route::get('plugin/servicio/{id}', 'ServicioController@servicio_plugin');
// 		Route::get('plugin/homenajes', 'ServicioController@homenajes_plugin');
// 		Route::get('plugin/homenaje/{id}', 'ServicioController@homenaje_plugin');
// //  });
