<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\ServicioController;
use App\Http\Controllers\SalaController;
use App\Http\Controllers\VelatorioController;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\ResponsoController;
use App\Http\Controllers\LicenciaController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\CVController;
use App\Http\Controllers\CSController;
use App\Http\Controllers\CHController;
use App\Http\Controllers\NIController;
use App\Http\Controllers\HomenajeController;
use App\Http\Controllers\ProductoController;


class RouterDeleteController extends Controller {

	public function router(Request $request){
        if($request->ruta =="usuario/eliminar"){ 
            $class = new LoginController(); return $class->eliminar($request); 
        } else

		if($request->ruta =="servicios/delete"){ //modificado por alberto
            $class = new ServicioController(); return $class->destroy($request->id); 
        } else
        if($request->ruta =="servicios/destino/eliminar"){ //modificado por alberto
            $class = new ServicioController(); return $class->eliminar_destino($request->id); 
        } else
        if($request->ruta =="servicios/destino/eliminar_img"){ //modificado por alberto
            $class = new ServicioController(); return $class->eliminar_destino_img($request->id); 
        } else
        if($request->ruta =="servicios/misa/eliminar"){ //modificado por alberto
            $class = new ServicioController(); return $class->eliminar_misa($request->id); 
        } else
        if($request->ruta =="servicios/velatorio/eliminar"){ //modificado por alberto
            $class = new ServicioController(); return $class->eliminar_velatorio($request->id); 
        } else
        if($request->ruta =="servicios/sala/eliminar"){ //modificado por alberto
            $class = new ServicioController(); return $class->eliminar_sala($request,$request->id); 
        } else
        if($request->ruta =="servicios/sala/eliminar_ico"){ //modificado por alberto
            $class = new ServicioController(); return $class->eliminar_sala_icono($request,$request->id); 
        } else
        if($request->ruta =="servicios/sala/eliminar_img"){ //modificado por alberto
            $class = new ServicioController(); return $class->eliminar_sala_img($request,$request->id); 
        } else
        if($request->ruta =="servicios/eliminar_img"){ //modificado por alberto
            $class = new ServicioController(); return $class->eliminar_img_serv($request->id); 
        } else
        if($request->ruta =="salas"){ 
            $class = new SalaController(); return $class->destroy($request,$request->id); 
        } else
        if($request->ruta =="velatorios"){ 
            $class = new VelatorioController(); return $class->destroy($request,$request->id); 
        } else
        if($request->ruta =="config/licencia"){ 
            $class = new LicenciaController(); return $class->delete($request,$request->id); 
        } else
        if($request->ruta =="eliminarPalabra"){ 
            $class = new CHController(); return $class->eliminarPalabra($request); 
        } else
        if($request->ruta =="servicios/homenajes"){ //modificado por alberto
            $class = new HomenajeController(); return $class->destroy($request,$request->id); 
        } else
        if($request->ruta =="api/homenaje"){ 
            $class = new HomenajeController(); return $class->api_delete($request->id); 
        } else
        if($request->ruta =="api/empresa/eliminar"){ 
            $class = new EmpresaController(); return $class->destroy($request->id); 
        } 
	}

}