<?php 
namespace App\Http\Controllers;

use App\CV;
use App\Sala;
use App\Tablet;
use App\Sesion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class CVController extends Controller {

    // private $models,$salas,$cliente;

    // public function __construct(CV $cv, Sala $sala){
    //     $this->models = $cv;
    //     $this->salas = $sala;
    //     $this->cliente = 1;
    // }

    public function index(Request $request){
        $numImagen = 0;
        $salas = Sala::cliente(1)->get();
        $configCV = CV::cliente(1)->first();
        if(empty($configCV)){
            $configCV = new CV();
        }

        $files = File::allFiles(CV::SIMBOLOS_DIR);
        $simbolos = array();
            
        foreach ($files as $file) 
        {
            $auxFile = str_replace('\\','/',(string)$file);
            $auxFile = explode('/', $auxFile);
            $auxFile = array_pop($auxFile);
            $auxFile2 = explode('.', $auxFile);
            $simbolos[$auxFile] = $auxFile2[0];
        }

        $files = File::allFiles(CV::BANNERS_DIR);
        $banners = array();

        foreach ($files as $file) 
        {
            $banners[] = str_replace('\\','/',(string)$file);
        }

        $files = File::allFiles(CV::FONDOS_DIR);
        $fondos = array();
            
        foreach ($files as $file) 
        {
            $fondos[] = str_replace('\\','/',(string)$file);
        }

        $files = File::allFiles(CV::PUBLI_DIR);
        $publicidades = array();
            
        foreach ($files as $file) 
        {
            $publicidades[] = str_replace('\\','/',(string)$file);
        }
        $files = File::allFiles(CV::SERVICIOS_DIR);
        $fondo_servicio = array();
            
        foreach ($files as $file) 
        {
            $fondo_servicio[] = str_replace('\\','/',(string)$file);
        }

        $sala_simbolos = unserialize($configCV->simbolos);

        $configCV->logo_encabezado = CV::CV_DIR.'/'.$configCV->logo_encabezado; 


        return view('configuracion.cv',
            [
                'cv'=>$configCV,
                'sala_simbolos'=>$sala_simbolos,
                'salas'=>$salas,
                'tipos_imagen'=> CV::getTipo_imagen(),
                'tipos_contenedor' => CV::getTipo_contenedor(),
                'simbolos' => $simbolos,
                'banners' => $banners,
                'fondos' => $fondos,
                'publicidades' => $publicidades,
                'request' => $request,
                'numImagen' => $numImagen,
                'fondo_servicio'=>$fondo_servicio
            ]);
    }

    public function store(Request $request){
        $cliente_id = 1;
        $cv = CV::cliente($cliente_id)->first();
        if(empty($cv)){
            $cv = new CV();
        }

        $cv->cliente_id = $cliente_id;
        // ENCABEZADO
        $cv->tipo_encabezado = $request->tipo_encabezado;
        $cv->texto_encabezado = $request->texto_encabezado;
        // if($request->hasFile("logo_encabezado")){
        //     $nameFile = $cv->cliente_id.'_'.uniqid().'.'.$request->file('logo_encabezado')->getClientOriginalExtension();
        //     $request->file("logo_encabezado")->move(CV::CV_DIR, $nameFile);
        //     $cv->logo_encabezado = $nameFile;
        // }

        // IMAGENES
        $cv->imagen1 = $request->imagen1;
        $cv->imagen2 = $request->imagen2;
        $cv->imagen3 = $request->imagen3;
        $cv->imagen4 = $request->imagen4;

        // CONTENEDORES 
        $cv->contenedor1 = $request->contenedor1;
        $cv->contenedor2 = $request->contenedor2;
        $cv->contenedor3 = $request->contenedor3;
        $cv->contenedor4 = $request->contenedor4;
        // $cv->color_back  = $request->color_back;


        // SIMBOLOS
        $simbolos = array();
        foreach($request->salas as $i=>$sala){
            $simbolos[$sala] = $request->simbolos[$i];
        }
        $cv->simbolos = serialize($simbolos);

        
        if(!File::exists(CV::CV_DIR)){
            File::makeDirectory(CV::CV_DIR);
            File::makeDirectory(CV::BANNERS_DIR);
            File::makeDirectory(CV::FONDOS_DIR);
            File::makeDirectory(CV::PUBLI_DIR);
        }

        // BANNERS
        $cv->tiempo_banner = $request->tiempo_banner;        
        if($request->banner_delete != ""){
            $filesDelete = explode(',',$request->banner_delete);
            foreach($filesDelete as $file){
                File::delete($file);
            }
        }
        if($request->hasFile('banner')){
            $files = $request->file('banner');
            foreach($files as $file){
                if($file != NULL){
                    $nameFile = $cv->cliente_id.'_'.uniqid().'.'.$file->getClientOriginalExtension();
                    $file->move(CV::BANNERS_DIR, $nameFile);  
                }
            }
        }

        // FONDOS
        if($request->fondo_delete != ""){
            $filesDelete = explode(',',$request->fondo_delete);
            foreach($filesDelete as $file){
                File::delete($file);
            }
        }
        if($request->hasFile('fondo')){
            $files = $request->file('fondo');
            foreach($files as $file){
                if($file != NULL){
                    $nameFile = $cv->cliente_id.'_'.uniqid().'.'.$file->getClientOriginalExtension();
                    $file->move(CV::FONDOS_DIR, $nameFile);  
                }
            }
        }

        // PUBLICIDADES
        $cv->tiempo_publi = $request->tiempo_publi;
         if($request->publi_delete != ""){
            $filesDelete = explode(',',$request->publi_delete);
            foreach($filesDelete as $file){
                File::delete($file);
            }
        }
        if($request->hasFile('publi')){
            $files = $request->file('publi');
            foreach($files as $file){
                if($file != NULL){
                    $nameFile = $cv->cliente_id.'_'.uniqid().'.'.$file->getClientOriginalExtension();
                    $file->move(CV::PUBLI_DIR, $nameFile);  
                }
            }
        }
        //SERVICIOS
         if($request->fondo_servicio_delete != ""){
            $filesDelete = explode(',',$request->fondo_servicio_delete);
            foreach($filesDelete as $file){
                File::delete($file);
            }
        }
        if($request->hasFile('fondo_servicio')){
            $files = $request->file('fondo_servicio');
            foreach($files as $file){
                if($file != NULL){
                    $nameFile = $cv->cliente_id.'_'.uniqid().'.'.$file->getClientOriginalExtension();
                    $file->move(CV::SERVICIOS_DIR, $nameFile);  
                }
            }
        }

        //CONFIG GENERAL
        $cv->multiples_empresas=$request->multiples_empresas;
        $cv->tiempo_servicios=$request->tiempo_servicios;

        //dd($cv);
        $cv->save();

        return redirect('/?ruta=config/cv&save=1');
    }

    /**
     * Devuelve la configuración del Back-end: los banners, los fondos, la 
     * publicidad, los servicios, los símbolos y el logo de la empresa.
     */
    public function api_config(){

        $configCV = CV::cliente(1)->first(); // ¿Por qué busca directamente al cliente 1?
        
        // BANNERS
        $files = File::allFiles(CV::BANNERS_DIR);
        $banners = array();
        foreach ($files as $file) 
        {
            $banners[] = str_replace('\\','/',(string)$file);
        }

        // FONDOS
        $files = File::allFiles(CV::FONDOS_DIR);
        $fondos = array();
        foreach ($files as $file) 
        {
            $fondos[] = str_replace('\\','/',(string)$file);
        }

        // PUBLICIDADES
        $files = File::allFiles(CV::PUBLI_DIR);
        $publicidades = array();
        foreach ($files as $file) 
        {
            $publicidades[] = str_replace('\\','/',(string)$file);
        }

        // servicios
        // $files = File::allFiles(CV::SERVICIOS_DIR);
        // $fondo_servicio = array();
        // foreach ($files as $file) 
        // {
        //     $fondo_servicio[] = str_replace('\\','/',(string)$file);
        // }
        
        // SIMBOLOS
        $sala_simbolos = unserialize($configCV->simbolos);
        foreach($sala_simbolos as $key=>$simbolo){
            $sala_simbolos[$key] = $simbolo;
        }

        $configCV->simbolos = $sala_simbolos;
        
        // LOGO
        $configCV->logo_encabezado = CV::CV_DIR.'/'.$configCV->logo_encabezado; 

        return response()->json(["configCV"=>$configCV, "fondos"=>$fondos, "banners" => $banners, "publicidades"=>$publicidades]);
    }

    public function api_salas(Request $request){
        $cv = CV::cliente(1)->first();

        $cv->tipo_servidor = $request->tipo;

        if($request->tipo == 'local'){
            $cv->servidor_ip = $request->url;
            $cv->salas_id = $request->sala;
        }elseif($request->tipo == 'online'){
            $cv->servidor_url = $request->url;
            $cv->salas_id = $request->sala;
        }elseif($request->tipo == 'web'){
            $cv->servidor_web = $request->url;
            
            $sala='';
            $sala_sep = (explode(",",$request->sala));
            // dd($sala_sep);
            for($i=0;$i<count($sala_sep);$i++){
                $sala_sel = Sala::where('id_online',$sala_sep[$i])->first();
                if($sala==''){
                    $sala=$sala.''.$sala_sel->id;
                }else{
                    $sala=$sala.','.$sala_sel->id;
                }
            }
            // dd($sala);
            $cv->salas_id = $sala;
        }else{
            $cv->salas_id = $request->sala;
        }
        

        $cv->save();

        return (['mensaje'=>'Servidor registrado CV']);
    }
     public function validarfinger(Request $request,$fp){
        $cv = CV::cliente(1)->first();
        $finger = $fp;
        $table= Tablet::where('fingerprint','=',$finger)->first();
        if (empty($table)){
            $true=false;

        }else{
            $true=true;

        }        

        return (['true'=>$true,'table'=>$table]);
    }
    public function iniciasesion(Request $request,$finger){ 
        $sesion=new Sesion();

        date_default_timezone_set('America/Argentina/Buenos_Aires');
        $fecha =date('Y-m-d').' '.date('H:i:s'); 
        $IP= $_SERVER['REMOTE_ADDR'];

        $sesion->fecha=$fecha;
        $sesion->ip=$IP;
        $sesion->fringerprint=$finger;
        $sesion->save();
        return ($sesion);
    }
}

