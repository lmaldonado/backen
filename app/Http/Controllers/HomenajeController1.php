<?php 

namespace App\Http\Controllers;


use App\Homenaje;
use App\Sala;
use App\Servicio;
use App\Destino;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Carbon\Carbon;
use Ixudra\Curl\Facades\Curl;
class HomenajeController extends Controller {

	public function index(Request $request, $id){
		$servicio = Servicio::find($id);
		$homenajes = Homenaje::toServicio($id)->get();
		return view('homenajes.index',
			[
				"servicio" => $servicio,
				"homenajes" => $homenajes
			]);
	}

	public function create(){
	}

	public function show($id){
		$models = Homenaje::where("servicios_id",$id)->get();
		return view('homenajes.index',
			[
				"homenaje" => $models
			]);
	}

	public function store(Request $request, $id){

		if($request->homenaje_id != ""){
			$homenaje = Homenaje::find($request->homenaje_id);
			// dd('Paso por aqui',$request->mensaje,$id);
		}else{
			$homenaje = new Homenaje();
		}
		// dd($homenaje);

		$homenaje->servicio_id = $id;
		$homenaje->nombre = $request->nombre;
		$homenaje->mensaje = $request->mensaje;
		$datetime = $datetime = date('Y-m-d h:m:s');
		$homenaje->created_at = $datetime;

		$homenaje->save();

		if($request->hasFile('foto')) {
			$nameFile = $request->file('foto')->getClientOriginalName();
			$request->file('foto')->move(Homenaje::FOTO_DIR, $nameFile);

			$homenaje->foto = $nameFile;
			$homenaje->save();
		}

	   return redirect("/?ruta=servicios/homenajes&id=".$id);
	}

	public function edit($id){
	}

	public function update(Request $request, $id, $accion){
		$homenaje = Homenaje::find($id);
		if ($homenaje!=null && $accion=='ocultar'){
			$datetime = date('0000-00-00 00:00:00');
			$homenaje->deleted_at = $datetime;
			$homenaje->save();
		}elseif($homenaje!=null && $accion=='mostrar'){
			$datetime = $datetime = date('Y-m-d h:m:s');
			$homenaje->deleted_at = $datetime;
			$homenaje->save();
		}
		
		return redirect("/?ruta=servicios/homenajes&id=".$homenaje->servicio_id);
	}
	
	public function destroy(Request $request, $id){
		
		$homenaje = Homenaje::find($request->homenaje_id1)->delete();

		return redirect("/?ruta=servicios/homenajes&id=".$id);
	}

	function api_show($id){


		$homenaje = Homenaje::with('servicio')->whereId($id)->get();
		return response()->json($homenaje);
	}

	function api_showAll($id){
		//$servicio = Servicio::find($id);
		
		$servicio = Servicio::where('sala_id', $id)
			->whereDate('fecha_inicio', '<=', Carbon::today()->toDateString())
			->whereDate('salida', '>=', Carbon::today()->toDateString())
			->where('status','=','0')
			->orderBy('id','desc')
			->first();
		// dd($servicio);
		if($servicio) 
		{
			$homenajes = Homenaje::with('servicio')->where('servicio_id', $servicio->id)->orderBy('id','desc')->get();
			
			return response()->json($homenajes);
		}
		else
		{
			return (['msj'=>'no hay homenajes']);
		}
	}

	function api_delete($id){
		$homenaje_delete = Homenaje::where('id',$id)->first();
		$homenaje = Homenaje::where('id', $id)->delete();

			$getHomenaje = Curl::to('http://homenaje.de/wp-content/plugins/neo_homenajes/puente/homenaje.php?eliminar')
						->withData( array(

										'eliminar'=>$homenaje_delete->id_online) )
						
						->post();
	   // return $homenaje;
		return $getHomenaje;
	}
}	