<?php 

namespace App\Http\Controllers;

use App\CV;
use App\Sala;
use App\Destino;
use App\Homenaje;
use App\Religion;
use App\Servicio;
use App\Producto;
use App\Velatorio;
use App\Pedidos;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class ProductoController extends Controller {

    public function index(Request $request){
	    $productos = Producto::all();
	    foreach($productos as $sala){
			$sala->foto = Producto::FOTOPROD_URL.'/'.$sala->foto; 
		}

		return view('configuracion.productos.index',
            [
                "producto" => $productos
            ]);
	}

	public function create(){

		
		return view('configuracion.productos.create');
	}

	public function show($id){
		
	}

	public function store(Request $request){
		$producto = new Producto();
        $producto->nombre = $request->nombre;
        $producto->precio = $request->precio;
        $producto->save();

        if($request->hasFile("icono")){
            $nameFile = $producto->id.'_'.uniqid().'.'.$request->file('icono')->getClientOriginalExtension();
            $request->file("icono")->move(Producto::FOTOPROD_URL, $nameFile);
            $producto->foto = $nameFile;
            $producto->save();
        }

		return redirect('/?ruta=productos');


	}

	public function update($id, Request $request){
		$producto = Producto::find($id);
        $producto->nombre = $request->nombre;
        $producto->precio = $request->precio;

        if($request->hasFile("icono")){
        	File::delete(Producto::FOTOPROD_URL.'/'.$producto->foto);
            $nameFile = $producto->id.'_'.uniqid().'.'.$request->file('icono')->getClientOriginalExtension();
            $request->file("icono")->move(Producto::FOTOPROD_URL, $nameFile);
            $producto->foto = $nameFile;
        }
        $producto->save();
        return redirect('/?ruta=productos');


	}

	public function destroy($id){
		$producto = Producto::find($id);
		$producto->delete();

		return redirect('/?ruta=productos');
	}


	public function api_show($id){
		//$servicio = Servicio::with('velatorios')->with('sala')->with('destino')->with('religion')->find($id);
		//BUSQUEDA POR SALA_ID
		
	}

	public function api_showAll(){
	 

		$productos = Producto::all();
		foreach($productos as $prod){
			$prod->foto = Producto::FOTOPROD_URL.'/'.$prod->foto; 
		}
		return response()->json($productos);
	}    
	public function api_pedido($idsala,$idservicio,$idproduto){
	 

		$pedido = new Pedidos();
		$pedido->sala_id=$idsala;
		$pedido->servicio_id=$idservicio;
		$pedido->producto_id=$idproduto;
		$pedido->save();
		
		return response()->json($pedido);
	}    
	public function pedidos_index(Request $request){
		$pedidos =  Pedidos::with('salas','servicios')->get();
		
		
	
			return view('configuracion.productos.pedidos',compact('pedidos'));
            
	}  
	public function pedidos_cobrar(Request $request,$id,$status){
		
		
		if($status=='P'){
		    $pedidos =  Pedidos::find($id);
		    $pedidos->estado='C' ;
		    $pedidos->update();
		}
		if($status=='C'){
		    $pedidos =  Pedidos::find($id);
		    $pedidos->estado='P';
		    $pedidos->update();
		}
		
		return redirect('/?ruta=pedidos');
            
	}  
           
	
}