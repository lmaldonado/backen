<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\ServicioController;
use App\Http\Controllers\SalaController;
use App\Http\Controllers\VelatorioController;
use App\Http\Controllers\EmpresaController;
use App\Http\Controllers\ResponsoController;
use App\Http\Controllers\LicenciaController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\CVController;
use App\Http\Controllers\CSController;
use App\Http\Controllers\CHController;
use App\Http\Controllers\NIController;
use App\Http\Controllers\HomenajeController;
use App\Http\Controllers\ProductoController;

class RouterPostController extends Controller {

	public function router(Request $request){
        if($request->ruta =="validacion"){ 
            $class = new LoginController(); return $class->validacion($request); 
        } else
        if($request->ruta =="usuario/crear"){ 
            $class = new LoginController(); return $class->registrar($request); 
        } else
        if($request->ruta=="usuario/actualizar"){//if($request->ruta =="items"){ 
            $class = new LoginController(); return $class->editar($request); 
        } else

        if($request->ruta =="servicios/nuevo"){ 
            $class = new ServicioController(); return $class->store($request); 
        } else
        if($request->ruta =="servicios/update"){ //modificado por alberto
            $class = new ServicioController(); return $class->update($request,$request->id); 
        } else
        if($request->ruta=="servicios/destino/editar"){//modificado por alberto
            $class = new ServicioController(); return $class->destinoEdit($request->id,$request);
        } else
        if($request->ruta =="servicios/destino/nuevo"){ 
            $class = new ServicioController(); return $class->destinoNew($request); 
        } else
        if($request->ruta=="servicios/misa/editar"){ //modificado por alberto
            $class = new ServicioController(); return $class->editar_misa($request->id,$request);
        } else
        if($request->ruta =="servicios/misa/nuevo"){ //modificado por alberto
            $class = new ServicioController(); return $class->nueva_misa($request); 
        } else
        if($request->ruta =="servicios/sala/editar"){ //modificado por alberto
            $class = new ServicioController(); return $class->editar_sala($request,$request->id); 
        } else
        if($request->ruta =="servicios/sala/nueva"){ //modificado por alberto
            $class = new ServicioController(); return $class->nueva_sala($request); 
        } else
        if($request->ruta=="salas"){
            $class = new SalaController(); return $class->store($request);
        } else
        if($request->ruta =="salas/update"){ 
            $class = new SalaController(); return $class->update($request,$request->id); 
        } else
        if($request->ruta=="empresas"){
            $class = new EmpresaController(); return $class->store($request);
        } else
        if($request->ruta =="responso/misa"){ 
            $class = new ResponsoController(); return $class->store($request); 
        } else
        if($request->ruta=="config/licencia"){
            $class = new LicenciaController(); return $class->delete($request);
        } else
        if($request->ruta =="config/licencia/validar"){ 
            $class = new LicenciaController(); return $class->check_license($request); 
        } else
        if($request->ruta=="config/configuracion"){
            $class = new ConfigController(); return $class->configuracionStore($request);
        } else
        if($request->ruta =="config/cv"){ 
            $class = new CVController(); return $class->store($request); 
        } else
        if($request->ruta=="config/cs"){
            $class = new CSController(); return $class->store($request);
        } else
        if($request->ruta =="config/ch"){ 
            $class = new CHController(); return $class->store($request); 
        } else
        if($request->ruta =="nuevaPalabra"){ 
            $class = new CHController(); return $class->nuevaPalabra($request); 
        } else        
        if($request->ruta=="config/ch/asociarSalas"){
            $class = new CHController(); return $class->asociarSalas($request);
        } else
        if($request->ruta =="config/ni"){ 
            $class = new NIController(); return $class->store($request); 
        } else
        if($request->ruta=="servicios/homenajes"){//modificado por alberto
            $class = new HomenajeController(); return $class->store($request,$request->id);
        } else
        if($request->ruta =="productos"){ 
            $class = new ProductoController(); return $class->store($request); 
        } else
        if($request->ruta=="productos/update"){
            $class = new ProductoController(); return $class->update($request->id,$request);
        } else
        if($request->ruta =="api/config/ch/sala"){ 
            $class = new CHController(); return $class->api_sala($request); 
        } else
        if($request->ruta=="api/config/cv/salas"){
            $class = new CVController(); return $class->api_salas($request);
        } else
        if($request->ruta =="api/empresa"){ 
            $class = new EmpresaController(); return $class->api_store($request); 
        } else
        if($request->ruta=="api/empresa/editar"){
            $class = new EmpresaController(); return $class->cambio($request,$request->id);
        } else
        if($request->ruta =="api/responso"){ 
            $class = new ResponsoController(); return $class->api_store($request); 
        }         else
        if($request->ruta =="api/download"){ 
            $class = new LicenciaController(); return $class->download($request); 
        } else
        if ($request->ruta=='carpeta') {
            $class = new LicenciaController(); return $class->crearCarpeta($request);
        } else
        if ($request->ruta=='api/descomprimir') {
            $class = new LicenciaController(); return $class->descomprimir($request);
        } else
        if ($request->ruta=='empresa/guardar') {
            $class = new LicenciaController(); return $class->storeEmpresa($request);
        } 
        else
        if ($request->ruta=='empresa/guardar/post') {
            $class = new LicenciaController(); return $class->storeEmpresapost($request);
        } else
        if ($request->ruta=='verifica_licencia') {
            $class = new LicenciaController(); return $class->chequea_licencias($request);
        } 
       

	}

}