<?php namespace App\Http\Controllers;
use App\Sala;
use App\Config;
use App\iphost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Ixudra\Curl\Facades\Curl;

class ConfigController extends Controller {

    public function index(Request $request){
        $client_id = 1;
        $config = Config::cliente(1)->first();
        if(empty($config)){
            $config = new Config();
        }

        return view('configuracion.index', ['config'=>$config]);
    }
     public function configuracion(Request $request){
        $client_id = 1;
        $config = iphost::where('idcliente',1)->first();
        
        if(empty($config)){
            $config = new iphost();
            $config->idcliente=1;
            $config->save();
           
        }
        $Local_IP = gethostbyname(getHostName());
        
     
        $config->iphost=$Local_IP;
        return view('configuracion.configuracion', ['config'=>$config]);
    }
     public function configuracionStore(Request $request){
        $client_id = 1;
        $config = iphost::find(1);
        $config->iphost=$request->iphost;
        $config->password=$request->password;
        $config->email=$request->email;
        $config->save();
        //$response = Curl::to('http://localhost/ClassCartelerias/webservice.php')
        $response = Curl::to('http://www.neosepelios.com.ar/neoinnovaciones/_deb/ClassCartelerias/webservice.php')
                    ->withData( array( 'accion' => 'Insertar', 
                                        'iphost'=>$request->iphost,
                                        'idCliente'=>1,
                                        'email'=>$config->email,
                                        'password'=>$config->password) )
                    ->returnResponseObject()
                    
                    ->post();
        // return view('configuracion.configuracion', ['config'=>$config]);
    }

    function api_show($id){
        /*$style = Styles::find($id);
        return response()->json($style);*/
    }

    function api_showAll(){
        /*$style = Styles::all();
        return response()->json($style);*/
    }
}
