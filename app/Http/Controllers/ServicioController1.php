<?php 

namespace App\Http\Controllers;

use App\CV;
use App\CH;
use App\Sala;
use App\Destino;
use App\Empresa;
use App\Homenaje;
use App\Religion;
use App\Servicio;
use App\Responso;
use App\Velatorio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Storage;
use Mockery\Undefined;
use Illuminate\Support\Facades\File;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
// use App\Http\Requests\Request;
// // use App\Http\Requests\Request;

class ServicioController extends Controller {
	
	/**
	 * Devuelve todos los servicios en la base de datos
	 */
	public function index(Request $request){
		$servicio = Servicio::with("salas", "homenajes" )->limit(25)->orderBy('id','desc')->get();
		
		return view('servicios.index',["servicio" => $servicio]);
	}
	
	
	/**
	 * Muestra el formulario para crear un servicio nuevo.
	 */
	public function create(){
		// $salas = Sala::all()->lists('nombre','id')->put('-10','En domicilio')->put('99','En privado')->put('100','A confirmar');
		$salas = Sala::all();
		$religions = Religion::all()->lists('nombre','id');
		// $empresas = Empresa::all()->lists('nombre', 'id')->put('Agregar', 'Agregar');
		// $destinos = Destino::all()->lists('nombre','id')->put('Agregar', 'Agregar');
		// $responso = Responso::all()->lists('nombre','id')->put('Agregar', 'Agregar');
		// $configCV = CV::cliente(1)->first();

		$destinos = Destino::all();
		// $destinoSel = Destino::find($servicio->destino_id);
		// $empresas = Empresa::all()->lists('nombre', 'id')->put('Agregar', 'Agregar');
		$empresas = Empresa::all();
		// $responso = Responso::all()->lists('nombre','id')->put('Agregar', 'Agregar');
		$responso = Responso::all();
		// $responsoSel = Responso::find($servicio->responso_id);
		$configCV = CV::cliente(1)->first();
		
		//dd($empresas);

		return view('servicios.create', ['salas'=>$salas, 'destinos'=>$destinos, 'empresas'=>$empresas,
			'religiones'=>$religions,'responso'=>$responso,'CV'=>$configCV]);
	}
	
	
	/**
	 * Muestra el servicio indicado en la variable $id para su edición
	 */
	public function show(Request $request){
		// dd($request->all());
		$servicio = Servicio::find($request->id);
		$servicio->velatorios();
		// $salas = Sala::all()->lists('nombre','id')->put('-10','En domicilio')->put('99','En privado')->put('100','A confirmar');
		$salas = Sala::all();
		$religions = Religion::all()->lists('nombre','id');
		// $destinos = Destino::all()->lists('nombre','id')->put('Agregar', 'Agregar');
		$destinos = Destino::all();
		// $destinoSel = Destino::find($servicio->destino_id);
		// $empresas = Empresa::all()->lists('nombre', 'id')->put('Agregar', 'Agregar');
		$empresas = Empresa::all();
		// $responso = Responso::all()->lists('nombre','id')->put('Agregar', 'Agregar');
		$responso = Responso::all();
		// $responsoSel = Responso::find($servicio->responso_id);
		$configCV = CV::cliente(1)->first();

		// dd($servicio);
		
		return view('servicios.create', ['salas'=>$salas, 'destinos'=>$destinos,'empresas'=>$empresas, 
			'religiones'=>$religions, 'servicio'=>$servicio,'responso'=>$responso, 'CV'=>$configCV]);
	}
	
	
	/**
	 * Cambia el estado de un servicio, de activo a inactivo y viseversa.
	 */
	public function status(Request $request,$id, $status){
		$date = new Carbon;
		if ($status == 'activar'){
			$servicio = Servicio::find($id);
			// $servicio->fecha_inicio= $date;
			// $servicio->salida=$date->addWeeks(1);
			$servicio->status = 0;
			$servicio->save();
			return redirect('/?ruta=servicios');
						
		}elseif ($status == 'inactivar'){
			$servicio = Servicio::find($id);
			// $servicio->fecha_inicio= $date->subWeeks(1);
			// $servicio->salida=$date->yesterday();
			$servicio->status = 1;
			$servicio->save();
			return redirect('/?ruta=servicios');
		}
	}
	
	
	/**
	 * Guarda en la base de datos un servicio nuevo
	 */
	public function store(Request $request){
		// dd($request->all(),'Paso');
		$cliente = 1;
		$servicio = new Servicio();
		$servicio->nombres = $request->nombres;
		$servicio->apellidos = $request->apellidos;
		
		if($request->fecha_nac == '0000-00-00' || $request->fecha_nac==null || $request->fecha_nac==''){
			// dd('paso',$request->fecha_nac);
			
		}else{
			// dd('no paso');
			$servicio->fecha_nac = Carbon::createFromFormat('d/m/Y',$request->fecha_nac);
		}

		$servicio->fecha_fac = Carbon::createFromFormat('d/m/Y',$request->fecha_fac);
		$servicio->religion_id = $request->religion_id;
		$servicio->status = 0;
		// $servicio->empresa_id = $request->empresa_id;
		$servicio->domicilio = $request->domicilio;
		$servicio->confirmar = $request->confirmar;

		if($request->cortejo_act == 'on'){
			$servicio->cortejo_act = 1;
			$servicio->cortejo_link = $request->tex_cortejo;

			// dd($servicio->cortejo_act,$servicio->cortejo_link);
		}else{
			$servicio->cortejo_act = 0;
			$servicio->cortejo_link = '';

			// dd($servicio->cortejo_act,$servicio->cortejo_link);
		}

		$configCV = CV::cliente(1)->first();
		if($configCV->multiples_empresas==1){
			$servicio->empresa_id = $request->empresa_id;
		}
		
		$servicio->sala_id = $request->sala_id;
		
		// if( $servicio->confirmar1=='1' || $servicio->sala_id !='99'  ){
			
		// }
		$servicio->salida = Carbon::createFromFormat('d/m/Y H:i:s', $request->salida_date.' '.$request->salida_time.':00');
		// $servicio->responso = Carbon::createFromFormat('d/m/Y H:i:s', $request->responso_date.' '.$request->responso_time.':00');
		
		$servicio->servicio_cementerio = Carbon::createFromFormat('d/m/Y H:i:s', $request->cementerio_date.' '.$request->cementerio_time.':00');
		
		// if($request->destino == 'Agregar'){
		// 	$destino = new Destino();
		// 	$destino->nombre = $request->nombre_destino;
		// 	$destino->direccion_destino;

		// 	$destino->save();
			
		// 	if($request->hasFile('mapa_destino')) {
		// 		$request->file('mapa_destino')->move('images/destinos',$destino->id.'.'.$request->file('mapa_destino')->getClientOriginalExtension());
		// 	}

		// 	$servicio->destino_id = $destino->id;
		// }else{
		// 	
		// }
		$servicio->destino_id = $request->destino_id;
		// Guarda el responso
		$servicio->responso_id = $request->responso_id;
		if($request->responso_id>0){			
			$servicio->responso = Carbon::createFromFormat('d/m/Y H:i:s', $request->responso_date.' '.$request->responso_time.':00');
			$servicio->tipo_responso = 0;
			// if($request->tipo_responso=='misa'){
			// 	$servicio->tipo_responso = 1;
			// }elseif($request->tipo_responso=='responso'){
			// 	$servicio->tipo_responso = 2;
			// }
		}

		if(isset($request->domicilio)){
			$servicio->domicilio = $request->domicilio;
		}		

		$servicio->cliente_id = $cliente;
		$servicio->save();
		// Sala, domicilio y velorio
		if($request->sala_id!=0 || $request->sala_id!=99 || $request->sala_id!=100){
			if($request->has('velatorio_fecha')){
				foreach($request->velatorio_fecha as $key=>$value){
					$velatorio = new Velatorio();
					$velatorio->servicio_id = $servicio->id;
					if($value!='' || $value!=null ){
						if($key == 0){
							$servicio->fecha_inicio = Carbon::createFromFormat('d/m/Y H:i:s', $value.' '.$request->velatorio_desde[$key].':00');
							
						}
						$velatorio->fecha = Carbon::createFromFormat('d/m/Y', $value);
						$velatorio->desde = Carbon::createFromFormat('H:i:s', $request->velatorio_desde[$key].':00');
						$velatorio->hasta = Carbon::createFromFormat('H:i:s', $request->velatorio_hasta[$key].':00');
						$velatorio->save();
						$servicio->fecha_fin = Carbon::createFromFormat('d/m/Y H:i:s', $value.' '.$request->velatorio_hasta[$key].':00');
					}
				}
				$servicio->save();
			}
		}		
		
		// if($request->responso_id == 'Agregar'){
		// 	$responso = new Responso();
		// 	$responso->nombre = $request->nombre_responso;
		// 	$responso->tipo = $request->tipo_responso;
		// 	$responso->direccion =$request->direccion_responso;

		// 	$responso->save();        

		// 	$servicio->responso_id = $responso->id;
		// }else{
		// 	$servicio->responso_id = $request->responso_id;
		// }

		
		
		if($request->hasFile('foto')) {
			$nameFile = $servicio->id.'_'.uniqid().'.'.$request->file('foto')->getClientOriginalExtension();
			$request->file('foto')->move(Servicio::FOTO_URL, $nameFile);

			$servicio->foto = Servicio::FOTO_URL . "/" . $nameFile;
			$servicio->save();
		}
		$sala_online=sala::where('id','=',$servicio->sala_id)->first();
		if(isset($sala_online->id_online)){
			$salon=$sala_online->id_online;
		}else{
			$salon=1;
		}
		if($servicio->destino_id>0){
			$destino_online=Destino::where('id','=',$servicio->destino_id)->first();
			$cementerio=$destino_online->id_online;
		}else{
			// $destino_online=0;
			$cementerio=0;
		}
			$nombre_fallecido=$servicio->nombres;
			$apellido_fallecido=$servicio->apellidos;
			$genero='';
			$fecha_nacimiento=$servicio->fecha_nac;
			$fecha_fallecido=$servicio->fecha_fac;
			$salida_servicio=date($servicio->salida);
			$hora_s_servicio=strtotime($servicio->salida);
			$servicio_cementerio=date($servicio->servicio_cementerio);
			$hora_s_cementerio=strtotime($servicio->servicio_cementerio);
			$velatorio_fecha=date($servicio->fecha_inico);
			$hora_v_inicio=strtotime($servicio->fecha_inico);
			$fotografia=0;
			
			
			$religion=$servicio->religion_id;
			$texto_homenaje='';
			$banner=0;
			$correo_familiar='';
			$clave_familiar='';
			$admin=$servicio->cliente_id;
			$modificado=date('Y/m/d H:i:s');
			$tipo='';
			$i=0;
			$velatorio=$servicio->velatorios;
			$files = Servicio::FOTO_URL;
			$configCH = CH::cliente(1)->first();
			
			$ch = CH::where('cliente_id',1)->first();
			$cv = CV::where('cliente_id',1)->first();
			if ($ch->tipo_servidor=='web' && !empty($ch->servidor_web)) {
				$url = $ch->servidor_web;
			}elseif ($cv->tipo_servidor=='web' && !empty($cv->servidor_web)) {
				$url = $cv->servidor_web;
			}
			if (!empty($url)){
				if($servicio->foto!=''){

				 	$getArray=
				
						Curl::to($url.'wp-content/plugins/neo_homenajes/puente/servicio.php')
						->withData( array('nombre_fallecido'=> $nombre_fallecido,
										'apellido_fallecido'=>  $apellido_fallecido,
										'genero'=>  $genero,
										'fecha_nacimiento'=>  $fecha_nacimiento,
										'fecha_fallecido'=> $fecha_fallecido,
										'salida_servicio'=>$salida_servicio,
										'hora_s_servicio'=>$hora_s_servicio,
										'servicio_cementerio'=>$servicio_cementerio,
										'hora_s_cementerio'=>$hora_s_cementerio,
										'velatorio_fecha'=>$velatorio_fecha,
										'hora_v_inicio'=>$hora_v_inicio,
										'fotografia'=>$fotografia,
										'cementerio'=>$cementerio,
										'salon'=>$salon,
										'religion'=>$religion,
										'texto_homenaje'=>$texto_homenaje,
										'banner'=>$banner,
										'correo_familiar'=>$correo_familiar,
										'clave_familiar'=>$clave_familiar,
										'admin'=>$admin,
										'modificado'=>$modificado,
										'tipo'=>$tipo,
										'info_velario_cv'=>$velatorio) )
						->withFile( 'imagen',public_path().'\\' .$servicio->foto, 'image/jpg', $files.$servicio->foto )
						->post();
				}else{
					$getArray=
				
						Curl::to($url.'wp-content/plugins/neo_homenajes/puente/servicio.php')
						->withData( array('nombre_fallecido'=> $nombre_fallecido,
										'apellido_fallecido'=>  $apellido_fallecido,
										'genero'=>  $genero,
										'fecha_nacimiento'=>  $fecha_nacimiento,
										'fecha_fallecido'=> $fecha_fallecido,
										'salida_servicio'=>$salida_servicio,
										'hora_s_servicio'=>$hora_s_servicio,
										'servicio_cementerio'=>$servicio_cementerio,
										'hora_s_cementerio'=>$hora_s_cementerio,
										'velatorio_fecha'=>$velatorio_fecha,
										'hora_v_inicio'=>$hora_v_inicio,
										'fotografia'=>$fotografia,
										'cementerio'=>$cementerio,
										'salon'=>$salon,
										'religion'=>$religion,
										'texto_homenaje'=>$texto_homenaje,
										'banner'=>$banner,
										'correo_familiar'=>$correo_familiar,
										'clave_familiar'=>$clave_familiar,
										'admin'=>$admin,
										'modificado'=>$modificado,
										'tipo'=>$tipo,
										'info_velario_cv'=>$velatorio) )
						->post();
				}
				
				$getArray=json_decode($getArray);
				$servicio->id_online=$getArray->id;
				$servicio->url1=$getArray->url;	
				$servicio->qrcode1 = $servicio->id.'_'.uniqid(). ".png";							
				QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($servicio->url1, CV::QRCODES_DIR . '/' . $servicio->qrcode1);
				$servicio->url2=$getArray->url_salon;	
				$servicio->qrcode2 = $servicio->id.'_'.uniqid(). ".png";					
				QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($servicio->url2, CV::QRCODES_DIR . '/' . $servicio->qrcode2); 
			}		
			$servicio->save();	


		

		return redirect('/?ruta=servicios');
	}

	
	/**
	 * Actualiza los datos de del servicios indocado en la variable $id
	 */
	public function update(Request $request){
		// dd($request->all());
		$cliente = 1;
		$servicio = Servicio::find($request->id);

		$servicio->nombres = $request->nombres;
		$servicio->apellidos = $request->apellidos;
		
		if($request->fecha_nac == '0000-00-00' || $request->fecha_nac==null || $request->fecha_nac==''){
			// dd('paso',$request->fecha_nac);
			$servicio->fecha_nac = 0000-00-00;
		}else{
			// dd('no paso');
			$servicio->fecha_nac = Carbon::createFromFormat('d/m/Y',$request->fecha_nac);
		}

		$servicio->fecha_fac = Carbon::createFromFormat('d/m/Y',$request->fecha_fac);
		$servicio->religion_id = $request->religion_id;
		$servicio->empresa_id = $request->empresa_id;
		$servicio->status = $request->status;
		
		// $servicio->confirmar = $request->confirmar;

		if($request->cortejo_act == 'on'){
			$servicio->cortejo_act = 1;
			$servicio->cortejo_link = $request->tex_cortejo;

			// dd($servicio->cortejo_act,$servicio->cortejo_link);
		}else{
			$servicio->cortejo_act = 0;
			$servicio->cortejo_link = '';

			// dd($servicio->cortejo_act,$servicio->cortejo_link);
		}

		// dd($request->cortejo_act);
		
		$servicio->sala_id = $request->sala_id;

		// if($request->sala_id==0){
		// 	$servicio->domicilio = $request->domicilio;
		// }else{
		// 	$servicio->domicilio = '';
		// }
		
		// if( $servicio->confirmar1=='1' || $servicio->sala_id !='99'  ){
		// 	$servicio->salida = Carbon::createFromFormat('d/m/Y H:i:s', $request->salida_date.' '.$request->salida_time.':00');
		// }
		$servicio->salida = Carbon::createFromFormat('d/m/Y H:i:s', $request->salida_date.' '.$request->salida_time.':00');
		$servicio->servicio_cementerio = Carbon::createFromFormat('d/m/Y H:i:s', $request->cementerio_date.' '.$request->cementerio_time.':00');
		
		// if($request->destino_id == 'Agregar'){
			
			
			// if($request->hasFile('mapa_destino_edit')) {
			// 	$destino = Destino::find($request->id_destino_editar);
			// 	$destino->nombre = $request->nombre_destino_editar;
			// 	$destino->direccion = $request->direccion_destino_editar;
			// 	$nameFile = $destino->id.'.'.$request->file('mapa_destino_edit')->getClientOriginalExtension();
			// 	$request->file('mapa_destino_edit')->move('images/destinos',$destino->id.'.'.$request->file('mapa_destino_edit')->getClientOriginalExtension());
			// 	$destino->mapa = $nameFile;
			// 	$destino->save();
			// 	$servicio->destino_id = $request->id_destino_editar;
			// }else{
			// 	$servicio->destino_id = $request->destino_id;
			// }
		$servicio->destino_id = $request->destino_id;	
		// }else{
			// $servicio->destino_id = $request->destino_id;
		// }
		// Guarda el responso
		$servicio->responso_id = $request->responso_id;
		if($request->responso_id>0){			
			$servicio->responso = Carbon::createFromFormat('d/m/Y H:i:s', $request->responso_date.' '.$request->responso_time.':00');
			$servicio->tipo_responso = 0;
			// if($request->tipo_responso=='misa'){
			// 	$servicio->tipo_responso = 1;
			// }elseif($request->tipo_responso=='responso'){
			// 	$servicio->tipo_responso = 2;
			// }
		}
		
		// foto del fallecido
		if($request->hasFile('foto')) {
			
			$nameFile = $servicio->id.'_'.uniqid().'.'.$request->file('foto')->getClientOriginalExtension();
			$request->file('foto')->move(Servicio::FOTO_URL, $nameFile);
			$servicio->foto = Servicio::FOTO_URL . "/" . $nameFile;
		}
		
		$servicio->cliente_id = $cliente;
		
		

		// Sala, domicilio y velorio
		if($request->sala_id!=0 || $request->sala_id!=99 || $request->sala_id!=100){
			Velatorio::where('servicio_id', $servicio->id)->delete();
			if($request->has('velatorio_fecha')){
				foreach($request->velatorio_fecha as $key=>$value){
					$velatorio = new Velatorio();
					$velatorio->servicio_id = $servicio->id;
					if($value!='' || $value!=null ){
						if($key == 0){
							$servicio->fecha_inicio = Carbon::createFromFormat('d/m/Y H:i:s', $value.' '.$request->velatorio_desde[$key].':00');
							
						}
						$velatorio->fecha = Carbon::createFromFormat('d/m/Y', $value);
						$velatorio->desde = Carbon::createFromFormat('H:i:s', $request->velatorio_desde[$key].':00');
						$velatorio->hasta = Carbon::createFromFormat('H:i:s', $request->velatorio_hasta[$key].':00');
						$velatorio->save();
						$servicio->fecha_fin = Carbon::createFromFormat('d/m/Y H:i:s', $value.' '.$request->velatorio_hasta[$key].':00');
					}
						
				}
				$servicio->save();
			}
		}
		// if($request->sala_id==-10){
		if(isset($request->domicilio)){
			$servicio->domicilio = $request->domicilio;
		}			

		$servicio->save();

		$files = Servicio::FOTO_URL;
		$sala_online=Sala::where('id',$servicio->sala_id)->first();
		$configCH = CH::cliente(1)->first();
		
		$ch = CH::where('cliente_id',1)->first();
		$cv = CV::where('cliente_id',1)->first();
		if ($ch->tipo_servidor=='web' && !empty($ch->servidor_web)) {
			$url = $ch->servidor_web;
		}elseif ($cv->tipo_servidor=='web' && !empty($cv->servidor_web)) {
			$url = $cv->servidor_web;
		}
		if (!empty($url)){
		
			if($servicio->foto!=''){

				 $getServicios=
				
						Curl::to($url.'wp-content/plugins/neo_homenajes/puente/servicio.php?edit')
						->withData( array(
										'editar'=>$servicio->id_online,
										'nombre_fallecido'=> $servicio->nombres,
										'apellido_fallecido'=>  $servicio->apellidos,
										'genero'=> '',
										'fecha_nacimiento'=>  $servicio->fecha_nac,
										'fecha_fallecido'=> $servicio->fac,
										'salida_servicio'=>date($servicio->salida),
										'hora_s_servicio'=>strtotime($servicio->salida),
										'servicio_cementerio'=>date($servicio->servicio_cementerio),
										'hora_s_cementerio'=>strtotime($servicio->servicio_cementerio),
										'velatorio_fecha'=>date($servicio->fecha_inico),
										'hora_v_inicio'=>strtotime($servicio->fecha_inico),
										'fotografia'=>0,
										'cementerio'=>$servicio->destino_id,
										'salon'=>isset($sala_online->id_online),
										'religion'=>$servicio->religion_id,
										'texto_homenaje'=>'',
										'banner'=>0,
										'correo_familiar'=>'',
										'clave_familiar'=>'',
										'admin'=>$servicio->cliente_id,
										'modificado'=>date('Y/m/d H:i:s'),
										'tipo'=>'',
										'info_velario_cv'=>$servicio->velatorios) )
						->withFile( 'imagen',public_path().'\\' .$servicio->foto, 'image/jpg', $files.$servicio->foto )
						->post();
				}else{
					$getServicios=
				
						Curl::to($url.'wp-content/plugins/neo_homenajes/puente/servicio.php?edit')
						->withData( array(
										'editar'=>$servicio->id_online,
										'nombre_fallecido'=> $servicio->nombres,
										'apellido_fallecido'=>  $servicio->apellidos,
										'genero'=> '',
										'fecha_nacimiento'=>  $servicio->fecha_nac,
										'fecha_fallecido'=> $servicio->fac,
										'salida_servicio'=>date($servicio->salida),
										'hora_s_servicio'=>strtotime($servicio->salida),
										'servicio_cementerio'=>date($servicio->servicio_cementerio),
										'hora_s_cementerio'=>strtotime($servicio->servicio_cementerio),
										'velatorio_fecha'=>date($servicio->fecha_inico),
										'hora_v_inicio'=>strtotime($servicio->fecha_inico),
										'fotografia'=>0,
										'cementerio'=>$servicio->destino_id,
										'salon'=>isset($sala_online->id_online),
										'religion'=>$servicio->religion_id,
										'texto_homenaje'=>'',
										'banner'=>0,
										'correo_familiar'=>'',
										'clave_familiar'=>'',
										'admin'=>$servicio->cliente_id,
										'modificado'=>date('Y/m/d H:i:s'),
										'tipo'=>'',
										'info_velario_cv'=>$servicio->velatorios) )
						->post();
				}
			}		
		return redirect('/?ruta=servicios/ver&id='.$servicio->id);
	}
	

	// buscar destinos
	public function destino($id){
		$destino = Destino::find($id);

		return response()->json($destino);
	}

	// editar destino
	public function destinoEdit($id, Request $request){
		$destino = Destino::find($id);
		$destino->nombre = $request->nombre_destino_editar;
		$destino->direccion = $request->direccion_destino_editar;
		
		if($request->hasFile('file-0')) {
			
			$nameFile = $destino->id.'_'.uniqid().'.'.$request->file('file-0')->getClientOriginalExtension();
			$request->file('file-0')->move('images/destinos',$nameFile);
			$destino->mapa = $nameFile;
		}
		$destino->save();
		return response()->json(['mensaje'=>'Registro actualizado','destino'=>$destino]);
		
	}

	// nuevo destino
	public function destinoNew(Request $request){
		$destino = new Destino();
		$destino->nombre = $request->nombre_destino_nuevo;
		$destino->direccion = $request->direccion_destino_nuevo;

		$destino->save();

		if($request->hasFile('file-0')) {
			$nameFile = $destino->id.'_'.uniqid().'.'.$request->file('file-0')->getClientOriginalExtension();
			$request->file('file-0')->move('images/destinos',$nameFile);
			$destino->mapa = $nameFile;
			$destino->save();
		}

		
		return response()->json(['mensaje'=>'Registro exitoso','destino'=>$destino]);
	}

	// eliminar destino
	public function eliminar_destino($id){
		$destino = Destino::find($id);
		$destino->delete();
		return response()->json(['mensaje'=>'Destino eliminado']);
	}
	// eliminar destino imagen
	function eliminar_destino_img($id){
		$destino = Destino::find($id);
		$destino->mapa = '';

		$destino->save();
		return response()->json(['mensaje'=>'Mapa eliminado']);
	}

	// buscar misa
	public function misa($id, Request $request){
		$misa = Responso::find($id);

		return response()->json($misa);
	}

	// agregar misa/responso
	public function nueva_misa(Request $request){
		$misa = new Responso();
		$misa->nombre = $request->nombre;
		$misa->direccion = $request->direccion;
		$misa->tipo = $request->tipo;

		$misa->save();
		return response()->json(['mensaje'=>'Registro exitoso','misa'=>$misa]);
	}

	// editar misa/responso
	public function editar_misa($id, Request $request){
		$misa = Responso::find($id);
		$misa->fill($request->all());

		$misa->save();
		return response()->json(['mensaje'=>'Sala actualizado','misa'=>$misa]);
	}

	// eliminar misa
	public function eliminar_misa($id){
		$misa = Responso::find($id);
		$misa->delete();
		return response()->json(['mensaje'=>'Misa eliminada']);
	}

	// obtener la sala seleccionada
	public function sala(Request $request,$id){
		// dd($request->all());
		$sala = Sala::find($request->id);

		return response()->json($sala);
	}

	// editar sala
	public function editar_sala(Request $request,$id){
		// return ('hola');
		$sala = Sala::find($id);
		$sala->cliente_id = 1;
		$sala->nombre = $request->nombre_sala;
		$sala->status = $request->status;
		$sala->codigo = $request->codigo_sala;
		$sala->save();
		if($request->hasFile("file_icono-0")){
			
			$nameFile = 'icono-'.$sala->id.'_'.uniqid().'.'.$request->file('file_icono-0')->getClientOriginalExtension();
			$mover = $request->file('file_icono-0')->move('images/salas',$nameFile);
			
			$sala->icono = $nameFile;
			
			$sala->save();
		}
		if($request->hasFile("file_letra-0")){
			
			$nameFile = 'letra-'.$sala->id.'_'.uniqid().'.'.$request->file('file_letra-0')->getClientOriginalExtension();
			$request->file('file_letra-0')->move('images/salas',$nameFile);
			
			$sala->letra = $nameFile;
			
			
			$sala->save();
		}
		
		return response()->json(['mensaje'=>'Sala actualizada','sala'=>$sala]);
	}

	// nueva sala
	public function nueva_sala(Request $request){
		$ch = CH::where('cliente_id',1)->first();
		$cv = CV::where('cliente_id',1)->first();
		if ($ch->tipo_servidor=='web' && !empty($ch->servidor_web)) {
			$url = $ch->servidor_web;
		}elseif ($cv->tipo_servidor=='web' && !empty($cv->servidor_web)) {
			$url = $cv->servidor_web;
		}

		$sala = new Sala();
		$sala->cliente_id = 1;
		$sala->nombre = $request->input('nombre_sala');
		$sala->status = $request->status;
		// $sala->codigo = $request->codigo_sala;
		$sala->save();

		if(isset($request->codigo_sala)){
			$sala->codigo = $request->codigo_sala;
			$sala->save();
		}
		if(isset($request->id_online)){
			$sala->id_online = $request->id_online;
			$sala->save();
		}
		if($request->hasFile("file_icono-0")){
			$nameFile = 'icono-'.$sala->id.'_'.uniqid().'.'.$request->file('file_icono-0')->getClientOriginalExtension();
			$request->file('file_icono-0')->move('images/salas',$nameFile);
			$sala->icono = $nameFile;
			$sala->save();
		}
		if($request->hasFile("file_letra-0")){
			$nameFile = 'letra-'.$sala->id.'_'.uniqid().'.'.$request->file('file_letra-0')->getClientOriginalExtension();
			$request->file('file_letra-0')->move('images/salas',$nameFile);
			$sala->letra = $nameFile;
			$sala->save();
		}
		if(!empty($url)){
			$salones_id_online = 
		 		Curl::to($url.'wp-content/plugins/neo_homenajes/puente/salon.php')
				->withData( array( 'nombre' => $sala->nombre, 'direccion' => '', 'url_maps' => '', 'frame_maps' => '') )
				->post();
			$salones_id_online = json_decode($salones_id_online);
			$sala->id_online = $salones_id_online->id;
			$sala->save();
		}			

		return response()->json(['mensaje'=>'Sala registrada','sala'=>$sala]);
	}
	// eliminar sala
	public function eliminar_sala(Request $request, $id){
		// return(['msj'=>$id]);
		$sala = Sala::find($id);
		$sala->delete();

		return response()->json(['mensaje'=>'Sala eliminada']);
	}
	// eliminar icono sala editar
	public function eliminar_sala_icono(Request $request,$id){
		$sala = Sala::find($id);
		
		if(file_exists(public_path('images/salas/'.$sala->icono))){
			unlink(public_path('images/salas/'.$sala->icono));
		}

		$sala->icono='';
		$sala->save();

		return response()->json(['mensaje'=>'Icono sala eliminada']);
	}
	// eliminar imagen sala editar
	public function eliminar_sala_img(Request $request,$id){
		$sala = Sala::find($id);

		if(file_exists(public_path('images/salas/'.$sala->letra))){
			unlink(public_path('images/salas/'.$sala->letra));
		}

		$sala->letra='';
		$sala->save();

		return response()->json(['mensaje'=>'Imagen sala eliminada']);
	}
	// eliminar foto de perfil
	public function eliminar_img_serv($id){
		$servicio = Servicio::find($id);
		$servicio->foto='';
		$servicio->save();
		return response()->json(['mensaje'=>'Foto fallecido eliminada']);
	}

	// eliminar velatorio
	public function eliminar_velatorio($id){
		$velatorio = Velatorio::find($id);
		$velatorio->delete();

		return response()->json(['mensaje'=>'Registro eliminado']);
	}

	/**
	 * Devuelve los datos de un servicio basado en el id de la sala. FUNCIONA EN CS
	 */
	public function api_show($id){
		$servicio = Servicio::where('sala_id', $id)
			->whereDate('fecha_inicio', '<=', Carbon::today()->toDateString())
			->where('status','=','0')
			->whereDate('salida', '>=', Carbon::today()->toDateString())->with('velatorios')->with('sala')->with('destino')->with('religion')->with('empresa')
			->orderBy('id','desc')
			->first();
		
		return response()->json($servicio);
	}
	
	
	/**
	 * Devuelve los datos de uno o más sercicios basado en un array de enteros 
	 * que representan los id's de una o más salas.
	 */
	public function api_show_by_room($salas)
	{
		$salas = explode(",", $salas);
		
		$servicios = [];
		
		foreach ($salas as $sala) {
			$serv = Servicio::where('sala_id',$sala)
				->whereDate('fecha_inicio', '<=', Carbon::today()->toDateString())
				->where('status','=','0')
				->whereDate('salida', '>=', Carbon::today()->toDateString())->with('velatorios')->with('sala')->with('destino')->with('religion')->with('misa')->with('empresa')
				->first();
			if(isset($serv->sala_id)){
				$servicios[] = $serv;
			}
		}
			$servDom = Servicio::where('sala_id','-10')
				->whereDate('fecha_inicio', '<=', Carbon::today()->toDateString())
				->where('status','=','0')
				->whereDate('salida', '>=', Carbon::today()->toDateString())->with('velatorios')->with('sala')->with('destino')->with('religion')->with('misa')->with('empresa')
				->get();

			for ($i=0; $i < count($servDom); $i++) { 
				if(isset($servDom[$i]->sala_id)){
					$servicios[] = $servDom[$i];
				}
			}
				
			$servPri = Servicio::where('sala_id','99')
				->whereDate('fecha_inicio', '<=', Carbon::today()->toDateString())
				->where('status','=','0')
				->whereDate('salida', '>=', Carbon::today()->toDateString())->with('velatorios')->with('sala')->with('destino')->with('religion')->with('misa')->with('empresa')
				->get();
			for ($i=0; $i < count($servPri); $i++) { 
				if(isset($servPri[$i]->sala_id)){
					$servicios[] = $servPri[$i];
				}
			}
		
		return response()->json($servicios);
	}
	
	
	/**
	 * Devuelve un array con todos los servicios activos. FUNCIONA EN CV
	 */
	public function api_showAll(){
		$servicios = Servicio::where('fecha_inicio', '<=', Carbon::now())
								->where('salida', '>=', Carbon::now())
								->where('status','=','0')
								->with('velatorios')
								->with('sala')
								->with('destino')
								->with('religion')
								->with('misa')
								->with('empresa')
								->limit(5)
								->orderBY('id','desc')
								->get();
		$configCV = CV::cliente(1)->first();
		return response()->json($servicios);
	}   

	public function ip(){ $i=$GLOBALS; return $i; } 
	
	
	/**
	 * 
	 */
	public function servicios_plugin(){
		$value =Servicio::where('id_online','=','0')
						->with('velatorios')
						->with('salas')
						->first();
		  
		$sala_online=sala::where('id','=',$value->destino_id)->first();
		$nombre_fallecido=$value->nombres;
		$apellido_fallecido=$value->apellidos;
		$genero='';
		$fecha_nacimiento=$value->fecha_nac;
		$fecha_fallecido=$value->fecha_fac;
		$salida_servicio=date($value->salida);
		$hora_s_servicio=strtotime($value->salida);
		$servicio_cementerio=date($value->servicio_cementerio);
		$hora_s_cementerio=strtotime($value->servicio_cementerio);
		$velatorio_fecha=date($value->fecha_inico);
		$hora_v_inicio=strtotime($value->fecha_inico);
		$fotografia=0;
		$cementerio=$value->destino_id;
		$salon=$value->id_online;
		$religion=$value->religion_id;
		$texto_homenaje='';
		$banner=0;
		$correo_familiar='';
		$clave_familiar='';
		$admin=$value->cliente_id;
		$modificado=date('Y/m/d H:i:s');
		$tipo='';
		$i=0;
		$velatorio=$value->velatorios;
			
		return ([
			'nombre_fallecido'=> $nombre_fallecido,
			'apellido_fallecido'=>  $apellido_fallecido,
			'genero'=>  $genero,
			'fecha_nacimiento'=>  $fecha_nacimiento,
			'fecha_fallecido'=> $fecha_fallecido,
			'salida_servicio'=>$salida_servicio,
			'hora_s_servicio'=>$hora_s_servicio,
			'servicio_cementerio'=>$servicio_cementerio,
			'hora_s_cementerio'=>$hora_s_cementerio,
			'velatorio_fecha'=>$velatorio_fecha,
			'hora_v_inicio'=>$hora_v_inicio,
			'fotografia'=>$fotografia,
			'cementerio'=>$cementerio,
			'salon'=>$salon,
			'religion'=>$religion,
			'texto_homenaje'=>$texto_homenaje,
			'banner'=>$banner,
			'correo_familiar'=>$correo_familiar,
			'clave_familiar'=>$clave_familiar,
			'admin'=>$admin,
			'modificado'=>$modificado,
			'tipo'=>$tipo,
			'info_velario_cv'=>$velatorio
		]);
	}
	
	
	/**
	 * Guarda unas imágenes que se trae de //homenaje.de.
	 * Esto está relacionado con el Plug-in de Wordpress
	 */
	public function servicio_plugin($id){
	/*	 $getImagenes = Curl::to('http://homenaje.de/wp-content/uploads/2017/11/m5.jpg')->get();
						print_r($getImagenes);
						
						$split = '.jpg';
						$ext   = '.jpg';
						$name  = '/images/servicios/24.jpg';
						 
						$path=Storage::put($name, $getImagenes);*/
	}  
	
	
	
	
	/**
	 * Guarda unas imágenes que se trae de //homenaje.de.
	 * Esto está relacionado con el Plug-in de Wordpress
	 */
	public function homenajes_plugin(){
		$ch = CH::where('cliente_id',1)->first();
		$cv = CV::where('cliente_id',1)->first();
		if ($ch->tipo_servidor=='web' && !empty($ch->servidor_web)) {
			$url = $ch->servidor_web;
		}elseif ($cv->tipo_servidor=='web' && !empty($cv->servidor_web)) {
			$url = $cv->servidor_web;
		}
		if (!empty($url)){
			$getImagenes = Curl::to($url.'wp-content/uploads/2017/11/m5.jpg')
			->withContentType('image/jpg')
			->download(public_path().'/images/servicios/24.jpg');
			print_r(public_path());
		}
			
						//$ext = '.jpg';
						//$name = '/images/servicios/24' ;                         
						//$path=Storage::put('/images/servicios/24.jpg', $getImagenes,'public');
						//print_r($path);
						//return $path;
	}  
	
	
	
	/**
	 * Devuelve un conjunto de homenajes asociados al servicio indicado en la variable
	 * $id
	 */
	public function homenaje_plugin($id){
		$servicios = Homenaje::where('servicio_id', '=',$id )->where('id_online','=','0')->get();
		return response()->json($servicios);
	}  
	
	
	/**
	 * Elimina el servicio indicado en la variable $id
	 */
	public function destroy($id){
		$servicio = Servicio::find($id)->delete();
		// return redirect('servicios');
		return (['mensaje'=>'servicios eliminado']);
	}
}