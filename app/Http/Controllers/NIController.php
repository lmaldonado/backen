<?php 

namespace App\Http\Controllers;

use App\CH;
use App\CV;
use App\CS;
use App\NI;
use App\Sala;
use App\Destino;
use App\Responso;
use App\Homenaje;
use App\Religion;
use App\Servicio;
use App\iphost;
use App\Licencia;
use App\Velatorio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Ixudra\Curl\Facades\Curl;
use SimpleSoftwareIO\QrCode\Facades\QrCode; // https://www.simplesoftware.io/docs/simple-qrcode/es

class NIController extends Controller {

	
	
	
	/**
	 * Muestra la vista de configuración de la cartelera de homenajes
	 */
	public function index(Request $request){
		$numImagen = 0;
		$cliente_id = 1;
		$configNI = NI::cliente($cliente_id)->first();
		
		if(empty($configNI)){
			$configNI = new NI();
		}

		$configNI->logo_encabezado = NI::LOGOS_DIR.'/'.$configNI->logo_encabezado; 
		$configNI->logo_pie = NI::LOGOS_DIR.'/'.$configNI->logo_pie; 
		$configNI->qrcode =  NI::QRCODES_DIR.'/'.$configNI->qrcode; 

		$files = File::allFiles(NI::PUBLI_DIR);
		$publicidades = array();
			
		foreach ($files as $file) 
		{
			$publicidades[] = str_replace('\\','/',(string)$file);
		}

		return view('configuracion.ni',
			[
				'ni'=>$configNI,
				'publicidades' => $publicidades,
				'request' => $request,
				'numImagen' => $numImagen
			]);
	}

	/**
	 * Almacena un homenaje creado con el formulario homenajes
	 */
	public function store(Request $request){
		
		//dd($request->all());
		
		$ni = NI::cliente($request->cliente_id)->first();
		
		if(empty($ni)){
			$ni = new NI();
		}
		
		$ni->cliente_id = $request->cliente_id;
		$ni->url_web = $request->url_web_homenaje;
		$ni->tipo_qrcode = $request->tipo_qrcode;
		$ni->color_back = $request->color_back;

		if($request->tipo_qrcode == 'QR2'){
			$ni->url_web = $request->url_web_homenaje;
			$ni->qrcode = $ni->cliente_id.'_'.uniqid(). ".png";
			
			QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($ni->url_web, NI::QRCODES_DIR . '/' . $ni->qrcode);
		}	
		
		// $ni->tipo_encabezado = $request->tipo_encabezado;
		
		// if ( $request->tipo_encabezado == "texto" ) {
		// 	$ni->texto_encabezado = $request->texto_encabezado;
		// 	// $ni->logo_encabezado = "";
		// } else if ( $request->tipo_encabezado == "logo")  {
		// 	// $ni->texto_encabezado = "";
		
		// 	if($request->hasFile("logo_encabezado")){
		// 		$nameFile = $ni->cliente_id.'_'.uniqid().'.'.$request->file('logo_encabezado')->getClientOriginalExtension();
		// 		$request->file("logo_encabezado")->move(NI::LOGOS_DIR, $nameFile);
		// 		$ni->logo_encabezado = $nameFile;
		// 	}
		// }

		$ni->duracion_publicidad = $request->duracion_publicidad;	
		
		// ENCABEZADO
		// if($request->hasFile("logo_pie")){
		// 	$nameFile = $ni->cliente_id.'_'.uniqid().'.'.$request->file('logo_pie')->getClientOriginalExtension();
		// 	$request->file("logo_pie")->move(NI::LOGOS_DIR, $nameFile);
		// 	$ni->logo_pie = $nameFile;
		// }

		if(!File::exists(NI::NI_DIR)){
			File::makeDirectory(NI::NI_DIR);
			File::makeDirectory(NI::PUBLIC_DIR);
		}

		// PUBLICIDADES
		if($request->publi_delete != ""){
			$filesDelete = explode(',',$request->publi_delete);
			foreach($filesDelete as $file){
				File::delete($file);
			}
		}

		if($request->hasFile('publi')){
			$files = $request->file('publi');
			foreach($files as $file){
				if($file != NULL){
					$nameFile = $ni->cliente_id.'_'.uniqid().'.'.$file->getClientOriginalExtension();
					$file->move(NI::PUBLI_DIR, $nameFile);  
				}
			}
		}

		$ni->save();

		return redirect('/?ruta=config/ni&save=1');
	}
	
	
	/**
	 * Devuelve la configuración de la cartelera de homenajes cuando la 
	 * consulta se hace a través de la API. Se espera que esta consulta 
	 * la haga el fornt-end de la cartelera para establecer su propia 
	 * configuración.
	 */
	public function api_config(){
		$cliente_id = 1;
		$configNI = NI::cliente($cliente_id)->first();
		// PUBLICIDADES
		$files = File::allFiles(NI::PUBLI_DIR);
		// $publi = array(); 
		
		$publis = array();
		foreach ($files as $file) 
		{
			$publis[] = str_replace('\\','/',(string)$file);
		}

		return response()->json(["configNI"=>$configNI, "publis" => $publis]);
	}
 
    public function conexion(){

    	$configCH = CH::cliente(1)->first();
    	$configCV = CV::cliente(1)->first();
    	$salas = Sala::all('nombre', 'id', 'id_online');
    	$licencias = Licencia::all();
    	
    	if($configCH->tipo_servidor == 'web'){
    		$sala = Sala::find($configCH->sala_id);
    		if(!empty($sala)){
    			$configCH->sala_id=$sala->id_online;
    		}    		
    	}

    	$array='';
        $sala_sep = (explode(",",$configCV->salas_id));
        if($configCV->tipo_servidor == 'web'){
        	for($i=0;$i<count($sala_sep);$i++){
        	
	    		$sala = Sala::find($sala_sep[$i]);
	    		if(!empty($sala)){
	    			if($array==''){
                        $array=$array.$sala->id_online;
                    }else{
                        $array=$array.','.$sala->id_online;
                    }
	    		}    		
	    	}            
        }
        // dd($array);
    	return view('conexion.index',['ch'=>$configCH, 'cv'=>$configCV,'salas'=>$salas, 'sala_sel'=>$array,'licencias'=>$licencias]);
    }
}
