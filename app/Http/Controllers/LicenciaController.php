<?php 
namespace App\Http\Controllers;
use App\Licencia;
use App\Empresas;
use App\NI;
use App\Sala;
use App\whmcs;
use App\Sesion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Ixudra\Curl\Facades\Curl;
use Chumper\Zipper\Facades\Zipper;
use ZipArchive;
//use Illuminate\Support\Facades\Mail;
use Mail;
class LicenciaController extends Controller {

	/**
	 * Si hay una licencia activa en la base de datos muestra todos los detalles
	 * De lo contrario indica que no hay licencias activas y muestra un botón 
	 * para realizar la activación]
	 */
	public function show() {
		$logos = File::allFiles('images/empresa/');
		$salas = Sala::all();
		$salasLis = Sala::all()->lists('nombre','id');
		 
		foreach ($logos as $logg) {
          $logo[] = str_replace('\\','/',(string)$logg);
        }
        $ni = NI::find(1);
        // dd($ni);
		$licencias = licencia::all();
		$empresa = Empresas::find(1);
		$validar_licencia=false;
		$info=[];
		return view('configuracion.licencias', compact("licencias" ,'empresa','logo','ni','salas','validar_licencia','info','salasLis'));	
	}


	/**
	 * Elimina la licencia indicada en la variable $id
	 */
	public function delete(Request $request)
	{
		$licencia = Licencia::find($request->id);
		$licencia->descarga=0;
		if ($_SERVER['SERVER_SOFTWARE']=='DroidPHP For Android'){
			$this_directory =  $_SERVER['DOCUMENT_ROOT'].'/';
		}else{
			$this_directory =  $_SERVER['DOCUMENT_ROOT'];
		}
		
		switch ($licencia->product_id) {
			case 10://Cartelera del último adiós
				$ruta='CA/01';
				$download='modulos/_ca';
				break;
			case 35://[CV-5M] Cartelera Android para Velatorio (5 Serv.)
				$ruta='CV/001';
				$download='modulos/_cv_n';
				break;
			case 7://Cartelera Android para Velatorio (Pantalla vertica
				$ruta='CV2/001';
				$download='modulos/_cv_2';
				break;
			case 5://Cartelera Android de homenajes
				$ruta='CH/001';
				$download='modulos/_ch';
				break;
			case 9://Cartelera de Información
				$ruta='CI/01';
				$download='modulos/_ci';
				break;
			case 33://Cartelera de catalogo
				$ruta='CD/01';
				$download='modulos/_cd';
				break;
			case 32://Control de rondas para velatorios
				$ruta='CR/01';
				$download='modulos_cr';
				break;
			case 13://Musica funcinal para velarorios
				$ruta='MF/001';
				$download='modulos/_mf';
				break;
			case 6://Carteleras Android para Salas Velatorias
				$ruta='CS/001';
				$download='modulos/_cs';
				break;			
			
		}		

		if($licencia->product_id == 13){
			$carpeta=$this_directory.'modulos/_mf';
			$this->rmDir_rf($carpeta);

			$carpeta=$this_directory.'modulos/_mf_mobile';
			$this->rmDir_rf($carpeta);

			$carpeta=$this_directory.'modulos/mf_n';
			$this->rmDir_rf($carpeta);
		}elseif($licencia->product_id == 32){
			$carpeta=$this_directory.'modulos/_cr';
			$this->rmDir_rf($carpeta);

			$carpeta=$this_directory.'modulos/cc0';
			$this->rmDir_rf($carpeta);
		}else{
			$licencia->update();
			$carpeta=$this_directory.$download;
			
		    $this->rmDir_rf($carpeta);
		}		
	      
	    

		//$this->sendEmail('Inactivar',$licencia->product_name);

		return redirect('?ruta=config/licencia');
	}
	
	public function rmDir_rf($carpeta)
  {
  	$folderCont=glob($carpeta . "/{,.}*",GLOB_BRACE);
	



    foreach($folderCont as $archivos_carpeta){             
    	$aux=explode(".",$archivos_carpeta) ;
    	if (count($aux) >=2){
	 			if ($aux[1]!=''){
	 				if (count($aux) >2){
	 					if ($aux[2]!=''){
			 				if (is_dir($archivos_carpeta)){
					          $this->rmDir_rf($archivos_carpeta);
					        } else {
					        unlink($archivos_carpeta);
					        }
					    }
				    }else{
				    	if (is_dir($archivos_carpeta)){
				          $this->rmDir_rf($archivos_carpeta);
				        } else {
				        unlink($archivos_carpeta);
				        }

				    }
	 			}
	 			
	 		}else {
	 			if (is_dir($archivos_carpeta)){
		          $this->rmDir_rf($archivos_carpeta);
		        } else {
		        unlink($archivos_carpeta);
		        }
	 		}

      }
      rmdir($carpeta);
  }
	
	public function chequea_licencias()
  {	
  	$empresa=Empresas::find(1);
		$aux=whmcs::where('fingerprint',$empresa->cuit)->get();
		return $aux;

  }          
    
	public function storeEmpresa(Request $request){
		$empresa = Empresas::find(1);

		$ni = NI::find(1);

		// dd($request->all());

        $empresa->update();
        if($request->hasFile('logo')) {
        	$name_f = uniqid().'.'.$request->file('logo')->getClientOriginalExtension();
            $request->file('logo')->move('images/empresa/',$name_f);
            $ni->logo_encabezado = $name_f;
            $ni->save();
        }
        if($request->hasFile('logo_pie')) {
        	$name_f = uniqid().'.'.$request->file('logo_pie')->getClientOriginalExtension();
            $request->file('logo_pie')->move('images/empresa/',$name_f);
            $ni->logo_pie = $name_f;
            $ni->save();
        }
       return redirect('?ruta=config/licencia');

	}
	
	public function storeEmpresapost(Request $request){
		$empresa = Empresas::find(1);
		$valor=true;
		if(empty($empresa )){
			$empresa =new Empresas();
			$empresa->id=1;
			$valor=false;
		}

		$empresa->Nombre = $request->input('Nombre');
		$empresa->Correo_1 = $request->input('Correo_1');
		$empresa->cuit = $request->input('fp');
		if($valor){
			$empresa->update();
		}else{
			$empresa->save();
		}
        
        
       

	}
	
	public function crearCarpeta(Request $request){
		$licencia = Licencia::find($request->id);
		/*******contruir ruta*/
		if ($_SERVER['SERVER_SOFTWARE']=='DroidPHP For Android'){
			$this_directory =  $_SERVER['DOCUMENT_ROOT'].'/';
		}else{
			$this_directory =  $_SERVER['DOCUMENT_ROOT'];
		}
		
		switch ($licencia->product_id) {
			case 10://Cartelera del último adiós
				$ruta='CA/01';
				$download='modulos/_ca';
				break;
			case 35://[CV-5M] Cartelera Android para Velatorio (5 Serv.)
				$ruta='CV/001';
				$download='modulos/_cv_n';
				break;
			case 7://Cartelera Android para Velatorio (Pantalla vertica
				$ruta='CV2/001';
				$download='modulos/_cv_2';
				break;
			case 5://Cartelera Android de homenajes
				$ruta='CH/001';
				$download='modulos/_ch';
				break;
			case 9://Cartelera de Información
				$ruta='CI/01';
				$download='modulos/_ci';
				break;
			case 33://Cartelera de catalogo
				$ruta='CD/01';
				$download='modulos/_cd';
				break;
			case 32://Control de rondas para velatorios
				$ruta='CR/01';
				$download='modulos';
				break;
			case 13://Musica funcinal para velarorios
				$ruta='MF/001';
				$download='modulos';
				break;
			case 6://Carteleras Android para Salas Velatorias
				$ruta='CS/001';
				$download='modulos/_cs';
				break;			
			
		}		
		
		$carpeta = $this_directory.$download;
		if (!file_exists($carpeta)) {
		    mkdir($carpeta, 0777, true);
		}
		return json_encode(['carpeta'=>$carpeta,'ruta'=>$ruta]);
	}

	public function download(Request $request)
	{
		$licencia = Licencia::find($request->id);
		$ruta=$request->ruta1;
		$carpeta=$request->carpeta;		
		$ch = curl_init();
			$source = 'http://neosepelios.com.ar/neoinnovaciones/zipProjects/'.$ruta.'/htdocs.zip';
			curl_setopt($ch, CURLOPT_URL, $source);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$data = curl_exec ($ch);
			curl_close ($ch);
			$destination = $carpeta.'/htdocs.zip';
			$file = fopen($destination, "w+");
			fputs($file, $data);
			fclose($file);	       
       
     // $this->sendEmail('Descargar',$licencia->product_name);
       $licencia->descarga=1;
       $licencia->save();
      return json_encode(['carpeta'=>$carpeta,'ruta'=>$ruta]);
		
	}
	
	public function download1(Request $request)
	{
		//$licencia = Licencia::find($request->id);
		//$ruta=$request->ruta1;
		//$carpeta=$request->carpeta;			
			
		//	$results		=	Curl::to('http://neosepelios.com.ar/neoinnovaciones/zipProjects/CA/01/htdocs.zip')
		//				->withContentType('application/zip')
											
	//					->download('C:\wamp\www\modulos\_ca\htdocs.zip');

      // dd( $results);
     	 
    // $this->sendEmail('Descargar',$licencia->product_name);
    //    $licencia->descarga=1;
    //   $licencia->save();
    //  return ();
		
	}
	
	public function descomprimir(Request $request)	
	{	
		$ruta=$request->ruta2;
		$carpeta=$request->carpeta2;	
		$zip = new ZipArchive;
        if ($zip->open($carpeta.'/htdocs.zip') === TRUE) {
	        $zip->extractTo($carpeta.'/');
	        $zip->close();
	        
	        //echo 'ok';
	    } else {
	        echo 'failed';
	    }
      
		
	}

	

	/**
	 * Almacena la licencia
	 */
	public function store($results)
	{
		# code...
	}

	public function validar($product_id)
	{
		// Si la licencia existe en la base de datos y está activa
		// validarla.
		// De lo contrario indicar si existe y en que estado está.

		$licencia = Licencia::where('product_id', $product_id)->first();

		if($licencia) 
		{
			return json_encode($licencia);
		}
		else 
		{
			return "no_license";
		}
	}
	public function sendEmail($accion,$producto){
	        
        $empresa= Empresas::find(1);
        Mail::send('mail.index', ['empresa'=>$empresa, 'accion'=>$accion,'producto'=>$producto], function ($m)  {

	        $m->to('info@neosepelios.com', 'David Perrone')->subject('Licencias de Carteleria');	          
	        $m->cc('luis.maldonado.sotillet@gmail.com');
	        

	        });
	            # code...
	    }
	    public function sendEmail2(){
	        
        $empresa= Empresas::find(1);
        Mail::send('mail.index', ['empresa'=>$empresa, 'accion'=>'accion','producto'=>'producto'], function ($m)  {

	        $m->to('luis.maldonado.sotillet@gmail.com', 'David Perrone')->subject('Licencias de Carteleria');	          
	        $m->cc('luis.maldonado.sotillet@gmail.com');
	        

	        });
	            # code...
	    }



	/**
	 * Verifica primero la existencia de la llave local.
	 * A contimuación verifica la licencia contra el servidor.
	 * Genera un array asociativo con el resultado de la verificación
	 * 
	 */
	function check_license(Request $request){
		$empresa = Empresas::find(1);
		$licensekey = $request->licencia;

		$licencia = Licencia::where('licence_key', $licensekey)->first();

		$localkey = $licencia? $licencia->local_key : null;

		
		// -----------------------------------
		//  -- Configuration Values --
		// -----------------------------------

		// Enter the url to your WHMCS installation here
		$whmcsurl = 'http://clientes.neosepelios.com.ar/';//whmcs/';
		

		// Must match what is specified in the MD5 Hash Verification field
		// of the licensing product that will be used with this check.
		$licensing_secret_key = 'NeoSepelioDB-v00-1.';
		// The number of days to wait between performing remote license checks
		$localkeydays = 15;
		// The number of days to allow failover for after local key expiry
		$allowcheckfaildays = 5;
		// -----------------------------------
		//  -- Do not edit below this line --
		// -----------------------------------

		// Genera un token de verificación ¿...?
		$check_token = time() . md5(mt_rand(1000000000, 9999999999) . $licensekey);
		
		// Genera una fecha de verificación ¿...?
		$checkdate = date("Ymd");
		
		// Determina el dominio del servidor.
		$domain = $_SERVER['SERVER_NAME'];
		
		// Determina la IP del usuario ¿...?
		//$usersip = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $_SERVER['LOCAL_ADDR'];
		// NULL
		$usersip = $empresa->cuit;
		
		// Determina la URL de este archivo...
		$dirpath = dirname(__FILE__);
		
		// Define una ruta y nombre para un archivo de verificación ¿...?
		$verifyfilepath = 'modules/servers/licensing/verify.php';
		





		/************************************************************************/
		/***    Primero revisamos la validez de la llave local, si existe     ***/
		/************************************************************************/
		
		$localkeyvalid = false;

		if ($localkey) {

			// Limpiamos el string de caracteres indeseados

			$localkey = str_replace("\n", '', $localkey); # Remove the line breaks
			$localdata = substr($localkey, 0, strlen($localkey) - 32); # Extract License Data
			$md5hash = substr($localkey, strlen($localkey) - 32); # Extract MD5 Hash
			
			if ($md5hash == md5($localdata . $licensing_secret_key)) {


				// M100
				// Aquí reversamos las operaciones que se realizan a la información 
				// de la licencia para generar la llave local. Ver M101
				// Así obtenemos de nuevo dicha información en un array asociativo.

				$localdata = strrev($localdata); # Reverse the string 
				$md5hash = substr($localdata, 0, 32); # Extract MD5 Hash
				$localdata = substr($localdata, 32); # Extract License Data
				$localdata = base64_decode($localdata); # Decode licence
				$localkeyresults = unserialize($localdata); # Extract licence data
				$originalcheckdate = $localkeyresults['checkdate']; # Verify chackdate

				if ($md5hash == md5($originalcheckdate . $licensing_secret_key)) {
					$localexpiry = date("Ymd", mktime(0, 0, 0, date("m"), date("d") - $localkeydays, date("Y")));
					
					if ($originalcheckdate > $localexpiry) {
						$localkeyvalid = true;
						$results = $localkeyresults;
						$validdomains = explode(',', $results['validdomain']);
					
						if (!in_array($_SERVER['SERVER_NAME'], $validdomains)) {
							$localkeyvalid = false;
							$localkeyresults['status'] = "Invalid";
							$results = array();
						}
					
						$validips = explode(',', $results['validip']);
					
						if (!in_array($usersip, $validips)) {
							$localkeyvalid = false;
							$localkeyresults['status'] = "Invalid";
							$results = array();
						}
					
						$validdirs = explode(',', $results['validdirectory']);
					
						if (!in_array($dirpath, $validdirs)) {
							$localkeyvalid = false;
							$localkeyresults['status'] = "Invalid";
							$results = array();
						}
					}
				}
			}
		}

		/***************************************************************************************/
		/*************************** Fin verivifación llave local ******************************/
		/***************************************************************************************/



		/*	Si la llave local EXISTE Y ES VÁLIDA entonces la verificación local 
			genera un array $results con los datos de la licencia y pone en true 
			la variable $localkeyvalid.
			Si la llave local NO EXISTE O ES INVÁLIDA entonces se genera un 
			array $results con un campo status=Invalid y se deja la variable 
			$localkeyvalid en false.
			Entonces consultamos al servidor de WHMCS para verificar si la 
			licencia existe en el servidor y cuál es su estado.
			Al final se genera un array asociativo con el resultado de la 
			verificación.
			*/

		if (!$localkeyvalid) {
			
			$responseCode = 0;

			$postfields = array(
				'licensekey' => $licensekey,
				'domain' => $domain,
				'ip' => $usersip,
				'dir' => $dirpath,
			);
			
			if ($check_token) $postfields['check_token'] = $check_token;
			
			/*	El siguiente foreach() convierte el array $postfileds a formado URL
				como si fueran campos de una petición GET
				*/
			$query_string = '';
			foreach ($postfields AS $k=>$v) {
				$query_string .= $k.'='.urlencode($v).'&';
			}
			
			// A Q U Í   E S   D O N D E   O C U R R E   L A   M A G I A //

			// Si la función curl_exec está disponible (... a partir de PHP 4.0.2)
			
			if (function_exists('curl_exec')) {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $whmcsurl . $verifyfilepath);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$data = curl_exec($ch);
				//print_R(curl_errno($ch) . ' - ' . curl_error($ch));

				$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				
				curl_close($ch);
			}

			// Si la función curl_exec no está disponible lo intenta con fsockopen()

			else {
				$responseCodePattern = '/^HTTP\/\d+\.\d+\s+(\d+)/';
				$fp = @fsockopen($whmcsurl, 80, $errno, $errstr, 5);
				if ($fp) {
					$newlinefeed = "\r\n";
					$header = "POST ".$whmcsurl . $verifyfilepath . " HTTP/1.0" . $newlinefeed;
					$header .= "Host: ".$whmcsurl . $newlinefeed;
					$header .= "Content-type: application/x-www-form-urlencoded" . $newlinefeed;
					$header .= "Content-length: ".@strlen($query_string) . $newlinefeed;
					$header .= "Connection: close" . $newlinefeed . $newlinefeed;
					$header .= $query_string;
					$data = $line = '';
					@stream_set_timeout($fp, 20);
					@fputs($fp, $header);
					$status = @socket_get_status($fp);
					while (!@feof($fp)&&$status) {
						$line = @fgets($fp, 1024);
						$patternMatches = array();
						if (!$responseCode
							&& preg_match($responseCodePattern, trim($line), $patternMatches)
						) {
							$responseCode = (empty($patternMatches[1])) ? 0 : $patternMatches[1];
						}
						$data .= $line;
						$status = @socket_get_status($fp);
					}
					@fclose ($fp);
				}
			}

			// Ahora ya tenemos la respuesta del Server de WHMCS.
			// La respuesta suele ser en XML

			// Si el servidor WHMSC responde con error chequeamos
			// la fecha de verificación

			if ($responseCode != 200) {
				$localexpiry = date("Ymd", mktime(0, 0, 0, date("m"), date("d") - ($localkeydays + $allowcheckfaildays), date("Y")));

				// Si la fecha de verificción es mayor de la de expiración
				// se carga la llave local en $results y se continúa...
				
				
				if ($originalcheckdate > $localexpiry) {
					$results = $localkeyresults;
				} 
				// De lo contrario se carga en $results status=Invalid y 
				// description=Remote_Check_Failed, y termina el script 
				// devolviendo $results...
				else {
					$results = array();
					$results['status'] = "Invalid";
					$results['description'] = "Remote Check Failed";
					return $results;
				}
			} 
		
			
			// Si la respuesta del servidor es 200 cargamos la respuesta 
			// en $results
			else {
				preg_match_all('/<(.*?)>([^<]+)<\/\\1>/i', $data, $matches);
				$results = array();
				foreach ($matches[1] AS $k=>$v) {
					$results[$v] = $matches[2][$k];
				}
			}
			// Aquí chequeamos que la respuesta no esté vacía
			if (!is_array($results)) {
				die("Invalid License Server Response");
			}
			// Verificamos la suma md5 y, si no coincide, 
			// cargamos en $results status=Invalid y 
			// description=MD5_Chechsum_Verification_Failed 
			// y terminamos devolviento $results
			if ($results['status']) {
				if ($results['status'] == "Invalid") {
					
					$results['description'] = "Licencia invalida Verificación fallida";
					
				}
			}else{
				if ($results['md5hash']) {
				if ($results['md5hash'] != md5($licensing_secret_key . $check_token)) {
					$results['status'] = "Invalid";
					$results['description'] = "MD5 Checksum Verification Failed";
					
				
				}
			}
			}

			

			// M101
			// Aquí codificamos el localkey para almacenarlo en la BBDD como 
			// un solo campo.
			// Esto se decodifica cuando se verifica el estado de la licencia 
			// al inicio de este script -- Ver M100
			if ($results['status'] == "Active") {
				$results['checkdate'] = $checkdate;
				$data_encoded = serialize($results);
				$data_encoded = base64_encode($data_encoded);
				$data_encoded = md5($checkdate . $licensing_secret_key) . $data_encoded;
				$data_encoded = strrev($data_encoded); 
				$data_encoded = $data_encoded . md5($data_encoded . $licensing_secret_key);
				$data_encoded = wordwrap($data_encoded, 80, "\n", true);
				$results['localkey'] = $data_encoded;
			}
			
			$results['remotecheck'] = true;
		}

		unset($postfields,$data,$matches,$whmcsurl,$licensing_secret_key,$checkdate,$usersip,$localkeydays,$allowcheckfaildays,$md5hash);

		if ($licencia === null) // Entonces es una licencia nueva
		{
			if ($results['status'] == "Active")
			{
				if (!array_key_exists('customfields', $results))
					$results['customfields'] = '';

					$licencia = Licencia::where('product_id', $results['productid'])->first();
				
					$licencia->status = $results['status'];
					$licencia->registered_name = $results['registeredname'];
					$licencia->company_name = $results['companyname'];
					$licencia->email = $results['email'];
					$licencia->service_id = $results['serviceid'];
					$licencia->product_id = $results['productid'];
					$licencia->product_name = $results['productname'];
					$licencia->reg_date = $results['regdate'];
					$licencia->next_due_date = $results['nextduedate'];
					$licencia->billing_cycle = $results['billingcycle'];
					$licencia->valid_domain = $results['validdomain'];
					$licencia->valid_ip = $results['validip'];
					$licencia->valid_directory = $results['validdirectory'];
					$licencia->config_options = $results['configoptions'];
					$licencia->custom_fields = $results['customfields'];
					$licencia->md5_hash = $results['md5hash'];
					$licencia->check_date = $results['checkdate'];
					$licencia->licence_key = $licensekey;
					$licencia->local_key = $results['localkey'];
					$licencia->save();
					//$this->sendEmail('Activar',$licencia->product_name);
			}
		}
		else // Entonces es una licencia existente
		{
			if (!array_key_exists('customfields', $results))
			$results['customfields'] = '';
			$licencia->status = $results['status'];
			$licencia->registered_name = $results['registeredname'];
			$licencia->company_name = $results['companyname'];
			$licencia->email = $results['email'];
			$licencia->service_id = $results['serviceid'];
			$licencia->product_id = $results['productid'];
			$licencia->product_name = $results['productname'];
			$licencia->reg_date = $results['regdate'];
			$licencia->next_due_date = $results['nextduedate'];
			$licencia->billing_cycle = $results['billingcycle'];
			$licencia->valid_domain = $results['validdomain'];
			$licencia->valid_ip = $results['validip'];
			$licencia->valid_directory = $results['validdirectory'];
			$licencia->config_options = $results['configoptions'];
			$licencia->custom_fields = $results['customfields'];
			$licencia->md5_hash = $results['md5hash'];
			$licencia->check_date = $results['checkdate'];
			$licencia->licence_key = $licensekey;
			//$licencia->local_key = $results['localkey'];
			$licencia->save();
			//$this->sendEmail('Activar',$licencia->product_name);
		}

	
	return json_encode($licencia);
	}
	function api_licencia(Request $request,$finger){
		$licencias = Licencia::all();
		$sesion = Sesion::where('fringerprint',$finger)->orderBy('id','desc')->first();

		return (['licencia'=>$licencias,'sesion'=>$sesion ]);
	} 

}