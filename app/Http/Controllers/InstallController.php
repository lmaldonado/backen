<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class InstallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function migration()
    {
        $exitCode = Artisan::call('migrate:refresh', ['--seed' => true, '--force' => true,]);
        echo "<center>Instalación finalizada!</center>";
    }
}
