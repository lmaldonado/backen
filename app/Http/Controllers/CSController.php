<?php 
namespace App\Http\Controllers;

use App\CS;
use App\Sala;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CSController extends Controller {

    // private $models,$salas,$cliente;

    // public function __construct(CS $cs, Sala $sala)
    // {
    //     $this->models = $cs;
    //     $this->salas = $sala;
    //     $this->cliente = 1;
    // }


    public function index(Request $request){
        $numImagen = 0;
        $salas = Sala::cliente(1)->get();
        $configCV = CS::cliente(1)->first();
        if(empty($configCV)){
            $configCV = new CS();
        }

        $files = File::allFiles(CS::SIMBOLOS_DIR);
        $simbolos = array();
            
        foreach ($files as $file) 
        {
            $auxFile = str_replace('\\','/',(string)$file);
            $auxFile = explode('/', $auxFile);
            $auxFile = array_pop($auxFile);
            $auxFile2 = explode('.', $auxFile);
            $simbolos[$auxFile] = $auxFile2[0];
        }

        $files = File::allFiles(CS::BANNERS_DIR);
        $banners = array();

        foreach ($files as $file) 
        {
            $banners[] = str_replace('\\','/',(string)$file);
        }

        $files = File::allFiles(CS::FONDOS_DIR);
        $fondos = array();
            
        foreach ($files as $file) 
        {
            $fondos[] = str_replace('\\','/',(string)$file);
        }

        $files = File::allFiles(CS::PUBLI_DIR);
        $publicidades = array();
            
        foreach ($files as $file) 
        {
            $publicidades[] = str_replace('\\','/',(string)$file);
        }
        $files = File::allFiles(CS::SERVICIOS_DIR);
        $fondo_servicio = array();
            
        foreach ($files as $file) 
        {
            $fondo_servicio[] = str_replace('\\','/',(string)$file);
        }

        $sala_simbolos = unserialize($configCV->simbolos);

        $configCV->logo = CS::CV_DIR.'/'.$configCV->logo; 
        $configCV->qrcode = '../' . CS::QRCODES_DIR.'/'.$configCV->qrcode; 

        // dd($configCV);
        
        return view('configuracion.cs',
            [
                'cs'=>$configCV,
                'sala_simbolos'=>$sala_simbolos,
                'salas'=>$salas,
                'tipos_imagen'=> CS::getTipo_imagen(),
                'tipos_contenedor' => CS::getTipo_contenedor(),
                'simbolos' => $simbolos,
                'banners' => $banners,
                'fondos' => $fondos,
                'publicidades' => $publicidades,
                'request' => $request,
                'numImagen' => $numImagen,
                'fondo_servicio'=>$fondo_servicio
            ]);
    }


    public function store(Request $request){
        $cliente_id = 1;
        $cs = CS::cliente($cliente_id)->first();
        if(empty($cs)){
            $cs = new CS();
        }

        $cs->cliente_id = $cliente_id;
        // ENCABEZADO
        $cs->qrcode=$request->qrcode;
        $cs->tipo_banner = $request->tipo_banner;
        $cs->texto_banner = $request->texto_banner;
        $cs->url_web_homenaje = $request->url_web_homenaje;
       
        if($request->hasFile("logo")){
            $nameFile = $cs->cliente_id.'_'.uniqid().'.'.$request->file('logo')->getClientOriginalExtension();
            $request->file("logo")->move(CS::CV_DIR, $nameFile);
            $cs->logo = $nameFile;
        }
        
        // CONTENEDORES 
        $cs->contenedor1 = $request->contenedor1;
        $cs->contenedor2 = $request->contenedor2;
        $cs->contenedor3 = $request->contenedor3;
        $cs->contenedor4 = $request->contenedor4;
        // $cs->color_back  = $request->color_back;
        
        if(!File::exists(CS::CV_DIR)){
            File::makeDirectory(CS::CV_DIR);
            File::makeDirectory(CS::BANNERS_DIR);
            File::makeDirectory(CS::FONDOS_DIR);
            File::makeDirectory(CS::PUBLI_DIR);
        }

        // BANNERS
        $cs->tiempo_banner = $request->tiempo_banner;        
        if($request->banner_delete != ""){
            $filesDelete = explode(',',$request->banner_delete);
            foreach($filesDelete as $file){
                File::delete($file);
            }
        }
        if($request->hasFile('banner')){
            $files = $request->file('banner');
            foreach($files as $file){
                if($file != NULL){
                    $nameFile = $cs->cliente_id.'_'.uniqid().'.'.$file->getClientOriginalExtension();
                    $file->move(CS::BANNERS_DIR, $nameFile);  
                }
            }
        }

        // FONDOS
        if($request->fondo_delete != ""){
            $filesDelete = explode(',',$request->fondo_delete);
            foreach($filesDelete as $file){
                File::delete($file);
            }
        }
        if($request->hasFile('fondo')){
            $files = $request->file('fondo');
            foreach($files as $file){
                if($file != NULL){
                    $nameFile = $cs->cliente_id.'_'.uniqid().'.'.$file->getClientOriginalExtension();
                    $file->move(CS::FONDOS_DIR, $nameFile);  
                }
            }
        }

        // PUBLICIDADES
        $cs->tiempo_publi = $request->tiempo_publi;
         if($request->publi_delete != ""){
            $filesDelete = explode(',',$request->publi_delete);
            foreach($filesDelete as $file){
                File::delete($file);
            }
        }
        if($request->hasFile('publi')){
            $files = $request->file('publi');
            foreach($files as $file){
                if($file != NULL){
                    $nameFile = $cs->cliente_id.'_'.uniqid().'.'.$file->getClientOriginalExtension();
                    $file->move(CS::PUBLI_DIR, $nameFile);  
                }
            }
        }
        //SERVICIOS
         if($request->fondo_servicio_delete != ""){
            $filesDelete = explode(',',$request->fondo_servicio_delete);
            foreach($filesDelete as $file){
                File::delete($file);
            }
        }
        if($request->hasFile('fondo_servicio')){
            $files = $request->file('fondo_servicio');
            foreach($files as $file){
                if($file != NULL){
                    $nameFile = $cs->cliente_id.'_'.uniqid().'.'.$file->getClientOriginalExtension();
                    $file->move(CS::SERVICIOS_DIR, $nameFile);  
                }
            }
        }
        
        $cs->save();

        return redirect('/?ruta=config/cs&save=1');
    }

    public function api_config(){
        $configCV = CS::cliente(1)->first();
        $salas = Sala::cliente(1)->get();
        // BANNERS
        $files = File::allFiles(CS::BANNERS_DIR);
        $banners = array();    
        foreach ($files as $file) 
        {
            $banners[] = str_replace('\\','/',(string)$file);
        }

        // FONDOS
        $files = File::allFiles(CS::FONDOS_DIR);
        $fondos = array();    
        foreach ($files as $file) 
        {
            $fondos[] = str_replace('\\','/',(string)$file);
        }

        // PUBLICIDADES
        $files = File::allFiles(CS::PUBLI_DIR);
        $publicidades = array();
        foreach ($files as $file) 
        {
            $publicidades[] = str_replace('\\','/',(string)$file);
        }
        // servicios
        $files = File::allFiles(CS::SERVICIOS_DIR);
        $fondo_servicio = array();
        foreach ($files as $file) 
        {
            $fondo_servicio[] = str_replace('\\','/',(string)$file);
        }
    

        // LOGO
        $configCV->logo = CS::CV_DIR.'/'.$configCV->logo; 

        return response()->json(["configCS"=>$configCV, "fondos"=>$fondos, "banners" => $banners, "publicidades"=>$publicidades,"fondo_servicio"=>$fondo_servicio]);
    }

    public function api_salas(){
        $salas = Sala::cliente(1)->get();
        return response()->json(["salas"=>$salas]);
    }
}

