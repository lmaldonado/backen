<?php 

namespace App\Http\Controllers;

use App\CH;
use App\CV;
use App\CS;
use App\Sala;
use App\Destino;
use App\Palabras;
use App\Responso;
use App\Homenaje;
use App\Religion;
use App\Servicio;
use App\Licencia;
use App\iphost;
use App\Velatorio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Ixudra\Curl\Facades\Curl;
use SimpleSoftwareIO\QrCode\Facades\QrCode; // https://www.simplesoftware.io/docs/simple-qrcode/es

class CHController extends Controller {

	private $models;
	
	
	/**
	 * Constructor
	 */
	// public function __construct(Ch $ch){
	// 	$this->models = $ch;
	// }
	
	
	/**
	 * Muestra la vista de configuración de la cartelera de homenajes
	 */
	public function index(Request $request){
		$numImagen = 0;
		$cliente_id = 1;
		$configCH = CH::cliente($cliente_id)->first();
		
		if(empty($configCH)){
			$configCH = new CH();
		}

		$configCH->logo_encabezado = CH::LOGOS_DIR.'/'.$configCH->logo_encabezado; 
		$configCH->logo_pie = CH::LOGOS_DIR.'/'.$configCH->logo_pie; 
		$configCH->qrcode =  CH::QRCODES_DIR.'/'.$configCH->qrcode; 

		$files = File::allFiles(CH::PUBLI_DIR);
		$publicidades = array();
			
		foreach ($files as $file) 
		{
			$publicidades[] = str_replace('\\','/',(string)$file);
		}
		$palabras = Palabras::all();
		$salas = Sala::all('nombre', 'id', 'id_online');
		$ch = CH::where('cliente_id',1)->first();
		$cv = CV::where('cliente_id',1)->first();
		if ($ch->tipo_servidor=='web' && !empty($ch->servidor_web)) {
			$url = $ch->servidor_web;
		}elseif ($cv->tipo_servidor=='web' && !empty($cv->servidor_web)) {
			$url = $cv->servidor_web;
		}
		// if(!empty($url)){
		// 	$salones_ss = Curl::to($url.'wp-content/plugins/neo_homenajes/json/conector.php')
		// 			->withData( array( 'salones' => '' ) )
		// 			->post();
		// }

		// $salas_online[0] = "[ SELECCIONE ]";
		// dd($salones_ss->resultado);
		// if ( isset($response) ) {
		// 	// $response = json_decode($response, true);
		// 	foreach ($response->resultado as $key => $value) {
		// 		$salas_online[$value["id"]] = $value["nombre"];
		// 	}
		// 	$conexionOnline = true;
		// } else {
		// 	$conexionOnline = false;
		// }
		// dd($configCH);
		return view('configuracion.ch',
			[
				'ch'=>$configCH,
				'publicidades' => $publicidades,
				'request' => $request,
				'salas' => $salas,
				'numImagen' => $numImagen,
				'palabras' => $palabras
			]);
	}

	public function nuevaPalabra(Request $request){
		$ch = CH::where('cliente_id',1)->first();
		$cv = CV::where('cliente_id',1)->first();
		if(!empty($request->url)){
			$url = $request->url;
		}elseif ($ch->tipo_servidor=='web' && !empty($ch->servidor_web)) {
			$url = $ch->servidor_web;
		}elseif ($cv->tipo_servidor=='web' && !empty($cv->servidor_web)) {
			$url = $cv->servidor_web;
		}

		if($request->id != '' && $request->id != null){
			$palabra = Palabras::find($request->id);
			$palabra->palabra = $request->palabra;
			$palabra->save();
			if(!empty($url)){
				if($palabra->id_online !== 0){
					$palabra_id_online = 
				 		Curl::to($url.'wp-content/plugins/neo_homenajes/puente/palabras.php')
						->withData( array( 'id' => $palabra->id_online, 'palabra' => $palabra->palabra) )
						->post();
				}else{
					$palabra_id_online = 
				 		Curl::to($url.'wp-content/plugins/neo_homenajes/puente/palabras.php')
						->withData( array( 'id' => '', 'palabra' => $palabra->palabra) )
						->post();
					$palabra_id_online = json_decode($palabra_id_online);
					$palabra->id_online = $palabra_id_online;
					$palabra->save();
				}
					
			}
		}else{
			$palabra = new Palabras();
			$palabra->palabra = $request->palabra;
			$palabra->save();
			if(!empty($url)){
				$palabra_id_online = 
			 		Curl::to($url.'wp-content/plugins/neo_homenajes/puente/palabras.php')
					->withData( array( 'id' => '', 'palabra' => $palabra->palabra) )
					->post();
				$palabra_id_online = json_decode($palabra_id_online);
				$palabra->id_online = $palabra_id_online;
				$palabra->save();
			}
		}		
		return (['msj'=>'palabra creada']);
	}

	public function eliminarPalabra(Request $request){
		$ch = CH::where('cliente_id',1)->first();
		$cv = CV::where('cliente_id',1)->first();
		if(!empty($request->url)){
			$url = $request->url;
		}elseif ($ch->tipo_servidor=='web' && !empty($ch->servidor_web)) {
			$url = $ch->servidor_web;
		}elseif ($cv->tipo_servidor=='web' && !empty($cv->servidor_web)) {
			$url = $cv->servidor_web;
		}

		$palabra = Palabras::find($request->id);

		if(!empty($url)){			
			if($palabra->id_online !== 0){
				$palabra_id_online = 
			 		Curl::to($url.'wp-content/plugins/neo_homenajes/puente/palabras.php?eliminar')
					->withData( array( 'id' => $palabra->id_online) )
					->post();
			}				
		}

		$palabra->delete();

		return (['msj'=>'palabra eliminada']);
	}
	/**
	 * Voncula un servicio a una sala determionada
	 */
	public function asociarSalas(Request $request){
		foreach ($request->sala_local_id as $key => $value) {
			$sala = Sala::find($value);
			$sala->id = $request->sala_local_id[$key];
			$sala->id_online = $request->sala_online[$key];

			$sala->save();  
		}

		$ch = CH::where('cliente_id',1)->first();
		$cv = CV::where('cliente_id',1)->first();
		if ($ch->tipo_servidor=='web' && !empty($ch->servidor_web)) {
			$url = $ch->servidor_web;
		}elseif ($cv->tipo_servidor=='web' && !empty($cv->servidor_web)) {
			$url = $cv->servidor_web;
		}
		if(!empty($url)){
			$response = Curl::to($url.'wp-content/plugins/neo_homenajes/json/conector.php')
					->withData( array( 'salones' => '' ) )
					->post();
		}
		
		
		if ( isset($response) ) {
			$response = json_decode($response);
			
			foreach ($response->resultado as $key => $value) {
				$sala_codigo= Sala::where('id_online', $value->id)->first(); 
				if (count($sala_codigo) > 0) $sala_codigo->codigo= $value->codigo;
				$sala_codigo->save();
			}		   
		} 
		
		return redirect('/?ruta=config/ch&save=1');
	}


	/**
	 * Almacena un homenaje creado con el formulario homenajes
	 */
	public function store(Request $request){
		
		// dd($request->all());
		
		$ch = CH::cliente($request->cliente_id)->first();
		
		if(empty($ch)){
			$ch = new CH();
		}
		
		$ch->cliente_id = $request->cliente_id;
		$ch->url_web_api = $request->url_web_api;
		$ch->tipo_qrcode = $request->tipo_qrcode;
		
		if($request->tipo_bloqueo == 'borrar'){
			$ch->tipo_bloqueo = 1;
		}else{
			$ch->tipo_bloqueo = 0;
		}
		$ch->num_palabras = $request->num_palabras;
		// $ch->tipo_encabezado = $request->tipo_encabezado;
		
		// if ( $request->tipo_encabezado == "texto" ) {
		// 	$ch->texto_encabezado = $request->texto_encabezado;
		// 	// $ch->logo_encabezado = "";
		// } 
		// else if ( $request->tipo_encabezado == "logo")  {
		// 	// $ch->texto_encabezado = "";
		
		// 	if($request->hasFile("logo_encabezado")){
		// 		$nameFile = $ch->cliente_id.'_'.uniqid().'.'.$request->file('logo_encabezado')->getClientOriginalExtension();
		// 		$request->file("logo_encabezado")->move(CH::LOGOS_DIR, $nameFile);
		// 		$ch->logo_encabezado = $nameFile;
		// 	}
		// }
		
		$ch->duracion_slider_homenaje = 
				empty($request->duracion_slider_homenaje) ? 20 : $request->duracion_slider_homenaje;
		
		$ch->duracion_transicion_slider_homenaje = 
				empty($request->duracion_transicion_slider_homenaje) ? 2 : $request->duracion_transicion_slider_homenaje;
		
		$ch->intervalo_publicidad = $request->intervalo_publicidad;
		$ch->duracion_publicidad = $request->duracion_publicidad;
		
		// Genera un nombre único para el archivo de imagen del código QR, que 
		// será usado por QrCode para guardar la imagen generada a partir de la 
		// URL sumonistrada por el server
		
		
		// $ch->musica_funcional = $request->musica_funcional;
		// $ch->cartelera_informacion = $request->cartelera_informacion;
		// $ch->cartelera_control = $request->cartelera_control;
		// $ch->catalogo_digital = $request->catalogo_digital;
		// $ch->url_api_mf = $request->url_api_mf;
		// $ch->url_api_cr = $request->url_api_cr;
		// $ch->tiempo_cr = $request->tiempo_cr;

		//GENERAR CODIGO QR
		
		//dd(base_path() . CH::QRCODES_DIR . '/' . $ch->qrcode);
		
		if($request->tipo_qrcode == 'QR2'){
			$ch->url_web_homenaje = $request->url_web_homenaje;
			$ch->qrcode = $ch->cliente_id.'_'.uniqid(). ".png";
			
			QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($ch->url_web_homenaje, CH::QRCODES_DIR . '/' . $ch->qrcode);
		}			
		
		// ENCABEZADO
		if($request->hasFile("logo_pie")){
			$nameFile = $ch->cliente_id.'_'.uniqid().'.'.$request->file('logo_pie')->getClientOriginalExtension();
			$request->file("logo_pie")->move(CH::LOGOS_DIR, $nameFile);
			$ch->logo_pie = $nameFile;
		}

		if(!File::exists(CH::CH_DIR)){
			File::makeDirectory(CH::CH_DIR);
			File::makeDirectory(CH::PUBLIC_DIR);
		}

		// PUBLICIDADES
		if($request->publi_delete != ""){
			$filesDelete = explode(',',$request->publi_delete);
			foreach($filesDelete as $file){
				File::delete($file);
			}
		}

		if($request->hasFile('publi')){
			$files = $request->file('publi');
			foreach($files as $file){
				if($file != NULL){
					$nameFile = $ch->cliente_id.'_'.uniqid().'.'.$file->getClientOriginalExtension();
					$file->move(CH::PUBLI_DIR, $nameFile);  
				}
			}
		}

		$ch->save();

		return redirect('/?ruta=config/ch&save=1');
	}
	
	
	/**
	 * Devuelve la configuración de la cartelera de homenajes cuando la 
	 * consulta se hace a través de la API. Se espera que esta consulta 
	 * la haga el fornt-end de la cartelera para establecer su propia 
	 * configuración.
	 */
	public function api_config(){
		$cliente_id = 1;
		$configCH = CH::cliente($cliente_id)->first();
		// PUBLICIDADES
		$files = File::allFiles(CH::PUBLI_DIR);
		// $publi = array(); 
		
		$publis = array();
		foreach ($files as $file) 
		{
			$publis[] = str_replace('\\','/',(string)$file);
		}


		return response()->json(["configCH"=>$configCH, "publis" => $publis]);
	}
	
	
	/**
	 * Sincroniza los servicios del back-end con el sitio homenaje.de 
	 * 
	 * NOTA: Esta función califica para ser convertida en un Service Provider 
	 * debido a su tamaño y complejidad.
	 */
	public function sync(Request $request) {
		$ch = CH::where('cliente_id',1)->first();
		$cv = CV::where('cliente_id',1)->first();
		if(!empty($request->url)){
			$url = $request->url;
		}elseif ($ch->tipo_servidor=='web' && !empty($ch->servidor_web)) {
			$url = $ch->servidor_web;
		}elseif ($cv->tipo_servidor=='web' && !empty($cv->servidor_web)) {
			$url = $cv->servidor_web;
		}
		if(!empty($url)){
			$getServicios = Curl::to($url.'wp-content/plugins/neo_homenajes/json/conector.php')
							->withData( array( 'listar' => '' ) )->post();
			$servicios = json_decode($getServicios);

			$getSalas = Curl::to($url.'wp-content/plugins/neo_homenajes/json/conector.php')
							->withData( array( 'salones' => '' ) )->post();
			$salas = json_decode($getSalas);

			$getHomenajes = Curl::to($url.'wp-content/plugins/neo_homenajes/json/conector.php')
							->withData( array( 'homenaje' => '' ) )->post();
			$homenajes = json_decode($getHomenajes);

			$getCementerios = Curl::to($url.'wp-content/plugins/neo_homenajes/json/conector.php')
							->withData( array( 'cementerios' => '' ) )->post();
			$cementerios = json_decode($getCementerios);
			
			$getResponsos = Curl::to($url.'wp-content/plugins/neo_homenajes/json/conector.php')
							->withData( array( 'responsos' => '' ) )->post();
			$responsos = json_decode($getResponsos);
			
			$getPalabras = Curl::to($url.'wp-content/plugins/neo_homenajes/json/conector.php')
							->withData( array( 'palabras' => '' ) )->post();
			$palabras = json_decode($getPalabras);

			if(!empty($palabras)){
				foreach ($palabras as $key => $value) {
					$palabra = Palabras::where('id_online',$value->id )->first();

					if(count($palabra)==0){
						$palabra = new Palabras();
						$palabra->id_online = $value->id;
						$palabra->palabra = $value->palabra;
						$palabra->save();						
					}
				}
			}

			$licencias = Licencia::all();
			$sala_cr = false;
			foreach ($licencias as $key => $value) {
				if($value->product_id==32 && $value->status=='Active' && $value->descarga==1){
					$sala_cr = true;
				}
			}
			if(!empty($salas->resultado)){
				foreach ($salas->resultado as $key => $value) {
					$salas_loc = Sala::where('id_online',$value->id )->first();

					if(count($salas_loc)==0){
						$sala_nueva = new Sala();
						$sala_nueva->id_online = $value->id;
						$sala_nueva->nombre = $value->nombre;
						$sala_nueva->codigo = $value->codigo;
						$sala_nueva->status = 1;
						$sala_nueva->cliente_id = 1;
						$sala_nueva->save();
						if(isset($sala_cr)){
							if(strpos($_SERVER['SERVER_NAME'], "localhost")){
								$sala_cr = Curl::to('http://localhost/modulos/cc0/public/?ruta=api/crear_sala')
									->withData( array( 'nombre' => $sala_nueva->nombre, 'idsala' => $sala_nueva->id ) )->post();
							}
							
						}
					}
				}
			}
		  	// dd($cementerios->resultado);
		  	if(!empty($cementerios->resultado)){
		  		foreach ($cementerios->resultado as $key => $val) {
					$getCementerio = Destino::where('id_online',$val->id )->get();
					if (count($getCementerio) == 0) {
						$getCementerio_set = new Destino();
						$getCementerio_set->id_online           =$val->id;
						$getCementerio_set->nombre              =$val->nombre;
						$getCementerio_set->direccion           =$val->direccion;
						//$getCementerio_set->imagen              =$val->imagen;
					  	//$getCementerio_set->short_url           =$val->url_maps;
						$getCementerio_set->save();
					}
				}
		  	}
				
			if(!empty($responsos->resultado)){
				foreach ($responsos->resultado as $key => $val) {
					$getResponso = Responso::where('id_online',$val->id )->get();
				
					if (count($getResponso) == 0) {
						$getResponso_set = new Responso();
						$getResponso_set->id_online = $val->id;
						$getResponso_set->nombre = $val->nombre;
						$getResponso_set->direccion	= $val->direccion;
						// $getResponso_set->url_maps = $val->url_maps;

						$getResponso_set->save();
					}
				}
			}
			
			$setPalabras = Palabras::where('id_online','0')->get();
			if(count($setPalabras)>0){
				foreach ($setPalabras as $key => $val) {
					
					$palabra_id_online = 
					 		Curl::to($url.'wp-content/plugins/neo_homenajes/puente/palabras.php')
							->withData( array( 'id' => '', 'palabra' => $val->palabra) )
							->post();
					// dd($palabra_id_online);
					$palabra_id_online = json_decode($palabra_id_online);
					// dd($palabra_id_online->id); 
				// dd($palabra_id_online);
					$getPalabra_set = Palabras::where('id',$val->id)->first();
					$getPalabra_set->id_online           =$palabra_id_online;
					
					$getPalabra_set->save();  

				}
			}

			$setSalones = Sala::where('id_online','0')->get();
			$sala_ruta = Sala::ICONO_URL;
			if(count($setSalones)>0){
				foreach ($setSalones as $key => $val) {
					
					$salones_id_online = 
					 		Curl::to($url.'wp-content/plugins/neo_homenajes/puente/salon.php')
							->withData( array( 'nombre' => $val->nombre, 'direccion' => '', 'url_maps' => '', 'frame_maps' => '') )
							->post();
					
					$salones_id_online = json_decode($salones_id_online);
					// dd($salones_id_online->id); 
				
					$getSalon_set = Sala::where('id',$val->id)->first();
					$getSalon_set->id_online           =$salones_id_online->id;
					
					$getSalon_set->save();  

				}
			}
			// dd($sal);
			$setCementerios = Destino::where('id_online','0')->get();
			if(count($setCementerios)>0){
				foreach ($setCementerios as $key => $val) {
					$cementerio_id_online = 
					 		Curl::to($url.'wp-content/plugins/neo_homenajes/puente/cementerios.php')
							->withData( array( 'nombre' => $val->nombre, 'direccion'=>$val->direccion ,'url_maps'=> $val->short_url,'imagen'=>$val->mapa) )
							->post();
				
					$getResponso_set = Destino::where('id',$val->id)->first();
					$getResponso_set->id_online           =$cementerio_id_online;
					
					$getResponso_set->save();   
				}
			}
			$setResponsos = Responso::where('id_online','0')->get();
			if(count($setResponsos)>0){
				foreach ($setResponsos as $key => $val) {
					 $responso_id_online = 
					 		Curl::to($url.'wp-content/plugins/neo_homenajes/puente/responso.php')
							->withData( array( 'nombre' => $val->nombre, 'direccion'=>$val->direccion, 'url_maps'=>$val->url_maps) )
							->post();
				
					$getResponso_set = Responso::where('id',$val->id)->first();
					$getResponso_set->id_online           =$responso_id_online;
					
					$getResponso_set->save();
				}  
			}
			if(!empty($servicios->resultado)){
				foreach ($servicios->resultado as $key => $value) {
					$getServicio = Servicio::where('id_online', $value->id)->get();

					
					if (count($getServicio) > 0) {
						$getServicioPorFechas = 
								Servicio::where('id_online', $value->id)
								->where('fecha_nac', $value->fecha_nacimiento)
								->where('fecha_fac', $value->fecha_fallecido)
								->first();
						
						if (count($getServicioPorFechas) > 0) {
							
							$fecha_servicio_local = strtotime($getServicioPorFechas->fecha_update);
							$fecha_servicio_web = strtotime(date('Y/m/d H:s:i'));
							
							if ($fecha_servicio_web >  $fecha_servicio_local) {
								
								$split = explode('.', $value->fotografia);
								$ext = array_pop($split);
								
								$getImagenes = Curl::to($value->fotografia)
											->withContentType('image/'.$ext)
											->download(public_path().'/images/servicios/'. $getServicioPorFechas->id .'.'.$ext);
								
								$name = 'images/servicios/' . $getServicioPorFechas->id . '.' . $ext;
								$cv = CV::cliente(1)->first();
								
								
									if($value->url == null){
                    					$getServicioPorFechas->url1='http://neosepelios.com.ar';	
                    					$getServicioPorFechas->url1 = $getServicioPorFechas->id.'_'.uniqid(). ".png";							
                    					QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate('http://neosepelios.com.ar', CV::QRCODES_DIR . '/' . $getServicioPorFechas->qrcode1);
                    				}else{
                    					$getServicioPorFechas->url1=$value->url;	
                    					$getServicioPorFechas->qrcode1 = $getServicioPorFechas->id.'_'.uniqid(). ".png";							
                    					QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($getServicioPorFechas->url1, CV::QRCODES_DIR . '/' . $getServicioPorFechas->qrcode1);
                    				}
                    				
                    				if($value->url_salon == null){
                    					$getServicioPorFechas->url2='http://neosepelios.com.ar';
                    					$getServicioPorFechas->qrcode2 = $getServicioPorFechas->id.'_'.uniqid(). ".png";					
                    					QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate('http://neosepelios.com.ar', CV::QRCODES_DIR . '/' . $getServicioPorFechas->qrcode2);
                    				}else{
                    					$getServicioPorFechas->url2=$value->url_salon;
                    					$getServicioPorFechas->qrcode2 = $getServicioPorFechas->id.'_'.uniqid(). ".png";					
                    					QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($getServicioPorFechas->url2, CV::QRCODES_DIR . '/' . $getServicioPorFechas->qrcode2); 
                    				}
                    				
									
								// 	if($getServicioPorFechas->url1 != $value->url){
								// 		$getServicioPorFechas->qrcode1 = $getServicioPorFechas->id.'_'.uniqid(). ".png";
								// 		$getServicioPorFechas->url1 = $value->url;								
								// 		QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($getServicioPorFechas->url1, CV::QRCODES_DIR . '/' . $getServicioPorFechas->qrcode1);



								// 	}
								// 	if ($getServicioPorFechas->url2 != $value->url_salon){
								// 		$getServicioPorFechas->qrcode2 = $getServicioPorFechas->id.'_'.uniqid(). ".png";
								// 		$getServicioPorFechas->url2 = $value->url_salon;							
								// 		QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($getServicioPorFechas->url2, CV::QRCODES_DIR . '/' . $getServicioPorFechas->qrcode2);    

								// 	}

									
								
								
								$getServicioPorFechas->save();
								if(count($value->velatorios)>0){
									if($value->velatorios[0]->inicio == null){
									   		//$getServicioPorFechas->fecha_inicio = $value->velatorios[0]->fecha.' 07:00:00';
									   }else{
									   		//$getServicioPorFechas->fecha_inicio = $value->velatorios[0]->fecha.' '.$value->velatorios[0]->inicio;
									   }
									
								}
								
								/*********cambio si hay un servicio distinto borro los que existen****/
								$cambio=false;
								foreach ($value->velatorios as $key => $vel) {
									$getVelatorio                = Velatorio::where('id_online', $vel->id)->first();
									if(empty($getVelatorio)){
										$cambio=true;
									}
								}
								if ($cambio){
									$getVelatorio  = Velatorio::where('servicio_id', $getServicioPorFechas->id)->delete();
								}
								/***************************************************************************/
								foreach ($value->velatorios as $key => $vel) {
								   $getVelatorio                = Velatorio::where('id_online', $vel->id)->first();
								   if(empty($getVelatorio)){
								   		Velatorio::where('servicio_id', $getServicioPorFechas->id)->where('id_online','0')->delete();
								   		$getVelatorio                = new Velatorio();
									   $getVelatorio->id_online     = $vel->id;
									   $getVelatorio->servicio_id   = $getServicioPorFechas->id;
									   if($vel->inicio == null){
									   		$getVelatorio->desde = '07:00:00';
									   }else{
									   		$getVelatorio->desde         = $vel->inicio;
									   }
									   if($vel->fin == null){
									   		$getVelatorio->hasta = '18:00:00';
									   }else{
									   		$getVelatorio->hasta         = $vel->fin;
									   }
									   
									   $getVelatorio->fecha         = $vel->fecha;
									
									   $getVelatorio->save();
								   }else{
								   		$getVelatorio->id_online     = $vel->id;
									   $getVelatorio->servicio_id   = $getServicioPorFechas->id;
									   if($vel->inicio == null){
									   		$getVelatorio->desde = '07:00:00';
									   }else{
									   		$getVelatorio->desde         = $vel->inicio;
									   }
									   if($vel->fin == null){
									   		$getVelatorio->hasta = '18:00:00';
									   }else{
									   		$getVelatorio->hasta         = $vel->fin;
									   }
									   $getVelatorio->fecha         = $vel->fecha;
									
									   $getVelatorio->save();
								   }
								   if($vel->fin == null){
								   		//$getServicioPorFechas->fecha_fin = $vel->fecha.' 18:00:00';
								   }else{
								   		//$getServicioPorFechas->fecha_fin = $vel->fecha.' '.$vel->fin;
								   }
									
								}
								$getServicioPorFechas->save();
							}
						 }
					}else{
						$servicio                       = new Servicio();
						$servicio->id_online            = $value->id;
						$servicio->nombres              = $value->nombre_fallecido;
						$servicio->apellidos            = $value->apellido_fallecido;
						$servicio->fecha_nac            = $value->fecha_nacimiento;
						$servicio->fecha_fac            = $value->fecha_fallecido;
						$servicio->servicio_cementerio  = $value->servicio_cementerio.' '.$value->hora_s_cementerio;
						$servicio->salida              = $value->servicio_cementerio.' '.$value->hora_s_cementerio;
						if($value->fecha_responso == null && $value->hora_responso == null){
							$servicio->responso              = $value->servicio_cementerio.' '.$value->hora_s_cementerio;
						}else{
							$servicio->responso              = $value->fecha_responso.' '.$value->hora_responso;
						}
						
						$servicio->cliente_id           = 1;
						$sala_id_online                 = Sala::where('id_online', $value->salon)->first();
						$servicio->sala_id              = $sala_id_online->id;
						$destino_id_online                 = Destino::where('id_online', $value->cementerio)->first();
						$servicio->destino_id              = $destino_id_online->id;
						$servicio->fecha_update = date('Y/m/d H:s:i');

						$cv = CV::cliente(1)->first();
						if($servicio->url1 != $value->url){
							$servicio->qrcode1 = $servicio->id.'_'.uniqid(). ".png";
							$servicio->url1 = $value->url;								
							QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($servicio->url1, CV::QRCODES_DIR . '/' . $servicio->qrcode1);



						}
						if ($servicio->url2 != $value->url_salon){
							$servicio->qrcode2 = $servicio->id.'_'.uniqid(). ".png";
							$servicio->url2 = $value->url_salon;							
							QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($servicio->url2, CV::QRCODES_DIR . '/' . $servicio->qrcode2);    

						}
						

						$servicio->save();
						$split = explode('.', $value->fotografia);
						$ext = array_pop($split);                
						$getImagenes = 
								Curl::to($value->fotografia)
								->withContentType('image/'.$ext)
								->download(public_path().'/images/servicios/'. $servicio->id .'.'.$ext);
						
						$name = 'images/servicios/' . $servicio->id . '.' . $ext;                
						$servicio->foto = $name;
						$servicio->save();

						if(count($value->velatorios)>0){
							if($value->velatorios[0]->inicio == null){
						   		//$servicio->fecha_inicio = $value->velatorios[0]->fecha.' 07:00:00';
						   }else{
						   		//$servicio->fecha_inicio = $value->velatorios[0]->fecha.' '.$value->velatorios[0]->inicio;
						   }
						}
						foreach ($value->velatorios as $key => $vel) {
						   $velatorio = new Velatorio();
						   $velatorio->id_online = $vel->homenaje;
						   $velatorio->servicio_id = $servicio->id;
						   if($vel->inicio == null){
						   		$velatorio->desde = '07:00:00';
						   }else{
						   		$velatorio->desde = $vel->inicio;
						   }
						   if($vel->fin == null){
						   		$velatorio->hasta = '18:00:00';
						   }else{
						   		$velatorio->hasta = $vel->fin;
						   }
						   // $velatorio->desde = $vel->inicio;
						   // $velatorio->hasta = $vel->fin;
						   $velatorio->fecha = $vel->fecha;
						   
						   $velatorio->save();
						   if($vel->fin == null){
						   		//$servicio->fecha_fin = $vel->fecha.' 18:00:00';
						   }else{
						   		//$servicio->fecha_fin = $vel->fecha.' '.$vel->fin;
						   }
						   
						}
						$servicio->save();
					}
				}
			}
			if(!empty($homenajes->resultado)){
				foreach ($homenajes->resultado as $key => $value) {
					$getHomenaje = Homenaje::where('id_online', $value->id)->first();
					
					if (count($getHomenaje) > 0) {
						$fecha_homenaje_local = strtotime($getHomenaje->updated_at);
						$fecha_homenaje_web = strtotime(date('Y/m/d H:i:s'));
						$getServicio_ = Servicio::where('id_online', $value->id_servicio)->first();
						if(!empty($getServicio_)){
							if ($fecha_homenaje_web > $fecha_homenaje_local) {
								$getHomenaje->id_online = $value->id;
								$getHomenaje->nombre = $value->nombre;
								$getHomenaje->mensaje = $value->texto_homenaje;
								$getHomenaje->created_at = $value->publicacion;
								$getHomenaje->updated_at = date('Y/m/d H:i:s');
								
								$getHomenaje->save();
								
								if($value->foto_homenaje == null){
									$getHomenaje->foto = '';
								}else{
									$split = explode('.', $value->foto_homenaje);
									$ext = array_pop($split);                
									$getImagenes = 
											Curl::to($value->foto_homenaje)
											->withContentType('image/'.$ext)
											->download(public_path().'/images/homenaje/'. $getHomenaje->id .'.'.$ext);
									
									$name = $getHomenaje->id . '.' . $ext;       
									$getHomenaje->foto=null;
									$getHomenaje->foto = $name;
								}
							
							
								$getHomenaje->servicio_id = $getServicio_->id;
								$getHomenaje->save();
							}
							
						}
					}else{
						$getServicio_ = Servicio::where('id_online', $value->id_servicio)->first();
						if(!empty($getServicio_)){
							$homenaje = new Homenaje();
							$homenaje->id_online = $value->id;
							
							$homenaje->nombre = $value->nombre;
							$homenaje->mensaje = $value->texto_homenaje;
							
							$homenaje->created_at = $value->publicacion;
							$homenaje->updated_at = date('Y/m/d H:i:s');
							$homenaje->save();

							if($value->foto_homenaje == null){
								$homenaje->foto = '';
							}else{
								$split = explode('.', $value->foto_homenaje);
								$ext = array_pop($split);                
								$getImagenes = Curl::to($value->foto_homenaje)
											->withContentType('image/'.$ext)
											->download(public_path().'/images/homenaje/'. $homenaje->id .'.'.$ext);
								
								$name = $homenaje->id . '.' . $ext;     
								//$getImagenes = Curl::to($value->foto_homenaje)->post();
								//$split = explode('.', $value->foto_homenaje);
								//$ext = array_pop($split);
								//$name = 'homenajes/' . $homenaje->id . '.' . $ext;
								//Storage::put($name, $getImagenes);

								$homenaje->foto = $name;

								//$name = Homenaje::descargarImagen($value->foto_homenaje, $homenaje->id);
								//$homenaje->foto = $name;
							}
						
						
							$homenaje->servicio_id = $getServicio_->id;
							$homenaje->save();
						}
						
					}
				}
			}
			$servicio_insert =
					Servicio::where('id_online','=','0')
					->with('velatorios')
					->with('salas')
					->get();
					
			$cont = 0;
			if(count($servicio_insert)>0){
				foreach ($servicio_insert as $key => $value) {
					
					if($value->sala_id==99 || $value->sala_id==-10 || $value->sala_id==0){
						
					}else{
						$files = Servicio::FOTO_URL;
						$sala_online=sala::where('id','=',$value->sala_id)->first();
						if(isset($sala_online->id_online)){
						    $salon=$sala_online->id_online;
						}else{
						    $salon=0;
						}
						
						$destino_online=Destino::where('id','=',$value->destino_id)->first();
						$nombre_fallecido=$value->nombres;
						$apellido_fallecido=$value->apellidos;
						$genero='';
						$fecha_nacimiento=$value->fecha_nac;
						$fecha_fallecido=$value->fecha_fac;
						$salida_servicio=date($value->salida);
						$hora_s_servicio=strtotime($value->salida);
						$servicio_cementerio=date($value->servicio_cementerio);
						$hora_s_cementerio=strtotime($value->servicio_cementerio);
						$velatorio_fecha=date($value->fecha_inico);
						$hora_v_inicio=strtotime($value->fecha_inico);
						$fotografia=0;
						if(isset($destino_online->id_online)){
							if($destino_online->id_online == 0 || $destino_online->id_online==null){
								$destino=1;
							}else{
								$destino=$destino_online->id_online;
							}
						}else{
							$destino=0;
						}													
						
						$religion=$value->religion_id;
						$texto_homenaje='';
						$banner=0;
						$correo_familiar='';
						$clave_familiar='';
						$admin=$value->cliente_id;
						$modificado=date('Y/m/d H:i:s');
						$tipo='';
						$i=0;
						$velatorio=$value->velatorios;

						$fecha_nac = explode(' ', date($value->fecha_nac));
						$fecha_fac = explode(' ', date($value->fecha_fac));
						$salida = explode(' ', date($value->salida));
						$cementerio = explode(' ', date($value->servicio_cementerio));
						if($value->fecha_inicio == '0000-00-00 00:00:00' || $value->fecha_inicio == null){
							//$value->fecha_inicio = $value->velatorios[0]->fecha.' '.$value->velatorios[0]->desde;
						}
						$inicio = explode(' ', date($value->fecha_inicio));
						$foto = explode("/", $value->foto);
						// dd(public_path());
						if($value->foto!=''){
						 $getArray=
						
								Curl::to($url.'wp-content/plugins/neo_homenajes/puente/servicio.php')
								->withData( array('nombre_fallecido'=> $value->nombres,
										'apellido_fallecido'=>  $value->apellidos,
										'genero'=> '',
										'fecha_nacimiento'=>  $fecha_nac[0],
										'fecha_fallecido'=> $fecha_fac[0],
										'salida_servicio'=>$salida[0],
										'hora_s_servicio'=>$salida[1],
										'servicio_cementerio'=>$cementerio[0],
										'hora_s_cementerio'=>$cementerio[1],
										'velatorio_fecha'=>$inicio[0],
										'hora_v_inicio'=>$inicio[1],
										'fotografia'=>$value->foto,
										'cementerio'=>$destino,
										'salon'=>$salon,
										'religion'=>$value->religion_id,
										'texto_homenaje'=>'',
										'banner'=>0,
										'correo_familiar'=>'',
										'clave_familiar'=>'',
										'admin'=>$value->cliente_id,
										'modificado'=>date('Y/m/d H:i:s'),
										'tipo'=>'',
										'info_velario_cv'=>$velatorio) )
								->withFile( 'imagen',public_path().'/' .$value->foto, 'image/jpg', $files.$value->foto )
								->post();
						}else{
							$getArray=
						
								Curl::to($url.'wp-content/plugins/neo_homenajes/puente/servicio.php')
								->withData( array('nombre_fallecido'=> $value->nombres,
										'apellido_fallecido'=>  $value->apellidos,
										'genero'=> '',
										'fecha_nacimiento'=>  $fecha_nac[0],
										'fecha_fallecido'=> $fecha_fac[0],
										'salida_servicio'=>$salida[0],
										'hora_s_servicio'=>$salida[1],
										'servicio_cementerio'=>$cementerio[0],
										'hora_s_cementerio'=>$cementerio[1],
										'velatorio_fecha'=>$inicio[0],
										'hora_v_inicio'=>$inicio[1],
										'fotografia'=>'',
										'cementerio'=>$destino,
										'salon'=>$salon,
										'religion'=>$value->religion_id,
										'texto_homenaje'=>'',
										'banner'=>0,
										'correo_familiar'=>'',
										'clave_familiar'=>'',
										'admin'=>$value->cliente_id,
										'modificado'=>date('Y/m/d H:i:s'),
										'tipo'=>'',
										'info_velario_cv'=>$velatorio)  )
								->post();
						}
						$cont++;
						$servicio_update=Servicio::where('id','=',$value->id)->first();
						$getArray=json_decode($getArray);
						if(!isset($getArray->id)){
						    dd($cont,$getArray);
						}
						
						$servicio_update->id_online=$getArray->id;
						$servicio_update->url1=$getArray->url;
						$servicio_update->url2=$getArray->url_salon;
						
						if($getArray->url == null){
        					$servicio_update->url1='http://neosepelios.com.ar';	
        					$servicio_update->url1 = $servicio_update->id.'_'.uniqid(). ".png";							
        					QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate('http://neosepelios.com.ar', CV::QRCODES_DIR . '/' . $servicio_update->qrcode1);
        				}else{
        					$servicio_update->url1=$getArray->url;	
        					$servicio_update->qrcode1 = $servicio_update->id.'_'.uniqid(). ".png";							
        					QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($servicio_update->url1, CV::QRCODES_DIR . '/' . $servicio_update->qrcode1);
        				}
        				
        				if($getArray->url_salon == null){
        					$servicio_update->url2='http://neosepelios.com.ar';
        					$servicio_update->qrcode2 = $servicio_update->id.'_'.uniqid(). ".png";					
        					QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate('http://neosepelios.com.ar', CV::QRCODES_DIR . '/' . $servicio_update->qrcode2);
        				}else{
        					$servicio_update->url2=$getArray->url_salon;
        					$servicio_update->qrcode2 = $servicio_update->id.'_'.uniqid(). ".png";					
        					QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($servicio_update->url2, CV::QRCODES_DIR . '/' . $servicio_update->qrcode2); 
        				}
						
				// 		if($servicio_update->url1 != $value->url ){
				// 			$servicio_update->qrcode1 = $servicio_update->id.'_'.uniqid(). ".png";
				// 			$servicio_update->url1 = $getArray->url;	
				// 			if($servicio_update->url1 != null){
				// 				QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($servicio_update->url1, CV::QRCODES_DIR . '/' . $servicio_update->qrcode1);
				// 			}	

				// 		}
				// 		if ($servicio_update->url2 != $value->url_salon ){
				// 			$servicio_update->qrcode2 = $servicio_update->id.'_'.uniqid(). ".png";
				// 			$servicio_update->url2 = $getArray->url_salon;	
				// 			if($servicio_update->url2 != null){
				// 				QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($servicio_update->url2, CV::QRCODES_DIR . '/' . $servicio_update->qrcode2); 
				// 			}						
							   

				// 		}
						
						$servicio_update->save();	
					}
					

						

				}
			}

			$homenaje_insert =Homenaje::where('id_online','=','0')
							->get();
			$files_homenaje = Homenaje::FOTO_DIR;
			// dd(count($homenaje_insert));
			if(count($homenaje_insert)>0){
				foreach ($homenaje_insert as $key => $value) {
					
					$nombre=$value->nombre;
					$correo='';
					$texto_homenaje=$value->mensaje;
					$foto_homenaje=$value->foto;
					$aprobado=1;
					$publicacion=date('d/m/Y');
					$servicio_homenje=Servicio::where('id','=',$value->servicio_id)->first();
					if(isset($servicio_homenje->id_online) && $servicio_homenje->id_online != 0){
						if($value->foto == '' || $value->foto==null){
							$getHomenaje = 
									Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php')
									->withData( array(
													'id_homenaje'=>$servicio_homenje->id_online,
													'nombre'=>  $nombre,
													'correo'=>  $correo,
													'texto_homenaje'=> $texto_homenaje,
													'foto_homenaje'=>'homenaje_gene.jpg',
													'aprobado'=>$aprobado,
													'publicacion'=>$publicacion) )
									->withFile( 'imagen',public_path().'/homenaje_gene.jpg', 'image/jpg', $files_homenaje.'homenaje_gene.jpg' )
									->post();
						}else{
							$getHomenaje = 
									Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php')
									->withData( array(
													'id_homenaje'=>$servicio_homenje->id_online,
													'nombre'=>  $nombre,
													'correo'=>  $correo,
													'texto_homenaje'=> $texto_homenaje,
													'foto_homenaje'=>$foto_homenaje,
													'aprobado'=>$aprobado,
													'publicacion'=>$publicacion) )
									->withFile( 'imagen',public_path().'/' .$value->foto, 'image/jpg', $files_homenaje.$value->foto )
									->post();
						}
							
						
						
						$homenaje_update=Homenaje::where('id','=',$value->id)->first();
						$homenaje_update->id_online=$getHomenaje;
						$homenaje_update->save();
					}
						
				}
			}

			//$getServicio = $servicios->status;
			return redirect('/?ruta=config/ch&save=1');
		}else{
			// return redirect('/?ruta=config/ch&error=url_vacia');
			return (['ERROR'=>'Error de sinc']);
		}
			
	}

	public function api_sala(Request $request){
        $ch = CH::cliente(1)->first();

        $ch->tipo_servidor = $request->tipo;

        if($request->tipo == 'local'){
            $ch->servidor_ip = $request->url;
            $ch->sala_id = $request->sala;
        }elseif($request->tipo == 'online'){
            $ch->servidor_url = $request->url;
            $ch->sala_id = $request->sala;
        }elseif($request->tipo == 'web'){
        	$ch->servidor_web = $request->url;

        	$sala=Sala::where('id_online',$request->sala)->first();
        	$ch->sala_id = $sala->id;
        }else{
        	$ch->sala_id = $request->sala;
        }
        

        $ch->save();

        $cs = CS::cliente(1)->first();

        $cs->tipo_servidor = $request->tipo;

        if($request->tipo == 'local'){
            $cs->servidor_ip = $request->url;
            $cs->sala_id = $request->sala;
        }elseif($request->tipo == 'online'){
            $cs->servidor_url = $request->url;
            $cs->sala_id = $request->sala;
        }elseif($request->tipo == 'web'){
            $cs->servidor_web = $request->url;

            $sala=Sala::where('id_online',$request->sala)->first();
        	$cs->sala_id = $sala->id;
        }else{
        	$cs->sala_id = $request->sala;
        }
        

        $cs->save();

        return (['mensaje'=>'Servidor registrado']);
    }

   

    public function conexion(){

    	$configCH = CH::cliente(1)->first();
    	$configCV = CV::cliente(1)->first();
    	$salas = Sala::all('nombre', 'id', 'id_online');

    	$array=array();
        $sala_sep = (explode(",",$configCH->sala_id));
        for($i=0;$i<count($sala_sep);$i++){
            array_push($array,(['sala'=>$sala_sep[$i]]));
        }

    	return view('conexion.index',['ch'=>$configCH, 'cv'=>$configCV,'salas'=>$salas, 'sala_sel'=>$array]);
    }
}
