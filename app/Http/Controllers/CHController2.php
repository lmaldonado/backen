<?php 

namespace App\Http\Controllers;

use App\CH;
use App\CV;
use App\Sala;
use App\Destino;
use App\Responso;
use App\Homenaje;
use App\Religion;
use App\Servicio;
use App\iphost;
use App\Velatorio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Ixudra\Curl\Facades\Curl;
use SimpleSoftwareIO\QrCode\Facades\QrCode; // https://www.simplesoftware.io/docs/simple-qrcode/es

class CHController extends Controller {

	private $models;
	
	
	/**
	 * Constructor
	 */
	public function __construct(Ch $ch){
		$this->models = $ch;
	}
	
	
	/**
	 * Muestra la vista de configuración de la cartelera de homenajes
	 */
	public function index(Request $request){
		$numImagen = 0;
		$cliente_id = 1;
		$configCH = CH::cliente($cliente_id)->first();
		
		if(empty($configCH)){
			$configCH = new CH();
		}

		$configCH->logo_encabezado = CH::LOGOS_DIR.'/'.$configCH->logo_encabezado; 
		$configCH->logo_pie = CH::LOGOS_DIR.'/'.$configCH->logo_pie; 
		$configCH->qrcode = '../' . CH::QRCODES_DIR.'/'.$configCH->qrcode; 

		$files = File::allFiles(CH::PUBLI_DIR);
		$publicidades = array();
			
		foreach ($files as $file) 
		{
			$publicidades[] = str_replace('\\','/',(string)$file);
		}

		$salas = Sala::all('nombre', 'id', 'id_online');
		
		$response = Curl::to('http://homenaje.de/wp-content/plugins/neo_homenajes/json/conector.php')
					->withData( array( 'salones' => '' ) )
					->post();
		
		$salas_online[0] = "[ SELECCIONE ]";
		
		if ( $response ) {
			$response = json_decode($response, true);
			foreach ($response["resultado"] as $key => $value) {
				$salas_online[$value["id"]] = $value["nombre"];
			}
			$conexionOnline = true;
		} else {
			$conexionOnline = false;
		}
		
		return view('configuracion.ch',
			[
				'ch'=>$configCH,
				'publicidades' => $publicidades,
				'request' => $request,
				'salas' => $salas,
				'salas_online' => $salas_online,
				'numImagen' => $numImagen,
				'conexionOnline' => $conexionOnline,
			]);
	}


	/**
	 * Voncula un servicio a una sala determionada
	 */
	public function asociarSalas(Request $request){
		foreach ($request->sala_local_id as $key => $value) {
			$sala = Sala::find($value);
			$sala->id = $request->sala_local_id[$key];
			$sala->id_online = $request->sala_online[$key];

			$sala->save();  
		}
		
		$response = Curl::to('http://homenaje.de/wp-content/plugins/neo_homenajes/json/conector.php')
					->withData( array( 'salones' => '' ) )
					->post();
		
		if ( $response ) {
			$response = json_decode($response);
			
			foreach ($response->resultado as $key => $value) {
				$sala_codigo= Sala::where('id_online', $value->id)->first(); 
				if (count($sala_codigo) > 0) $sala_codigo->codigo= $value->codigo;
				$sala_codigo->save();
			}		   
		} 
		
		return redirect('config/ch?save=1');
	}


	/**
	 * Almacena un homenaje creado con el formulario homenajes
	 */
	public function store(Request $request){
		
		//dd($request->all());
		
		$ch = CH::cliente($request->cliente_id)->first();
		
		if(empty($ch)){
			$ch = new CH();
		}
		
		$ch->cliente_id = $request->cliente_id;
		$ch->url_web_api = $request->url_web_api;
		$ch->qrcode = $request->qrcode;
		$ch->url_web_homenaje = $request->url_web_homenaje;
		$ch->tipo_encabezado = $request->tipo_encabezado;
		
		if ( $request->tipo_encabezado == "texto" ) {
			$ch->texto_encabezado = $request->texto_encabezado;
			$ch->logo_encabezado = "";
		} else if ( $request->tipo_encabezado == "logo")  {
			$ch->texto_encabezado = "";
		
			if($request->hasFile("logo_encabezado")){
				$nameFile = $ch->cliente_id.'_'.uniqid().'.'.$request->file('logo_encabezado')->getClientOriginalExtension();
				$request->file("logo_encabezado")->move(CH::LOGOS_DIR, $nameFile);
				$ch->logo_encabezado = $nameFile;
			}
		}
		
		$ch->duracion_slider_homenaje = 
				empty($request->duracion_slider_homenaje) ? 20 : $request->duracion_slider_homenaje;
		
		$ch->duracion_transicion_slider_homenaje = 
				empty($request->duracion_transicion_slider_homenaje) ? 2 : $request->duracion_transicion_slider_homenaje;
		
		$ch->intervalo_publicidad = $request->intervalo_publicidad;
		$ch->duracion_publicidad = $request->duracion_publicidad;
		
		// Genera un nombre único para el archivo de imagen del código QR, que 
		// será usado por QrCode para guardar la imagen generada a partir de la 
		// URL sumonistrada por el server
		$ch->qrcode = $ch->cliente_id.'_'.uniqid(). ".png";
		
		// $ch->musica_funcional = $request->musica_funcional;
		// $ch->cartelera_informacion = $request->cartelera_informacion;
		// $ch->cartelera_control = $request->cartelera_control;
		// $ch->catalogo_digital = $request->catalogo_digital;
		// $ch->url_api_mf = $request->url_api_mf;
		// $ch->url_api_cr = $request->url_api_cr;
		// $ch->tiempo_cr = $request->tiempo_cr;

		//GENERAR CODIGO QR
		
		//dd(base_path() . CH::QRCODES_DIR . '/' . $ch->qrcode);
		
		QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($ch->url_web_homenaje, CH::QRCODES_DIR . '/' . $ch->qrcode);
		
		// ENCABEZADO
		if($request->hasFile("logo_pie")){
			$nameFile = $ch->cliente_id.'_'.uniqid().'.'.$request->file('logo_pie')->getClientOriginalExtension();
			$request->file("logo_pie")->move(CH::LOGOS_DIR, $nameFile);
			$ch->logo_pie = $nameFile;
		}

		if(!File::exists(CH::CH_DIR)){
			File::makeDirectory(CH::CH_DIR);
			File::makeDirectory(CH::PUBLIC_DIR);
		}

		// PUBLICIDADES
		if($request->publi_delete != ""){
			$filesDelete = explode(',',$request->publi_delete);
			foreach($filesDelete as $file){
				File::delete($file);
			}
		}

		if($request->hasFile('publi')){
			$files = $request->file('publi');
			foreach($files as $file){
				if($file != NULL){
					$nameFile = $ch->cliente_id.'_'.uniqid().'.'.$file->getClientOriginalExtension();
					$file->move(CH::PUBLI_DIR, $nameFile);  
				}
			}
		}

		$ch->save();

		return redirect('config/ch?save=1');
	}
	
	
	/**
	 * Devuelve la configuración de la cartelera de homenajes cuando la 
	 * consulta se hace a través de la API. Se espera que esta consulta 
	 * la haga el fornt-end de la cartelera para establecer su propia 
	 * configuración.
	 */
	public function api_config(){
		$cliente_id = 1;
		$configCH = CH::cliente($cliente_id)->first();
		// PUBLICIDADES
		$files = File::allFiles(CH::PUBLI_DIR);
		// $publi = array(); 
		
		$publis = array();
		foreach ($files as $file) 
		{
			$publis[] = str_replace('\\','/',(string)$file);
		}

		return response()->json(["configCH"=>$configCH, "publis" => $publis]);
	}
	
	
	/**
	 * Sincroniza los servicios del back-end con el sitio homenaje.de 
	 * 
	 * NOTA: Esta función califica para ser convertida en un Service Provider 
	 * debido a su tamaño y complejidad.
	 */
	public function sync() {
		
		$getServicios = Curl::to('http://homenaje.de/wp-content/plugins/neo_homenajes/json/conector.php')
						->withData( array( 'listar' => '' ) )->post();
		$servicios = json_decode($getServicios);

		$getHomenajes = Curl::to('http://homenaje.de/wp-content/plugins/neo_homenajes/json/conector.php')
						->withData( array( 'homenaje' => '' ) )->post();
		$homenajes = json_decode($getHomenajes);

		$getCementerios = Curl::to('http://homenaje.de/wp-content/plugins/neo_homenajes/json/conector.php')
						->withData( array( 'cementerios' => '' ) )->post();
		$cementerios = json_decode($getCementerios);
		
		$getResponsos = Curl::to('http://homenaje.de/wp-content/plugins/neo_homenajes/json/conector.php')
						->withData( array( 'responsos' => '' ) )->post();
		$responsos = json_decode($getResponsos);
	  
		foreach ($cementerios->resultado as $key => $val) {
			$getCementerio = Destino::where('id_online',$val->id )->get();
			if (count($getCementerio) == 0) {
				$getCementerio_set = new Destino();
				$getCementerio_set->id_online           =$val->id;
				$getCementerio_set->nombre              =$val->nombre;
				$getCementerio_set->direccion           =$val->direccion;
				//$getCementerio_set->imagen              =$val->imagen;
			  	//$getCementerio_set->short_url           =$val->url_maps;
				$getCementerio_set->save();
			}
		}
		
		foreach ($responsos->resultado as $key => $val) {
			$getResponso = Responso::where('id_online',$val->id )->get();
		
			if (count($getResponso) == 0) {
				$getResponso_set = new Responso();
				$getResponso_set->id_online = $val->id;
				$getResponso_set->nombre = $val->nombre;
				$getResponso_set->direccion	= $val->direccion;
				$getResponso_set->url_maps = $val->url_maps;

				$getResponso_set->save();
			}
		}

		$setCementerios = Destino::where('id_online','0')->get();
		
		foreach ($setCementerios as $key => $val) {
			$cementerio_id_online = 
			 		Curl::to('http://homenaje.de/wp-content/plugins/neo_homenajes/puente/cementerios.php')
					->withData( array( 'nombre' => $val->nombre, 'direccion'=>$val->direccion ,'url_maps'=> $val->short_url,'imagen'=>$val->mapa) )
					->post();
		
			$getResponso_set = Destino::where('id',$val->id)->first();
			$getResponso_set->id_online           =$cementerio_id_online;
			
			$getResponso_set->save();   
		}

		$setResponsos = Responso::where('id_online','0')->get();
		
		foreach ($setResponsos as $key => $val) {
			 $responso_id_online = 
			 		Curl::to('http://homenaje.de/wp-content/plugins/neo_homenajes/puente/responso.php')
					->withData( array( 'nombre' => $val->nombre, 'direccion'=>$val->direccion, 'url_maps'=>$val->url_maps) )
					->post();
		
			$getResponso_set = Responso::where('id',$val->id)->first();
			$getResponso_set->id_online           =$responso_id_online;
			
			$getResponso_set->save();
		}  
		// dd($cementerios->resultado);
		foreach ($servicios->resultado as $key => $value) {
			$getServicio = Servicio::where('id_online', $value->id)->get();
			
			if (count($getServicio) > 0) {
				$getServicioPorFechas = 
						Servicio::where('id_online', $value->id)
						->where('fecha_nac', $value->fecha_nacimiento)
						->where('fecha_fac', $value->fecha_fallecido)
						->first();
				
				if (count($getServicioPorFechas) > 0) {
					$fecha_servicio_local = strtotime($getServicioPorFechas->fecha_update);
					$fecha_servicio_web = strtotime(date('Y/m/d H:s:i'));
					
					if ($fecha_servicio_web >  $fecha_servicio_local) {
						$split = explode('.', $value->fotografia);
						$ext = array_pop($split);
						
						$getImagenes = Curl::to($value->fotografia)
									->withContentType('image/'.$ext)
									->download(public_path().'/images/servicios/'. $getServicioPorFechas->id .'.'.$ext);
						
						$name = 'images/servicios/' . $getServicioPorFechas->id . '.' . $ext;
						$cv = CV::cliente(1)->first();
						
						if ($cv->imagen4=='QR1' || $cv->imagen3=='QR1' || $cv->imagen2=='QR1' || $cv->imagen1=='QR1'){
							$getServicioPorFechas->qrcode1 = $getServicioPorFechas->id.'_'.uniqid(). ".png";
							$getServicioPorFechas->qrcode2 = $getServicioPorFechas->id.'_'.uniqid(). ".png";
							$getServicioPorFechas->url1 = $value->url;
							$getServicioPorFechas->url2 = $value->url_salon;
							QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($getServicioPorFechas->url1, CV::QRCODES_DIR . '/' . $getServicioPorFechas->qrcode1);
							QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($getServicioPorFechas->url2, CV::QRCODES_DIR . '/' . $getServicioPorFechas->qrcode2);    
						}elseif($cv->imagen4=='QR2' || $cv->imagen3=='QR2' || $cv->imagen2=='QR2' || $cv->imagen1=='QR2'){
							$getServicioPorFechas->qrcode1 = $getServicioPorFechas->id.'_'.uniqid(). ".png";
							$getServicioPorFechas->qrcode2 = $getServicioPorFechas->id.'_'.uniqid(). ".png";
							$getServicioPorFechas->url1 = $value->url;
							$getServicioPorFechas->url2 = $value->url_salon;
							QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($getServicioPorFechas->url1, CV::QRCODES_DIR . '/' . $getServicioPorFechas->qrcode1);
							QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($getServicioPorFechas->url2, CV::QRCODES_DIR . '/' . $getServicioPorFechas->qrcode2);    
						}
						
						$getServicioPorFechas->save();
						
						foreach ($value->velatorios as $key => $vel) {
						   $getVelatorio                = Velatorio::where('id_online', $vel->homenaje)->first();
						   $getVelatorio->id_online     = $vel->homenaje;
						   $getVelatorio->servicio_id   = $getServicioPorFechas->id;
						   $getVelatorio->desde         = $vel->inicio;
						   $getVelatorio->hasta         = $vel->fin;
						   $getVelatorio->fecha         = $vel->fecha;
						
						   $getVelatorio->save();
						}
					}
				 }
			}else{
				$servicio                       = new Servicio();
				$servicio->id_online            = $value->id;
				$servicio->nombres              = $value->nombre_fallecido;
				$servicio->apellidos            = $value->apellido_fallecido;
				$servicio->fecha_nac            = $value->fecha_nacimiento;
				$servicio->fecha_fac            = $value->fecha_fallecido;
				$servicio->servicio_cementerio  = $value->servicio_cementerio;
				$servicio->cliente_id           = $value->admin;
				$sala_id_online                 = Sala::where('id_online', $value->salon)->first();
				$servicio->sala_id              = $sala_id_online->id;
				$servicio->fecha_update = date('Y/m/d H:s:i');

				$cv = CV::cliente(1)->first();
				
				if ($cv->imagen4=='QR1' || $cv->imagen3=='QR1' || $cv->imagen2=='QR1' || $cv->imagen1=='QR1'){
					$servicio->qrcode1 = $value->id.'_'.uniqid(). ".png";
					$servicio->qrcode2 = $value->id.'_'.uniqid(). ".png";
					$servicio->url1 = $value->url;
					$servicio->url2 = $value->url_salon;
					
					//GENERAR CODIGO QR
					QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($servicio->url1, CV::QRCODES_DIR . '/' . $servicio->qrcode1);
					QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($servicio->url2, CV::QRCODES_DIR . '/' . $servicio->qrcode2);    
				}elseif ($cv->imagen4=='QR2' || $cv->imagen3=='QR2' || $cv->imagen2=='QR2' || $cv->imagen1=='QR2'){
					$servicio->qrcode1 = $value->id.'_'.uniqid(). ".png";
					$servicio->qrcode2 = $value->id.'_'.uniqid(). ".png";
					$servicio->url1 = $value->url;
					$servicio->url2 = $value->url_salon;
					
					//GENERAR CODIGO QR
					QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($servicio->url1, CV::QRCODES_DIR . '/' . $servicio->qrcode1);
					QrCode::encoding('UTF-8')->format('png')->margin(1)->size(400)->generate($servicio->url2, CV::QRCODES_DIR . '/' . $servicio->qrcode2);    
				}

				$servicio->save();
				$split = explode('.', $value->fotografia);
				$ext = array_pop($split);                
				$getImagenes = 
						Curl::to($value->fotografia)
						->withContentType('image/'.$ext)
						->download(public_path().'/images/servicios/'. $servicio->id .'.'.$ext);
				
				$name = 'images/servicios/' . $servicio->id . '.' . $ext;                
				$servicio->foto = $name;
				$servicio->save();

				foreach ($value->velatorios as $key => $vel) {
				   $velatorio = new Velatorio();
				   $velatorio->id_online = $vel->homenaje;
				   $velatorio->servicio_id = $servicio->id;
				   $velatorio->desde = $vel->inicio;
				   $velatorio->hasta = $vel->fin;
				   $velatorio->fecha = $vel->fecha;
				   
				   $velatorio->save();
				}
			}
		}

		foreach ($homenajes->resultado as $key => $value) {
			$getHomenaje = Homenaje::where('id_online', $value->id)->first();
			
			if (count($getHomenaje) > 0) {
				$fecha_homenaje_local = strtotime($getHomenaje->updated_at);
				$fecha_homenaje_web = strtotime(date('Y/m/d H:i:s'));
			
				if ($fecha_homenaje_web > $fecha_homenaje_local) {
					$getHomenaje->id_online = $value->id;
					$getHomenaje->nombre = $value->nombre;
					$getHomenaje->mensaje = $value->texto_homenaje;
					$getHomenaje->created_at = $value->publicacion;
					$getHomenaje->updated_at = date('Y/m/d H:i:s');
					
					$getHomenaje->save();
					
					if($value->foto_homenaje == null){
						$getHomenaje->foto = '';
					}else{
						$split = explode('.', $value->foto_homenaje);
						$ext = array_pop($split);                
						$getImagenes = 
								Curl::to($value->foto_homenaje)
								->withContentType('image/'.$ext)
								->download(public_path().'/images/homenaje/'. $getHomenaje->id .'.'.$ext);
						
						$name = $getHomenaje->id . '.' . $ext;       
						$getHomenaje->foto=null;
						$getHomenaje->foto = $name;
					}
					
					$getServicio_ = Servicio::where('id_online', $value->id_servicio)->get()->first();
					$getHomenaje->servicio_id = $getHomenaje->servicio_id;
					$getHomenaje->save();
				}
			}else{
				$homenaje = new Homenaje();
				$homenaje->id_online = $value->id;
				
				$homenaje->nombre = $value->nombre;
				$homenaje->mensaje = $value->texto_homenaje;
				
				$homenaje->created_at = $value->publicacion;
				$homenaje->updated_at = date('Y/m/d H:i:s');
				$homenaje->save();

				if($value->foto_homenaje == null){
					$homenaje->foto = '';
				}else{
					$split = explode('.', $value->foto_homenaje);
					$ext = array_pop($split);                
					$getImagenes = Curl::to($value->foto_homenaje)
								->withContentType('image/'.$ext)
								->download(public_path().'/images/homenaje/'. $homenaje->id .'.'.$ext);
					
					$name = $homenaje->id . '.' . $ext;     
					//$getImagenes = Curl::to($value->foto_homenaje)->post();
					//$split = explode('.', $value->foto_homenaje);
					//$ext = array_pop($split);
					//$name = 'homenajes/' . $homenaje->id . '.' . $ext;
					//Storage::put($name, $getImagenes);

					$homenaje->foto = $name;

					//$name = Homenaje::descargarImagen($value->foto_homenaje, $homenaje->id);
					//$homenaje->foto = $name;
				}
				
				$getServicio_ = Servicio::where('id_online', $value->id_servicio)->first();
				$homenaje->servicio_id = $homenaje->servicio_id;
				$homenaje->save();
			}
		}
		
		$servicio_insert =
				Servicio::where('id_online','=','0')
				->with('velatorios')
				->with('salas')
				->get();
				
		$files = Servicio::FOTO_URL;
		
		foreach ($servicio_insert as $key => $value) {
			$sala_online=sala::where('id','=',$value->destino_id)->first();
			$nombre_fallecido=$value->nombres;
			$apellido_fallecido=$value->apellidos;
			$genero='';
			$fecha_nacimiento=$value->fecha_nac;
			$fecha_fallecido=$value->fecha_fac;
			$salida_servicio=date($value->salida);
			$hora_s_servicio=strtotime($value->salida);
			$servicio_cementerio=date($value->servicio_cementerio);
			$hora_s_cementerio=strtotime($value->servicio_cementerio);
			$velatorio_fecha=date($value->fecha_inico);
			$hora_v_inicio=strtotime($value->fecha_inico);
			$fotografia=0;
			$cementerio=$value->destino_id;
			$salon=$value->id_online;
			$religion=$value->religion_id;
			$texto_homenaje='';
			$banner=0;
			$correo_familiar='';
			$clave_familiar='';
			$admin=$value->cliente_id;
			$modificado=date('Y/m/d H:i:s');
			$tipo='';
			$i=0;
			$velatorio=$value->velatorios;
			 
			$getServicios = 
					Curl::to('http://homenaje.de/wp-content/plugins/neo_homenajes/puente/servicio.php')
					->withData( array('nombre_fallecido'=> $nombre_fallecido,
									'apellido_fallecido'=>  $apellido_fallecido,
									'genero'=>  $genero,
									'fecha_nacimiento'=>  $fecha_nacimiento,
									'fecha_fallecido'=> $fecha_fallecido,
									'salida_servicio'=>$salida_servicio,
									'hora_s_servicio'=>$hora_s_servicio,
									'servicio_cementerio'=>$servicio_cementerio,
									'hora_s_cementerio'=>$hora_s_cementerio,
									'velatorio_fecha'=>$velatorio_fecha,
									'hora_v_inicio'=>$hora_v_inicio,
									'fotografia'=>$fotografia,
									'cementerio'=>$cementerio,
									'salon'=>$salon,
									'religion'=>$religion,
									'texto_homenaje'=>$texto_homenaje,
									'banner'=>$banner,
									'correo_familiar'=>$correo_familiar,
									'clave_familiar'=>$clave_familiar,
									'admin'=>$admin,
									'modificado'=>$modificado,
									'tipo'=>$tipo,
									'info_velario_cv'=>$velatorio) )
					->withFile( 'imagen',public_path().'\\' .$value->foto, 'image/jpg', $files.$value->foto )
					->post();

			$servicio_update=Servicio::where('id','=',$value->id)->first();
			$servicio_update->id_online=$getServicios;
			$servicio_update->save();	
		}
		
		$homenaje_insert =Homenaje::where('id_online','=','0')
						->get();
		$files_homenaje = Homenaje::FOTO_DIR;
		foreach ($homenaje_insert as $key => $value) {
			
			$nombre=$value->nombre;
			$correo='';
			$texto_homenaje=$value->mensaje;
			$foto_homenaje=$value->foto;
			$aprobado=1;
			$publicacion=date('d/m/Y');
			$servicio_homenje=Servicio::where('id','=',$value->servicio_id)->first();

			$getHomenaje = 
					Curl::to('http://homenaje.de/wp-content/plugins/neo_homenajes/puente/homenaje.php')
					->withData( array(
									'id_homenaje'=>$servicio_homenje->id_online,
									'nombre'=>  $nombre,
									'correo'=>  $correo,
									'texto_homenaje'=> $texto_homenaje,
									'foto_homenaje'=>$foto_homenaje,
									'aprobado'=>$aprobado,
									'publicacion'=>$publicacion) )
					->withFile( 'imagen',public_path().'\\' .$value->foto, 'image/jpg', $files.$value->foto )
					->post();
			
			print_r($servicio_homenje->id_online);
			
			print_r($getHomenaje);
			$homenaje_update=Homenaje::where('id','=',$value->id)->first();
			$homenaje_update->id_online=$getHomenaje;
			$homenaje_update->save();
		}

		//$getServicio = $servicios->status;
		return redirect('config/ch?save=1');
	}
}
