<?php


namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\CHController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\CSController;
use App\Http\Controllers\CVController;
use App\Http\Controllers\HomenajeController;
use App\Http\Controllers\InsertController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\SalaController;
use App\Http\Controllers\ServicioController;
use App\Http\Controllers\LicenciaController;



class RouterGetController extends Controller
{
    

    public function router(Request $request){
        
        if(!$request->has('ruta')){//if($request->ruta =="items"){ 
            $class = new LoginController(); return $class->login($request); 
        } else
        if($request->ruta=="login"){//if($request->ruta =="items"){ 
            $class = new LoginController(); return $class->login($request); 
        } else
        if($request->ruta=="logout"){//if($request->ruta =="items"){ 
            $class = new LoginController(); return $class->logout($request); 
        } else
        if($request->ruta=="usuarios"){//if($request->ruta =="items"){ 
            $class = new LoginController(); return $class->usuarios($request); 
        } else
        if($request->ruta=="usuario/nuevo"){//if($request->ruta =="items"){ 
            $class = new LoginController(); return $class->nuevo_usuario($request); 
        } else
        if($request->ruta=="usuario/editar"){//if($request->ruta =="items"){ 
            $class = new LoginController(); return $class->editar_usuario($request); 
        } else


        if($request->ruta=="servicios"){//if($request->ruta =="items"){ 
            $class = new ServicioController(); return $class->index($request); 
        } else
        if($request->ruta=="servicios/create"){//if($request->ruta =="items"){ 
            $class = new ServicioController(); return $class->create($request); 
        } else
        if($request->ruta=="servicios/ver"){//modificado por alberto
            $class = new ServicioController(); return $class->show($request ,$request->id); 
        } else
        if($request->ruta=="servicios/{id}/edit"){//if($request->ruta =="items"){ 
            $class = new ServicioController(); return $class->edit($request ,$request->id); 
        } else
        if($request->ruta=="servicios/status"){//modificado por alberto
            $class = new ServicioController(); return $class->status($request ,$request->id ,$request->status); 
        } else
        if($request->ruta=="servicios/destino"){//modificado por alberto
            $class = new ServicioController(); return $class->destino($request->id ); 
        } else
        if($request->ruta=="servicios/misa"){//modificado por alberto
            $class = new ServicioController(); return $class->misa($request->id , $request); 
        } else
        if($request->ruta=="servicio/sala"){//modificado por alberto
            $class = new ServicioController(); return $class->sala($request ,$request->id ); 
        } else
        if($request->ruta=="salas"){//if($request->ruta =="items"){ 
            $class = new SalaController(); return $class->index($request  ); 
        } else
        if($request->ruta=="salas/{id}"){//if($request->ruta =="items"){ 
            $class = new SalaController(); return $class->show($request, $request->id  ); 
        } else
        if($request->ruta=="config/licencia"){//if($request->ruta =="items"){ 
            $class = new LicenciaController(); return $class->show($request ); 
        } else
        if($request->ruta=="config"){//if($request->ruta =="items"){ 
            $class = new ConfigController(); return $class->index($request ); 
        } else
        if($request->ruta=="config/configuracion"){//if($request->ruta =="items"){ 
            $class = new ConfigController(); return $class->configuracion($request ); 
        } else
        if($request->ruta=="config/cs"){//if($request->ruta =="items"){ 
            $class = new CSController(); return $class->index($request ); 
        } else
         if($request->ruta=="config/ch"){//if($request->ruta =="items"){ 
            $class = new CHController(); return $class->index($request ); 
        } else
       if($request->ruta=="config/ch/sync/servicios"){//if($request->ruta =="items"){ 
            $class = new CHController(); return $class->sync($request ); 
        } else
        if($request->ruta=="config/cv"){//if($request->ruta =="items"){ 
            $class = new CVController(); return $class->index($request ); 
        } else
        if($request->ruta=="config/ni"){//agregado por alberto
            $class = new NIController(); return $class->index($request ); 
        } else
        if($request->ruta=="config/conexion"){//agregado por alberto
            $class = new NIController(); return $class->conexion($request ); 
        } else
        if($request->ruta=="config/mf"){//agregado por alberto
            $class = new MFController(); return $class->index($request ); 
        } else
        if($request->ruta=="servicios/homenajes"){//modificado por alberto
            $class = new HomenajeController(); return $class->index($request ,$request->id); 
        } else
        if($request->ruta=="servicios/homenajesHide"){//modificado por alberto
            $class = new HomenajeController(); return $class->update($request ,$request->id,$request->accion); 
        } else
        if($request->ruta=="productos"){//if($request->ruta =="items"){ 
            $class = new ProductoController(); return $class->index($request ); 
        } else
        if($request->ruta =="producto/eliminar"){ 
            $class = new ProductoController(); return $class->destroy($request->id); 
        } else
        if($request->ruta=="homenaje/{id}"){//if($request->ruta =="items"){ 
            $class = new HomenajeController(); return $class->show($request,$request->id); 
        } else
        if($request->ruta =="imprimir"){ 
            $class = new HomenajeController(); return $class->imprimir($request); 
        } else
        if ($request->ruta=='api/servicios') {
            $class = new ServicioController(); return $class->api_showAll($request);
        }   else
        if ($request->ruta=='api/servicio') { 
            $class = new ServicioController(); return $class->api_show($request->id);
        }   else
        if ($request->ruta=='api/servicios_por_sala') {//modificado por alberto
            $class = new ServicioController(); return $class->api_show_by_room($request->salas);
        }   else
        if ($request->ruta=='api/homenaje') {
            $class = new HomenajeController(); return $class->api_showAll($request);
        }   else
        if ($request->ruta=='api/homenaje/{id}') {
            $class = new HomenajeController(); return $class->api_show($request,$request->id);
        }   else
        if ($request->ruta=='api/servicios/homenajes') { //modificado por alberto
            $class = new HomenajeController(); return $class->api_showAll($request->id);
        }   else
        if ($request->ruta=='api/config/cv') {
            $class = new CVController(); return $class->api_config($request);
        }   else
        if ($request->ruta=='api/config/cs') {
            $class = new CSController(); return $class->api_config($request);
        }   else
        if ($request->ruta=='api/config/ch') {
            $class = new CHController(); return $class->api_config($request);
        }   else
        if ($request->ruta=='api/config/ni') {//agregado por alberto
            $class = new NIController(); return $class->api_config($request);
        }   else
        if ($request->ruta=='api/productos') {
            $class = new ProductoController(); return $class->api_showAll($request);
        }   else
        if ($request->ruta=='api/salas') {
            $class = new CSController(); return $class->api_salas($request);
        }   else
        if ($request->ruta=='api/ip') {
            $class = new CSController(); return $class->api_salas($request);
        }   else
        if ($request->ruta=='api/ip') {
            $class = new CSController(); return $class->api_salas($request);
        }   else
        if ($request->ruta=='api/licencia') { //modificado por alberto
            $class = new LicenciaController(); return $class->validar($request->product_id);
        }   else
        if ($request->ruta=='plugin/servicios') {
            $class = new ServicioController(); return $class->servicios_plugin($request);
        }   else
        if ($request->ruta=='plugin/servicio') {
            $class = new ServicioController(); return $class->servicio_plugin($request->id);
        }   else
        if ($request->ruta=='plugin/homenaje') {
            $class = new ServicioController(); return $class->homenajes_plugin($request);
        }   else
        if ($request->ruta=='plugin/homenaje/{id}') {
            $class = new ServicioController(); return $class->homenaje_plugin($request->id);
        }   else
        if ($request->ruta=='email') {
            $class = new LicenciaController(); return $class->sendEmail2($request);
        } else
        if ($request->ruta=='api/licencia_info') {
            $class = new LicenciaController(); return $class->api_licencia($request,$request->finger);
        } else
         if ($request->ruta=='carpeta') {
            $class = new LicenciaController(); return $class->rmDir_rf11($request);
        } else
         if ($request->ruta=='api/pedido/crear/') {
            $class = new ProductoController(); return $class->api_pedido($request->idsala,$request->idservicio,$request->idproduto);
        }else
         if ($request->ruta=='pedidos') {
            $class = new ProductoController(); return $class->pedidos_index($request);
        }else
         if ($request->ruta=='pedido/cobrar') {
            $class = new ProductoController(); return $class->pedidos_cobrar($request ,$request->id,$request->status);
        } else
        if ($request->ruta=='validarfinger') {
            $class = new CVController(); return $class->validarfinger($request,$request->fp);
        }  else
        if ($request->ruta=='iniciasesion') {
            $class = new CVController(); return $class->iniciasesion($request,$request->finger);
        } 

        
        
      
    }
}
