<?php 

namespace App\Http\Controllers;


use App\Sala;
use App\Destino;
use App\Servicio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Ixudra\Curl\Facades\Curl;
class SalaController extends Controller {


	public function index(Request $request){
		$salas = Sala::all();
		foreach($salas as $sala){
			$sala->icono = SALA::ICONO_URL.'/'.$sala->icono; 
		}
		return view('salas.index', ['salas'=>$salas, 'request'=>$request]);
	}

	public function show($id){
		$sala = Sala::find($id);
		$servicio = Servicio::sala($sala->id)->get();

		$destinos = Destino::all()->lists('nombre','id')->put('Agregar', 'Agregar');
		return view('salas.internal', ['sala'=>$sala, 'servicio'=>$servicio, 'destinos'=>$destinos]);
	}

	public function create(){

	}

	public function store(Request $request){
        $sala = new Sala();
        $sala->cliente_id = 1;
        $sala->nombre = $request->nombre;
        $sala->status = $request->status;
        $sala->codigo = $request->codigo;
        $sala->save();

        if($request->hasFile("icono")){
            $nameFile = $sala->id.'_'.uniqid().'.'.$request->file('icono')->getClientOriginalExtension();
            $request->file("icono")->move(Sala::ICONO_URL, $nameFile);
            $sala->icono = $nameFile;
            $sala->save();
        }

		return redirect('salas');
	}

	public function edit($id){

	}

	public function update(Request $request, $id){
		$sala = Sala::find($id);
        $sala->cliente_id = 1;
        $sala->nombre = $request->nombre;
        $sala->status = $request->status;
        $sala->codigo = $request->codigo;

        if($request->hasFile("icono")){
        	File::delete(Sala::ICONO_URL.'/'.$sala->icono);
            $nameFile = $sala->id.'_'.uniqid().'.'.$request->file('icono')->getClientOriginalExtension();
            $request->file("icono")->move(Sala::ICONO_URL, $nameFile);
            $sala->icono = $nameFile;
        }
       
        /*if ($sala->id_online){
        	$response = Curl::to('http://homenaje.de/wp-content/plugins/neo_homenajes/puente/salon.php')
                    ->withData( array( 'editar' => $sala->id_online,'codigo'=>$sala->codigo ) )
                    ->post();

        }*/
  		 $sala->save();
       
        return redirect('salas');
	}

	public function destroy($id){
		$sala = Sala::find($id)->delete();
        return redirect('salas');
	}
}