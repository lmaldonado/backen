<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CV;
use App\Responso;
use App\Empresa;
use App\Sala;
use App\Destino;
use App\Religion;


use App\Http\Requests;
use App\Http\Controllers\Controller;

class ResponsoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $responso= new Responso;
        $responso->nombre=$request->nombre_responso_misa;
        $responso->direccion=$request->direccion_responso_misa;
        //dd($request->all());
        $responso->save();
        
        $salas = Sala::all()->lists('nombre','id')->put('0','En domicilio')->put('99','En privado')->put('100','A confirmar');
		$religions = Religion::all()->lists('nombre','id');
		$empresas = Empresa::all()->lists('nombre', 'id')->put('Agregar', 'Agregar');
		$destinos = Destino::all()->lists('nombre','id')->put('Agregar', 'Agregar');
		$responso = Responso::all()->lists('nombre','id')->put('Agregar', 'Agregar');
		$configCV = CV::cliente(1)->first();

		
		return view('servicios.create', ['salas'=>$salas, 'destinos'=>$destinos, 'empresas'=>$empresas,
            'religiones'=>$religions,'responso'=>$responso,'CV'=>$configCV]);
    }
    
    
    public function api_store(Request $request)
    {
        $responso = new Responso;
        
        $responso->nombre = $request->nombre;
        $responso->direccion = $request->direccion;
        
        $responso->save();
        
        return $responso;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
