<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CV;
use App\Empresa;
use App\Sala;
use App\Destino;
use App\Religion;
use App\Responso;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $empresa= new Empresa;
        $empresa->nombre=$request->nombre_empresa;
        $empresa->save();
        
        $salas = Sala::all()->lists('nombre','id')->put('0','En domicilio')->put('99','En privado')->put('100','A confirmar');
		$religions = Religion::all()->lists('nombre','id');
		$empresas = Empresa::all()->lists('nombre', 'id')->put('Agregar', 'Agregar');
		$destinos = Destino::all()->lists('nombre','id')->put('Agregar', 'Agregar');
		$responso = Responso::all()->lists('nombre','id')->put('Agregar', 'Agregar');
		$configCV = CV::cliente(1)->first();

		
		return view('servicios.create', ['salas'=>$salas, 'destinos'=>$destinos, 'empresas'=>$empresas,
            'religiones'=>$religions,'responso'=>$responso,'CV'=>$configCV]);
    }
    
    
    /**
     * Store resource "empresa" through API
     */
    public function api_store(Request $request)
    {
        $empresa = new Empresa;
        
        $empresa->nombre = $request->empresa;
        
        $empresa->save();
        
        return $empresa;
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $empresa = Empresa::find($id);
        $empresa->delete();

        return response()->json(['mensaje'=>'Empresa eliminada']);
    }

    public function cambio(Request $request,$id)
    {
        // dd($request->all());
        $empresa = Empresa::find($id);
        $empresa->nombre = $request->nombre;
        $empresa->save();

        // $empresa = Empresa::findOrFail($request->id_empresa);
    	// $empresa->fill($request->all());    

    	// $empresa->save();
        

        return response()->json(['mensaje'=>'Registro actualizado']);
    }

     public function api_index()
    {
        $empresa->logo = Empresa::LOGOS_DIR.'/logo.png'; 
        $empresa->logo_pie = Empresa::LOGOS_DIR.'/logo_pie.png'; 

        return (['empresa'=>$empresa]);
    }
}
