<?php 

namespace App\Http\Controllers;


use App\Homenaje;
use App\CH;
use App\CV;
use App\NI;
use App\Sala;
use App\Servicio;
use App\Palabras;
use App\Destino;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Carbon\Carbon;
use Ixudra\Curl\Facades\Curl;


class HomenajeController extends Controller {

	public function index(Request $request, $id){

		$servicio = Servicio::find($id);
		$homenajes = Homenaje::toServicio($id)->get();
		// dd($id);
		return view('homenajes.index',
			[
				"servicio" => $servicio,
				"homenajes" => $homenajes
			]);
	}

	public function create(){
	}

	public function show($id){
		$models = Homenaje::where("servicios_id",$id)->get();
		return view('homenajes.index',
			[
				"homenaje" => $models
			]);
	}

	public function store(Request $request, $id){

		$ch = CH::where('cliente_id',1)->first();
		$cv = CV::where('cliente_id',1)->first();
		if ($ch->tipo_servidor=='web' && !empty($ch->servidor_web)) {
			$url = $ch->servidor_web;
		}elseif ($cv->tipo_servidor=='web' && !empty($cv->servidor_web)) {
			$url = $cv->servidor_web;
		}

		if($request->homenaje_id != ""){
			$homenaje = Homenaje::find($request->homenaje_id);
			// dd('Paso por aqui',$request->mensaje,$id);
		}else{
			$homenaje = new Homenaje();
		}
		// dd($homenaje);

		$homenaje->servicio_id = $id;
		$homenaje->nombre = $request->nombre;

		$prohibido = Palabras::all();
		// dd($prohibido);
		$msj_limpio = array();
		$msj = explode(' ', $request->mensaje);
		$cont = 0;
		foreach ($msj as $value) {
			$i=0;
			foreach ($prohibido as $val) {
				if($value == $val->palabra || $value == $val->palabra.',' || $value == $val->palabra.'.' || $value == $val->palabra.':' || $value == $val->palabra.';'){
					$i++;
					$cont++;
				}
			}
			if($i == 0){
				array_push($msj_limpio,$value);
			}
		}
		$msj_comp = '';
		foreach ($msj_limpio as $key => $value) {
			$msj_comp=$msj_comp.''.$value.' ';
		}
		// dd($cont,$msj_comp,$msj,$msj_limpio,$prohibido);

		if($cont < $ch->num_palabras){
			$homenaje->mensaje = $msj_comp;
			$homenaje->status = 1;

			$datetime = $datetime = date('Y-m-d h:m:s');
			$homenaje->created_at = $datetime;

			$homenaje->save();

			if($request->hasFile('foto')) {
				$nameFile = $homenaje->servicio_id.'-'.uniqid().'.'.$request->file('foto')->getClientOriginalExtension();
				$request->file('foto')->move(Homenaje::FOTO_DIR, $nameFile);

				$homenaje->foto = $nameFile;
				$homenaje->save();
			}

			$files_homenaje = Homenaje::FOTO_DIR;
			$servicio_homenje=Servicio::find($homenaje->servicio_id);
			// dd(count($homenaje_insert));
			if(!empty($url)){
				if(isset($servicio_homenje->id_online) && $servicio_homenje->id_online!=0){
					if($request->homenaje_id == ""){
						if($homenaje->foto != ''){				
							$nombre=$homenaje->nombre;
							$correo='';
							$texto_homenaje=$homenaje->mensaje;
							$foto_homenaje=$homenaje->foto;
							$aprobado=1;
							$publicacion=date('d/m/Y');
							$servicio_homenje=Servicio::find($homenaje->servicio_id);
							$getHomenaje = 
									Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php')
									->withData( array(
													'id_homenaje'=>$servicio_homenje->id_online,
													'nombre'=>  $nombre,
													'correo'=>  $correo,
													'texto_homenaje'=> $texto_homenaje,
													'foto_homenaje'=>$foto_homenaje,
													'aprobado'=>$aprobado,
													'publicacion'=>$publicacion) )
									->withFile( 'imagen',public_path().'/' .$homenaje->foto, 'image/jpg', $files_homenaje.$homenaje->foto )
									->post();
							// $homenaje=Homenaje::where('id','=',$value->id)->first();
							$homenaje->id_online=$getHomenaje;
							$homenaje->save();							
						}else{
							$nombre=$homenaje->nombre;
							$correo='';
							$texto_homenaje=$homenaje->mensaje;
							$foto_homenaje='homenaje_gene.jpg';
							$aprobado=1;
							$publicacion=date('d/m/Y');
							$servicio_homenje=Servicio::find($homenaje->servicio_id);
							$getHomenaje = 
									Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php')
									->withData( array(
													'id_homenaje'=>$servicio_homenje->id_online,
													'nombre'=>  $nombre,
													'correo'=>  $correo,
													'texto_homenaje'=> $texto_homenaje,
													'foto_homenaje'=>$foto_homenaje,
													'aprobado'=>$aprobado,
													'publicacion'=>$publicacion) )
									->withFile( 'imagen',public_path().'/' .$foto_homenaje, 'image/jpg', $files_homenaje.$foto_homenaje )
									->post();					
							// $homenaje=Homenaje::where('id','=',$value->id)->first();
							$homenaje->id_online=$getHomenaje;
							$homenaje->save();
						}
					}else{
						if($homenaje->foto != ''){	
							// $getHomenaje = Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php?eliminar')
							// 	->withData( array('eliminar'=>$homenaje->id_online) )						
							// 	->post();
							// if(isset($getHomenaje)){
								$nombre=$homenaje->nombre;
								$correo='';
								$texto_homenaje=$homenaje->mensaje;
								$foto_homenaje=$homenaje->foto;
								$aprobado=1;
								$publicacion=date('d/m/Y');
								$servicio_homenje=Servicio::find($homenaje->servicio_id);

								$getHomenaje = 
										Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php?editar')
										->withData( array(
														'id'=>$homenaje->id_online,
														'nombre'=>  $nombre,
														'imagen'=>$foto_homenaje,
														'texto_homenaje'=> $texto_homenaje) )
										->withFile( 'imagen',public_path().'/' .$homenaje->foto, 'image/jpg', $files_homenaje.$homenaje->foto )
										->post();
								// $homenaje->id_online=$getHomenaje;
								// $homenaje->save();
							// }
								// dd($getHomenaje);											
						}else{
							// $getHomenaje = Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php?eliminar')
							// 	->withData( array('eliminar'=>$homenaje->id_online) )						
							// 	->post();
							// if(isset($getHomenaje)){
								$nombre=$homenaje->nombre;
								$correo='';
								$texto_homenaje=$homenaje->mensaje;
								$foto_homenaje='homenaje_gene.jpg';
								$aprobado=1;
								$publicacion=date('d/m/Y');
								$servicio_homenje=Servicio::find($homenaje->servicio_id);
								$getHomenaje = 
										Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php?editar')
										->withData( array(
														'id'=>$homenaje->id_online,
														'nombre'=>  $nombre,
														'imagen'=>$foto_homenaje,
														'texto_homenaje'=> $texto_homenaje) )
										->post();
							// 	$homenaje->id_online=$getHomenaje;
							// 	$homenaje->save();
							// }	
								// dd($getHomenaje);							
						}
					}
				}					
			}
			return redirect("/?ruta=servicios/homenajes&id=".$id);
		}else{
			if($ch->tipo_bloqueo == 0){
				$homenaje->mensaje = $msj_comp;
				$homenaje->status = 0;

				$datetime = $datetime = date('Y-m-d h:m:s');
				$homenaje->created_at = $datetime;

				$homenaje->save();

				if($request->hasFile('foto')) {
					$nameFile = $homenaje->servicio_id.'-'.uniqid().'.'.$request->file('foto')->getClientOriginalExtension();
					$request->file('foto')->move(Homenaje::FOTO_DIR, $nameFile);

					$homenaje->foto = $nameFile;
					$homenaje->save();
				}

				$files_homenaje = Homenaje::FOTO_DIR;
				$servicio_homenje=Servicio::find($homenaje->servicio_id);
				// dd(count($homenaje_insert));
				if(!empty($url)){
					if(isset($servicio_homenje->id_online) && $servicio_homenje->id_online!=0){
						if($request->homenaje_id == ""){
							if($homenaje->foto != ''){				
								$nombre=$homenaje->nombre;
								$correo='';
								$texto_homenaje=$homenaje->mensaje;
								$foto_homenaje=$homenaje->foto;
								$aprobado=0;
								$publicacion=date('d/m/Y');
								$servicio_homenje=Servicio::find($homenaje->servicio_id);
								$getHomenaje = 
										Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php')
										->withData( array(
														'id_homenaje'=>$servicio_homenje->id_online,
														'nombre'=>  $nombre,
														'correo'=>  $correo,
														'texto_homenaje'=> $texto_homenaje,
														'foto_homenaje'=>$foto_homenaje,
														'aprobado'=>$aprobado,
														'publicacion'=>$publicacion) )
										->withFile( 'imagen',public_path().'/' .$homenaje->foto, 'image/jpg', $files_homenaje.$homenaje->foto )
										->post();
								// $homenaje=Homenaje::where('id','=',$value->id)->first();
								$homenaje->id_online=$getHomenaje;
								$homenaje->save();							
							}else{
								$nombre=$homenaje->nombre;
								$correo='';
								$texto_homenaje=$homenaje->mensaje;
								$foto_homenaje='homenaje_gene.jpg';
								$aprobado=0;
								$publicacion=date('d/m/Y');
								$servicio_homenje=Servicio::find($homenaje->servicio_id);
								$getHomenaje = 
										Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php')
										->withData( array(
														'id_homenaje'=>$servicio_homenje->id_online,
														'nombre'=>  $nombre,
														'correo'=>  $correo,
														'texto_homenaje'=> $texto_homenaje,
														'foto_homenaje'=>$foto_homenaje,
														'aprobado'=>$aprobado,
														'publicacion'=>$publicacion) )
										->withFile( 'imagen',public_path().'/' .$foto_homenaje, 'image/jpg', $files_homenaje.$foto_homenaje )
										->post();					
								// $homenaje=Homenaje::where('id','=',$value->id)->first();
								$homenaje->id_online=$getHomenaje;
								$homenaje->save();
							}
						}else{
							if($homenaje->foto != ''){	
								// $getHomenaje = Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php?eliminar')
								// 	->withData( array('eliminar'=>$homenaje->id_online) )						
								// 	->post();
								// if(isset($getHomenaje)){
									$nombre=$homenaje->nombre;
									$correo='';
									$texto_homenaje=$homenaje->mensaje;
									$foto_homenaje=$homenaje->foto;
									$aprobado=0;
									$publicacion=date('d/m/Y');
									$servicio_homenje=Servicio::find($homenaje->servicio_id);

									$getHomenaje = 
											Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php?editar')
											->withData( array(
															'id'=>$homenaje->id_online,
															'nombre'=>  $nombre,
															'imagen'=>$foto_homenaje,
															'texto_homenaje'=> $texto_homenaje) )
											->withFile( 'imagen',public_path().'/' .$homenaje->foto, 'image/jpg', $files_homenaje.$homenaje->foto )
											->post();
									// $homenaje->id_online=$getHomenaje;
									// $homenaje->save();
								// }
									// dd($getHomenaje);											
							}else{
								// $getHomenaje = Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php?eliminar')
								// 	->withData( array('eliminar'=>$homenaje->id_online) )						
								// 	->post();
								// if(isset($getHomenaje)){
									$nombre=$homenaje->nombre;
									$correo='';
									$texto_homenaje=$homenaje->mensaje;
									$foto_homenaje='homenaje_gene.jpg';
									$aprobado=0;
									$publicacion=date('d/m/Y');
									$servicio_homenje=Servicio::find($homenaje->servicio_id);
									$getHomenaje = 
											Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php?editar')
											->withData( array(
															'id'=>$homenaje->id_online,
															'nombre'=>  $nombre,
															'imagen'=>$foto_homenaje,
															'texto_homenaje'=> $texto_homenaje) )
											->post();
								// 	$homenaje->id_online=$getHomenaje;
								// 	$homenaje->save();
								// }	
									// dd($getHomenaje);							
							}
						}
					}					
				}
				return redirect("/?ruta=servicios/homenajes&id=".$id);
			}else{
				return redirect("/?ruta=servicios/homenajes&id=".$id);
			}
			// return redirect("/?ruta=servicios/homenajes&id=".$id)->with('error', 'El mensaje contiene muchas palabras prohibidas, no se pudo guardar.');
		}
	}
	public function imprimir(Request $request){
		$ni = NI::find(1);
		$servicio = Servicio::find($request->id);
		if(isset($request->oculto) && $request->oculto == true){
			$oculto = true;
			$homenajes = Homenaje::where('servicio_id',$request->id)->where('status',0)->get();
		}else{
			$oculto = false;
			$homenajes = Homenaje::where('servicio_id',$request->id)->where('status',1)->get();
		}
		// $homenajes = Homenaje::where('servicio_id',$request->id)->->get();
		// dd($servicio);
		return view('homenajes.imprimir',['homenajes'=>$homenajes,'servicio'=>$servicio,'ni'=>$ni,'oculto'=>$oculto]);
	}
	public function edit($id){
	}

	public function update(Request $request, $id, $accion){
		$homenaje = Homenaje::find($id);
		if ($homenaje!=null && $accion=='ocultar'){
			$datetime = date('0000-00-00 00:00:00');
			$homenaje->deleted_at = $datetime;
			$homenaje->save();
		}elseif($homenaje!=null && $accion=='mostrar'){
			$datetime = $datetime = date('Y-m-d h:m:s');
			$homenaje->deleted_at = $datetime;
			$homenaje->save();
		}
		
		return redirect("/?ruta=servicios/homenajes&id=".$homenaje->servicio_id);
	}
	
	public function destroy(Request $request, $id){
		// dd($request->all());
		$homenaje = Homenaje::find($request->homenaje_id1);
		// $homenaje = Homenaje::find($id)->delete();		
		$true='0';
		$ch = CH::where('cliente_id',1)->first();
		$cv = CV::where('cliente_id',1)->first();
		if ($ch->tipo_servidor=='web' && !empty($ch->servidor_web)) {
			$url = $ch->servidor_web;
		}elseif ($cv->tipo_servidor=='web' && !empty($cv->servidor_web)) {
			$url = $cv->servidor_web;
		}
		if (!empty($url)){
			$getHomenaje = Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php?eliminar')
						->withData( array('eliminar'=>$homenaje->id_online) )						
						->post();

			if(isset($getHomenaje)){
				$homenaje->delete();
				$true='1';

			}
		}else{
			$homenaje->delete();
			$true='1';
		}
		
		// dd($getHomenaje);

		// return $true;
		return redirect("/?ruta=servicios/homenajes&id=".$id);
	}

	function api_show($id){


		$homenaje = Homenaje::with('servicio')->whereId($id)->get();
		return response()->json($homenaje);
	}

	function api_showAll($id){
		//$servicio = Servicio::find($id);
		
		$servicio = Servicio::where('sala_id', $id)
			->whereDate('fecha_inicio', '<=', Carbon::today()->toDateString())
			->whereDate('salida', '>=', Carbon::today()->toDateString())
			->where('status','=','0')
			->orderBy('id','desc')
			->first();
		// dd($servicio);
		if($servicio) 
		{
			$homenajes = Homenaje::with('servicio')->where('servicio_id', $servicio->id)->where('status', 1)->orderBy('id','desc')->get();
			
			return response()->json($homenajes);
		}
		else
		{
			return ([]);
		}
	}

	function api_delete($id){
		$homenaje = Homenaje::find($id);
		// $homenaje = Homenaje::find($id)->delete();

		$ch = CH::where('cliente_id',1)->first();
		$cv = CV::where('cliente_id',1)->first();
		if ($ch->tipo_servidor=='web' && !empty($ch->servidor_web)) {
			$url = $ch->servidor_web;
		}elseif ($cv->tipo_servidor=='web' && !empty($cv->servidor_web)) {
			$url = $cv->servidor_web;
		}
		if (!empty($url)){
			$getHomenaje = Curl::to($url.'wp-content/plugins/neo_homenajes/puente/homenaje.php?eliminar')
						->withData( array('eliminar'=>$homenaje->id_online) )						
						->post();

			if(isset($getHomenaje)){
				$homenaje->delete();
			}
		}else{
			$homenaje->delete();
		}
		
		// dd($getHomenaje);

		return redirect("/?ruta=servicios/homenajes&id=".$id);
	}
}	