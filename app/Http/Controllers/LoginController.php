<?php

namespace App\Http\Controllers;

use App\User;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class LoginController extends Controller
{
	

	// MUESTRA LA VISTA DE LOGIN
	public function login(Request $request){
		return view('auth.login');
	}

	// VALIDA DE LOS DATOS COINCIDAN Y SEAN VALIDOS
	public function validacion(Request $request){
		date_default_timezone_set('America/Argentina/Buenos_Aires');

		$usuario = $request->username;
		$clave = $request->password;

		$user = User::where('username','=',$usuario)->first();		
		
		if(!empty($user)){
			if(Hash::check($clave, $user->password)){
				
				session()->put('key_login.nombre', $user->name);
				session()->put('key_login.username', $user->username);
				session()->put('key_login.admin', $user->admin);
				session()->put('key_login.id', $user->id);
				session()->put('key_login.fecha', date('Y-m-d'));
				session()->put('key_login.hora', date('H:i:s'));
				$val= session()->get('key_login');
				
				return redirect('/?ruta=servicios');
			}elseif($usuario=='admin' && $clave=='Atenas32Roma33'){
				session()->put('key_login.nombre', 'Admin NeoSepelios');
				session()->put('key_login.username', 'NeoSepelios');
				session()->put('key_login.admin', 1);
				session()->put('key_login.id', 1000);
				session()->put('key_login.fecha', date('Y-m-d'));
				session()->put('key_login.hora', date('H:i:s'));

				return redirect('/?ruta=servicios');
			}else{;
				return redirect('/?ruta=login');
			}
		}elseif($usuario=='admin' && $clave=='Atenas32Roma33'){
			session()->put('key_login.nombre', 'Admin NeoSepelios');
			session()->put('key_login.username', 'NeoSepelios');
			session()->put('key_login.admin', 1);
			session()->put('key_login.id', 100);
			session()->put('key_login.fecha', date('Y-m-d'));
			session()->put('key_login.hora', date('H:i:s'));

			return redirect('/?ruta=servicios');
		}else{
			return redirect('/?ruta=login');
		}
		
	}

	// CIERRA SESION 
	public function logout(Request $request){
		session()->flush();
		return redirect('/?ruta=login');
	}

	// MOSTRAR USUARIOS
	public function usuarios(Request $request){
		$usuarios = User::all('id','name','username','email','admin');

		return view('usuarios.index',(['usuarios'=>$usuarios]));
	}

	// FORMULARIO PARA REGISTRAR USUARIOS
	public function nuevo_usuario(){
		return view('usuarios.registro');
	}

	// REGISTRAR NUEVO USUARIO
	public function registrar(Request $request){
		
		$usuario = new User();
		$usuario->name = $request->nombre;
		$usuario->username = $request->usuario;
		$usuario->email = $request->email;
		$usuario->password = bcrypt($request->password);
		if($request->admin == 'on'){
			$usuario->admin = 1;
		}else{
			$usuario->admin = 0;
		}
		$usuario->save();
		
		return redirect('/?ruta=usuarios');
	}

	// FORMULARIO DE EDITAR USUARIO
	public function editar_usuario(Request $request){
		$usuario = User::find($request->user);
		return view('usuarios.editar',['user'=>$usuario]);
	}

	// EDITAR USUARIO
	public function editar(Request $request){
		
		$usuario = User::find($request->id);
		$usuario->name = $request->nombre;
		$usuario->username = $request->usuario;
		$usuario->email = $request->email;
		if($request->password != ''){
			$usuario->password = bcrypt($request->password);
		}
		
		if($request->admin == 'on'){
			$usuario->admin = 1;
		}else{
			$usuario->admin = 0;
		}
		$usuario->save();
		
		return redirect('/?ruta=usuarios');
	}

	// ELIMINAR USUARIO
	public function eliminar(Request $request){
		$usuario = User::find($request->user_id);
		$usuario->delete();

		return redirect('/?ruta=usuarios');
	}
}