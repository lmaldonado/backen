<?php 
namespace App\Http\Controllers;
use App\licencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class LicenciaController extends Controller {

	/**
	 * Si hay una licencia activa en la base de datos muestra todos los detalles
	 * De lo contrario indica que no hay licencias activas y muestra un botón 
	 * para realizar la activación
	 */
	public function show() {
		$licencias = licencia::all();
		// var_dump($licencias);
		return view('configuracion.licencias', compact("licencias"));	
	}


	/**
	 * Elimina la licencia indicada en la variable $id
	 */
	public function delete($id)
	{
		$licencia = Licencia::find($id);

		$licencia->delete();

		return back();
	}


	/**
	 * Almacena la licencia
	 */
	public function store($results)
	{
		# code...
	}

	public function validar($product_id)
	{
		// Si la licencia existe en la base de datos y está activa
		// validarla.
		// De lo contrario indicar si existe y en que estado está.

		$licencia = Licencia::where('product_id', $product_id)->first();

		if($licencia) 
		{
			return json_encode($licencia);
		}
		else 
		{
			return "no_license";
		}
	}



	/**
	 * Verifica primero la existencia de la llave local.
	 * A contimuación verifica la licencia contra el servidor.
	 * Genera un array asociativo con el resultado de la verificación
	 * 
	 */
	function check_license(Request $request){

		$licensekey = $request->licencia;

		$licencia = Licencia::where('licence_key', $licensekey)->first();

		$localkey = $licencia? $licencia->local_key : null;

		// -----------------------------------
		//  -- Configuration Values --
		// -----------------------------------

		// Enter the url to your WHMCS installation here
		$whmcsurl = 'http://clientes.neosepelios.com.ar/';//whmcs/';
		

		// Must match what is specified in the MD5 Hash Verification field
		// of the licensing product that will be used with this check.
		$licensing_secret_key = 'NeoSepelioDB-v00-1.';
		// The number of days to wait between performing remote license checks
		$localkeydays = 15;
		// The number of days to allow failover for after local key expiry
		$allowcheckfaildays = 5;
		// -----------------------------------
		//  -- Do not edit below this line --
		// -----------------------------------

		// Genera un token de verificación ¿...?
		$check_token = time() . md5(mt_rand(1000000000, 9999999999) . $licensekey);
		
		// Genera una fecha de verificación ¿...?
		$checkdate = date("Ymd");
		
		// Determina el dominio del servidor.
		$domain = $_SERVER['SERVER_NAME'];
		
		// Determina la IP del usuario ¿...?
		$usersip = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $_SERVER['LOCAL_ADDR'];
		// NULL
		
		// Determina la URL de este archivo...
		$dirpath = dirname(__FILE__);
		
		// Define una ruta y nombre para un archivo de verificación ¿...?
		$verifyfilepath = 'modules/servers/licensing/verify.php';
		





		/************************************************************************/
		/***    Primero revisamos la validez de la llave local, si existe     ***/
		/************************************************************************/
		
		$localkeyvalid = false;

		if ($localkey) {

			// Limpiamos el string de caracteres indeseados

			$localkey = str_replace("\n", '', $localkey); # Remove the line breaks
			$localdata = substr($localkey, 0, strlen($localkey) - 32); # Extract License Data
			$md5hash = substr($localkey, strlen($localkey) - 32); # Extract MD5 Hash

			if ($md5hash == md5($localdata . $licensing_secret_key)) {


				// M100
				// Aquí reversamos las operaciones que se realizan a la información 
				// de la licencia para generar la llave local. Ver M101
				// Así obtenemos de nuevo dicha información en un array asociativo.

				$localdata = strrev($localdata); # Reverse the string 
				$md5hash = substr($localdata, 0, 32); # Extract MD5 Hash
				$localdata = substr($localdata, 32); # Extract License Data
				$localdata = base64_decode($localdata); # Decode licence
				$localkeyresults = unserialize($localdata); # Extract licence data
				$originalcheckdate = $localkeyresults['checkdate']; # Verify chackdate

				if ($md5hash == md5($originalcheckdate . $licensing_secret_key)) {
					$localexpiry = date("Ymd", mktime(0, 0, 0, date("m"), date("d") - $localkeydays, date("Y")));
					
					if ($originalcheckdate > $localexpiry) {
						$localkeyvalid = true;
						$results = $localkeyresults;
						$validdomains = explode(',', $results['validdomain']);
					
						if (!in_array($_SERVER['SERVER_NAME'], $validdomains)) {
							$localkeyvalid = false;
							$localkeyresults['status'] = "Invalid";
							$results = array();
						}
					
						$validips = explode(',', $results['validip']);
					
						if (!in_array($usersip, $validips)) {
							$localkeyvalid = false;
							$localkeyresults['status'] = "Invalid";
							$results = array();
						}
					
						$validdirs = explode(',', $results['validdirectory']);
					
						if (!in_array($dirpath, $validdirs)) {
							$localkeyvalid = false;
							$localkeyresults['status'] = "Invalid";
							$results = array();
						}
					}
				}
			}
		}

		/***************************************************************************************/
		/*************************** Fin verivifación llave local ******************************/
		/***************************************************************************************/



		/*	Si la llave local EXISTE Y ES VÁLIDA entonces la verificación local 
			genera un array $results con los datos de la licencia y pone en true 
			la variable $localkeyvalid.
			Si la llave local NO EXISTE O ES INVÁLIDA entonces se genera un 
			array $results con un campo status=Invalid y se deja la variable 
			$localkeyvalid en false.
			Entonces consultamos al servidor de WHMCS para verificar si la 
			licencia existe en el servidor y cuál es su estado.
			Al final se genera un array asociativo con el resultado de la 
			verificación.
			*/

		if (!$localkeyvalid) {
			
			$responseCode = 0;

			$postfields = array(
				'licensekey' => $licensekey,
				'domain' => $domain,
				'ip' => $usersip,
				'dir' => $dirpath,
			);
			
			if ($check_token) $postfields['check_token'] = $check_token;
			
			/*	El siguiente foreach() convierte el array $postfileds a formado URL
				como si fueran campos de una petición GET
				*/
			$query_string = '';
			foreach ($postfields AS $k=>$v) {
				$query_string .= $k.'='.urlencode($v).'&';
			}
			
			// A Q U Í   E S   D O N D E   O C U R R E   L A   M A G I A //

			// Si la función curl_exec está disponible (... a partir de PHP 4.0.2)
			
			if (function_exists('curl_exec')) {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $whmcsurl . $verifyfilepath);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $query_string);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$data = curl_exec($ch);
				print_R(curl_errno($ch) . ' - ' . curl_error($ch));

				$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				
				curl_close($ch);
			}

			// Si la función curl_exec no está disponible lo intenta con fsockopen()

			else {
				$responseCodePattern = '/^HTTP\/\d+\.\d+\s+(\d+)/';
				$fp = @fsockopen($whmcsurl, 80, $errno, $errstr, 5);
				if ($fp) {
					$newlinefeed = "\r\n";
					$header = "POST ".$whmcsurl . $verifyfilepath . " HTTP/1.0" . $newlinefeed;
					$header .= "Host: ".$whmcsurl . $newlinefeed;
					$header .= "Content-type: application/x-www-form-urlencoded" . $newlinefeed;
					$header .= "Content-length: ".@strlen($query_string) . $newlinefeed;
					$header .= "Connection: close" . $newlinefeed . $newlinefeed;
					$header .= $query_string;
					$data = $line = '';
					@stream_set_timeout($fp, 20);
					@fputs($fp, $header);
					$status = @socket_get_status($fp);
					while (!@feof($fp)&&$status) {
						$line = @fgets($fp, 1024);
						$patternMatches = array();
						if (!$responseCode
							&& preg_match($responseCodePattern, trim($line), $patternMatches)
						) {
							$responseCode = (empty($patternMatches[1])) ? 0 : $patternMatches[1];
						}
						$data .= $line;
						$status = @socket_get_status($fp);
					}
					@fclose ($fp);
				}
			}

			// Ahora ya tenemos la respuesta del Server de WHMCS.
			// La respuesta suele ser en XML

			// Si el servidor WHMSC responde con error chequeamos
			// la fecha de verificación
			if ($responseCode != 200) {
				$localexpiry = date("Ymd", mktime(0, 0, 0, date("m"), date("d") - ($localkeydays + $allowcheckfaildays), date("Y")));

				// Si la fecha de verificción es mayor de la de expiración
				// se carga la llave local en $results y se continúa...
				if ($originalcheckdate > $localexpiry) {
					$results = $localkeyresults;
				} 
				// De lo contrario se carga en $results status=Invalid y 
				// description=Remote_Check_Failed, y termina el script 
				// devolviendo $results...
				else {
					$results = array();
					$results['status'] = "Invalid";
					$results['description'] = "Remote Check Failed";
					return $results;
				}
			} 
			// Si la respuesta del servidor es 200 cargamos la respuesta 
			// en $results
			else {
				preg_match_all('/<(.*?)>([^<]+)<\/\\1>/i', $data, $matches);
				$results = array();
				foreach ($matches[1] AS $k=>$v) {
					$results[$v] = $matches[2][$k];
				}
			}
			// Aquí chequeamos que la respuesta no esté vacía
			if (!is_array($results)) {
				die("Invalid License Server Response");
			}
			// Verificamos la suma md5 y, si no coincide, 
			// cargamos en $results status=Invalid y 
			// description=MD5_Chechsum_Verification_Failed 
			// y terminamos devolviento $results
			if ($results['md5hash']) {
				if ($results['md5hash'] != md5($licensing_secret_key . $check_token)) {
					$results['status'] = "Invalid";
					$results['description'] = "MD5 Checksum Verification Failed";
					return $results;
				}
			}

			// M101
			// Aquí codificamos el localkey para almacenarlo en la BBDD como 
			// un solo campo.
			// Esto se decodifica cuando se verifica el estado de la licencia 
			// al inicio de este script -- Ver M100
			if ($results['status'] == "Active") {
				$results['checkdate'] = $checkdate;
				$data_encoded = serialize($results);
				$data_encoded = base64_encode($data_encoded);
				$data_encoded = md5($checkdate . $licensing_secret_key) . $data_encoded;
				$data_encoded = strrev($data_encoded); 
				$data_encoded = $data_encoded . md5($data_encoded . $licensing_secret_key);
				$data_encoded = wordwrap($data_encoded, 80, "\n", true);
				$results['localkey'] = $data_encoded;
			}
			
			$results['remotecheck'] = true;
		}

		unset($postfields,$data,$matches,$whmcsurl,$licensing_secret_key,$checkdate,$usersip,$localkeydays,$allowcheckfaildays,$md5hash);

		if ($licencia === null) // Entonces es una licencia nueva
		{
			if ($results['status'] == "Active")
			{
				if (!array_key_exists('customfields', $results))
					$results['customfields'] = '';

				$licencia = Licencia::create([
					'status' => $results['status'],
					'registered_name' => $results['registeredname'],
					'company_name' => $results['companyname'],
					'email' => $results['email'],
					'service_id' => $results['serviceid'],
					'product_id' => $results['productid'],
					'product_name' => $results['productname'],
					'reg_date' => $results['regdate'],
					'next_due_date' => $results['nextduedate'],
					'billing_cycle' => $results['billingcycle'],
					'valid_domain' => $results['validdomain'],
					'valid_ip' => $results['validip'],
					'valid_directory' => $results['validdirectory'],
					'config_options' => $results['configoptions'],
					'custom_fields' => $results['customfields'],
					'md5_hash' => $results['md5hash'],
					'check_date' => $results['checkdate'],
					'licence_key' => $licensekey,
					'local_key' => $results['localkey']
				]);
			}
		}
		else // Entonces es una licencia existente
		{
			if (!array_key_exists('customfields', $results))
				$results['customfields'] = '';

			$licencia->status = $results['status'];
			$licencia->registered_name = $results['registeredname'];
			$licencia->company_name = $results['companyname'];
			$licencia->email = $results['email'];
			$licencia->service_id = $results['serviceid'];
			$licencia->product_id = $results['productid'];
			$licencia->product_name = $results['productname'];
			$licencia->reg_date = $results['regdate'];
			$licencia->next_due_date = $results['nextduedate'];
			$licencia->billing_cycle = $results['billingcycle'];
			$licencia->valid_directory = $results['validdomain'];
			$licencia->config_options = $results['validip'];
			$licencia->custom_fields = $results['validdirectory'];
			$licencia->md5_hash = $results['configoptions'];
			$licencia->check_date = $results['customfields'];
			$licencia->licence_key = $results['md5hash'];
			$licencia->local_key = $results['checkdate'];
			$licencia->save();
		}

		return back();
	}

}