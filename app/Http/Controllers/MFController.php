<?php 

namespace App\Http\Controllers;

use App\CH;
use App\CV;
use App\CS;
use App\NI;
use App\Sala;
use App\Destino;
use App\Responso;
use App\Homenaje;
use App\Religion;
use App\Servicio;
use App\iphost;
use App\Licencia;
use App\Velatorio;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Ixudra\Curl\Facades\Curl;
use SimpleSoftwareIO\QrCode\Facades\QrCode; // https://www.simplesoftware.io/docs/simple-qrcode/es

class MFController extends Controller {


	public function index(Request $request){
		
		return view('configuracion.mf');
	}

	
}
