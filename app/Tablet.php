<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tablet extends Model {


    protected $table = 'tablets';
   
    public $timestamps = false;


}
