<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class CV extends Model {

    protected $table = "cv";

    public $timestamps = false;

    const SIMBOLOS_DIR = 'images/simbolos';
    const CV_DIR = 'images/cv';
    const FONDOS_DIR = 'images/cv/fondos';
    const BANNERS_DIR = 'images/cv/banners';
    const PUBLI_DIR = 'images/cv/publicidades';
    const SERVICIOS_DIR = 'images/cv/fondo_servicio';
    const QRCODES_DIR = 'images/servicios/qrcodes';

    public static function getTipo_imagen(){
        return ['ninguna'=>'Vacio', 'foto'=>"Foto", 'cruz' => "Cruz", 'icono'=>"Icono Sala", 'simbolo'=>"Simbolo Sala",'QR1'=>"QR1", 'QR2'=>"QR2"];
    }

    public static function getTipo_contenedor(){
        return ['nada'=>'Vacio','Salida'=>"Salida", 'Velatorio' => "Velatorio", 'Responso/Misa'=>'Misa/Responso', 'Cementerio'=>"Cementerio", 'Ingreso'=>"Ingreso", 'Sala'=>"Sala" ,'Empresa'=>"Empresa", 'Sepelio' => "Sepelio"];
    }

    public function scopeCliente($query, $id){
        return $query->where('cliente_id', $id);
    }

    public function saveCV($array){
        \DB::beginTransaction();
        try{
            $this->fill($array);
            $bool = $this->save();
        }catch (\Exception $exception){
            \DB::rollback();
            return $exception->getMessage();
        }
        \DB::commit();
        return $bool;
    }

    public function updateCV($array, $id)
    {
        \DB::beginTransaction();
        try{
            $bool = $this->where("cliente_id",$id)->update($array);
        }catch (\Exception $exception){
            \DB::rollback();
            return $exception->getMessage();
        }
        \DB::commit();
        return $bool;
    }

}
