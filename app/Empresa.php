<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model {
	protected $table='empresas';
	protected $fillable = ['nombre','id'];
	public $timestamps = false;

	const LOGOS_DIR = 'images/empresa';

}
