<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Velatorio extends Model {

	protected $fillable = ['id','servicio_id', 'desde', 'hasta', 'fecha', 'id_online'];

	public $timestamps = false;

	public function scopeServicio($query, $servicio){
		$rs = $query->where('servicio', $servicio);
		return $rs;
	}

	public function servicio(){
		return $this->belongsTo('App\Servicio');
	}

}
