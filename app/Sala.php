<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;


class Sala extends Model
{

    protected $fillable = ['nombre', 'cliente','status','icono'];

    public $timestamps = false;

    const INACTIVA = '0';
    const ACTIVA = '1';
    const ICONO_URL = 'images/salas';

    public function scopeCliente($query, $id){
    	return $query->where('cliente_id', $id);
    }

    public function servicios()
    {
        return $this->hasMany(Servicio::class);
    }
}
