<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Responso extends Model {


    protected $table = 'responso';
    protected $fillable = ['nombre','direccion','tipo'];
    public $timestamps = false;


}
