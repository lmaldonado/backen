<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model {

	protected $fillable = ['nombre','apellido','fecha','sol_nombre','sol_apellido','sol_telefono'];

	public $timestamps = false;

	public function items(){
		return $this->belongsToMany('\App\Item','temp', 'cliente_id', 'item_id')
			->withPivot('importe');
	}

}
