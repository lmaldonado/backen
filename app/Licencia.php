<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Licencia extends Model {

	protected $table = 'licencias';

	protected $fillable = [
		'status',
		'registered_name',
		'company_name',
		'email',
		'service_id',
		'product_id',
		'product_name',
		'reg_date',
		'next_due_date',
		'billing_cycle',
		'valid_domain',
		'valid_ip',
		'valid_directory',
		'config_options',
		'custom_fields',
		'md5_hash',
		'check_date',
		'licence_key',
		'local_key'
	];

	public $timestamps = false;

}