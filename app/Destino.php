<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Destino extends Model {

	const FOTO_DIR = 'images/destinos/';


	protected $table = "destinos";
	protected $fillable = ['nombre', 'direccion', 'mapa'];

	public $timestamps = false;


}
