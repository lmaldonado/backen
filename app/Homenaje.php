<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Servicio;

class Homenaje extends Model {

    const FOTO_DIR = 'images/homenaje/';
    public $timestamps = false;


    // Relationships
    public function servicio()
    {
        return $this->belongsTo(Servicio::class);
    }

    public function scopeToServicio($query,$servicio)
    {
        return $query->where("servicio_id",$servicio);
    }

    //Accesores

    public function getFotoAttribute($value)
    {
        return Homenaje::FOTO_DIR.$value;
    }

}
