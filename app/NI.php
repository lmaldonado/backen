<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class NI extends Model {

    protected $table = "ni";

    public $timestamps = false;

    const NI_DIR = 'images/ni';
    const PUBLI_DIR = 'images/ni/publicidades';
    const LOGOS_DIR = 'images/ni/logos';
    const QRCODES_DIR = 'images/ni/qrcodes';
    
    public function scopeCliente($query, $id){
        return $query->where('cliente_id', $id);
    }

    public function saveNI($array){
        \DB::beginTransaction();
        try{
            $this->fill($array);
            $bool = $this->save();
        }catch (\Exception $exception){
            \DB::rollback();
            return $exception->getMessage();
        }
        \DB::commit();
        return $bool;
    }

    public function updateNI($array, $id)
    {
        \DB::beginTransaction();
        try{
            $bool = $this->where("cliente_id",$id)->update($array);
        }catch (\Exception $exception){
            \DB::rollback();
            return $exception->getMessage();
        }
        \DB::commit();
        return $bool;
    }

}
