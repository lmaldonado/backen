<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Religion extends Model {

	protected $fillable = ['nombre'];

	protected $table = 'religiones';
	public $timestamps = false;

	const CRUCES_URL = '';

}
