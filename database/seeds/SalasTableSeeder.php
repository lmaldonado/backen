<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class SalasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create('es_AR');
    	for ($i=1; $i < 4; $i++) {
        	\DB::table('salas')->insert(array(
	           	'id_online' 	=> $faker->randomFloat(0, 1, 2),
	           	'nombre' 		=> 'Sala ' . $i,
	           	'cliente_id' 	=> 1,
	           	'status'		=> 1,
	            'icono' 		=> $faker->word() . '.jpg',
		    ));
		}
    }
}
