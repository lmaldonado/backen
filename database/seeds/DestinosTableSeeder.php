<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class DestinosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('es_AR');
        for ($i=0; $i < 3; $i++) {
		    \DB::table('destinos')->insert(array(
	           	'nombre' 				=> $faker->catchPhrase,
	           	'direccion' 			=> $faker->address,
	           	'short_url'				=> $faker->url,
                'mapa'                  => $faker->word() . '.jpg'
		    ));
		}
    }
}
