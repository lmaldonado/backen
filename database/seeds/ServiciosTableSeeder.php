<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ServiciosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create('es_AR');
        for ($i=0; $i < 3; $i++) {
		    \DB::table('servicios')->insert(array(
	           	'sala_id' 				=> $faker->randomFloat(0, 1, 2),
	           	'nombres' 				=> $faker->firstName() . ' ' . $faker->firstName(),
	           	'apellidos' 			=> $faker->lastName() . ' ' . $faker->lastName(),
	           	'fecha_nac'				=> $faker->date('Y-m-d', '-30 years'),
	           	'fecha_fac'				=> $faker->date('Y-m-d', 'now'),
	            'salida'				=> $faker->date('Y-m-d', 'now') . ' ' . $faker->time('H:i:s', 'now'),
	            'domicilio_id'			=> 0,
	            'destino_id'			=> $faker->randomFloat(0, 1, 2),
	            'servicio_cementerio'	=> $faker->date('Y-m-d', 'now') . ' ' . $faker->time('H:i:s', 'now'),
	            'religion_id'			=> $faker->randomFloat(0, 1, 2),
	            'foto'					=> $faker->word() . '.jpg',
	            'status'				=> $faker->randomFloat(0, 0, 1),
	            'cliente_id'			=> 1,
	            'id_online'				=> $faker->randomFloat(0, 1, 3),
	            'fecha_inicio'			=> $faker->date('Y-m-d', 'now') . ' ' . $faker->time('H:i:s', 'now'),
	            'fecha_fin'				=> $faker->date('Y-m-d', 'now') . ' ' . $faker->time('H:i:s', 'now'),
	            'fecha_update'			=> $faker->date('Y-m-d', 'now') . ' ' . $faker->time('H:i:s', 'now'),
	            'empresa'				=>''
		    ));
		}
    }
}
