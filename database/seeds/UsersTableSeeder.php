<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            'name' => "Administrador",
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456'),
            'password_new'=>md5('123456'),
            'admin' => '0',
        ]);
        \DB::table('users')->insert([
            'name' => "Admin_servicio",
            'username' => 'admin_servicio',
            'email' => 'admin2@admin.com',
            'password' => bcrypt('1234'),
            'password_new'=>md5('1234'),
            'admin' => '0',
        ]);
        \DB::table('users')->insert([
            'name' => "Administrador2",
            'username' => 'admin_cv',
            'email' => 'admin3@admin.com',
            'password' => bcrypt('NeoCVSepelios'),
            'password_new'=>md5('NeoCVSepelios'),
            'admin' => '1',
        ]);
    }
}
