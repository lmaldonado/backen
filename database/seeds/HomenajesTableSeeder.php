<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class HomenajesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('es_AR');
		for ($i=1; $i < 4; $i++) {
			for ($j=0; $j < 10; $j++) {    
			    \DB::table('homenajes')->insert(array(
		           	'id_online' 	=> 0,
		           	'servicio_id'  	=> $i,
		           	'nombre' 		=> $faker->firstNameFemale,
		           	'mensaje' 		=> $faker->paragraph(10, true),
		           	'foto'			=> $faker->word() . '.jpg',
		            'created_at' 	=> date('Y-m-d H:m:s'),
           			'updated_at' 	=> date('Y-m-d H:m:s')
			    ));
			}
		}
    }
}
