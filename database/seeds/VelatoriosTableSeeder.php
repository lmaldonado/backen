<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class VelatoriosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('es_AR');
		for ($i=1; $i < 4; $i++) {
			for ($j=0; $j < 3; $j++) {    
			    \DB::table('velatorios')->insert(array(
		           	'servicio_id'  	=> $i,
		           	'desde' 		=> $faker->time('H:i:s', 'now'),
		           	'hasta' 		=> $faker->time('H:i:s', 'now'),
		           	'fecha'			=> $faker->date('Y-m-d', 'now'),
		            'id_online' 	=> $faker->randomFloat(0, 1, 3),
			    ));
			}
		}
    }
}
