<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create('es_AR');
        \DB::table('clientes')->insert(array(
           	'nombre' => $faker->company
	    ));
    }
}
