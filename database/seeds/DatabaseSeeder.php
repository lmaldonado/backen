<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        //$this->call(UserTableSeeder::class);
        $this->call(ClientesTableSeeder::class);        
        $this->call(HomenajesTableSeeder::class);
        
        $this->call(DestinosTableSeeder::class);
        $this->call(SalasTableSeeder::class);
        $this->call(ReligionesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ServiciosTableSeeder::class);
        $this->call(VelatoriosTableSeeder::class);
        Model::reguard();
    }
}
