<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ReligionesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker::create('es_AR');
		\DB::table('religiones')->insert(array(
			'nombre'        => 'Cristianismo',
			'qepd'          => 'Q.E.P.D.',
			'cruz'          => 'cruz_cristianismo.png'
		));

		\DB::table('religiones')->insert(array(
			'nombre'        => 'Budismo',
			'qepd'          => '',
			'cruz'          => 'cruz_budismo.png'
		));

		\DB::table('religiones')->insert(array(
			'nombre'        => 'Judaísmo',
			'qepd'          => "Z''L",
			'cruz'          => 'cruz_judaismo.png'
		));

	}
}
