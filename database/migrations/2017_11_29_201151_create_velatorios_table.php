<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVelatoriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('velatorios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('servicio_id');
            $table->time('desde');
            $table->time('hasta');
            $table->date('fecha');
            $table->integer('id_online');
        });

        DB::unprepared('
                        CREATE TRIGGER actualizar_fechas_servicio BEFORE UPDATE ON velatorios FOR EACH ROW
                        BEGIN
                            CALL actualiza_fechas_servicio(NEW.servicio_id);
                        END
                        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('velatorios');
    }
}
