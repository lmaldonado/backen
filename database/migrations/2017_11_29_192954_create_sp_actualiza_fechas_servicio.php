<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpActualizaFechasServicio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE PROCEDURE actualiza_fechas_servicio( IN p_servicio_id INT(11) )  BEGIN 
                            DECLARE p_fecha_inicio datetime;
                            DECLARE p_fecha_fin datetime;
                            
                            SELECT MIN(CONCAT(fecha, " ", desde)), MAX(CONCAT(fecha, " ", hasta)) 
                            INTO p_fecha_inicio, p_fecha_fin 
                            FROM velatorios 
                            WHERE servicio_id = p_servicio_id;
                            
                            UPDATE servicios SET fecha_inicio = p_fecha_inicio, fecha_fin =  p_fecha_fin WHERE id = p_servicio_id;
                            END'
                        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS actualiza_fechas_servicio');
    }
}
