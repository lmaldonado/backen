<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLicenciasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('licencias', function (Blueprint $table) {
			$table->increments('id');

			$table->string('status', 10);
			$table->string('registered_name', 100);
			$table->string('company_name', 100);
			$table->string('email');
			$table->integer('service_id')->unsigned();
			$table->integer('product_id')->unsigned();
			$table->string('product_name', 100);
			$table->dateTime('reg_date');
			$table->date('next_due_date');
			$table->string('billing_cycle');
			$table->string('valid_domain', 150);
			$table->string('valid_ip', 16);
			$table->string('valid_directory', 150);
			$table->string('config_options', 150);
			$table->string('custom_fields', 150);
			$table->string('md5_hash', 32); //7f81834eca7b3ffd1b31502d7f35ee4c
			$table->date('check_date');
			$table->string('licence_key', 38);
			$table->string('local_key', 988);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('licencias');
	}
}
