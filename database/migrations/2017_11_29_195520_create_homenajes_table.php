<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomenajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homenajes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_online');
            $table->integer('servicio_id');
            $table->string('nombre', 255);
            $table->text('mensaje');
            $table->string('foto', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('homenajes');
    }
}
