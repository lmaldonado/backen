<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sala_id');
            $table->string('nombres', 100);
            $table->string('apellidos', 100);
            $table->date('fecha_nac');
            $table->date('fecha_fac');
            $table->dateTime('salida');
            $table->string('domicilio_id', 100);
            $table->integer('destino_id');
            $table->dateTime('servicio_cementerio');
            $table->integer('religion_id');
            $table->text('foto');
            $table->integer('status');
            $table->integer('cliente_id');
            $table->integer('id_online');
            $table->dateTime('fecha_inicio');
            $table->dateTime('fecha_fin');
            $table->timestamp('fecha_update');            
            $table->string('empresa', 100);
            $table->string('domicilio',250)->nullable();
            $table->integer('confirmar')->nullable();
            $table->dateTime('responso')->nullable();
            $table->string('direccion_misa',100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('servicios');
    }
}
