<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ch', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cliente_id');
            $table->string('tipo_encabezado', 10);
            $table->string('texto_encabezado', 100);
            $table->string('logo_encabezado', 50);
            $table->string('logo_pie', 50);
            $table->string('url_web_api', 250);
            $table->integer('intervalo_publicidad');
            $table->integer('duracion_publicidad');
            $table->integer('duracion_slider_homenaje')->default(20);            
            $table->string('url_web_homenaje', 250);
            $table->string('qrcode', 50);
            $table->integer('musica_funcional');
            $table->integer('cartelera_informacion');
            $table->integer('cartelera_control');
            $table->integer('catalogo_digital');
            $table->integer('duracion_transicion_slider_homenaje')->nullable();            
            $table->string('url_api_mf',250)->nullable();
            $table->string('url_api_cr',250)->nullable();
            $table->integer('tiempo_cr')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ch');
    }
}
