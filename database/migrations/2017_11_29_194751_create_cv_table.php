<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cv', function (Blueprint $table) {    
            $table->increments('id');
            $table->integer('cliente_id');
            $table->string('tipo_encabezado', 10);
            $table->string('texto_encabezado', 60);
            $table->string('logo_encabezado', 50);
            $table->string('contenedor1', 20);
            $table->string('contenedor2', 20);
            $table->string('contenedor3', 20);
            $table->string('contenedor4', 20);
            $table->string('imagen1', 20);
            $table->string('imagen2', 20);
            $table->string('imagen3', 20);
            $table->string('simbolos', 500);
            $table->string('imagen4', 20);
            $table->integer('tiempo_publi');
            $table->integer('tiempo_banner');
            $table->string('color_back', 35)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cv');
    }
}
